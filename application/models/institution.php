<?php
class Institution extends CI_Model
{	
	function exists($institution_id)
	{
		$this->db->from('institutions');
		#$this->db->where('deleted',0);
		$this->db->where('institution_id',$institution_id);
		$query = $this->db->get();
		
		return ($query->num_rows()==1);
	}	
	
	function get_all()
	{
		$this->db->from('institutions');
		$this->db->where('deleted',0);			
		$this->db->order_by("institution_name", "asc");
		return $this->db->get();		
	}
	
	function get_info($institution_id)
	{
		$this->db->from('institutions');
		$this->db->where('institution_id',$institution_id);
		return $this->db->get()->row();
	}

	function save($institution_data, $institution_id = null)
	{
		if ( $institution_id && !$this->exists($institution_id) )
			return false;
		$this->db->trans_start();

		if($institution_id):
			$this->db->where('institution_id',$institution_id);
			$this->db->update('institutions',$institution_data);
		else:
			$this->db->insert('institutions',$institution_data);
			$institution_id = $this->db->insert_id();
		endif;
		
		$this->db->trans_complete();	
		return $this->db->trans_status() ? $institution_id : false;	
	}
	
	function delete($institution_id)
	{
		$this->db->trans_start();

		$this->db->set('deleted',1);
		$this->db->where('institution_id',$institution_id);
		$this->db->update('institutions');

		$this->db->set('institution_id',null);
		$this->db->where('institution_id',$institution_id);
		$this->db->update('patients');

		$this->db->trans_complete();		
		return $this->db->trans_status();
	}

	function get_institution_search_suggestions($search,$limit=25)
	{
		$suggestions = array();
		
		$this->db->from('institutions');	
		$this->db->where('deleted', 0);
		$this->db->like("institution_name",$search);
		$this->db->order_by("institution_name", "asc");		
		$by_institution_name = $this->db->get();
		foreach($by_institution_name->result() as $row)
		{
			$suggestions[]=$row->institution_id.'|'.$row->institution_name;		
		}
		
		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	function get_patient_institution($patient_id)
	{
		$this->db->select('institution_id');
		$this->db->from('patients');
		$this->db->where('patient_id',$patient_id);

		$institution_id = $this->db->get()->row()->institution_id;

		if($institution_id && $this->exists($institution_id)):
			$this->db->from('institutions');
			$this->db->where('institution_id',$institution_id);
			return $this->db->get()->row();
		else:
			return false;
		endif;
	}

	function workload($start_date,$end_date)
	{
		$institutions = array();
		
		foreach($this->get_all()->Result() as $institution):
			$institutions[$institution->institution_id] = array('value'=>0,'discount'=>0,'patients'=>0,'name'=>$institution->institution_name,
				'institution_discount'=>$institution->institution_discount,'institution_liability'=>$institution->institution_liability,
				'outstanding_debt'=>$this->get_outstanding_debt($institution->institution_id));
		endforeach;

		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		foreach(array_keys($institutions) as $institution):
			$invoice_ids = array();

			$this->db->distinct();
			$this->db->select('patient_id');
			$this->db->from('invoices');
			$this->db->where('invoice_status !=',0);
			$this->db->where('institution_id',$institution);
			$this->db->where('invoice_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');
			
			$institutions[$institution]['patients'] = $this->db->get()->num_rows();

			$this->db->select('invoice_id');
			$this->db->from('invoices');
			$this->db->where('invoice_status !=',0);
			$this->db->where('institution_id',$institution);
			$this->db->where('invoice_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');

			$invoices = $this->db->get();
			if($invoices->num_rows() > 0):
				foreach($invoices->result() as $invoice):
					$invoice_ids[] = $invoice->invoice_id;
				endforeach;
			endif;

			if(!empty($invoice_ids)):
				$value = 0;
				
				$this->db->from('invoices_items');
				$this->db->where('status',1);
				$this->db->where_in('invoice_id',$invoice_ids);
				foreach($this->db->get()->Result() as $item):
					$value = $value + ($item->quantity * $item->unit_price);
				endforeach;

				$institutions[$institution]['value'] = $value * $institutions[$institution]['institution_liability'] / 100;
				$institutions[$institution]['discount'] = $value * $institutions[$institution]['institution_discount'] / 100;
			endif;
		endforeach;

		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_off();
		return $institutions;
	}

	function get_invoice($start_date,$end_date,$institution_id)
	{
		$encounters = array();
		$item_ids = array();
		$invoice_ids = array();
		$categories = array();
		$institution = $this->get_info($institution_id);

		#foreach($this->Item->get_categories()->result() as $item):
			#$categories[$item->category] = 0;
		foreach($this->Item->get_categories_subcategories() as $category):
			$categories[$category] = 0;
		endforeach;

		$headers = $categories;

		foreach(array_keys($categories) as $category):
			$item_ids[$category] = array();
			$this->db->select('item_id');
			$this->db->from('items');
			$this->db->where("category = '$category' OR (category = 'Medical Services' AND sub_category = '$category')");
			foreach($this->db->get()->Result() as $item):
				$item_ids[$category][] = $item->item_id;
			endforeach;
		endforeach;
		
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->from('invoices');
		$this->db->where('institution_id',$institution_id);
		$this->db->where('invoice_status !=',0);
		$this->db->where('invoice_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');
		
		foreach($this->db->get()->Result() as $invoice):
			$patient = $this->Patient->get_info($invoice->patient_id);
			$name = $this->Patient->patient_name($patient->first_name,$patient->middle_name,$patient->last_name);
			$date = date('d/m/Y',strtotime($invoice->invoice_time));
			$patient_no = $this->Patient->patient_number($patient->patient_id);
			$encounters[$invoice->invoice_id] = array('patient_no'=>$patient_no,'name'=>$name,'date'=>$date,'categories' => $categories);
			$invoice_ids[] = $invoice->invoice_id;

			foreach(array_keys($categories) as $category):
				$value = 0;
				if(!empty($item_ids[$category])):
					$this->db->from('invoices_items');
					$this->db->where('status',1);
					$this->db->where('invoice_id',$invoice->invoice_id);
					$this->db->where_in('item_id',$item_ids[$category]);
					foreach($this->db->get()->Result() as $item):
						$value = $value + $item->quantity * $item->unit_price;
					endforeach;

					$encounters[$invoice->invoice_id]['categories'][$category] = $value * $institution->institution_liability / 100;
					$headers[$category] = $headers[$category] + $value * $institution->institution_liability / 100;
				endif;
			endforeach;
		endforeach;

		

		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_off();

		foreach(array_keys($headers) as $header):
			if($headers[$header] == 0) unset($headers[$header]);
		endforeach;

		return array('workload'=>$encounters,'headers'=>array_keys($headers));
	}

	function get_payments($institution_id)
	{
		$this->db->from('institution_payments');
		$this->db->where('institution_id',$institution_id);
		$this->db->order_by('payment_time','desc');
		return $this->db->get();
	}

	function save_payment($payment_data)
	{
		$this->db->trans_start();

		$this->db->insert('institution_payments',$payment_data);
		$payment_id = $this->db->insert_id();
		
		$this->db->trans_complete();	
		return $this->db->trans_status();	
	}

	function get_outstanding_debt($institution_id)
	{
		$institution = $this->get_info($institution_id);
		$this->db->select('SUM(amount) as total');
		$this->db->from('institution_payments');
		$this->db->where('institution_id',$institution_id);
		$credit = $this->db->get()->row()->total;

		$this->db->select('invoice_id');
		$this->db->from('invoices');
		$this->db->where('invoice_status !=',0);
		$this->db->where('institution_id',$institution_id);

		$invoices = $this->db->get();
		if($invoices->num_rows() > 0):
			foreach($invoices->result() as $invoice):
				$invoice_ids[] = $invoice->invoice_id;
			endforeach;
		endif;

		$debit = 0;
		if(!empty($invoice_ids)):
			$this->db->from('invoices_items');
			$this->db->where('status',1);
			$this->db->where_in('invoice_id',$invoice_ids);
			foreach($this->db->get()->Result() as $item):
				$debit = $debit + ($item->quantity * $item->unit_price);
			endforeach;
		endif;
		$debit = $debit * $institution->institution_liability / 100;
		return ($debit - $credit) <= 0 ? 0 : ($debit - $credit);
	}
}
?>
