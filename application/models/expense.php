<?php
class Expense extends CI_Model
{
	function get_info($expense_id)
	{
		$this->db->from('expenses');
		$this->db->where('expense_id',$expense_id);

		return $this->db->get()->row();
	}

	function exists($expense_id)
	{
		$this->db->select('expense_id');
		$this->db->from('expenses');
		$this->db->where('expense_id',$expense_id);
		$query = $this->db->get();
		
		return ($query->num_rows()==1);
	}

	/*function save_budget($budget_data, $expense_id)
	{
		$this->db->trans_start();

		$this->db->select('expense_id');
		$this->db->from('expenses_budget');
		$this->db->where('expense_id',$expense_id);
		$this->db->where('period',date('Y-m-01'));
		$query = $this->db->get();
		
		if($query->num_rows()==1):
			$this->db->where('expense_id',$expense_id);
			$this->db->where('period',date('Y-m-01'));
			$this->db->update('expenses_budget',$budget_data);
		else:
			$budget_data['expense_id'] = $expense_id;
			$this->db->insert('expenses_budget',$budget_data);
		endif;

		$this->db->trans_complete();	
		return $this->db->trans_status();
	}*/

	function save($expense_data, $budget_data, $expense_id = null)
	{
		if ( $expense_id && !$this->exists($expense_id) )
			return false;
		$this->db->trans_start();

		if($expense_id):
			$this->db->where('expense_id',$expense_id);
			$this->db->update('expenses',$expense_data);
		else:
			$this->db->insert('expenses',$expense_data);
			$expense_id = $this->db->insert_id();
		endif;

		//$budget = $this->save_budget($budget_data, $expense_id);

		$this->db->trans_complete();	
		return $this->db->trans_status() ? $expense_id : false;	
	}

	function delete($expense_id)
	{
		$this->db->trans_start();

		$this->db->set('deleted',1);
		$this->db->where('expense_id',$expense_id);
		$this->db->update('expenses');

		$this->db->trans_complete();		
		return $this->db->trans_status();
	}

	function get_all($period=null,$chapter=null,$category=null,$subcategory=null)
	{
		if(!$period) $period = date('Y-m-01');
		$data = array();

		if(!$chapter):
			foreach($this->get_chapters()->result() as $chapter):
				$data[] = array("expense_no"=>"Chapter $chapter->chapter_no",
								"description"=>$chapter->description,
								"expenditure"=>$this->get_expense_expenditure($period,$chapter->chapter_id),
								"budget"=>$this->get_expense_budget($period,$chapter->chapter_id),
								"balance_cf"=>$this->get_expense_balance($period,$chapter->chapter_id),
								"expand"=>true,
								"expense_id"=>'',
								"subcategory"=>'',
								"category"=>'',
								"chapter"=>$chapter->chapter_id);
			endforeach;
		else:
			$chapter = $this->get_chapter_info($chapter);

			if(!$category):
				foreach($this->get_chapter_categories($chapter->chapter_id)->result() as $category):
					$category = $category->category;
					$data[] = array("expense_no"=>"",
								"description"=>"$category",
								"expenditure"=>$this->get_expense_expenditure($period,$chapter->chapter_id,$category),
								"budget"=>$this->get_expense_budget($period,$chapter->chapter_id,$category),
								"balance_cf"=>$this->get_expense_balance($period,$chapter->chapter_id,$category),
								"expand"=>($category) ? true : false,
								"expense_id"=>'',
								"subcategory"=>'',
								"category"=>$category,
								"chapter"=>$chapter->chapter_id);
				endforeach;
			else:
				if(!$subcategory):
					foreach($this->get_category_subcategories($chapter->chapter_id,$category)->result() as $subcategory):
						$subcategory = $subcategory->subcategory;
						$data[] = array("expense_no"=>"",
									"description"=>"$subcategory",
									"expenditure"=>$this->get_expense_expenditure($period,$chapter->chapter_id,$category,$subcategory),
									"budget"=>$this->get_expense_budget($period,$chapter->chapter_id,$category,$subcategory),
									"balance_cf"=>$this->get_expense_balance($period,$chapter->chapter_id,$category,$subcategory),
									"expand"=>($category && $subcategory) ? true : false,
									"expense_id"=>'',
									"subcategory"=>$subcategory,
									"category"=>$category,
									"chapter"=>$chapter->chapter_id);
					endforeach;
				else:
					foreach($this->get_subcategory_expenses($chapter->chapter_id,$category,$subcategory)->result() as $expense):
						$expense = $this->get_info($expense->expense_id);
						$data[] = array("expense_no"=>"$expense->expense_no",
									"description"=>"$expense->description",
									"expenditure"=>$this->get_expense_expenditure($period,$chapter->chapter_id,$category,$subcategory,$expense->expense_id),
									"budget"=>$this->get_expense_budget($period,$chapter->chapter_id,$category,$subcategory,$expense->expense_id),
									"balance_cf"=>$this->get_expense_balance($period,$chapter->chapter_id,$category,$subcategory,$expense->expense_id),
									"expense_id"=>$expense->expense_id,
									"subcategory"=>$subcategory,
									"category"=>$category,
									"chapter"=>$chapter->chapter_id);
					endforeach;
				endif;
			endif;
		endif;

		return $data;
	}

	function expense_no_exists($expense_no, $expense_id = null)
	{
		$this->db->select('expense_id');
		$this->db->from('expenses');
		$this->db->where('expense_no',$expense_no);
		if($expense_id) $this->db->where('expense_id !=',$expense_id);
		return $this->db->get()->num_rows() == 1;
	}

	function get_chapters()
	{
		$this->db->from('chapters');
		$this->db->where('type','expense');
		$this->db->where('deleted',0);
		$this->db->order_by('chapter_no','asc');

		return $this->db->get();
	}

	function get_chapter_categories($chapter)
	{
		$this->db->distinct();
		$this->db->select('category');
		$this->db->from('expenses');
		$this->db->where('chapter',$chapter);
		$this->db->where('deleted',0);
		$this->db->order_by('category','asc');

		return $this->db->get();
	}

	function get_category_subcategories($chapter,$category)
	{
		$this->db->distinct();
		$this->db->select('subcategory');
		$this->db->from('expenses');
		$this->db->where('chapter',$chapter);
		$this->db->where('category',$category);
		$this->db->where('deleted',0);
		$this->db->order_by('subcategory','asc');

		return $this->db->get();
	}

	function get_subcategory_expenses($chapter,$category,$subcategory)
	{
		$this->db->distinct();
		$this->db->select('expense_id');
		$this->db->from('expenses');
		$this->db->where('chapter',$chapter);
		$this->db->where('category',$category);
		$this->db->where('subcategory',$subcategory);
		$this->db->where('deleted',0);

		return $this->db->get();
	}

	function get_chapter_info($chapter_id)
	{
		$this->db->from('chapters');
		$this->db->where('chapter_id',$chapter_id);
		return $this->db->get()->row();
	}

	function save_chapter($chapter_data, $chapter_id = null)
	{
		$this->db->trans_start();

		if($chapter_id):
			$this->db->where('chapter_id',$chapter_id);
			$this->db->update('chapters',$chapter_data);
		else:
			$this->db->insert('chapters',$chapter_data);
			$chapter_id = $this->db->insert_id();
		endif;

		$this->db->trans_complete();	
		return $this->db->trans_status();	
	}

	function delete_chapter($chapter_id)
	{
		$this->db->trans_start();

		$this->db->set('deleted',1);
		$this->db->where('chapter_id',$chapter_id);
		$this->db->update('chapters');

		$this->db->trans_complete();		
		return $this->db->trans_status();
	}

	function chapter_no_exists($chapter_no, $chapter_id = null)
	{
		$this->db->select('chapter_id');
		$this->db->from('chapters');
		$this->db->where('chapter_no',$chapter_no);
		$this->db->where('type','expense');
		if($chapter_id) $this->db->where('chapter_id !=',$chapter_id);
		return $this->db->get()->num_rows() == 1;
	}

	function get_category_suggestions($search)
	{
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('category');
		$this->db->from('expenses');
		$this->db->like('category', $search);
		$this->db->order_by("category", "asc");
		
		foreach($this->db->get()->result() as $row)
		{
			$suggestions[]=$row->category;
		}

		return $suggestions;
	}

	function get_subcategory_suggestions($search)
	{
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('subcategory');
		$this->db->from('expenses');
		$this->db->like('subcategory', $search);
		$this->db->order_by("subcategory", "asc");
		
		foreach($this->db->get()->result() as $row)
		{
			$suggestions[]=$row->subcategory;
		}

		return $suggestions;
	}

	function get_expense_expenditure($period,$chapter,$category=null,$subcategory=null,$expense_id=null)
	{
		$expense_ids = array(0);

		$this->db->select('expense_id');
		$this->db->from('expenses');
		$this->db->where('chapter',$chapter);
		if($category !== null) $this->db->where('category',$category);
		if($subcategory !== null) $this->db->where('subcategory',$subcategory);
		if($expense_id) $this->db->where('expense_id',$expense_id);

		$expenses = $this->db->get();
		if($expenses->num_rows() > 0):
			foreach($expenses->result() as $expense):
				$expense_ids[] = $expense->expense_id;
			endforeach;
		endif;

		if ($period < date('Y-m-01')) $this->db->cache_on();

		$this->db->select_sum('amount','total');
		$this->db->from('expenses_expenditure');
		$this->db->where('period',$period);
		$this->db->where_in('expense_id',$expense_ids);
		$total = $this->db->get()->row()->total;

		$this->db->cache_off();
		return $total;
	}

	function get_expense_budget($period,$chapter,$category=null,$subcategory=null,$expense_id=null)
	{
		$expense_ids = array(0);

		$this->db->select('expense_id');
		$this->db->from('expenses');
		$this->db->where('chapter',$chapter);
		if($category !== null) $this->db->where('category',$category);
		if($subcategory !== null) $this->db->where('subcategory',$subcategory);
		if($expense_id) $this->db->where('expense_id',$expense_id);

		$expenses = $this->db->get();
		if($expenses->num_rows() > 0):
			foreach($expenses->result() as $expense):
				$expense_ids[] = $expense->expense_id;
			endforeach;
		endif;

		if ($period < date('Y-m-01')) $this->db->cache_on();

		$this->db->select_sum('amount','total');
		$this->db->from('expenses_budget');
		$this->db->where('period',$period);
		$this->db->where_in('expense_id',$expense_ids);
		$total = $this->db->get()->row()->total;

		$this->db->cache_off();
		return $total;
	}

	function get_expense_balance($period,$chapter,$category=null,$subcategory=null,$expense_id=null)
	{
		$expense_ids = array(0);

		$this->db->select('expense_id');
		$this->db->from('expenses');
		$this->db->where('chapter',$chapter);
		if($category !== null) $this->db->where('category',$category);
		if($subcategory !== null) $this->db->where('subcategory',$subcategory);
		if($expense_id) $this->db->where('expense_id',$expense_id);

		$expenses = $this->db->get();
		if($expenses->num_rows() > 0):
			foreach($expenses->result() as $expense):
				$expense_ids[] = $expense->expense_id;
			endforeach;
		endif;

		if ($period < date('Y-m-01')) $this->db->cache_on();

		$this->db->select_sum('amount','total');
		$this->db->from('expenses_budget');
		$this->db->where('period <',$period);
		$this->db->where_in('expense_id',$expense_ids);
		$budget = $this->db->get()->row()->total;

		$this->db->select_sum('amount','total');
		$this->db->from('expenses_expenditure');
		$this->db->where('period <',$period);
		$this->db->where_in('expense_id',$expense_ids);
		$expenditure = $this->db->get()->row()->total;

		$this->db->cache_off();
		return $budget - $expenditure;
	}

	function get_expenditures($expense_id,$period = null)
	{
		if(!$period) $period = date('Y-m-01');

		$this->db->from('expenses_expenditure');
		$this->db->where('expense_id',$expense_id);
		$this->db->where('period',$period);
		$this->db->order_by('payment_time','desc');
		return $this->db->get();
	}

	function get_expenditure_info($expenditure_id)
	{
		$this->db->from('expenses_expenditure');
		$this->db->where('expenditure_id',$expenditure_id);
		return $this->db->get()->row();
	}

	function save_expenditure($expenditure_data, $expenditure_id = null)
	{
		$this->db->trans_start();

		if($expenditure_id):
			$this->db->where('expenditure_id',$expenditure_id);
			$this->db->update('expenses_expenditure',$expenditure_data);
		else:
			$this->db->insert('expenses_expenditure',$expenditure_data);
			$expenditure_id = $this->db->insert_id();
		endif;

		$this->db->trans_complete();	
		return $this->db->trans_status();	
	}

	function delete_expenditure($expenditure_id)
	{
		$this->db->trans_start();

		$this->db->where('expenditure_id',$expenditure_id);
		$this->db->delete('expenses_expenditure');

		$this->db->trans_complete();		
		return $this->db->trans_status();
	}

	function ref_no_exists($ref_no, $expenditure_id = null)
	{
		$this->db->select('expenditure_id');
		$this->db->from('expenses_expenditure');
		$this->db->where('ref_no',$ref_no);
		if($expenditure_id) $this->db->where('expenditure_id !=',$expenditure_id);
		return $this->db->get()->num_rows() == 1;
	}

	function get_payee_suggestions($search)
	{
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('payee');
		$this->db->from('expenses_expenditure');
		$this->db->like('payee', $search);
		$this->db->order_by("payee", "asc");
		
		foreach($this->db->get()->result() as $row)
		{
			$suggestions[]=$row->payee;
		}

		return $suggestions;
	}

	function get_budgets()
	{
		$this->db->from('expenses_budget');
		$this->db->order_by('period','desc');
		return $this->db->get();
	}

	function get_current_budget()
	{
		$this->db->from('expenses_budget');
		$this->db->where('period',date('Y-m-01'));
		return $this->db->get()->row();
	}

	function save_budget($budget_data, $budget_id = null)
	{
		$this->db->trans_start();

		if($budget_id):
			$this->db->where('budget_id',$budget_id);
			$this->db->update('expenses_budget',$budget_data);
		else:
			$this->db->insert('expenses_budget',$budget_data);
			$budget_id = $this->db->insert_id();
		endif;

		$this->db->trans_complete();	
		return $this->db->trans_status();	
	}

	function get_total_expenditure($period)
	{
		if ($period < date('Y-m-01')) $this->db->cache_on();

		$this->db->select_sum('amount','total');
		$this->db->from('expenses_expenditure');
		$this->db->where('period',$period);
		$total = $this->db->get()->row()->total;

		$this->db->cache_off();
		return $total;
	}

	function get_total_budget($period)
	{
		$this->db->select_sum('amount','total');
		$this->db->from('expenses_budget');
		$this->db->where('period',$period);
		$total = $this->db->get()->row()->total;

		$this->db->cache_off();
		return $total;
	}

	function get_total_balance($period)
	{
		if ($period < date('Y-m-01')) $this->db->cache_on();

		$this->db->select_sum('amount','total');
		$this->db->from('expenses_budget');
		$this->db->where('period <',$period);
		$budget = $this->db->get()->row()->total;

		$this->db->select_sum('amount','total');
		$this->db->from('expenses_expenditure');
		$this->db->where('period <',$period);
		$expenditure = $this->db->get()->row()->total;

		$this->db->cache_off();
		return $budget - $expenditure;
	}
}
?>