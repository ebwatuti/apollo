<?php
class Consultation extends CI_Model
{
	function get_info($consultation_id)
	{
		$this->db->from('consultation');
		$this->db->where('consultation_id',$consultation_id);

		$consultation = $this->db->get()->row();

		$this->db->from('consultation_diagnosis');
		$this->db->where('consultation_id',$consultation_id);

		$consultation->diagnosis = $this->db->get()->result();

		$consultation->prescription = $this->Pharm->get_consultation_prescription($consultation_id);

		$consultation->lab_tests = $this->Lab->get_consultation_lab_tests($consultation_id);

		$consultation->procedures = $this->get_consultation_procedures($consultation_id);

		return $consultation;
	}

	function consultation_status($consultation_id)
	{
		$this->db->select('consultation_status');
		$this->db->from('consultation');
		$this->db->where('consultation_id',$consultation_id);
		$this->db->where('consultation_status !=',3);
		
		return $this->db->get()->row()->consultation_status;
	}

	function get_diagnosis_info($diagnosis_code)
	{
		$this->db->from('consultation_icd10');
		$this->db->where('diagnosis_code',$diagnosis_code);
		$this->db->or_where('description',$diagnosis_code);
		
		return $this->db->get()->row();
	}
	
	function diagnosis_exists($diagnosis_code)
	{
		$this->db->from('consultation_icd10');
		$this->db->where('diagnosis_code',$diagnosis_code);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	function returning($patient_id,$consultant_id=null,$clinic=null)
	{
		$this->db->select('consultation_id, consultation_start, consultation_end');
		$this->db->from('consultation');
		$this->db->where('patient_id',$patient_id);
		if($clinic):
			$this->db->where('consultation_type',$clinic);
		else:
			if($consultant_id) $this->db->where('consultant_id',$consultant_id);
		endif;
		$this->db->where('consultation_status !=',3);
		return $this->db->get()->row();
	}

	function save($consultation_data,$diagnosis_data,$lab_request_data,$prescription_data,$procedures_data,$nursing_data,$encounter_status,$patient_id,$consultation_id=null)
	{
		if ( !$this->Patient->exists($patient_id) )
			return false;
		$encounter_id = $this->Encounter->check_encounter($patient_id);
		if ( !$encounter_id )
			return false;

		$this->db->trans_start();

		$encounter_data = array(
			'encounter_end'=>date('Y-m-d H:i:s'),
			'encounter_status'=>$encounter_status,
			);
		$encounter_id = $this->Encounter->update_encounter($encounter_data, $encounter_id);
			
		$consultation_data['encounter_id'] = $encounter_id;

		if ($consultation_id):
			$this->db->where('consultation_id',$consultation_id);
			$this->db->update('consultation',$consultation_data);
			
			$this->db->where('consultation_id', $consultation_id);
			$this->db->delete("consultation_diagnosis");
		else:
			$this->db->insert('consultation',$consultation_data);
			$consultation_id = $this->db->insert_id();
		endif;
		
		foreach($diagnosis_data as $diagnosis):
			$diagnosis['consultation_id'] = $consultation_id;
			$this->db->insert('consultation_diagnosis',$diagnosis);
		endforeach;

		if( !empty($lab_request_data) ):
			$lab_request_data['invoice_data']['consultation_id'] = $consultation_id;
			$this->Lab->save_lab_tests($lab_request_data['invoice_data'],$lab_request_data['invoice_item_data'],$lab_request_data['invoice_id']);
		else:
			$this->db->where('consultation_id',$consultation_id);
			$this->db->where('invoice_type','Lab');
			$this->db->where('(invoice_status = 0 OR (invoice_status = 1 AND institution_id IS NOT NULL AND institution_id != ""))');
			$this->db->delete('invoices');
		endif;

		if( !empty($prescription_data) ):
			$prescription_data['invoice_data']['consultation_id'] = $consultation_id;
			$this->Pharm->save_prescription($prescription_data['invoice_data'],$prescription_data['invoice_item_data'],$prescription_data['invoice_id']);
		else:
			$this->db->where('consultation_id',$consultation_id);
			$this->db->where('invoice_type','Pharmacy');
			$this->db->where('(invoice_status = 0 OR (invoice_status = 1 AND institution_id IS NOT NULL AND institution_id != ""))');
			$this->db->delete('invoices');
		endif;

		if( !empty($procedures_data) ):
			$procedures_data['invoice_data']['consultation_id'] = $consultation_id;
			$this->Invoice->save($procedures_data['invoice_data'],$procedures_data['invoice_item_data'],$procedures_data['invoice_id']);
		else:
			$this->db->where('consultation_id',$consultation_id);
			$this->db->like('invoice_type','Procedures');
			$this->db->where('(invoice_status = 0 OR (invoice_status = 1 AND institution_id IS NOT NULL AND institution_id != ""))');
			$this->db->delete('invoices');
		endif;

		if( !empty($nursing_data) ):
			$nursing_data['invoice_data']['consultation_id'] = $consultation_id;
			$this->Invoice->save($nursing_data['invoice_data'],$nursing_data['invoice_item_data'],$nursing_data['invoice_id']);
		else:
			$this->db->where('consultation_id',$consultation_id);
			$this->db->like('invoice_type','Nursing');
			$this->db->where('(invoice_status = 0 OR (invoice_status = 1 AND institution_id IS NOT NULL AND institution_id != ""))');
			$this->db->delete('invoices');
		endif;
		
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}

	function discharge($consultation_id)
	{
		$this->db->set('consultation_status',3);
		$this->db->where('consultation_id',$consultation_id);
		$this->db->update('consultation');
	}

	function diagnosis_search_suggestions($search,$limit=25)
	{
		$suggestions = array();
		
		$this->db->select('diagnosis_code,description');
		$this->db->from('consultation_icd10');
		$this->db->like("diagnosis_code",$search);
		$this->db->where("classification !=",'N');
		$this->db->order_by("diagnosis_code", "asc");		
		$by_code = $this->db->get();
		foreach($by_code->result() as $row)
		{
			$suggestions[]=$row->diagnosis_code.'|'.$row->description;		
		}
		
		$this->db->select('diagnosis_code,description');
		$this->db->from('consultation_icd10');
		$this->db->like("description",$search);
		$this->db->where("classification !=",'N');
		$this->db->order_by("diagnosis_code", "asc");		
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->diagnosis_code.'|'.$row->description;
		}
		
		
		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}

	function get_main_queue($clinic="general")
	{
		$this->db->from('encounter');
		$this->db->where('encounter_status', 2);
		$this->db->where('encounter_type', $clinic);
		$this->db->where('encounter_date <=', date('Y-m-d'));
		$this->db->order_by("priority", "desc");
		$this->db->order_by("encounter_id", "asc");
		//$this->db->limit(30);
		return $this->db->get();
	}

	function get_returning_queue($clinic="general")
	{
		$this->db->from('encounter');
		$this->db->where('encounter_status', 3);
		$this->db->where('encounter_type', $clinic);
		$this->db->order_by("priority", "desc");
		$this->db->order_by("encounter_id", "asc");
		//$this->db->limit(30);
		return $this->db->get();
	}
	
	function get_lab_queue($clinic="general")
	{
		$this->db->select('consultation_id');
		$this->db->from('consultation');
		$this->db->where('lab_request','1');
		$this->db->where('consultation_status !=',3);
		$this->db->where('consultation_type', $clinic);
		$this->db->order_by("consultation_end", "asc");
		
		$consultations = array(0);
		foreach($this->db->get()->result() as $consultation):			
			$consultations[] = $consultation->consultation_id;
		endforeach;

		$this->db->from('invoices');
		$this->db->where('invoice_type','Lab');
		$this->db->where_in('consultation_id',$consultations);
		$this->db->where('invoice_status',3);

		return $this->db->get();
	}

	function consultation_history($patient_id)
	{
		$this->db->select('consultation_id,consultation_start,consultant_id');
		$this->db->from('consultation');	
		$this->db->where('patient_id',$patient_id);
		$this->db->order_by("consultation_start", "desc");

		return $this->db->get();
	}

	function get_clinics()
	{
		$this->db->select('clinic_code,clinic_name');
		$this->db->from('clinics');
		return $this->db->get()->result();
	}

	function clinic_name($clinic_code)
	{
		switch ($clinic_code) {
			case 'lab':
				$clinic_name = 'Lab';
				break;
			case 'nursing':
				$clinic_name = 'Nursing';
				break;
			case 'pharmacy':
				$clinic_name = 'Pharmacy';
				break;
			default:
				$this->db->select('clinic_name');
				$this->db->from('clinics');
				$this->db->where('clinic_code',$clinic_code);
				$clinic_name = ucwords(strtolower($this->db->get()->row()->clinic_name));
				break;
		}
		
		return $clinic_name;
	}

	function archive_consultations($start_date,$end_date,$clinic=null)
	{
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->select('consultation_id,patient_id,consultant_id,consultation_start,consultation_end,consultation_type');
		$this->db->from('consultation');
		$this->db->where_in('consultation_status', array(1,3));
		if($clinic) $this->db->where('consultation_type', $clinic);
		$this->db->having('consultation_start BETWEEN "' .$start_date. '" AND "' .$end_date. '"');
		//$this->db->order_by('consultation_end','desc');
		//$this->db->limit(30);
		$result = $this->db->get();
		$this->db->cache_off();
		return $result;
	}

	function workload($start_date,$end_date,$where=null)
	{
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->select('count(consultation_id) as total');
		$this->db->from('consultation');
		$this->db->where('consultation_start BETWEEN "' .$start_date. '" AND "' .$end_date. '"');
		if($where) $this->db->where($where);

		$total = $this->db->get()->row()->total;
		$this->db->cache_off();
		return $total;
	}

	function morbidity($start_date,$end_date,$clinic=null,$gender=null,$ages=null)
	{
		$icd10 = array();
		
		if ($start_date < date('Y-m-d') || $end_date < date('Y-m-d')) $this->db->cache_on();

		$consultation_clause = 'SELECT consultation_id FROM '.$this->db->dbprefix('consultation').'
				WHERE consultation_start BETWEEN "' .$start_date. '" AND "' .$end_date. '"';
		if($clinic) $consultation_clause .= ' AND consultation_type = "'.$clinic.'"';
		if($gender) $consultation_clause .= ' AND patient_id IN (SELECT patient_id FROM '.$this->db->dbprefix('patients').' WHERE gender = "'.$gender.'")';
		if($ages) $consultation_clause .= ' AND patient_id IN (SELECT patient_id FROM '.$this->db->dbprefix('patients').' 
			WHERE DATEDIFF(DATE(consultation_start),STR_TO_DATE(dob,"%d-%m-%Y")) BETWEEN '.($ages['start'] * 365). ' AND '.($ages['end'] * 365 + 364).')';

		$this->db->select('diagnosis_code, count(*) as total',false);
		$this->db->from('consultation_diagnosis');
		$this->db->where('consultation_id IN ('.$consultation_clause.')');
		$this->db->where('diagnosis_code IN (SELECT diagnosis_code FROM '.$this->db->dbprefix('consultation_icd10').')');
		$this->db->group_by('diagnosis_code');

		foreach($this->db->get()->result() as $diagnosis):
			$icd10[$diagnosis->diagnosis_code] = $diagnosis->total;
		endforeach;
		
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_off();
		return $icd10;
	}

	function get_procedure_info($procedure_id)
	{
		$this->db->from('procedures');
		$this->db->where('procedure_id',$procedure_id);
		$procedure = $this->db->get()->row();

		$this->db->from('items');
		$this->db->where('item_number','PROC'.$procedure_id);
		$procedure->item_info = $this->db->get()->row();

		return $procedure;
	}

	function get_procedure_id($item_id)
	{
		return str_ireplace('PROC', '', $this->Item->get_info($item_id)->item_number);
	}

	function procedure_suggestions($clinic,$search,$limit=25)
	{
		$suggestions = array();
		
		$this->db->select('procedure_id,name');
		$this->db->from('procedures');
		//$this->db->where('clinic',$clinic);
		$this->db->where_in('clinic',array('nursing',$clinic));
		$this->db->like('name',$search);		
		
		foreach($this->db->get()->result() as $row)
		{
			$suggestions[]=$row->procedure_id.'|'.$row->name;		
		}		
		
		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}

	function get_consultation_procedures($consultation_id)
	{
		$this->db->select('invoice_id,invoice_status');
		$this->db->from('invoices');
		$this->db->where('consultation_id',$consultation_id);
		//$this->db->like('invoice_type','Procedures');
		$this->db->where('(invoice_type = "Nursing" OR invoice_type LIKE "%Procedures%")');
		$invoices = $this->db->get();

		$invoices_items = array();

		if($invoices->num_rows() > 0):
			foreach($invoices->result() as $invoice):
				$this->db->from('invoices_items');
				$this->db->where('invoice_id',$invoice->invoice_id);
				foreach($this->db->get()->result() as $item):
					$item->invoice_status = $invoice->invoice_status;
					$invoices_items[] = $item;
				endforeach;
			endforeach;
		endif;

		return $invoices_items;
	}

	function get_procedures($clinic= null)
	{
		$this->db->from('procedures');
		if($clinic) $this->db->where('clinic',$clinic);
		return $this->db->get();
	}

	function save_procedure($procedure_data,$item_data,$procedure_id=null)
	{
		if ( $procedure_id && !$this->procedure_exists($procedure_id) )
			return false;
		$this->db->trans_start();

		if($procedure_id):
			$this->db->where('procedure_id',$procedure_id);
			$this->db->update('procedures',$procedure_data);
			
			$this->db->where('item_number', 'PROC'.$procedure_id);
			$this->db->update('items',$item_data);
		else:
			$this->db->insert('procedures',$procedure_data);
			$procedure_id = $this->db->insert_id();
			$item_data['item_number'] = 'PROC'.$procedure_id;
			$this->db->insert('items',$item_data);
		endif;

		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}

	function procedure_exists($procedure_id)
	{
		$this->db->select('procedure_id');
		$this->db->from('procedures');
		$this->db->where('procedure_id',$procedure_id);
		$query = $this->db->get();
		
		return ($query->num_rows()==1);
	}

	function delete_procedure($procedure_id)
	{
		$this->db->trans_start();

		$this->db->where('procedure_id',$procedure_id);
		$this->db->delete('procedures');

		$this->db->where('item_number','PROC'.$procedure_id);
		$this->db->set('item_number',NULL);
		$this->db->set('deleted',1);
		$this->db->update('items');

		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}

	function procedures_workload($clinic,$start_date,$end_date)
	{
		$procedures = array();
		
		$this->db->select('procedure_id');
		$this->db->from('procedures');
		$this->db->where('clinic',$clinic);
		$result = $this->db->get()->result();
		foreach($result as $procedure):
			$this->db->select('item_id');
			$this->db->from('items');
			$this->db->where('item_number','PROC'.$procedure->procedure_id);
			$item = $this->db->get()->row();
			$procedures[$item->item_id] = 0;
		endforeach;
		

		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$invoice_ids = array();
		$this->db->where('invoice_type',$this->clinic_name($clinic).' Procedures');
		$this->db->where_in('invoice_status',array(1,2,3));
		$this->db->where('service_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');
		$this->db->select('invoice_id');
		$this->db->from('invoices');

		$invoices = $this->db->get();
		if($invoices->num_rows() > 0):
			foreach($invoices->result() as $invoice):
				$invoice_ids[] = $invoice->invoice_id;
			endforeach;
		endif;

		if(!empty($invoice_ids)):
			foreach(array_keys($procedures) as $item_id):
				$this->db->select('sum(quantity) as total');
				$this->db->from('invoices_items');
				$this->db->where('item_id',$item_id);
				$this->db->where('status',1);
				$this->db->where_in('invoice_id',$invoice_ids);
				$procedures[$item_id] = $this->db->get()->row()->total;
			endforeach;
		endif;
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_off();
		return $procedures;
	}

	function get_sections($clinic_code)
	{
		$this->db->from('consultation_sections');
		$this->db->where('clinic',$clinic_code);
		$this->db->order_by('sort','desc');

		return $this->db->get();
	}

	function section_info($section_id,$clinic_code)
	{
		$this->db->from('consultation_sections');
		$this->db->where('section_id',$section_id);
		$this->db->where('clinic',$clinic_code);
		return $this->db->get()->row();
	}

	function save_section($section_data,$clinic,$section_id=null)
	{
		$this->db->trans_start();

		if($section_id):
			$this->db->where('section_id',$section_id);
			$this->db->where('clinic',$clinic);
			$this->db->update('consultation_sections',$section_data);
		else:
			$this->db->insert('consultation_sections',$section_data);
			$section_id = $this->db->insert_id();
		endif;

		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}

	function delete_section($clinic,$section_id)
	{
		$this->db->trans_start();

		$this->db->where('section_id',$section_id);
		$this->db->where('clinic',$clinic);
		$this->db->delete('consultation_sections');

		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}
}
?>