<?php
class Pharm extends CI_Model
{
	function get_main_queue()
	{
		$this->db->from('invoices');
		$this->db->where('invoice_status', 1);
		$this->db->where('invoice_type', 'Pharmacy');
		$this->db->order_by('priority','desc');
		//$this->db->limit(30);
		return $this->db->get();
	}

	function get_pending_queue()
	{
		$this->db->from('invoices');
		$this->db->where('invoice_status', 2);
		$this->db->where('invoice_type', 'Pharmacy');
		$this->db->order_by('priority','desc');
		//$this->db->limit(30);
		return $this->db->get();
	}

	function drugs_list($limit = null, $offset = 0, $search = null)
	{
		$this->db->from('drugs');
		if($limit) $this->db->limit($limit,$offset);
		if($search):
			$this->db->where("(generic_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			brand_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			drug_id LIKE 'PHARM%".$this->db->escape_like_str($search)."%')");
		endif;

		return $this->db->get();
	}

	function count_all_drugs()
	{
		$this->db->select('count(drug_id) as total');
		$this->db->from('drugs');
		return $this->db->get()->row()->total;
	}

	function get_drug_info($drug_id)
	{
		$this->db->from('drugs');
		$this->db->where('drug_id',$drug_id);
		$drug = $this->db->get()->row();

		$this->db->from('items');
		$this->db->where('item_number','PHARM'.$drug_id);
		$drug->item_info = $this->db->get()->row();

		return $drug;
	}

	function save_drug($drug_data,$item_data,$drug_id=null)
	{
		$this->db->trans_start();

		if($drug_id):
			$this->db->where('drug_id',$drug_id);
			$this->db->update('drugs',$drug_data);
			
			$this->db->where('item_number', 'PHARM'.$drug_id);
			$this->db->update('items',$item_data);
		else:
			$this->db->insert('drugs',$drug_data);
			$drug_id = $this->db->insert_id();
			$item_data['item_number'] = 'PHARM'.$drug_id;
			$this->db->insert('items',$item_data);
		endif;

		$this->db->trans_complete();
		
		return $this->db->trans_status() ? $drug_id : false;
	}

	function get_class_suggestions($search)
	{
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('class');
		$this->db->from('drugs');
		$this->db->like('class', $search);
		$this->db->order_by("class", "asc");
		$by_class = $this->db->get();
		foreach($by_class->result() as $row)
		{
			$suggestions[]=$row->class;
		}

		return $suggestions;
	}

	function delete_drug($drug_id)
	{
		$this->db->trans_start();

		$this->db->where('drug_id',$drug_id);
		$this->db->delete('drugs');

		$this->db->where('item_number','PHARM'.$drug_id);
		$this->db->set('item_number',NULL);
		$this->db->set('deleted',1);
		$this->db->update('items');

		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}

	function archive_prescriptions($start_date,$end_date)
	{
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->from('invoices');
		$this->db->where_in('invoice_status', array(2,3));
		$this->db->where('invoice_type', 'Pharmacy');
		$this->db->having('service_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');
		$this->db->order_by('service_time','desc');
		//$this->db->limit(30);
		$result = $this->db->get();
		$this->db->cache_off();
		return $result;
	}

	function get_prescription($invoice_id)
	{
		$this->db->from('invoices');
		$this->db->where('invoice_id',$invoice_id);

		$invoice = $this->db->get()->row();

		$this->db->from('invoices_items');
		$this->db->where('invoice_id',$invoice_id);

		$invoice->invoice_items = $this->db->get()->result();

		return $invoice;
	}

	function get_drug_id($item_id)
	{
		return str_ireplace('PHARM', '', $this->Item->get_info($item_id)->item_number);
	}

	function drug_suggestions($search,$limit=25)
	{
		$suggestions = array();
		
		$this->db->select('drug_id,generic_name,brand_name');
		$this->db->from('drugs');
		$this->db->like('generic_name',$search);
		$this->db->or_like('brand_name',$search);
		
		foreach($this->db->get()->result() as $row)
		{
			$suggestions[] = $row->generic_name ? $row->drug_id.'|'.$row->brand_name.' ('.$row->generic_name.')' : $row->drug_id.'|'.$row->brand_name;		
		}		
		
		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}

	function save_prescription($invoice_data,$invoice_items_data,$invoice_id=null)
	{
		$this->db->trans_start();

		$invoice_id = $this->Invoice->save($invoice_data,$invoice_items_data,$invoice_id);

		if($invoice_id && array_key_exists('status', $invoice_data) && $invoice_data['status'] == 3):
			foreach($invoice_items_data as $item_data):
				$inv_data = array
				(
					'trans_date'=>date('Y-m-d H:i:s'),
					'trans_items'=>$item_data['item_id'],
					'trans_user'=>$invoice_data['employee_id'],
					'trans_comment'=>'Dispensed',
					'trans_inventory'=>-($item_data['quantity'])
				);
				$this->Inventory->insert($inv_data);
			endforeach;
		endif;

		$this->db->trans_complete();
		
		return $this->db->trans_status() ? $invoice_id : false;
	}

	function get_invoice_drugs($patient_id)
	{
		$patient_liability = 100;
		$institution = $patient_id ? $this->Institution->get_patient_institution($patient_id) : '';
		if($institution):
			$patient_liability = 100 - ($institution->institution_discount + $institution->institution_liability);
		endif;
		
		$this->db->select('invoice_id,invoice_status');
		$this->db->from('invoices');
		$this->db->where('patient_id',$patient_id);
		if($patient_liability == 0) $this->db->where('invoice_status',1);
		else $this->db->where('invoice_status',0);
		$this->db->where('invoice_type','Pharmacy');
		$invoices = $this->db->get();

		$invoices_items = array();

		if($invoices->num_rows() > 0):
			foreach($invoices->result() as $invoice):
				$this->db->from('invoices_items');
				$this->db->where('invoice_id',$invoice->invoice_id);
				foreach($this->db->get()->result() as $item):
					$item->invoice_status = $invoice->invoice_status;
					$invoices_items[] = $item;
				endforeach;
			endforeach;
		endif;

		return $invoices_items;
	}

	function save_dispense($invoices_data,$invoices_items_data)
	{
		$this->db->trans_start();

		foreach($invoices_data as $invoice_id=>$invoice_data):
			$this->db->where('invoice_id',$invoice_id);
			$this->db->update('invoices',$invoice_data);
		endforeach;

		foreach($invoices_items_data as $item_data):
			$this->db->where('item_id',$item_data['item_id']);
			$this->db->where('invoice_id',$item_data['invoice_id']);
			$this->db->where('line',$item_data['line']);
			$this->db->update('invoices_items',$item_data);
		endforeach;

		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}

	function get_prescriptions($patient_id)
	{
		$this->db->select('invoice_id,invoice_status');
		$this->db->from('invoices');
		$this->db->where('patient_id',$patient_id);
		$this->db->where_in('invoice_status',array(1,2));
		$this->db->where('invoice_type','Pharmacy');
		$invoices = $this->db->get();

		$invoices_items = array();

		if($invoices->num_rows() > 0):
			foreach($invoices->result() as $invoice):
				$this->db->from('invoices_items');
				$this->db->where('invoice_id',$invoice->invoice_id);
				foreach($this->db->get()->result() as $item):
					$item->invoice_status = $invoice->invoice_status;
					$invoices_items[] = $item;
				endforeach;
			endforeach;
		endif;

		return $invoices_items;
	}

	function get_consultation_notes($patient_id)
	{

		$this->db->select('consultation_id');
		$this->db->from('invoices');
		$this->db->where('patient_id',$patient_id);
		$this->db->where('invoice_type','Pharmacy');
		$invoices = $this->db->get();

		$consultation_ids = array('0');

		if($invoices->num_rows() > 0):
			foreach($invoices->result() as $invoice):
				$consultation_ids[] = $invoice->consultation_id;
			endforeach;
		endif;

		$this->db->select('consultation_id,consultation_start,consultant_id');
		$this->db->from('consultation');	
		$this->db->where_in('consultation_id',$consultation_ids);
		$this->db->order_by("consultation_start", "desc");

		return $this->db->get();
	}

	function get_consultation_prescription($consultation_id)
	{
		$this->db->select('invoice_id,invoice_status');
		$this->db->from('invoices');
		$this->db->where('consultation_id',$consultation_id);
		$this->db->where('invoice_type','Pharmacy');
		$invoices = $this->db->get();

		$invoices_items = array();

		if($invoices->num_rows() > 0):
			foreach($invoices->result() as $invoice):
				$this->db->from('invoices_items');
				$this->db->where('invoice_id',$invoice->invoice_id);
				foreach($this->db->get()->result() as $item):
					$item->invoice_status = $invoice->invoice_status;
					$invoices_items[] = $item;
				endforeach;
			endforeach;
		endif;

		return $invoices_items;
	}

	function workload($start_date,$end_date,$employee_id=null,$gender=null,$ages=null)
	{
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->select('count(invoice_id) as total');
		$this->db->from('invoices');
		$this->db->where('invoice_type','Pharmacy');
		$this->db->where_in('invoice_status',array(2,3));
		$this->db->where('service_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');
		if($employee_id) $this->db->where('served_by',$employee_id);
		if($gender) $this->db->where('patient_id IN (SELECT patient_id FROM '.$this->db->dbprefix('patients').' WHERE gender = "'.$gender.'")');
		if($ages) $this->db->where('patient_id IN (SELECT patient_id FROM '.$this->db->dbprefix('patients').' 
			WHERE DATEDIFF(DATE(service_time),STR_TO_DATE(dob,"%d-%m-%Y")) BETWEEN '.($ages['start'] * 365). ' AND '.($ages['end'] * 365 + 364).')');

		$total = $this->db->get()->row()->total;
		$this->db->cache_off();
		return $total;
	}

	function drugs_workload($start_date,$end_date)
	{
		$drugs = array();
		
		$this->db->select('item_id');
		$this->db->from('items');
		$this->db->where('category','Pharmacy');
		$this->db->where('deleted',0);
		foreach($this->db->get()->result() as $item):
			$drugs[$item->item_id] = 0;
		endforeach;
		

		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$invoice_ids = array();
		$this->db->select('invoice_id');
		$this->db->from('invoices');
		$this->db->where('invoice_type','Pharmacy');
		$this->db->where_in('invoice_status',array(2,3));
		$this->db->where('service_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');

		$invoices = $this->db->get();
		if($invoices->num_rows() > 0):
			foreach($invoices->result() as $invoice):
				$invoice_ids[] = $invoice->invoice_id;
			endforeach;
		endif;

		if(!empty($invoice_ids)):
			foreach(array_keys($drugs) as $item_id):
				$this->db->select('sum(quantity) as total');
				$this->db->from('invoices_items');
				$this->db->where('item_id',$item_id);
				$this->db->where('status',1);
				$this->db->where_in('invoice_id',$invoice_ids);
				$drugs[$item_id] = $this->db->get()->row()->total;
			endforeach;
		endif;
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_off();
		return $drugs;
	}
}
?>