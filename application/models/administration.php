<?php
class Administration extends CI_Model
{
	function get_config()
	{
		$config = new stdClass();
		$this->db->from('app_config');
		foreach($this->db->get()->result() as $item):
			$key = $item->key;
			$value = $item->value;
			$config->$key = $value;
		endforeach;

		return $config;
	}

	function save_config($config_data)
	{
		$this->db->trans_start();

		foreach ($config_data as $key => $value):
			$this->db->set('value',$value);
			$this->db->where('key',$key);
			$this->db->update('app_config');
		endforeach;
		
		$this->db->trans_complete();	
		return $this->db->trans_status();
	}

	function get_modules()
	{
		$this->db->from('modules');
		$this->db->where('deleted',0);
		$this->db->order_by('sort', 'asc');
		return $this->db->get();
	}

	function get_module_info($module_id)
	{
		$this->db->from('modules');
		$this->db->where('deleted',0);
		$this->db->where('module_id',$module_id);
		return $this->db->get()->row();
	}

	function save_module($module_data,$module_id=null)
	{
		if ( $module_id && !$this->module_exists($module_id) )
			return false;
		$this->db->trans_start();

		if($module_id):
			$this->db->where('module_id',$module_id);
			$this->db->update('modules',$module_data);
		else:
			$this->db->insert('modules',$module_data);
			$module_id = $module_data['module_id'];
		endif;
		
		$this->db->trans_complete();	
		return $this->db->trans_status() ? $module_id : false;
	}

	function module_exists($module_id)
	{
		$this->db->select('module_id');
		$this->db->from('modules');
		$this->db->where('deleted',0);
		$this->db->where('module_id',$module_id);
		$query = $this->db->get();
		
		return ($query->num_rows()==1);
	}

	function delete_module($module_id)
	{
		$this->db->trans_start();
		$this->db->set('deleted',1);
		$this->db->where('module_id',$module_id);
		$this->db->update('modules');

		//$this->db->where('module_id',$module_id);
		//$this->db->delete('permissions');

		$this->db->trans_complete();	
		return $this->db->trans_status();
	}

	function get_clinics()
	{
		$this->db->from('clinics');
		return $this->db->get();
	}

	function get_clinic_info($clinic_id)
	{
		$this->db->from('clinics');
		$this->db->where('clinic_id',$clinic_id);
		$clinic = $this->db->get()->row();

		$this->db->from('items');
		$this->db->where('item_number',$clinic->clinic_code);
		$clinic->item_info = $this->db->get()->row();

		return $clinic;
	}

	function save_clinic($clinic_data,$item_data,$clinic_id=null)
	{
		if ( $clinic_id && !$this->clinic_exists($clinic_id) )
			return false;
		$this->db->trans_start();

		if($clinic_id):
			$this->db->where('clinic_id',$clinic_id);
			$this->db->update('clinics',$clinic_data);

			$this->db->where('item_number', 'clinic'.$clinic_id);
			$this->db->update('items',$item_data);
		else:
			$this->db->insert('clinics',$clinic_data);
			$clinic_id = $this->db->insert_id();

			$this->db->set('clinic_code','clinic'.$clinic_id);
			$this->db->where('clinic_id',$clinic_id);
			$this->db->update('clinics');

			$item_data['item_number'] = 'clinic'.$clinic_id;
			$this->db->insert('items',$item_data);

			$sections = array(
				'chief_complaints'=>'Chief Complaints',
				'medical_history'=>'Medical Hx',
				'obs_gyn'=>'Obst &amp; Gynae Hx',
				'family_history'=>'Family Hx',
				'examination'=>'Examination',
				'lab_request'=>'Lab Request',
				'diagnosis'=>'Diagnosis',
				'procedures'=>'Procedures',
				'treatment_plan'=>'Treatment Plan',
				'prescription'=>'Prescription',
				);
			$sort = 100;
			foreach($sections as $section_id=>$section_name):
				$this->db->set('clinic','clinic'.$clinic_id);
				$this->db->set('section_id',$section_id);
				$this->db->set('section_name',$section_name);
				$this->db->set('sort',$sort);
				$this->db->insert('consultation_sections');
				$sort -= 5;
			endforeach;
		endif;
		
		$this->db->trans_complete();	
		return $this->db->trans_status() ? $clinic_id : false;
	}

	function clinic_exists($clinic_id)
	{
		$this->db->select('clinic_id');
		$this->db->from('clinics');
		$this->db->where('clinic_id',$clinic_id);
		$query = $this->db->get();
		
		return ($query->num_rows()==1);
	}

	function delete_clinic($clinic_id)
	{
		$this->db->trans_start();

		$this->db->where('clinic_id',$clinic_id);
		$this->db->delete('clinics');

		$this->db->where('item_number','clinic'.$clinic_id);
		$this->db->set('deleted',1);
		$this->db->update('items');

		$this->db->where('clinic','clinic'.$clinic_id);
		$this->db->delete('consultation_sections');

		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}

	function get_complaints()
	{
		$this->db->from('chief_complaints');
		return $this->db->get();
	}

	function get_complaint_info($complaint_id)
	{
		$this->db->from('chief_complaints');
		$this->db->where('complaint_id',$complaint_id);
		return $this->db->get()->row();
	}

	function save_complaint($complaint_data,$complaint_id=null)
	{
		if ( $complaint_id && !$this->complaint_exists($complaint_id) )
			return false;
		$this->db->trans_start();

		if($complaint_id):
			$this->db->where('complaint_id',$complaint_id);
			$this->db->update('chief_complaints',$complaint_data);
		else:
			$this->db->insert('chief_complaints',$complaint_data);
		endif;
		
		$this->db->trans_complete();	
		return $this->db->trans_status();
	}

	function complaint_exists($complaint_id)
	{
		$this->db->select('complaint_id');
		$this->db->from('chief_complaints');
		$this->db->where('complaint_id',$complaint_id);
		$query = $this->db->get();
		
		return ($query->num_rows()==1);
	}

	function delete_complaint($complaint_id)
	{
		$this->db->trans_start();

		$this->db->where('complaint_id',$complaint_id);
		$this->db->delete('chief_complaints');

		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}

	function get_examinations()
	{
		$this->db->from('examinations');
		return $this->db->get();
	}

	function get_examination_info($examination_id)
	{
		$this->db->from('examinations');
		$this->db->where('examination_id',$examination_id);
		return $this->db->get()->row();
	}

	function save_examination($examination_data,$examination_id=null)
	{
		if ( $examination_id && !$this->examination_exists($examination_id) )
			return false;
		$this->db->trans_start();

		if($examination_id):
			$this->db->where('examination_id',$examination_id);
			$this->db->update('examinations',$examination_data);
		else:
			$this->db->insert('examinations',$examination_data);
		endif;
		
		$this->db->trans_complete();	
		return $this->db->trans_status();
	}

	function examination_exists($examination_id)
	{
		$this->db->select('examination_id');
		$this->db->from('examinations');
		$this->db->where('examination_id',$examination_id);
		$query = $this->db->get();
		
		return ($query->num_rows()==1);
	}

	function delete_examination($examination_id)
	{
		$this->db->trans_start();

		$this->db->where('examination_id',$examination_id);
		$this->db->delete('examinations');

		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}

	function get_category_suggestions($table,$search)
	{
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('category');
		$this->db->from($table);
		$this->db->like('category', $search);
		$this->db->order_by("category", "asc");
		$by_category = $this->db->get();
		foreach($by_category->result() as $row)
		{
			$suggestions[]=$row->category;
		}

		return $suggestions;
	}

	function get_subcategory_suggestions($table,$search)
	{
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('sub_category');
		$this->db->from($table);
		//$this->db->where('category',$category);
		$this->db->like('sub_category', $search);
		$this->db->order_by("sub_category", "asc");
		$by_subcategory = $this->db->get();
		foreach($by_subcategory->result() as $row)
		{
			$suggestions[] = $row->sub_category;
		}

		return $suggestions;
	}
}
?>
