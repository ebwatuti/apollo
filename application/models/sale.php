<?php
class Sale extends CI_Model
{
	public function get_info($sale_id)
	{
		$this->db->from('sales');
		$this->db->where('sale_id',$sale_id);
		return $this->db->get();
	}

	function exists($sale_id)
	{
		$this->db->from('sales');
		$this->db->where('sale_id',$sale_id);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}
	
	function update($sale_data, $sale_id)
	{
		$this->db->where('sale_id', $sale_id);
		$success = $this->db->update('sales',$sale_data);
		
		return $success;
	}
	
	function save ($items,$customer_id,$employee_id,$comment,$payments,$sale_id=false,$invoice_data = null)
	{
		if(count($items)==0)
			return -1;

		//Alain Multiple payments
		//Build payment types string
		$payment_types='';
		foreach($payments as $payment_id=>$payment)
		{
			$payment_types=$payment_types.$payment['payment_type'].': '.to_currency($payment['payment_amount']).'<br />';
		}

		$sales_data = array(
			'sale_time' => date('Y-m-d H:i:s'),
			'customer_id'=> $this->Patient->exists($customer_id) ? $customer_id : null,
			'employee_id'=>$employee_id,
			'payment_type'=>$payment_types,
			'comment'=>$comment
		);

		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();

		$this->db->insert('sales',$sales_data);
		$sale_id = $this->db->insert_id();

		foreach($payments as $payment_id=>$payment)
		{
			if ( substr( $payment['payment_type'], 0, strlen( $this->lang->line('sales_giftcard') ) ) == $this->lang->line('sales_giftcard') )
			{
				/* We have a gift card and we have to deduct the used value from the total value of the card. */
				$splitpayment = explode( ':', $payment['payment_type'] );
				$cur_giftcard_value = $this->Giftcard->get_giftcard_value( $splitpayment[1] );
				$this->Giftcard->update_giftcard_value( $splitpayment[1], $cur_giftcard_value - $payment['payment_amount'] );
			}

			$sales_payments_data = array
			(
				'sale_id'=>$sale_id,
				'payment_type'=>$payment['payment_type'],
				'payment_amount'=>$payment['payment_amount'],
				'payment_remarks'=>$payment['payment_remarks']
			);
			$this->db->insert('sales_payments',$sales_payments_data);
		}

		foreach($items as $line=>$item)
		{
			$cur_item_info = $this->Item->get_info($item['item_id']);

			$sales_items_data = array
			(
				'sale_id'=>$sale_id,
				'item_id'=>$item['item_id'],
				'line'=>$item['line'],
				'description'=>$item['description'],
				'serialnumber'=>$item['serialnumber'],
				'quantity_purchased'=>$item['quantity'],
				'discount_percent'=>$item['discount'],
				'item_cost_price' => $cur_item_info->cost_price,
				'item_unit_price'=>$item['price']
			);

			$this->db->insert('sales_items',$sales_items_data);

			//Update stock quantity
			if($cur_item_info->quantifiable == 1):
				$item_data = array('quantity'=>$cur_item_info->quantity - $item['quantity']);
				$this->Item->save($item_data,$item['item_id']);
			endif;
			
			//Ramel Inventory Tracking
			//Inventory Count Details
			$qty_buy = -$item['quantity'];
			$sale_remarks = $this->receipt_no($sale_id);
			$inv_data = array
			(
				'trans_date'=>date('Y-m-d H:i:s'),
				'trans_items'=>$item['item_id'],
				'trans_user'=>$employee_id,
				'trans_comment'=>$sale_remarks,
				'trans_inventory'=>$qty_buy
			);
			$this->Inventory->insert($inv_data);
		}

		if($invoice_data):
			foreach($invoice_data as $invoice_id=>$invoice):
				$invoice_info = $this->Invoice->get_info($invoice_id);
				if($invoice_info->invoice_type == 'Consultation'):
					$encounter = $this->Encounter->get_info($invoice_info->encounter_id);
					if($encounter):
						$encounter_data = array('encounter_status'=> 2 - $encounter->triage);
						$this->Encounter->update_encounter($encounter_data,$invoice_info->encounter_id);
					endif;
				endif;
				$invoice['invoices_data']['sale_id'] = $sale_id;
				$this->Invoice->save_sale_invoice($invoice['invoices_data'],$invoice['invoices_items_data'],$invoice_id);
			endforeach;
		endif;
		$this->db->trans_complete();
		
		return $this->db->trans_status() ? $sale_id : false;
	}
	
	function delete($sale_id)
	{
		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();
		
		$this->db->set('deleted',1);
		$this->db->where('sale_id',$sale_id);
		$this->db->update('sales'); 
		
		$this->db->trans_complete();
				
		return $this->db->trans_status();
	}

	function get_sale_items($sale_id)
	{
		$this->db->from('sales_items');
		$this->db->where('sale_id',$sale_id);
		return $this->db->get();
	}

	function get_sale_payments($sale_id)
	{
		$this->db->from('sales_payments');
		$this->db->where('sale_id',$sale_id);
		return $this->db->get();
	}

	function get_customer($sale_id)
	{
		$this->db->from('sales');
		$this->db->where('sale_id',$sale_id);
		return $this->Patient->get_info($this->db->get()->row()->customer_id);
	}

	//We create a temp table that allows us to do easy report/sales queries
	public function create_sales_items_temp_table()
	{
		$this->db->query("CREATE TEMPORARY TABLE ".$this->db->dbprefix('sales_items_temp')."
		(SELECT date(sale_time) as sale_date, ".$this->db->dbprefix('sales_items').".sale_id, comment,payment_type, customer_id, employee_id, 
		".$this->db->dbprefix('items').".item_id, supplier_id, quantity_purchased, item_cost_price, item_unit_price, SUM(percent) as item_tax_percent,
		discount_percent, (item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100) as subtotal,
		".$this->db->dbprefix('sales_items').".line as line, serialnumber, ".$this->db->dbprefix('sales_items').".description as description,
		ROUND((item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)*(1+(SUM(percent)/100)),2) as total,
		ROUND((item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)*(SUM(percent)/100),2) as tax,
		(item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100) - (item_cost_price*quantity_purchased) as profit
		FROM ".$this->db->dbprefix('sales_items')."
		INNER JOIN ".$this->db->dbprefix('sales')." ON  ".$this->db->dbprefix('sales_items').'.sale_id='.$this->db->dbprefix('sales').'.sale_id'."
		INNER JOIN ".$this->db->dbprefix('items')." ON  ".$this->db->dbprefix('sales_items').'.item_id='.$this->db->dbprefix('items').'.item_id'."
		LEFT OUTER JOIN ".$this->db->dbprefix('suppliers')." ON  ".$this->db->dbprefix('items').'.supplier_id='.$this->db->dbprefix('suppliers').'.person_id'."
		LEFT OUTER JOIN ".$this->db->dbprefix('sales_items_taxes')." ON  "
		.$this->db->dbprefix('sales_items').'.sale_id='.$this->db->dbprefix('sales_items_taxes').'.sale_id'." and "
		.$this->db->dbprefix('sales_items').'.item_id='.$this->db->dbprefix('sales_items_taxes').'.item_id'." and "
		.$this->db->dbprefix('sales_items').'.line='.$this->db->dbprefix('sales_items_taxes').'.line'."
		GROUP BY sale_id, item_id, line)");

		//Update null item_tax_percents to be 0 instead of null
		$this->db->where('item_tax_percent IS NULL');
		$this->db->update('sales_items_temp', array('item_tax_percent' => 0));

		//Update null tax to be 0 instead of null
		$this->db->where('tax IS NULL');
		$this->db->update('sales_items_temp', array('tax' => 0));

		//Update null subtotals to be equal to the total as these don't have tax
		$this->db->query('UPDATE '.$this->db->dbprefix('sales_items_temp'). ' SET total=subtotal WHERE total IS NULL');
	}
	
	public function get_giftcard_value( $giftcardNumber )
	{
		if ( !$this->Giftcard->exists( $this->Giftcard->get_giftcard_id($giftcardNumber)))
			return 0;
		
		$this->db->from('giftcards');
		$this->db->where('giftcard_number',$giftcardNumber);
		return $this->db->get()->row()->value;
	}

	function receipt_no($sale_id)
	{
		return RECEIPT_PREFIX." $sale_id";
	}

	function workload($start_date,$end_date,$where=null)
	{

		$workload = array();
		$value = 0;
		$discount = 0;
		$profit = 0;
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->select('sale_id');
		$this->db->from('sales');
		$this->db->where('deleted',0);
		$this->db->where('sale_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');
		if($where) $this->db->where($where);

		$sales = $this->db->get();
		foreach($sales->Result() as $sale):
			$this->db->from('sales_items');
			$this->db->where('sale_id',$sale->sale_id);
			foreach($this->db->get()->Result() as $item):
				$value = $value + ($item->quantity_purchased * $item->item_unit_price * (100 - $item->discount_percent) / 100);
				$discount = $discount + ($item->quantity_purchased * $item->item_unit_price * $item->discount_percent / 100);
				$profit = $profit + ($item->quantity_purchased * (($item->item_unit_price * (100 - $item->discount_percent) / 100) - $item->item_cost_price));
			endforeach;
		endforeach;
		$workload['sales'] = $sales->num_rows();
		$workload['value'] = $value;
		$workload['discount'] = $discount;
		$workload['profit'] = $profit;
		$this->db->cache_off();
		return $workload;
	}

	function categories_workload($start_date,$end_date,$employee_id = null)
	{
		$categories = array();
		$sale_ids = array();
		
		foreach($this->Item->get_categories()->Result() as $item):
			$categories[$item->category] = array('value'=>0,'discount'=>0,'profit'=>0,'name'=>$item->category);
		endforeach;
		

		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->select('sale_id');
		$this->db->from('sales');
		$this->db->where('deleted',0);
		if($employee_id) $this->db->where('employee_id', $employee_id);
		$this->db->where('sale_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');

		$sales = $this->db->get();
		if($sales->num_rows() > 0):
			foreach($sales->result() as $sale):
				$sale_ids[] = $sale->sale_id;
			endforeach;
		endif;

		if(!empty($sale_ids)):
			foreach(array_keys($categories) as $category):
				$item_ids = array();
				$this->db->select('item_id');
				$this->db->from('items');
				$this->db->where('category',$category);
				foreach($this->db->get()->Result() as $item):
					$item_ids[] = $item->item_id;
				endforeach;

				if(!empty($item_ids)):
					$value = 0;
					$discount = 0;
					$profit = 0;
					
					$this->db->from('sales_items');
					$this->db->where_in('item_id',$item_ids);
					$this->db->where_in('sale_id',$sale_ids);
					foreach($this->db->get()->Result() as $item):
						$value = $value + ($item->quantity_purchased * $item->item_unit_price * (100 - $item->discount_percent) / 100);
						$discount = $discount + ($item->quantity_purchased * $item->item_unit_price * $item->discount_percent / 100);
						$profit = $profit + ($item->quantity_purchased * (($item->item_unit_price * (100 - $item->discount_percent) / 100) - $item->item_cost_price));
					endforeach;

					$categories[$category]['value'] = $value;
					$categories[$category]['discount'] = $discount;
					$categories[$category]['profit'] = $profit;
				endif;
			endforeach;
		endif;

		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_off();
		return $categories;
	}

	function items_workload($start_date,$end_date,$employee_id = null)
	{
		$items = array();
		$sale_ids = array();

		$this->db->from('items');
		#$this->db->where('deleted',0);
		foreach($this->db->get()->Result() as $item):
			$items[$item->item_id] = array('value'=>0,'discount'=>0,'profit'=>0,'quantity'=>0,'name'=>$item->name,'price'=>$item->unit_price);
		endforeach;
		

		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->select('sale_id');
		$this->db->from('sales');
		$this->db->where('deleted',0);
		if($employee_id) $this->db->where('employee_id', $employee_id);
		$this->db->where('sale_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');

		$sales = $this->db->get();
		if($sales->num_rows() > 0):
			foreach($sales->result() as $sale):
				$sale_ids[] = $sale->sale_id;
			endforeach;
		endif;

		if(!empty($sale_ids)):
			foreach(array_keys($items) as $item_id):
				$value = 0;
				$discount = 0;
				$profit = 0;
				$quantity = 0;
				
				$this->db->from('sales_items');
				$this->db->where('item_id',$item_id);
				$this->db->where_in('sale_id',$sale_ids);
				foreach($this->db->get()->Result() as $item):
					$value = $value + ($item->quantity_purchased * $item->item_unit_price * (100 - $item->discount_percent) / 100);
					$discount = $discount + ($item->quantity_purchased * $item->item_unit_price * $item->discount_percent / 100);
					$profit = $profit + ($item->quantity_purchased * (($item->item_unit_price * (100 - $item->discount_percent) / 100) - $item->item_cost_price));
					$quantity = $quantity + $item->quantity_purchased;
				endforeach;

				$items[$item_id]['value'] = $value;
				$items[$item_id]['discount'] = $discount;
				$items[$item_id]['profit'] = $profit;
				$items[$item_id]['quantity'] = $quantity;
			endforeach;
		endif;

		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_off();
		return $items;
	}

	function employees_workload($start_date,$end_date)
	{
		$employees = array();
		
		foreach($this->Employee->get_all()->Result() as $employee):
			$name = $this->Patient->patient_name($employee->first_name,$employee->middle_name,$employee->last_name);
			$employees[$employee->person_id] = array('value'=>0,'discount'=>0,'profit'=>0,'name'=>$name,'sales'=>0);
		endforeach;
		

		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		foreach(array_keys($employees) as $employee):
			$sale_ids = array();

			$this->db->select('sale_id');
			$this->db->from('sales');
			$this->db->where('deleted',0);
			$this->db->where('employee_id',$employee);
			$this->db->where('sale_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');

			$sales = $this->db->get();
			$employees[$employee]['sales'] = $sales->num_rows();
			if($sales->num_rows() > 0):
				foreach($sales->result() as $sale):
					$sale_ids[] = $sale->sale_id;
				endforeach;
			endif;

			if(!empty($sale_ids)):
				$value = 0;
				$discount = 0;
				$profit = 0;
				
				$this->db->from('sales_items');
				$this->db->where_in('sale_id',$sale_ids);
				foreach($this->db->get()->Result() as $item):
					$value = $value + ($item->quantity_purchased * $item->item_unit_price * (100 - $item->discount_percent) / 100);
					$discount = $discount + ($item->quantity_purchased * $item->item_unit_price * $item->discount_percent / 100);
					$profit = $profit + ($item->quantity_purchased * (($item->item_unit_price * (100 - $item->discount_percent) / 100) - $item->item_cost_price));
				endforeach;

				$employees[$employee]['value'] = $value;
				$employees[$employee]['discount'] = $discount;
				$employees[$employee]['profit'] = $profit;
			endif;
		endforeach;

		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_off();
		return $employees;
	}

	function payments_workload($start_date,$end_date,$employee_id = null)
	{
		$payments = array();
		$sale_ids = array();

		$this->db->distinct();
		$this->db->select('payment_type');
		$this->db->from('sales_payments');
		
		foreach($this->db->get()->Result() as $payment):
			$payments[$payment->payment_type] = array('value'=>0,'discount'=>0,'profit'=>0,'name'=>$payment->payment_type);
		endforeach;
		

		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->select('sale_id');
		$this->db->from('sales');
		$this->db->where('deleted',0);
		if($employee_id) $this->db->where('employee_id', $employee_id);
		$this->db->where('sale_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');

		$sales = $this->db->get();
		if($sales->num_rows() > 0):
			foreach($sales->result() as $sale):
				$sale_ids[] = $sale->sale_id;
			endforeach;
		endif;

		if(!empty($sale_ids)):
			foreach(array_keys($payments) as $payment_type):
				$this->db->select('SUM(payment_amount) as total');
				$this->db->from('sales_payments');
				$this->db->where('payment_type',$payment_type);
				$this->db->where_in('sale_id',$sale_ids);
				$value = $this->db->get()->row()->total;

				$payments[$payment_type]['value'] = $value;
			endforeach;
		endif;

		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_off();
		return $payments;
	}

	function subcategories_workload($start_date,$end_date,$category,$employee_id = null)
	{
		$subcategories = array();
		$sale_ids = array();

		$sub_categories = $this->Item->get_subcategories($category);
		foreach($sub_categories->Result() as $item):
			//$sub_category = $item->sub_category ? $item->sub_category : $category;
			$sub_category = $item->sub_category;
			if($sub_category == '') $name = ($sub_categories->num_rows() == 1) ? $category : 'Others';
			else $name = $sub_category;
			$subcategories[$sub_category] = array('value'=>0,'discount'=>0,'profit'=>0,'name'=>$name);
		endforeach;
		

		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->select('sale_id');
		$this->db->from('sales');
		$this->db->where('deleted',0);
		if($employee_id) $this->db->where('employee_id', $employee_id);
		$this->db->where('sale_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');

		$sales = $this->db->get();
		if($sales->num_rows() > 0):
			foreach($sales->result() as $sale):
				$sale_ids[] = $sale->sale_id;
			endforeach;
		endif;

		if(!empty($sale_ids)):
			foreach(array_keys($subcategories) as $subcategory):
				$item_ids = array();
				$this->db->select('item_id');
				$this->db->from('items');
				$this->db->where('category',$category);
				if($category != $subcategory) $this->db->where('sub_category',$subcategory);
				foreach($this->db->get()->Result() as $item):
					$item_ids[] = $item->item_id;
				endforeach;

				if(!empty($item_ids)):
					$value = 0;
					$discount = 0;
					$profit = 0;
					
					$this->db->from('sales_items');
					$this->db->where_in('item_id',$item_ids);
					$this->db->where_in('sale_id',$sale_ids);
					foreach($this->db->get()->Result() as $item):
						$value = $value + ($item->quantity_purchased * $item->item_unit_price * (100 - $item->discount_percent) / 100);
						$discount = $discount + ($item->quantity_purchased * $item->item_unit_price * $item->discount_percent / 100);
						$profit = $profit + ($item->quantity_purchased * (($item->item_unit_price * (100 - $item->discount_percent) / 100) - $item->item_cost_price));
					endforeach;

					$subcategories[$subcategory]['value'] = $value;
					$subcategories[$subcategory]['discount'] = $discount;
					$subcategories[$subcategory]['profit'] = $profit;
				endif;
			endforeach;
		endif;

		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_off();
		return $subcategories;
	}

	function sales_archive($start_date,$end_date)
	{
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->from('sales');
		$this->db->where('deleted', 0);
		$this->db->having('sale_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');
		$this->db->order_by('sale_time','desc');
		$result = $this->db->get();
		$this->db->cache_off();
		return $result;
	}

	function get_sale_info($sale_id)
	{
		$this->db->from('sales');
		$this->db->where('sale_id',$sale_id);
		$sale = $this->db->get()->row();

		$this->db->from('sales_items');
		$this->db->where('sale_id',$sale_id);
		$sale->sale_items = $this->db->get()->Result();

		$this->db->from('sales_payments');
		$this->db->where('sale_id',$sale_id);
		$sale->sale_payments = $this->db->get()->Result();

		return $sale;
	}
}
?>
