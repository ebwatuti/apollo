<?php
class Supplier extends CI_Model
{	
	function exists($supplier_id)
	{
		$this->db->from('suppliers');
		$this->db->where('person_id',$supplier_id);
		$query = $this->db->get();
		
		return ($query->num_rows()==1);
	}	
	
	function get_all()
	{
		$this->db->from('suppliers');
		$this->db->where('deleted',0);			
		$this->db->order_by("company_name", "asc");
		return $this->db->get();		
	}
	
	function get_info($supplier_id)
	{
		$this->db->from('suppliers');
		$this->db->where('person_id',$supplier_id);
		return $this->db->get()->row();
	}

	function save($supplier_data, $supplier_id = null)
	{
		if ( $supplier_id && !$this->exists($supplier_id) )
			return false;
		$this->db->trans_start();

		if($supplier_id):
			$this->db->where('person_id',$supplier_id);
			$this->db->update('suppliers',$supplier_data);
		else:
			$this->db->insert('suppliers',$supplier_data);
			$supplier_id = $this->db->insert_id();
		endif;
		
		$this->db->trans_complete();	
		return $this->db->trans_status() ? $supplier_id : false;	
	}
	
	function delete($supplier_id)
	{
		$this->db->trans_start();

		$this->db->set('deleted',1);
		$this->db->set('account_number',null);
		$this->db->where('person_id',$supplier_id);
		$this->db->update('suppliers');

		$this->db->trans_complete();		
		return $this->db->trans_status();
	}

	function account_no_exists($account_no, $supplier_id = null)
	{
		$this->db->select('person_id');
		$this->db->from('suppliers');
		$this->db->where('account_number',$account_no);
		$this->db->where('deleted',0);
		if($supplier_id) $this->db->where('person_id !=',$supplier_id);
		return $this->db->get()->num_rows() == 1;
	}

	function get_supplier_search_suggestions($search,$limit=25)
	{
		$suggestions = array();
		
		$this->db->from('suppliers');	
		$this->db->where('deleted', 0);
		$this->db->like("company_name",$search);
		$this->db->order_by("company_name", "asc");		
		$by_company_name = $this->db->get();
		foreach($by_company_name->result() as $row)
		{
			$suggestions[]=$row->person_id.'|'.$row->company_name;		
		}
		
		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}
}
?>
