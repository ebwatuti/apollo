<?php
class Income extends CI_Model
{
	function get_info($income_id)
	{
		$this->db->from('incomes');
		$this->db->where('income_id',$income_id);

		return $this->db->get()->row();
	}

	function exists($income_id)
	{
		$this->db->select('income_id');
		$this->db->from('incomes');
		$this->db->where('income_id',$income_id);
		$query = $this->db->get();
		
		return ($query->num_rows()==1);
	}

	function save_revenue($revenue_data, $income_id)
	{
		$this->db->trans_start();

		$this->db->select('revenue_id');
		$this->db->from('incomes_revenue');
		$this->db->where('income_id',$income_id);
		$this->db->where('period',date('Y-m-01'));
		$query = $this->db->get();
		
		if($query->num_rows()==1):
			$this->db->where('income_id',$income_id);
			$this->db->where('period',date('Y-m-01'));
			$this->db->update('incomes_revenue',$revenue_data);
		else:
			$revenue_data['income_id'] = $income_id;
			$this->db->insert('incomes_revenue',$revenue_data);
		endif;

		$this->db->trans_complete();	
		return $this->db->trans_status();
	}

	function save($income_data, $revenue_data, $income_id = null)
	{
		if ( $income_id && !$this->exists($income_id) )
			return false;
		$this->db->trans_start();

		if($income_id):
			$this->db->where('income_id',$income_id);
			$this->db->update('incomes',$income_data);
		else:
			$this->db->insert('incomes',$income_data);
			$income_id = $this->db->insert_id();
		endif;

		$revenue = $this->save_revenue($revenue_data, $income_id);

		$this->db->trans_complete();	
		return $this->db->trans_status() ? $income_id : false;	
	}

	function delete($income_id)
	{
		$this->db->trans_start();

		$this->db->set('deleted',1);
		$this->db->where('income_id',$income_id);
		$this->db->update('incomes');

		$this->db->trans_complete();		
		return $this->db->trans_status();
	}

	function get_all($period=null,$chapter=null,$category=null,$subcategory=null)
	{
		if(!$period) $period = date('Y-m-01');
		$data = array();

		if(!$chapter):
			foreach($this->get_chapters()->result() as $chapter):
				if($chapter->source == 'income'):
					$revenue = $this->get_income_revenue($period,$chapter->chapter_id);
				else:
					$category = explode(':', $chapter->source);
					$category = $category[1];
					$revenue = $this->get_sales_revenue($period,$category);
					$revenue = $revenue + $this->get_institutions_revenue($period,$category);
				endif;
				$data[] = array("income_no"=>"Chapter $chapter->chapter_no",
								"description"=>$chapter->description,
								"revenue"=>$revenue,
								"expand"=>true,
								"income_id"=>'',
								"subcategory"=>'',
								"category"=>'',
								"chapter"=>$chapter->chapter_id);
			endforeach;
		else:
			$chapter = $this->get_chapter_info($chapter);

			switch ($chapter->source) {
				case 'income':
					if(!$category):
						foreach($this->get_chapter_categories($chapter->chapter_id)->result() as $category):
							$category = $category->category;
							$revenue = $this->get_income_revenue($period,$chapter->chapter_id,$category);
							$data[] = array("income_no"=>"",
										"description"=>"$category",
										"revenue"=>$revenue,
										"expand"=>($category) ? true : false,
										"income_id"=>'',
										"subcategory"=>'',
										"category"=>$category,
										"chapter"=>$chapter->chapter_id);
						endforeach;
					else:
						if(!$subcategory):
							foreach($this->get_category_subcategories($chapter->chapter_id,$category)->result() as $subcategory):
								$subcategory = $subcategory->subcategory;
								$revenue = $this->get_income_revenue($period,$chapter->chapter_id,$category,$subcategory);
								$data[] = array("income_no"=>"",
											"description"=>"$subcategory",
											"revenue"=>$revenue,
											"expand"=>($category && $subcategory) ? true : false,
											"income_id"=>'',
											"subcategory"=>$subcategory,
											"category"=>$category,
											"chapter"=>$chapter->chapter_id);
							endforeach;
						else:
							foreach($this->get_subcategory_incomes($chapter->chapter_id,$category,$subcategory)->result() as $income):
								$income = $this->get_info($income->income_id);
								$revenue = $this->get_income_revenue($period,$chapter->chapter_id,$category,$subcategory,$income->income_id);
								$data[] = array("income_no"=>"$income->income_no",
											"description"=>"$income->description",
											"revenue"=>$revenue,
											"income_id"=>$income->income_id,
											"subcategory"=>$subcategory,
											"category"=>$category,
											"chapter"=>$chapter->chapter_id);
							endforeach;
						endif;
					endif;

					break;
				
				default:
					if(!$category):
						$category = explode(':', $chapter->source);
						$category = $category[1];

						$data[] = array("income_no"=>"",
									"description"=>"$category (Sales)",
									"revenue"=>$this->get_sales_revenue($period,$category),
									"expand"=>($category) ? true : false,
									"income_id"=>'',
									"subcategory"=>'',
									"category"=>"sales:$category",
									"chapter"=>$chapter->chapter_id);
						$data[] = array("income_no"=>"",
									"description"=>"$category (Institutions)",
									"revenue"=>$this->get_institutions_revenue($period,$category),
									"expand"=>($category) ? true : false,
									"income_id"=>'',
									"subcategory"=>'',
									"category"=>"institutions:$category",
									"chapter"=>$chapter->chapter_id);
					else:
						$category = explode(':', $category);
						$source = $category[0];
						$category = $category[1];

						switch ($source) {
							case 'sales':
								if(!$subcategory):
									foreach($this->Item->get_subcategories($category)->result() as $subcategory):
										$subcategory = $subcategory->sub_category;
										$data[] = array("income_no"=>"",
													"description"=>"$subcategory",
													"revenue"=>$this->get_sales_revenue($period,$category,$subcategory),
													"expand"=>($category && $subcategory) ? true : false,
													"income_id"=>'',
													"subcategory"=>$subcategory,
													"category"=>"sales:$category",
													"chapter"=>$chapter->chapter_id);
									endforeach;
								else:
									foreach($this->Item->get_subcategory_items($category,$subcategory)->result() as $item):
										$item = $this->Item->get_info($item->item_id);
										$data[] = array("income_no"=>"",
													"description"=>"$item->name",
													"revenue"=>$this->get_sales_revenue($period,$category,$subcategory,$item->item_id),
													"income_id"=>'',
													"subcategory"=>$subcategory,
													"category"=>"sales:$category",
													"chapter"=>$chapter->chapter_id);
									endforeach;
								endif;
								break;
							case 'institutions':
								if(!$subcategory):
									foreach($this->Item->get_subcategories($category)->result() as $subcategory):
										$subcategory = $subcategory->sub_category;
										$data[] = array("income_no"=>"",
													"description"=>"$subcategory",
													"revenue"=>$this->get_institutions_revenue($period,$category,$subcategory),
													"expand"=>($category && $subcategory) ? true : false,
													"income_id"=>'',
													"subcategory"=>$subcategory,
													"category"=>"institutions:$category",
													"chapter"=>$chapter->chapter_id);
									endforeach;
								else:
									foreach($this->Item->get_subcategory_items($category,$subcategory)->result() as $item):
										$item = $this->Item->get_info($item->item_id);
										$data[] = array("income_no"=>"",
													"description"=>"$item->name",
													"revenue"=>$this->get_institutions_revenue($period,$category,$subcategory,$item->item_id),
													"income_id"=>'',
													"subcategory"=>$subcategory,
													"category"=>"institutions:$category",
													"chapter"=>$chapter->chapter_id);
									endforeach;
								endif;
								break;
							default:
								if(!$subcategory):
									foreach($subcategories as $subcategory):
										$subcategory = $subcategory->subcategory;
										$data[] = array("income_no"=>"",
													"description"=>"$subcategory",
													"revenue"=>$this->get_sales_revenue($period,$category,$subcategory),
													"expand"=>($category && $subcategory) ? true : false,
													"income_id"=>'',
													"subcategory"=>$subcategory,
													"category"=>"sales:$category",
													"chapter"=>$chapter->chapter_id);
									endforeach;
								else:
									foreach($this->Item->get_subcategory_items($category,$subcategory)->result() as $item):
										$item = $this->Item->get_info($item->item_id);
										$data[] = array("income_no"=>"",
													"description"=>"$item->name",
													"revenue"=>$this->get_sales_revenue($period,$category,$subcategory,$item->item_id),
													"income_id"=>'',
													"subcategory"=>$subcategory,
													"category"=>"sales:$category",
													"chapter"=>$chapter->chapter_id);
									endforeach;
								endif;
								break;
						}
					endif;
					break;
			}
		endif;

		return $data;
	}

	function income_no_exists($income_no, $income_id = null)
	{
		$this->db->select('income_id');
		$this->db->from('incomes');
		$this->db->where('income_no',$income_no);
		if($income_id) $this->db->where('income_id !=',$income_id);
		return $this->db->get()->num_rows() == 1;
	}

	function get_income_revenue_info($income_id,$period = null)
	{
		if(!$period) $period = date('Y-m-01');

		$this->db->from('incomes_revenue');
		$this->db->where('income_id',$income_id);
		$this->db->where('period',$period);
		return $this->db->get()->row();
	}

	function get_chapters()
	{
		$this->db->from('chapters');
		$this->db->where('type','income');
		$this->db->where('deleted',0);
		$this->db->order_by('chapter_no','asc');

		return $this->db->get();
	}

	function get_chapter_categories($chapter)
	{
		$this->db->distinct();
		$this->db->select('category');
		$this->db->from('incomes');
		$this->db->where('chapter',$chapter);
		$this->db->where('deleted',0);
		$this->db->order_by('category','asc');

		return $this->db->get();
	}

	function get_category_subcategories($chapter,$category)
	{
		$this->db->distinct();
		$this->db->select('subcategory');
		$this->db->from('incomes');
		$this->db->where('chapter',$chapter);
		$this->db->where('category',$category);
		$this->db->where('deleted',0);
		$this->db->order_by('subcategory','asc');

		return $this->db->get();
	}

	function get_subcategory_incomes($chapter,$category,$subcategory)
	{
		$this->db->distinct();
		$this->db->select('income_id');
		$this->db->from('incomes');
		$this->db->where('chapter',$chapter);
		$this->db->where('category',$category);
		$this->db->where('subcategory',$subcategory);
		$this->db->where('deleted',0);

		return $this->db->get();
	}

	function get_chapter_info($chapter_id)
	{
		$this->db->from('chapters');
		$this->db->where('chapter_id',$chapter_id);
		return $this->db->get()->row();
	}

	function save_chapter($chapter_data, $chapter_id = null)
	{
		$this->db->trans_start();

		if($chapter_id):
			$this->db->where('chapter_id',$chapter_id);
			$this->db->update('chapters',$chapter_data);
		else:
			$this->db->insert('chapters',$chapter_data);
			$chapter_id = $this->db->insert_id();
		endif;

		$this->db->trans_complete();	
		return $this->db->trans_status();	
	}

	function delete_chapter($chapter_id)
	{
		$this->db->trans_start();

		$this->db->set('deleted',1);
		$this->db->where('chapter_id',$chapter_id);
		$this->db->update('chapters');

		$this->db->trans_complete();		
		return $this->db->trans_status();
	}

	function chapter_no_exists($chapter_no, $chapter_id = null)
	{
		$this->db->select('chapter_id');
		$this->db->from('chapters');
		$this->db->where('chapter_no',$chapter_no);
		$this->db->where('type','income');
		if($chapter_id) $this->db->where('chapter_id !=',$chapter_id);
		return $this->db->get()->num_rows() == 1;
	}

	function get_category_suggestions($search)
	{
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('category');
		$this->db->from('incomes');
		$this->db->like('category', $search);
		$this->db->order_by("category", "asc");
		
		foreach($this->db->get()->result() as $row)
		{
			$suggestions[]=$row->category;
		}

		return $suggestions;
	}

	function get_subcategory_suggestions($search)
	{
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('subcategory');
		$this->db->from('incomes');
		$this->db->like('subcategory', $search);
		$this->db->order_by("subcategory", "asc");
		
		foreach($this->db->get()->result() as $row)
		{
			$suggestions[]=$row->subcategory;
		}

		return $suggestions;
	}

	function get_income_revenue($period,$chapter,$category=null,$subcategory=null,$income_id=null)
	{
		$income_ids = array(0);

		$this->db->select('income_id');
		$this->db->from('incomes');
		$this->db->where('chapter',$chapter);
		if($category !== null) $this->db->where('category',$category);
		if($subcategory !== null) $this->db->where('subcategory',$subcategory);
		if($income_id) $this->db->where('income_id',$income_id);

		$incomes = $this->db->get();
		if($incomes->num_rows() > 0):
			foreach($incomes->result() as $income):
				$income_ids[] = $income->income_id;
			endforeach;
		endif;

		if ($period < date('Y-m-01')) $this->db->cache_on();

		$this->db->select_sum('amount','total');
		$this->db->from('incomes_revenue');
		$this->db->where('period',$period);
		$this->db->where_in('income_id',$income_ids);
		$total = $this->db->get()->row()->total;

		$this->db->cache_off();
		return $total;
	}

	function get_sales_revenue($period,$category=null,$subcategory=null,$item_id=null)
	{
		$item_ids = array(0);
		$sale_ids = array(0);

		$start_date = $period;
		$end_date = date('Y-m-t',strtotime($period));

		if ($period < date('Y-m-01')) $this->db->cache_on();

		$this->db->select('item_id');
		$this->db->from('items');
		if($category !== null) $this->db->where('category',$category);
		if($subcategory !== null) $this->db->where('sub_category',$subcategory);
		if($item_id) $this->db->where('item_id',$item_id);

		$items = $this->db->get();
		if($items->num_rows() > 0):
			foreach($items->result() as $item):
				$item_ids[] = $item->item_id;
			endforeach;
		endif;

		$this->db->select('sale_id');
		$this->db->from('sales');
		$this->db->where('deleted',0);
		$this->db->where('sale_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');

		$sales = $this->db->get();
		if($sales->num_rows() > 0):
			foreach($sales->result() as $sale):
				$sale_ids[] = $sale->sale_id;
			endforeach;
		endif;
		
		$this->db->select_sum('quantity_purchased * item_unit_price * (100 - discount_percent) / 100','total');
		$this->db->from('sales_items');
		$this->db->where_in('item_id',$item_ids);
		$this->db->where_in('sale_id',$sale_ids);
		$total = $this->db->get()->row()->total;

		$this->db->cache_off();
		return $total;
	}

	function get_institutions_revenue($period,$category=null,$subcategory=null,$item_id=null)
	{
		$item_ids = array(0);
		$invoice_ids = array(0);
		$institution_ids = array(0);

		$start_date = $period;
		$end_date = date('Y-m-t',strtotime($period));

		if ($period < date('Y-m-01')) $this->db->cache_on();

		$this->db->select('item_id');
		$this->db->from('items');
		if($category !== null) $this->db->where('category',$category);
		if($subcategory !== null) $this->db->where('sub_category',$subcategory);
		if($item_id) $this->db->where('item_id',$item_id);

		$items = $this->db->get();
		if($items->num_rows() > 0):
			foreach($items->result() as $item):
				$item_ids[] = $item->item_id;
			endforeach;
		endif;

		foreach($this->Institution->get_all()->result() as $institution):
			$institution_ids[] = $institution->institution_id;
		endforeach;

		$this->db->select('invoice_id');
		$this->db->from('invoices');
		$this->db->where_in('institution_id',$institution_ids);	
		$this->db->where('invoice_status !=',0);
		$this->db->where('invoice_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');

		$invoices = $this->db->get();
		if($invoices->num_rows() > 0):
			foreach($invoices->result() as $invoice):
				$invoice_ids[] = $invoice->invoice_id;
			endforeach;
		endif;
		
		$this->db->select_sum('quantity * unit_price * (discount) / 100','total');
		$this->db->from('invoices_items');
		$this->db->where_in('item_id',$item_ids);
		$this->db->where_in('invoice_id',$invoice_ids);
		$total = $this->db->get()->row()->total;

		$this->db->cache_off();
		return $total;
	}
}
?>