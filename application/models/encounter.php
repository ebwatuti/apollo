<?php
class Encounter extends CI_Model
{
#encounter_status
#	0 = registered but not paid
#	1 = paid
#	2 = gone through triage
#	3 = in consultation
#	5 = discharged
	function check_encounter($patient_id)
	{
		$this->db->select('encounter_id');
		$this->db->from('encounter');
		$this->db->where("patient_id",$patient_id);
		$this->db->where('encounter_status !=',5);	
				
		return $this->db->get()->row()->encounter_id;
	}
	
	function update_encounter($encounter_data, $encounter_id=null)
	{
		if (!$encounter_id){
			 $this->db->insert('encounter',$encounter_data);
			 $encounter_id = $this->db->insert_id();
		}
		else{
			$this->db->where('encounter_id',$encounter_id);	
			$this->db->update('encounter',$encounter_data);
		}

		//$this->db->set('encounter_status', $status);		
		//$this->db->where('encounter_id',$encounter_id);	
		//$this->db->update('encounter_queue');

		return $encounter_id;
	}

	function delete_encounter($encounter_id)
	{
		$this->db->trans_start();

		$this->db->where('encounter_id',$encounter_id);
		$this->db->delete('encounter');

		$this->db->trans_complete();	
		return $this->db->trans_status();
	}

	function get_info($encounter_id)
	{
		$this->db->from('encounter');
		$this->db->where('encounter_id',$encounter_id);

		return $this->db->get()->row();
	}

	function last_visit($patient_id)
	{
		$this->db->from('encounter');
		$this->db->where('patient_id',$patient_id);
		$this->db->where('encounter_status',5);
		$this->db->order_by('encounter_id','desc');
		return $this->db->get()->row();
	}

	function get_all($encounter_status, $encounter_type)
	{
		$this->db->from('encounter');
		$this->db->where('encounter_status', $encounter_status);
		$this->db->where('encounter_type', $encounter_type);
		$this->db->order_by('priority','desc');
		$this->db->order_by('encounter_end','asc');

		return $this->db->get();
	}

	function workload($start_date,$end_date,$employee_id=null)
	{
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->select('count(encounter_id) as total');
		$this->db->from('encounter');
		if($employee_id) $this->db->where('employee_id',$employee_id);
		$this->db->where('encounter_start BETWEEN "' .$start_date. '" AND "' .$end_date. '"');

		$total = $this->db->get()->row()->total;
		$this->db->cache_off();
		return $total;
	}

	function clinic_workload($start_date,$end_date,$clinic_code,$gender=null,$ages=null)
	{
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->select('count(encounter_id) as total');
		$this->db->from('encounter');
		$this->db->where('encounter_type',$clinic_code);
		$this->db->where('encounter_start BETWEEN "' .$start_date. '" AND "' .$end_date. '"');
		if($gender) $this->db->where('patient_id IN (SELECT patient_id FROM '.$this->db->dbprefix('patients').' WHERE gender = "'.$gender.'")');
		if($ages) $this->db->where('patient_id IN (SELECT patient_id FROM '.$this->db->dbprefix('patients').' 
			WHERE DATEDIFF(DATE(encounter_start),STR_TO_DATE(dob,"%d-%m-%Y")) BETWEEN '.($ages['start'] * 365). ' AND '.($ages['end'] * 365 + 364).')');

		$total = $this->db->get()->row()->total;
		$this->db->cache_off();
		return $total;
	}

	function visits($start_date,$end_date,$review = 0,$clinic_code=null,$gender=null,$ages=null)
	{
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->select('count(encounter_id) as total');
		$this->db->from('encounter');
		if($clinic_code) $this->db->where('encounter_type',$clinic_code);
		$this->db->where('encounter_start BETWEEN "' .$start_date. '" AND "' .$end_date. '"');
		$this->db->where('review',$review);
		if($gender) $this->db->where('patient_id IN (SELECT patient_id FROM '.$this->db->dbprefix('patients').' WHERE gender = "'.$gender.'")');
		if($ages) $this->db->where('patient_id IN (SELECT patient_id FROM '.$this->db->dbprefix('patients').' 
			WHERE DATEDIFF(DATE(encounter_start),STR_TO_DATE(dob,"%d-%m-%Y")) BETWEEN '.($ages['start'] * 365). ' AND '.($ages['end'] * 365 + 364).')');

		$total = $this->db->get()->row()->total;
		$this->db->cache_off();
		return $total;
	}

	function get_encounters($start_date, $end_date, $clinic = null)
	{
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->from('encounter');
		if($clinic) $this->db->where('encounter_type',$clinic);
		$this->db->where('encounter_start BETWEEN "' .$start_date. '" AND "' .$end_date. '"');
		$this->db->order_by('encounter_start','desc');

		$encounters = $this->db->get();
		$this->db->cache_off();
		return $encounters;
	}
}
?>