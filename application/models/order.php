<?php
class Order extends CI_Model
{
	function get_info($order_id)
	{
		$this->db->from('orders');
		$this->db->where('order_id',$order_id);

		$order = $this->db->get()->row();

		$this->db->from('orders_items');
		$this->db->where('order_id',$order_id);

		$order->order_items = $this->db->get()->result();

		return $order;
	}

	function already_ordered($employee_id)
	{
		$this->db->select('order_id');
		$this->db->from('orders');
		$this->db->where('employee_id',$employee_id);
		$this->db->where('order_status',0);
		
		$order = $this->db->get()->row();
		return $order ? $order->order_id : false;
	}

	function save($orders_data,$orders_items_data,$order_id = null)
	{
		$this->db->trans_start();

		if($order_id):
			$this->db->where('order_id',$order_id);
			$this->db->update('orders',$orders_data);
			
			$this->db->where('order_id', $order_id);
			$this->db->delete('orders_items');
		else:
			$this->db->insert('orders',$orders_data);
			$order_id = $this->db->insert_id();
		endif;

		foreach($orders_items_data as $item_data):
			$item_data['order_id'] = $order_id;
			$this->db->insert('orders_items',$item_data);
		endforeach;

		$this->db->trans_complete();
		
		return $this->db->trans_status() ? $order_id : false;
	}

	function delete_order($order_id)
	{
		$this->db->trans_start();

		$this->db->where('order_id',$order_id);
		$this->db->where('order_status',0);
		$this->db->delete('orders');
		$affected_rows = $this->db->affected_rows();
		if($affected_rows == 1):
			$this->db->where('order_id',$order_id);
			$this->db->delete('orders_items');
		endif;

		$this->db->trans_complete();
		
		return $this->db->trans_status() ? $affected_rows : false;
	}

	function get_processed_orders($employee_id = null)
	{
		$this->db->from('orders');
		$this->db->where('order_status !=', 0);
		if($employee_id) $this->db->where('employee_id', $employee_id);
		$this->db->order_by('order_id','desc');
		//$this->db->limit(30);
		return $this->db->get();
	}

	function get_pending_orders($employee_id = null)
	{
		$this->db->from('orders');
		$this->db->where('order_status', 0);
		if($employee_id) $this->db->where('employee_id', $employee_id);
		$this->db->order_by('order_id','desc');
		//$this->db->limit(30);
		return $this->db->get();
	}
}
?>