<?php
class Patient extends CI_Model
{
	function get_info($patient_id)
	{
		$this->db->from('patients');
		$this->db->where('patient_id',$patient_id);

		return $this->db->get()->row();
	}

	function exists($patient_id)
	{
		$this->db->select('patient_id');
		$this->db->from('patients');
		$this->db->where('patient_id',$patient_id);
		$this->db->where('deleted',0);
		$query = $this->db->get();
		
		return ($query->num_rows()==1);
	}

	function save($patient_data, $patient_id = null)
	{
		$this->db->trans_start();

		if($patient_id && $this->exists($patient_id)):
			$this->db->where('patient_id',$patient_id);
			$this->db->update('patients',$patient_data);
		else:
			$this->db->insert('patients',$patient_data);
			$patient_id = $this->db->insert_id();
		endif;
		
		$this->db->trans_complete();	
		return $this->db->trans_status() ? $patient_id : false;	
	}

	function delete($patient_id)
	{
		$this->db->trans_start();
		
		$this->db->where('patient_id',$patient_id);
		$this->db->set('deleted',1);
		$this->db->update('patients');

		$this->db->trans_complete();	
		return $this->db->trans_status();
	}

	function get_all($limit = 10, $offset = 0, $search = null)
	{
		if($search):
			$institutions = array();
			$this->db->select('institution_id');
			$this->db->from('institutions');
			$this->db->like('institution_name',$search);
			foreach($this->db->get()->result() as $institution):
				$institutions[] = $institution->institution_id;
			endforeach;
		endif;
		$this->db->from('patients');
		$this->db->where('deleted',0);
		$this->db->order_by('patient_id','desc');
		$this->db->limit($limit,$offset);
		if($search):
			$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			last_name LIKE '%".$this->db->escape_like_str($search)."%' or middle_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%' or 
			patient_id LIKE '%".$this->db->escape_like_str($search)."%' or 
			LPAD(patient_id, 5,'0') LIKE '%".$this->db->escape_like_str($search)."%' or
			national_id LIKE '%".$this->db->escape_like_str($search)."%')");
			if(!empty($institutions)) $this->db->or_where_in('institution_id',$institutions);
		endif;
		return $this->db->get();
	}

	function count_all()
	{
		$this->db->select('count(patient_id) as total');
		$this->db->from('patients');
		$this->db->where('deleted',0);
		return $this->db->get()->row()->total;
	}

	function get_patient_search_suggestions($search,$limit=10)
	{
		$suggestions = array();
		
		$this->db->from('patients');
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or middle_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%')");
		$this->db->order_by("last_name", "asc");		
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[] = $row->patient_id.'|'. $this->patient_name($row->first_name,$row->middle_name,$row->last_name);		
		}
		
		$this->db->from('patients');
		$this->db->like("patient_id",str_ireplace(PATIENT_PREFIX,"",$search));
		$this->db->or_like("LPAD(patient_id, 5,'0')",str_ireplace(PATIENT_PREFIX,"",$search));
		$this->db->order_by("patient_id", "asc");		
		$by_patient_id = $this->db->get();
		foreach($by_patient_id->result() as $row)
		{
			$suggestions[] = $row->patient_id.'|'.$this->patient_number($row->patient_id);		
		}
		
		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}

	function patient_name($first_name, $middle_name, $last_name)
	{
		return ucwords(strtolower("$first_name $middle_name $last_name"));
	}

	function patient_age($dob)
	{
		$dob = date_create(date("Y-m-d",strtotime($dob)));
		$today = date_create(date("Y-m-d"));
		$age = date_diff($today,$dob)->format('%yy');
		if(intval($age) < 2):
			$age = (intval($age) * 12 + date_diff($today,$dob)->format('%m')) . "m";
			if(intval($age) < 2):
				$age = intval(date_diff($today,$dob)->format('%a') / 7) . "w";
				if(intval($age) < 2):
					$age = date_diff($today,$dob)->format('%dd');
				endif;
			endif;
		endif;
		return $age;
	}

	function patient_number($patient_id)
	{
		return PATIENT_PREFIX.str_pad($patient_id, 5, "0", STR_PAD_LEFT);
	}

	function get_contacts()
	{
		$this->db->from('patients');
		$this->db->where('deleted',0);
		$this->db->where('((phone_number IS NOT NULL AND phone_number != "") OR (email IS NOT NULL AND email != ""))');
		$this->db->order_by('patient_id','asc');
		
		return $this->db->get();
	}
}
?>
