<?php
class Item extends CI_Model
{
	function exists($item_id)
	{
		$this->db->select('item_id');
		$this->db->from('items');
		$this->db->where('item_id',$item_id);
		$this->db->where('deleted',0);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	function get_info($item_id)
	{
		$this->db->from('items');
		$this->db->where('item_id',$item_id);
		$this->db->or_where('item_number',$item_id);
		
		return $this->db->get()->row();
	}

	function get_item_id($item_number)
	{
		$this->db->select('item_id');
		$this->db->from('items');
		$this->db->where('item_number',$item_number);

		return $this->db->get()->row()->item_id;
	}

	function save($item_data,$item_id=false)
	{
		if ( $item_id && !$this->exists($item_id) )
			return false;
		$this->db->trans_start();

		if($item_id):
			$this->db->where('item_id',$item_id);
			$this->db->update('items',$item_data);
		else:
			$this->db->insert('items',$item_data);
			$item_id = $this->db->insert_id();
		endif;
		
		$this->db->trans_complete();	
		return $this->db->trans_status() ? $item_id : false;
	}

	function delete($item_id)
	{
		$this->db->trans_start();
		
		$this->db->where('item_id',$item_id);
		$this->db->set('deleted',1);
		$this->db->set('item_number',null);
		$this->db->update('items');

		$this->db->trans_complete();	
		return $this->db->trans_status();
	}

	function get_item_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->like('name', $search);
		$this->db->order_by("name", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->item_id.'|'.$row->name;
		}

		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}

	function get_category_suggestions($search)
	{
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('category');
		$this->db->from('items');
		$this->db->like('category', $search);
		$this->db->where('deleted', 0);
		$this->db->order_by("category", "asc");
		$by_category = $this->db->get();
		foreach($by_category->result() as $row)
		{
			$suggestions[]=$row->category;
		}

		return $suggestions;
	}

	function get_subcategory_suggestions($search)
	{
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('sub_category');
		$this->db->from('items');
		$this->db->like('sub_category', $search);
		$this->db->where('deleted', 0);
		$this->db->order_by("sub_category", "asc");
		$by_subcategory = $this->db->get();
		foreach($by_subcategory->result() as $row)
		{
			$suggestions[]=$row->sub_category;
		}

		return $suggestions;
	}

	function get_location_suggestions($search)
	{
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('location');
		$this->db->from('items');
		$this->db->like('location', $search);
		$this->db->where('deleted', 0);
		$this->db->order_by("location", "asc");
		$by_category = $this->db->get();
		foreach($by_category->result() as $row)
		{
			$suggestions[]=$row->location;
		}
	
		return $suggestions;
	}

	function get_all($limit = null, $offset = 0, $search = null)
	{
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->order_by('item_id','desc');
		if($limit) $this->db->limit($limit,$offset);
		if($search):
			$this->db->where("(name LIKE '%".$this->db->escape_like_str($search)."%' or 
			category LIKE '%".$this->db->escape_like_str($search)."%' or sub_category LIKE '%".$this->db->escape_like_str($search)."%' or 
			item_number LIKE '%".$this->db->escape_like_str($search)."%')");
		endif;
		return $this->db->get();
	}

	function count_all()
	{
		$this->db->select('count(item_id) as total');
		$this->db->from('items');
		$this->db->where('deleted',0);
		return $this->db->get()->row()->total;
	}

	function get_categories()
	{
		$this->db->select('category');
		$this->db->from('items');
		#$this->db->where('deleted',0);
		$this->db->distinct();
		$this->db->order_by("category", "asc");

		return $this->db->get();
	}

	function get_subcategories($category)
	{
		$this->db->select('sub_category');
		$this->db->from('items');
		#$this->db->where('deleted',0);
		$this->db->where('category',$category);
		$this->db->distinct();
		$this->db->order_by("sub_category", "asc");

		return $this->db->get();
	}

	function get_subcategory_items($category,$subcategory)
	{
		$this->db->select('item_id');
		$this->db->from('items');
		#$this->db->where('deleted',0);
		$this->db->where('category',$category);
		$this->db->where('sub_category',$subcategory);
		$this->db->distinct();

		return $this->db->get();
	}

	function get_categories_subcategories($breakdown = array('Medical Services'))
	{
		$categories = array();
		$this->db->distinct();
		$this->db->select('category');
		$this->db->where_not_in('category',$breakdown);
		$this->db->from('items');
		#$this->db->where('deleted',0);

		foreach($this->db->get()->Result() as $item):
			$categories[] = $item->category;
		endforeach;

		$this->db->distinct();
		$this->db->select('sub_category');
		$this->db->where_in('category',$breakdown);
		$this->db->from('items');

		foreach($this->db->get()->Result() as $item):
			$categories[] = $item->sub_category;
		endforeach;

		return $categories;
	}

	function item_number_exists($item_number, $item_id = null)
	{
		$this->db->select('item_id');
		$this->db->from('items');
		$this->db->where('item_number',$item_number);
		$this->db->where('deleted',0);
		if($item_id) $this->db->where('item_id !=',$item_id);
		return $this->db->get()->num_rows() == 1;
	}
}
?>
