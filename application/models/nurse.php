<?php
class Nurse extends CI_Model
{
	function get_main_queue()
	{
		$this->db->from('invoices');
		$this->db->where('invoice_status', 1);
		$this->db->where('invoice_type', 'Nursing');
		$this->db->order_by('priority','desc');
		//$this->db->limit(30);
		return $this->db->get();
	}

	function get_pending_queue()
	{
		$this->db->from('invoices');
		$this->db->where('invoice_status', 2);
		$this->db->where('invoice_type', 'Nursing');
		$this->db->order_by('priority','desc');
		//$this->db->limit(30);
		return $this->db->get();
	}

	function archive_invoices($start_date,$end_date)
	{
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->from('invoices');
		$this->db->where_in('invoice_status', array(2,3));
		$this->db->where('invoice_type', 'Nursing');
		$this->db->having('service_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');
		$this->db->order_by('service_time','desc');
		//$this->db->limit(30);
		$result = $this->db->get();
		$this->db->cache_off();
		return $result;
	}

	function workload($start_date,$end_date,$employee_id=null,$gender=null,$ages=null)
	{
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->select('count(invoice_id) as total');
		$this->db->from('invoices');
		$this->db->where('invoice_type','Nursing');
		$this->db->where_in('invoice_status',array(2,3));
		$this->db->where('service_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');
		if($employee_id) $this->db->where('served_by',$employee_id);
		if($gender) $this->db->where('patient_id IN (SELECT patient_id FROM '.$this->db->dbprefix('patients').' WHERE gender = "'.$gender.'")');
		if($ages) $this->db->where('patient_id IN (SELECT patient_id FROM '.$this->db->dbprefix('patients').' 
			WHERE DATEDIFF(DATE(service_time),STR_TO_DATE(dob,"%d-%m-%Y")) BETWEEN '.($ages['start'] * 365). ' AND '.($ages['end'] * 365 + 364).')');

		$total = $this->db->get()->row()->total;
		$this->db->cache_off();
		return $total;
	}

	function get_procedure_info($procedure_id)
	{
		$this->db->from('procedures');
		$this->db->where('procedure_id',$procedure_id);
		$procedure = $this->db->get()->row();

		$this->db->from('items');
		$this->db->where('item_number','PROC'.$procedure_id);
		$procedure->item_info = $this->db->get()->row();

		return $procedure;
	}

	function get_procedure_id($item_id)
	{
		return str_ireplace('PROC', '', $this->Item->get_info($item_id)->item_number);
	}

	function procedure_suggestions($search,$limit=25)
	{
		$suggestions = array();
		
		$this->db->select('procedure_id,name');
		$this->db->from('procedures');
		$this->db->where('clinic','nursing');
		$this->db->like('name',$search);		
		
		foreach($this->db->get()->result() as $row)
		{
			$suggestions[]=$row->procedure_id.'|'.$row->name;		
		}		
		
		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}

	function get_procedures()
	{
		$this->db->from('procedures');
		$this->db->where('clinic','nursing');
		return $this->db->get();
	}

	function save_procedure($procedure_data,$item_data,$procedure_id=null)
	{
		if ( $procedure_id && !$this->procedure_exists($procedure_id) )
			return false;
		$this->db->trans_start();

		if($procedure_id):
			$this->db->where('procedure_id',$procedure_id);
			$this->db->update('procedures',$procedure_data);
			
			$this->db->where('item_number', 'PROC'.$procedure_id);
			$this->db->update('items',$item_data);
		else:
			$this->db->insert('procedures',$procedure_data);
			$procedure_id = $this->db->insert_id();
			$item_data['item_number'] = 'PROC'.$procedure_id;
			$this->db->insert('items',$item_data);
		endif;

		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}

	function procedure_exists($procedure_id)
	{
		$this->db->select('procedure_id');
		$this->db->from('procedures');
		$this->db->where('procedure_id',$procedure_id);
		$query = $this->db->get();
		
		return ($query->num_rows()==1);
	}

	function delete_procedure($procedure_id)
	{
		$this->db->trans_start();

		$this->db->where('procedure_id',$procedure_id);
		$this->db->delete('procedures');

		$this->db->where('item_number','PROC'.$procedure_id);
		$this->db->set('deleted',1);
		$this->db->update('items');

		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}

	function procedures_workload($start_date,$end_date)
	{
		$procedures = array();
		
		$this->db->select('procedure_id');
		$this->db->from('procedures');
		$this->db->where('clinic','nursing');
		$result = $this->db->get()->result();
		foreach($result as $procedure):
			$this->db->select('item_id');
			$this->db->from('items');
			$this->db->where('item_number','PROC'.$procedure->procedure_id);
			$item = $this->db->get()->row();
			$procedures[$item->item_id] = 0;
		endforeach;
		

		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$invoice_ids = array();
		$this->db->where('invoice_type','Nursing');
		$this->db->where_in('invoice_status',array(2,3));
		$this->db->where('service_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');
		$this->db->select('invoice_id');
		$this->db->from('invoices');

		$invoices = $this->db->get();
		if($invoices->num_rows() > 0):
			foreach($invoices->result() as $invoice):
				$invoice_ids[] = $invoice->invoice_id;
			endforeach;
		endif;

		if(!empty($invoice_ids)):
			foreach(array_keys($procedures) as $item_id):
				$this->db->select('sum(quantity) as total');
				$this->db->from('invoices_items');
				$this->db->where('item_id',$item_id);
				$this->db->where('status',1);
				$this->db->where_in('invoice_id',$invoice_ids);
				$procedures[$item_id] = $this->db->get()->row()->total;
			endforeach;
		endif;
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_off();
		return $procedures;
	}
}
?>