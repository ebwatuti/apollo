<?php
class Triaging extends CI_Model
{
	function get_info($encounter_id)
	{
		$this->db->from('triage');
		$this->db->where('encounter_id',$encounter_id);
		$this->db->order_by('triage_id','asc');

		return $this->db->get();
	}

	function get_triage_info($triage_id)
	{
		$this->db->from('triage');
		$this->db->where('triage_id',$triage_id);

		return $this->db->get()->row();
	}

	function save($triage_data, $patient_id)
	{
		if ( !$this->Patient->exists($patient_id) )
			return false;
		$encounter_id = $this->Encounter->check_encounter($patient_id);
		if ( !$encounter_id )
			return false;

		$this->db->trans_start();

		$encounter_data = array(
			'encounter_end'=>date('Y-m-d H:i:s'),
			'encounter_status'=>2,
			);
		$encounter_id = $this->Encounter->update_encounter($encounter_data, $encounter_id);
		
		
		$this->db->insert('triage',$triage_data);
		
		$this->db->set('priority',  $triage_data['priority']);
		$this->db->where('encounter_id', $encounter_id);
		$this->db->update('encounter');
		
		
		$this->db->trans_complete();	
		return $this->db->trans_status();	
	}

	function get_main_queue()
	{
		$this->db->from('encounter');
		$this->db->where('encounter_status', 1);
		$this->db->order_by("priority", "desc");
		$this->db->order_by("encounter_id", "asc");
		//$this->db->limit(30);
		return $this->db->get();
	}

	function get_completed_queue()
	{
		$encounters = array(0);
		$this->db->distinct('encounter_id');
		$this->db->from('triage');
		$triages = $this->db->get();
		if($triages->num_rows() > 0):
			foreach($triages->result() as $encounter):
				$encounters[] = $encounter->encounter_id;
			endforeach;
		endif;

		$this->db->from('encounter');
		$this->db->where_not_in('encounter_status', array(1,5));
		$this->db->where_in('encounter_id',$encounters);
		$this->db->order_by("priority", "desc");
		$this->db->order_by("encounter_id", "asc");
		return $this->db->get();
	}

	function archive_triages($start_date,$end_date)
	{
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->select('triage_id,patient_id,employee_id,triage_time');
		$this->db->from('triage');
		$this->db->having('triage_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');
		//$this->db->order_by('triage_time','desc');
		//$this->db->limit(30);
		$result = $this->db->get();
		$this->db->cache_off();
		return $result;
	}

	function workload($start_date,$end_date,$where=null)
	{
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->select('count(triage_id) as total');
		$this->db->from('triage');
		$this->db->where('triage_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');
		if($where) $this->db->where($where);

		$total = $this->db->get()->row()->total;
		$this->db->cache_off();
		return $total;
	}
}
?>