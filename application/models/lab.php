<?php
class Lab extends CI_Model
{
	function get_lab_test_info($lab_test_id)
	{
		$this->db->from('laboratory_tests');
		$this->db->where('lab_test_id',$lab_test_id);
		$lab_test = $this->db->get()->row();

		$this->db->from('items');
		$this->db->where('item_number','LAB'.$lab_test_id);
		$lab_test->item_info = $this->db->get()->row();

		return $lab_test;
	}

	function get_lab_test_id($item_id)
	{
		return str_ireplace('LAB', '', $this->Item->get_info($item_id)->item_number);
	}

	function lab_test_suggestions($search,$limit=25)
	{
		$suggestions = array();
		
		$this->db->select('lab_test_id,value');
		$this->db->from('laboratory_tests');
		$this->db->like('value',$search);		
		
		foreach($this->db->get()->result() as $row)
		{
			$suggestions[]=$row->lab_test_id.'|'.$row->value;		
		}		
		
		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}

	function save_lab_tests($invoice_data,$invoice_items_data,$invoice_id=null)
	{
		return $this->Invoice->save($invoice_data,$invoice_items_data,$invoice_id);
	}

	function save_lab_report($invoices_data,$invoices_items_data)
	{
		$this->db->trans_start();

		foreach($invoices_data as $invoice_id=>$invoice_data):
			$this->db->where('invoice_id',$invoice_id);
			$this->db->update('invoices',$invoice_data);
		endforeach;

		foreach($invoices_items_data as $item_data):
			$this->db->where('item_id',$item_data['item_id']);
			$this->db->where('invoice_id',$item_data['invoice_id']);
			$this->db->where('line',$item_data['line']);
			$this->db->update('invoices_items',$item_data);
		endforeach;

		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}

	function get_consultation_lab_tests($consultation_id)
	{
		$this->db->select('invoice_id,invoice_status');
		$this->db->from('invoices');
		$this->db->where('consultation_id',$consultation_id);
		$this->db->where('invoice_type','Lab');
		$invoices = $this->db->get();

		$invoices_items = array();

		if($invoices->num_rows() > 0):
			foreach($invoices->result() as $invoice):
				$this->db->from('invoices_items');
				$this->db->where('invoice_id',$invoice->invoice_id);
				foreach($this->db->get()->result() as $item):
					$item->invoice_status = $invoice->invoice_status;
					$invoices_items[] = $item;
				endforeach;
			endforeach;
		endif;

		return $invoices_items;
	}

	function get_invoice_lab_tests($patient_id)
	{
		$patient_liability = 100;
		$institution = $patient_id ? $this->Institution->get_patient_institution($patient_id) : '';
		if($institution):
			$patient_liability = 100 - ($institution->institution_discount + $institution->institution_liability);
		endif;
		$this->db->select('invoice_id,invoice_status');
		$this->db->from('invoices');
		$this->db->where('patient_id',$patient_id);
		if($patient_liability == 0) $this->db->where('invoice_status',1);
		else $this->db->where('invoice_status',0);
		$this->db->where('invoice_type','Lab');
		$invoices = $this->db->get();

		$invoices_items = array();

		if($invoices->num_rows() > 0):
			foreach($invoices->result() as $invoice):
				$this->db->from('invoices_items');
				$this->db->where('invoice_id',$invoice->invoice_id);
				foreach($this->db->get()->result() as $item):
					$item->invoice_status = $invoice->invoice_status;
					$invoices_items[] = $item;
				endforeach;
			endforeach;
		endif;

		return $invoices_items;
	}

	function get_lab_tests($patient_id)
	{
		$this->db->select('invoice_id,invoice_status');
		$this->db->from('invoices');
		$this->db->where('patient_id',$patient_id);
		$this->db->where_in('invoice_status',array(1,2));
		$this->db->where('invoice_type','Lab');
		$invoices = $this->db->get();

		$invoices_items = array();

		if($invoices->num_rows() > 0):
			foreach($invoices->result() as $invoice):
				$this->db->from('invoices_items');
				$this->db->where('invoice_id',$invoice->invoice_id);
				foreach($this->db->get()->result() as $item):
					$item->invoice_status = $invoice->invoice_status;
					$invoices_items[] = $item;
				endforeach;
			endforeach;
		endif;

		return $invoices_items;
	}

	function get_consultation_notes($patient_id)
	{

		$this->db->select('consultation_id');
		$this->db->from('invoices');
		$this->db->where('patient_id',$patient_id);
		$this->db->where('invoice_type','Lab');
		$invoices = $this->db->get();

		$consultation_ids = array('0');

		if($invoices->num_rows() > 0):
			foreach($invoices->result() as $invoice):
				$consultation_ids[] = $invoice->consultation_id;
			endforeach;
		endif;

		$this->db->select('consultation_id,consultation_start,consultant_id');
		$this->db->from('consultation');	
		$this->db->where_in('consultation_id',$consultation_ids);
		$this->db->order_by("consultation_start", "desc");

		return $this->db->get();
	}

	function get_main_queue()
	{
		$this->db->from('invoices');
		$this->db->where('invoice_status', 1);
		$this->db->where('invoice_type', 'Lab');
		$this->db->order_by('priority','desc');
		//$this->db->limit(30);
		return $this->db->get();
	}

	function get_pending_queue()
	{
		$this->db->from('invoices');
		$this->db->where('invoice_status', 2);
		$this->db->where('invoice_type', 'Lab');
		$this->db->order_by('priority','desc');
		//$this->db->limit(30);
		return $this->db->get();
	}

	function lab_test_options($lab_test_id)
	{
		$this->db->select('options');
		$this->db->from('laboratory_tests');
		$this->db->where('lab_test_id',$lab_test_id);

		$options = $this->db->get()->row()->options;
		$options_array = array();
		if($options):
			foreach(explode(',', $options) as $option):
				$options_array[$option] = '';
			endforeach;
			return $options_array;
		else:
			return array('');
		endif;
	}

	function lab_reports($start_date,$end_date)
	{
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->from('invoices');
		$this->db->where_in('invoice_status', array(2,3));
		$this->db->where('invoice_type', 'Lab');
		$this->db->having('service_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');
		$this->db->order_by('service_time','desc');
		//$this->db->limit(30);
		$result = $this->db->get();
		$this->db->cache_off();
		return $result;
	}

	function get_lab_report($invoice_id)
	{
		$this->db->from('invoices');
		$this->db->where('invoice_id',$invoice_id);

		$invoice = $this->db->get()->row();

		$this->db->from('invoices_items');
		$this->db->where('invoice_id',$invoice_id);

		$invoice->invoice_items = $this->db->get()->result();

		return $invoice;
	}

	function lab_tests()
	{
		$this->db->from('laboratory_tests');

		return $this->db->get();
	}

	function save_lab_test($lab_test_data,$item_data,$lab_test_id=null)
	{
		$this->db->trans_start();

		if($lab_test_id):
			$this->db->where('lab_test_id',$lab_test_id);
			$this->db->update('laboratory_tests',$lab_test_data);
			
			$this->db->where('item_number', 'LAB'.$lab_test_id);
			$this->db->update('items',$item_data);
		else:
			$this->db->insert('laboratory_tests',$lab_test_data);
			$lab_item_id = $this->db->insert_id();
			$item_data['item_number'] = 'LAB'.$lab_item_id;
			$this->db->insert('items',$item_data);
		endif;

		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}

	function get_category_suggestions($search)
	{
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('category');
		$this->db->from('laboratory_tests');
		$this->db->like('category', $search);
		$this->db->order_by("category", "asc");
		$by_category = $this->db->get();
		foreach($by_category->result() as $row)
		{
			$suggestions[]=$row->category;
		}

		return $suggestions;
	}

	function delete_lab_test($lab_test_id)
	{
		$this->db->trans_start();

		$this->db->where('lab_test_id',$lab_test_id);
		$this->db->delete('laboratory_tests');

		$this->db->where('item_number','LAB'.$lab_test_id);
		$this->db->set('item_number',NULL);
		$this->db->set('deleted',1);
		$this->db->update('items');

		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}

	function workload($start_date,$end_date,$employee_id=null,$gender=null,$ages=null)
	{
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$this->db->select('count(invoice_id) as total');
		$this->db->from('invoices');
		$this->db->where('invoice_type','Lab');
		$this->db->where_in('invoice_status',array(2,3));
		$this->db->where('service_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');
		if($employee_id) $this->db->where('served_by',$employee_id);
		if($gender) $this->db->where('patient_id IN (SELECT patient_id FROM '.$this->db->dbprefix('patients').' WHERE gender = "'.$gender.'")');
		if($ages) $this->db->where('patient_id IN (SELECT patient_id FROM '.$this->db->dbprefix('patients').' 
			WHERE DATEDIFF(DATE(service_time),STR_TO_DATE(dob,"%d-%m-%Y")) BETWEEN '.($ages['start'] * 365). ' AND '.($ages['end'] * 365 + 364).')');

		$total = $this->db->get()->row()->total;
		$this->db->cache_off();
		return $total;
	}

	function lab_tests_workload($start_date,$end_date)
	{
		$lab_tests = array();
		
		$this->db->select('item_id');
		$this->db->from('items');
		$this->db->where('category','Lab');
		$this->db->where('deleted',0);
		foreach($this->db->get()->result() as $item):
			$lab_tests[$item->item_id] = 0;
		endforeach;
		

		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_on();

		$invoice_ids = array();
		$this->db->select('invoice_id');
		$this->db->from('invoices');
		$this->db->where('invoice_type','Lab');
		$this->db->where_in('invoice_status',array(2,3));
		$this->db->where('service_time BETWEEN "' .$start_date. '" AND "' .$end_date. '"');

		$invoices = $this->db->get();
		if($invoices->num_rows() > 0):
			foreach($invoices->result() as $invoice):
				$invoice_ids[] = $invoice->invoice_id;
			endforeach;
		endif;

		if(!empty($invoice_ids)):
			foreach(array_keys($lab_tests) as $item_id):
				$this->db->select('count(invoice_id) as total');
				$this->db->from('invoices_items');
				$this->db->where('item_id',$item_id);
				$this->db->where('status',1);
				$this->db->where_in('invoice_id',$invoice_ids);
				$lab_tests[$item_id] = $this->db->get()->row()->total;
			endforeach;
		endif;
		if ($start_date < date('Y-m-d') && $end_date < date('Y-m-d')) $this->db->cache_off();
		return $lab_tests;
	}
}
?>