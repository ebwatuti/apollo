<?php
class Invoice extends CI_Model
{
	function get_info($invoice_id)
	{
		$this->db->from('invoices');
		$this->db->where('invoice_id',$invoice_id);

		$invoice = $this->db->get()->row();

		$this->db->from('invoices_items');
		$this->db->where('invoice_id',$invoice_id);

		$invoice->invoice_items = $this->db->get()->result();

		return $invoice;
	}

	function already_invoiced($patient_id,$employee_id,$invoice_type="Normal")
	{
		$patient_liability = 100;
		$institution = $this->Institution->get_patient_institution($patient_id);
		if($institution) $patient_liability = 100 - ($institution->institution_discount + $institution->institution_liability);
		
		$this->db->select('invoice_id');
		$this->db->from('invoices');
		$this->db->where('patient_id',$patient_id);
		$this->db->where('invoice_type',$invoice_type);		
		if($patient_liability == 0) $this->db->where('invoice_status',1);
		else $this->db->where('invoice_status',0);
		
		$invoice = $this->db->get()->row();
		return $invoice ? $invoice->invoice_id : false;
	}

	function save($invoices_data,$invoices_items_data,$invoice_id = null)
	{
		$this->db->trans_start();

		$patient_id = $invoices_data['patient_id'];
		$encounter_id = $this->Encounter->check_encounter($patient_id);
		if($encounter_id && !isset($invoices_data['encounter_id'])) $invoices_data['encounter_id'] = $encounter_id;
		$patient_liability = 100;
		$institution = $this->Institution->get_patient_institution($patient_id);
		if($institution):
			$invoices_data['institution_id'] = $institution->institution_id;
			$patient_liability = 100 - ($institution->institution_discount + $institution->institution_liability);			
			if($patient_liability == 0):
				$invoices_data['invoice_status'] = 1;
			endif;
		endif;

		if($invoice_id):
			$this->db->where('invoice_id',$invoice_id);
			$this->db->update('invoices',$invoices_data);
			
			$this->db->where('invoice_id', $invoice_id);
			$this->db->delete('invoices_items');
		else:
			$this->db->insert('invoices',$invoices_data);
			$invoice_id = $this->db->insert_id();
		endif;

		foreach($invoices_items_data as $item_data):
			$cur_item_info = $this->Item->get_info($item_data['item_id']);
			$item_data['invoice_id'] = $invoice_id;
			$item_data['discount'] = $item_data['discount'] ? $item_data['discount'] : $institution->institution_discount + $institution->institution_liability;
			$item_data['cost_price'] = $cur_item_info->cost_price;
			$item_data['unit_price'] = $item_data['unit_price'] ? $item_data['unit_price'] : $cur_item_info->unit_price;
			if($patient_liability == 0):
				$item_data['status'] = 1;
			endif;
			$this->db->insert('invoices_items',$item_data);
		endforeach;

		$this->db->trans_complete();
		
		return $this->db->trans_status() ? $invoice_id : false;
	}

	function delete_invoice($invoice_id)
	{
		$this->db->trans_start();

		$this->db->where('invoice_id',$invoice_id);
		$this->db->where('invoice_status',0);
		$this->db->delete('invoices');
		$affected_rows = $this->db->affected_rows();
		if($affected_rows == 1):
			$this->db->where('invoice_id',$invoice_id);
			$this->db->delete('invoices_items');
		endif;

		$this->db->trans_complete();
		
		return $this->db->trans_status() ? $affected_rows : false;
	}

	function save_sale_invoice($invoices_data,$invoices_items_data,$invoice_id)
	{
		$this->db->trans_start();

		$this->db->where('invoice_id',$invoice_id);
		$this->db->update('invoices',$invoices_data);

		foreach($invoices_items_data as $item_data):
			$this->db->where('item_id',$item_data['item_id']);
			$this->db->where('invoice_id',$invoice_id);
			$this->db->where('line',$item_data['line']);
			$this->db->update('invoices_items',$item_data);
		endforeach;

		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}

	function get_sale_invoices($patient_id)
	{
		$this->db->select('invoice_id,invoice_status');
		$this->db->from('invoices');
		$this->db->where('patient_id',$patient_id);
		$this->db->where('invoice_status',0);
		$invoices = $this->db->get()->result();

		$sale_invoices = array();
		foreach($invoices as $invoice):
			$this->db->from('invoices_items');
			$this->db->where('invoice_id',$invoice->invoice_id);
			$invoice->invoice_items = $this->db->get()->result();
			$sale_invoices[] = $invoice;
		endforeach;

		return $sale_invoices;
	}

	function get_pending_invoices()
	{
		$this->db->from('invoices');
		$this->db->where('invoice_status', 0);
		//$this->db->where('invoice_type', 'Normal');
		$this->db->order_by('invoice_id','desc');
		//$this->db->limit(30);
		return $this->db->get();
	}
}
?>