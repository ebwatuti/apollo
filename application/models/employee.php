<?php
class Employee extends CI_Model
{
	function exists($employee_id)
	{
		$this->db->from('employees');
		$this->db->where('person_id',$employee_id);
		$query = $this->db->get();
		
		return ($query->num_rows()==1);
	}	
	
	function get_all()
	{
		$this->db->from('employees');
		$this->db->where('deleted',0);			
		$this->db->order_by("last_name", "asc");
		return $this->db->get();		
	}
	
	function get_info($employee_id)
	{
		$this->db->from('employees');
		$this->db->where('person_id',$employee_id);
		return $this->db->get()->row();
	}

	function save($employee_data, $permission_data, $employee_id = null)
	{
		if ( $employee_id && !$this->exists($employee_id) )
			return false;
		$this->db->trans_start();

		if($employee_id):
			$this->db->where('person_id',$employee_id);
			$this->db->update('employees',$employee_data);
		else:
			$this->db->insert('employees',$employee_data);
			$employee_id = $this->db->insert_id();
		endif;
		$this->db->where('person_id',$employee_id);
		$this->db->delete('permissions');
		foreach($permission_data as $module_id):
			$this->db->set('module_id',$module_id);
			$this->db->set('person_id',$employee_id);
			$this->db->insert('permissions');
		endforeach;
		
		$this->db->trans_complete();	
		return $this->db->trans_status() ? $employee_id : false;	
	}

	function save_profile($employee_data, $employee_id)
	{
		if ( !$employee_id || !$this->exists($employee_id) )
			return false;
		$this->db->trans_start();

		$this->db->where('person_id',$employee_id);
		$this->db->update('employees',$employee_data);
		
		$this->db->trans_complete();	
		return $this->db->trans_status();	
	}

	function delete($employee_id)
	{
		if($employee_id == $this->get_logged_in_employee_info()->person_id)
			return false;
		
		$this->db->trans_start();

		$this->db->where('person_id',$employee_id);
		$this->db->delete('permissions');

		$this->db->set('deleted',1);
		$this->db->set('username',null);
		$this->db->set('employee_no',null);
		$this->db->set('password',null);
		$this->db->where('person_id',$employee_id);
		$this->db->update('employees');

		$this->db->trans_complete();		
		return $this->db->trans_status();
	}

	function login($username, $password)
	{
		$query = $this->db->get_where('employees', array('username' => $username,'password'=>md5($password), 'deleted'=>0), 1);
		if ($query->num_rows() ==1)
		{
			$row=$query->row();
			$this->session->set_userdata('person_id', $row->person_id);
			return true;
		}
		return false;
	}
	
	function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}
	
	function is_logged_in()
	{
		return $this->session->userdata('person_id')!=false;
	}
	
	function get_logged_in_employee_info()
	{
		if($this->is_logged_in())
			return $this->get_info($this->session->userdata('person_id'));
		
		return false;
	}
	
	function has_permission($module_id,$person_id)
	{
		//if no module_id is null, allow access
		if($module_id==null) return true;
		
		$query = $this->db->get_where('permissions', array('person_id' => $person_id,'module_id'=>$module_id), 1);
		return $query->num_rows() == 1;
		
		
		return false;
	}

	function username_exists($username, $employee_id = null)
	{
		$this->db->select('person_id');
		$this->db->from('employees');
		$this->db->where('username',$username);
		$this->db->where('deleted',0);
		if($employee_id) $this->db->where('person_id !=',$employee_id);
		return $this->db->get()->num_rows() == 1;
	}

	function employee_no_exists($employee_no, $employee_id = null)
	{
		$this->db->select('person_id');
		$this->db->from('employees');
		$this->db->where('employee_no',$employee_no);
		$this->db->where('deleted',0);
		if($employee_id) $this->db->where('person_id !=',$employee_id);
		return $this->db->get()->num_rows() == 1;
	}

	function get_activity($employee_id)
	{
		$this->db->where('employee_id',$employee_id);
		$this->db->where('online',1);
		$this->db->from('account_activity');
		return $this->db->get()->last_row();
	}

	function save_activity($activity_data,$activity_id=null)
	{
		$this->db->trans_start();

		if($activity_id):
			$this->db->where('activity_id',$activity_id);
			$this->db->update('account_activity',$activity_data);
		else:
			$this->db->insert('account_activity',$activity_data);
		endif;

		$this->db->trans_complete();		
		return $this->db->trans_status();
	}

	function active_users()
	{
		$this->db->select('activity_id,activity_end');
		$this->db->where('online',1);
		$this->db->from('account_activity');
		return $this->db->get();
	}
}
?>
