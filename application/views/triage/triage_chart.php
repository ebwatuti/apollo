<?php if($triage_history->num_rows() > 0 ): ?>
<?php 
    $x_axis = "";
    $series = array(
        'Temperature'=>'',
        'Weight'=>'',
        'Systolic BP'=>'',
        'Diastolic BP'=>'',
        'Pulse Rate'=>'',
        );
    foreach ($triage_history->result() as $triage):
        $x_axis .= "'" . date('g:i a',strtotime($triage->triage_time)) . "',";
        $triage_details = unserialize($triage->triage_details);
        $series['Temperature'] .= $triage_details['temperature'] . ",";
        $series['Weight'] .= $triage_details['weight'] . ",";
        $series['Systolic BP'] .= $triage_details['blood pressure']['systolic'] . ",";
        $series['Diastolic BP'] .= $triage_details['blood pressure']['diastolic'] . ",";
        $series['Pulse Rate'] .= $triage_details['pulse rate'] . ",";
    endforeach;
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#chart_container').highcharts({
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: 'Vital Signs'
            },
            xAxis: [{
                categories: [<?php echo $x_axis; ?>]
            }],
            yAxis: [{// Temperature Axis
                labels: {
                    formatter: function() {
                        return this.value +'°C';
                    },
                    style: {
                        color: '#2f7ed8',
                    }
                },
                title: {
                    text: 'Temperature',
                    style: {
                        color: '#2f7ed8',
                    }
                },
                opposite: true
    
            }, { // Weight yAxis
                gridLineWidth: 0,
                title: {
                    text: 'Weight',
                    style: {
                        color: '#8bbc21',
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' kg';
                    },
                    style: {
                        color: '#8bbc21',
                    }
                }
    
            }, { // BP yAxis
                gridLineWidth: 0,
                title: {
                    text: 'Blood Pressure',
                    style: {
                        color: '#c42525',
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mmHg';
                    },
                    style: {
                        color: '#c42525',
                    }
                },
                opposite: true
            }, { // Pulse Rate yAxis
                gridLineWidth: 0,
                title: {
                    text: 'Pulse Rate',
                    style: {
                        color: '#f28f43',
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' bpm';
                    },
                    style: {
                        color: '#f28f43',
                    }
                },
            }],
            tooltip: {
                shared: true
            },
            legend: {
                //layout: 'vertical',
                //align: 'left',
                //x: 120,
                //verticalAlign: 'top',
                //y: 80,
                //floating: true,
                //backgroundColor: '#FFFFFF'
            },
            series: [{
                name: 'Temperature',
                color: '#2f7ed8',
                type: 'spline',
                yAxis: 0,
                data: [<?php echo $series['Temperature']; ?>],
                tooltip: {
                    valueSuffix: ' °C'
                }
    
            }, {
                name: 'Weight',
                type: 'spline',
                color: '#8bbc21',
                yAxis: 1,
                data: [<?php echo $series['Weight']; ?>],
                tooltip: {
                    valueSuffix: ' kg'
                }
    
            }, {
                name: 'Systolic BP',
                color: '#c42525',
                type: 'spline',
                yAxis: 2,
                data: [<?php echo $series['Systolic BP']; ?>],
                tooltip: {
                    valueSuffix: ' mmHg'
                }
            }, {
                name: 'Diastolic BP',
                color: '#c42525',
                type: 'spline',
                yAxis: 2,
                data: [<?php echo $series['Diastolic BP']; ?>],
                tooltip: {
                    valueSuffix: ' mmHg'
                }
            }, {
                name: 'Pulse Rate',
                color: '#f28f43',
                type: 'spline',
                yAxis: 3,
                data: [<?php echo $series['Pulse Rate']; ?>],
                tooltip: {
                    valueSuffix: ' bpm'
                }
            }]
        });
    });
    

</script>

<div id='chart_container' style="min-width: 400px; margin: 0 auto">
    
</div>
<?php endif; ?>