<table width="100%" id="patients_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Patient #</th>
        <th style="background-color: #0a6184; color:#FFF">Patient Name</th>
        <th style="background-color: #0a6184; color:#FFF">Age</th>
        <th style="background-color: #0a6184; color:#FFF">Priority</th>
        <th style="background-color: #0a6184; color:#FFF">Comments</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>
    </tr></thead>
    <tbody>
<?php
if($main_queue->num_rows() > 0):
	foreach($main_queue->result() as $encounter):
        $patient = $this->Patient->get_info($encounter->patient_id);
        $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
        $patient->age = $this->Patient->patient_age($patient->dob);
        $patient->patient_no = $this->Patient->patient_number($patient->patient_id);
        $patient->last_encounter = $this->Encounter->last_visit($patient->patient_id);

        switch ($patient->priority) {
            case 1:
                $patient->priority = 'Normal';
                break;
            case 2:
                $patient->priority = 'Urgent';
                break;
            case 3:
                $patient->priority = 'Emergency';
                break;
            default:
                $patient->priority = 'Normal';
                break;
        }

        if($patient->last_encounter) $patient->last_visit = "Last visit: " . date("M j, Y",strtotime($patient->last_encounter->encounter_start));
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $patient->patient_no; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->patient_name; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $patient->age; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->priority; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $patient->last_visit; ?></td>
        <td style="background-color: #DDD">
			<?php echo anchor("triage/view_triage/$patient->patient_id/width:750",
            form_button(array('name'=>'submit','content'=>'View','class'=>'DTTT_button float_left')),
            array("class"=>"thickbox vote_thickbox","title"=>"Triage Details")); ?>
        </td>
        </tr>
<?php
	endforeach;
endif;
?>

<?php
if($completed_queue->num_rows() > 0):
    foreach($completed_queue->result() as $encounter):
        $patient = $this->Patient->get_info($encounter->patient_id);
        $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
        $patient->age = $this->Patient->patient_age($patient->dob);
        $patient->patient_no = $this->Patient->patient_number($patient->patient_id);
        $patient->last_encounter = $this->Encounter->last_visit($patient->patient_id);
        $patient->triage_time = date("g:i a",strtotime($this->Triaging->get_info($encounter->encounter_id)->last_row()->triage_time));

        switch ($patient->priority) {
            case 1:
                $patient->priority = 'Normal';
                break;
            case 2:
                $patient->priority = 'Urgent';
                break;
            case 3:
                $patient->priority = 'Emergency';
                break;
            default:
                $patient->priority = 'Normal';
                break;
        }
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $patient->patient_no; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->patient_name; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $patient->age; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->priority; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo "Last seen at: " . $patient->triage_time; ?></td>
        <td style="background-color: #DDD">
            <?php echo anchor("triage/view_triage/$patient->patient_id/width:750",
            form_button(array('name'=>'submit','content'=>'View','class'=>'DTTT_button float_left')),
            array("class"=>"thickbox vote_thickbox","title"=>"Triage Details")); ?>
        </td>
        </tr>
<?php
    endforeach;
endif;
?>
	</tbody>
</table>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	tb_init("a.vote_thickbox");
	$('#patients_table').dataTable({
		//"bPaginate": false,
		//"bLengthChange": false,
		//"bFilter": false,
		"bSort": false,
		//"bInfo": false,
        "bStateSave": true,
	});
});
</script>
