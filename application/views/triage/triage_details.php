<table>
<tr><td><b>Patient #:&emsp;</b></td><td><?php echo $patient->patient_no; ?></td></tr>
<tr><td><b>Patient Name:&emsp;</b></td><td><?php echo $patient->patient_name; ?></td></tr>
<tr><td><b>Age:&emsp;</b></td><td><?php echo $patient->age; ?></td></tr>
<?php
	$employee = $this->Employee->get_info($triage_info->employee_id);
	$employee->employee_name = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
?>
<tr><td><b>Served by:&emsp;</b></td><td><?php echo $employee->employee_name; ?></td></tr>
<tr><td><b>Date:&emsp;</b></td><td><?php echo date('j/n/Y g:i a',strtotime($triage_info->triage_time)); ?></td></tr>
</table>
<hr />

<div id="examination_header_bar">Triage Information</div>
<br>
<?php $triage_details = unserialize($triage_info->triage_details); ?>
<div class="field_row clearfix">	
<strong><?php echo form_label('Temperature:', 'temperature'); ?></strong>
	<div class='form_field'>
	<?php echo $triage_details['temperature'];?>
	</div>
</div>
<div class="field_row clearfix">	
<strong><?php echo form_label('Weight:', 'weight'); ?></strong>
	<div class='form_field'>
	<?php echo $triage_details['weight'];?>
	</div>
</div>

<div class="field_row clearfix">	
<strong><?php echo form_label('Blood Pressure:', 'blood_pressure'); ?></strong>
	<div class='form_field'>
	<?php echo $triage_details['blood pressure']['systolic'];?>&emsp;/&emsp;<?php echo $triage_details['blood pressure']['diastolic'];?>
	</div>
</div>

<div class="field_row clearfix">	
<strong><?php echo form_label('Pulse Rate:', 'pulse_rate'); ?></strong>
	<div class='form_field'>
	<?php echo $triage_details['pulse rate'];?>
	</div>
</div>

<div class="field_row clearfix">	
<strong><?php echo form_label('Other Remarks:', 'other_remarks'); ?></strong>
	<div class='form_field'>
	<?php echo $triage_details['other remarks'];?>
	</div>
</div>


