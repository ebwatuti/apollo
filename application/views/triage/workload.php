<div id="archive">
<ul id="error_message_box"></ul>
<?php echo form_open("triage/workload_date_input",array('id'=>'workload_date_form')); ?>
<table width="100%"><tfoot><tr><td colspan="10"><br><hr></td></tr></tfoot>
<tbody><tr>
<td width="20%"><strong>Start Date: &emsp;</strong></td>
<td width="50%" align="right"><?php echo form_input(array(
                'name'=>'start_date',
                'id'=>'start_date',
                'value'=>$start_date_input, 
                'class'=>'date')
        ); ?></td><td width="30%"></td>
</tr><tr>
<td width="20%"><strong>End Date: &emsp;</strong></td>
<td width="50%" align="right"><?php echo form_input(array(
                'name'=>'end_date',
                'id'=>'end_date',
                'value'=>$end_date_input, 
                'class'=>'date')
        ); ?></td>
</tr><tr>
<td colspan="2"><?php echo form_button(array("id"=>"workload_date_submit","content"=>"Submit","class"=>"submit_button float_right"));?></td>
</tr></tbody></table>
<?php echo form_close(); ?>

<div id="page_subtitle">
<?php
    if($start_date == $end_date) echo $start_date;
    else echo $start_date . "&emsp;to&emsp;" . $end_date;
?>
</div>
<div id="workload_table_holder">
<table width="60%" id="workload_table" class="display">
    <tr>
        <th style="background-color: #DDD">Total Hospital Workload</th>
        <td style="background-color: #DDD"><?php echo $hospital_workload; ?></td>
    </tr>
    <tr>
        <th style="background-color: #DDD"><?php echo $employee; ?>'s Total Workload</th>
        <td style="background-color: #DDD"><?php echo $employee_workload; ?></td>
    </tr>
</table>
</div>
</div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	Date.format = 'dd-mm-yyyy';
    $('.date').datepicker({
      changeMonth: true,
      changeYear: true,
      //minDate: new Date(2014, 0, 1),
      maxDate: "0",
      dateFormat: "dd-mm-yy",
    });

    $("#workload_date_submit").click(function(){
        $("#workload_date_form").ajaxSubmit({
            success:function(response)
            {
                if(response.form_validation)
                {
                    var html = "<table width='100%' height='100px'><tr><td align='center'><img src='<?php echo base_url()?>images/loading_animation.gif' alt='spinner' /></td></tr><table>";
                    $("#workload_table_holder").html(html);
                    $("#TB_ajaxContent").load('<?php echo site_url("triage/view_workload"); ?>');
                }
                else
                {
                    $("#error_message_box").html(response.error_messages);
                }
            },
            dataType:'json',
        });
    });
});
</script>
