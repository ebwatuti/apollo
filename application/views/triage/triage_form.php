<div width="100%" id="message_bar"></div>

<div id="queue_section">
<div style="background-color: #0a6184; color:#FFF" width="100%" align="center">Patient Info</div>
<div class="field_row clearfix">	
<?php echo form_label('Patient #:'); ?>
	<div class='form_field'>
	<?php echo $patient->patient_no;?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Patient Name:'); ?>
	<div class='form_field'>
	<?php echo $patient->patient_name;?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Age:'); ?>
	<div class='form_field'>
	<?php echo $patient->age; ?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Last Visit:'); ?>
	<div class='form_field'>
	<?php echo $patient->last_visit; ?>
	</div>
</div>
</div>

<fieldset id="employee_login_info">
<legend>Triage Information</legend>
<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>
<?php
echo form_open("triage/save_triage/$patient->patient_id",array('id'=>'triage_form'));
?>

<div class="field_row clearfix">	
<?php echo form_label('Temperature:', 'temperature',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'temperature',
		'id'=>'temperature',
		'value'=>'')
	);?>
	</div>
</div>
<div class="field_row clearfix">	
<?php echo form_label('Weight:', 'weight',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'weight',
		'id'=>'weight',
		'value'=>'')
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Blood Pressure:', 'blood_pressure',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'blood_pressure_systolic',
		'id'=>'blood_pressure_systolic',
		'size'=>4,
		'value'=>''))
		. "&emsp;/&emsp;" .
		form_input(array(
		'name'=>'blood_pressure_diastolic',
		'id'=>'blood_pressure_diastolic',
		'size'=>4,
		'value'=>''));?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Pulse Rate:', 'pulse_rate',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'pulse_rate',
		'id'=>'pulse_rate',
		'value'=>'')
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Priority:', 'priority'); ?>
	<div class='form_field'>
	<?php echo form_dropdown('priority',array(
		1=>'Normal',
		2=>'Urgent',
		3=>'Emergency'),$encounter_info->priority,'id=priority'
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Other Remarks:', 'other_remarks'); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
		'name'=>'other_remarks',
		'id'=>'other_remarks',
		'value'=>'',
		'cols'=>18,
		'rows'=>5)
	);?>
	</div>
</div>

<?php
echo form_button(array(
	'name'=>'submit',
	'id'=>'triage_form_submit',
	'content'=>'Submit',
	'class'=>'submit_button float_right')
);
?>
<?php 
echo form_close();
?>
</fieldset>
<div class="clearfix" style="margin-bottom:1px;">&nbsp;</div>

<div class="clearfix" id="triage_chart_holder"></div>

<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{	
	$('#triage_chart_holder').load('<?php echo site_url("triage/triage_chart/$encounter_info->encounter_id"); ?>');

	$('#triage_form_submit').click(function(){
		$('#triage_form').ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					if(response.success)
					{
						$("#TB_ajaxContent").load('<?php echo site_url("triage/view_triage/$patient->patient_id"); ?>',
							function(result){
								$("#message_bar").removeClass('error_message');
	                            $("#message_bar").removeClass('warning_message');
	                            $("#message_bar").removeClass('success_message');
	                            $("#message_bar").addClass(response.message_class);
	                            $("#message_bar").html(response.message);
	                            $('#message_bar').fadeTo(5000, 1);
	                            $('#message_bar').fadeTo("fast",0);
							});
						$("#table_holder").load('<?php echo site_url("triage/view_all"); ?>');
					}
					else
					{
						$("#message_bar").removeClass('error_message');
                        $("#message_bar").removeClass('warning_message');
                        $("#message_bar").removeClass('success_message');
                        $("#message_bar").addClass(response.message_class);
                        $("#message_bar").html(response.message);
                        $('#message_bar').fadeTo(5000, 1);
                        $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});
});
</script>