<div width="100%" id="message_bar"></div>

<div id="main_area">
<div id="queue_section">
<?php
    $patient = $this->Patient->get_info($patient_id);
    $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
    if($patient->dob) $patient->age = $this->Patient->patient_age($patient->dob);
    $patient->patient_no = $this->Patient->patient_number($patient->patient_id);
?>
<table width="100%">
    <tr><th style="background-color: #0a6184; color:#FFF; color:#FFF" colspan="2" align="center">Patient Info</th></tr>
    <tr><td colspan="2">
    <div class="field_row clearfix">    
    <?php echo form_label('Patient #:'); ?>
        <div class='form_field'>
        <?php echo $patient->patient_no;?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Name:'); ?>
        <div class='form_field'>
        <?php echo $patient->patient_name;?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Age:'); ?>
        <div class='form_field'>
        <?php echo $patient->age;?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Total:'); ?>
        <div class='form_field' id="invoice_total">
        
        </div>
    </div>
    <?php
        echo form_button(array(
            'name'=>'submit',
            'invoice_id'=>$invoice_id,
            'onclick'=>'save_invoice(this)',
            'content'=>'Save Invoice',
            'class'=>'submit_button float_right')
        );
    ?>
    </td></tr>
    <tr><td colspan="2" width="100%"><br><hr></td></tr>

</table>
</div>

<div id="register_wrapper">
<table id="register">
<thead>
<tr>
<th>Name</th>
<th>Unit Price</th>
<th>Qty</th>
<th>Disc %</th>
<th>Total</th>
<th>Remarks</th>
<th></th>
</tr>
</thead>
<tbody id="prescription_contents">

<?php foreach(array_reverse($invoice_items, true) as $item): ?>
		<tr>
		<td style="align:center;"><?php echo $item['name']; ?></td>
		<td style="align:center;"><?php echo number_format($item['unit_price'],2,'.',''); ?></td>
		<td style="align:center;"><?php echo form_input(array(
									'name'=>'quantity',
									'value'=>$item['quantity'],
									'item_id'=>$item['item_id'],
									'type'=>'number',
									'length'=>'1',
									'min'=>'1',
									'onchange' => 'save_item_info(this)')); ?></td>
		<td style="align:center;"><?php echo form_input(array(
									'name'=>'discount',
									'value'=>$item['discount'],
									'item_id'=>$item['item_id'],
									'type'=>'number',
									'length'=>'1',
									'min'=>'0',
									'max'=>'100',
									'onchange' => 'save_item_info(this)')); ?></td>
		<td style="align:center;" class="item_total" id="total_<?php echo $item['item_id'];?>"><?php echo number_format(($item['quantity'] * $item['unit_price'] * (100 - $item['discount']) / 100),2,'.',''); ?></td>
        <td style="align:center;"><?php echo form_textarea(array(
									'name'=>'remarks',
									'value'=>$item['remarks'],
									'rows'=>'3',
									'cols'=>15,
									'item_id'=>$item['item_id'],
									'onchange' => 'save_item_info(this)')); ?></td>
        <td><?php echo form_button(array(
        'content'=>'Delete',
        'class'=>'submit_button float_right',
        'item_id'=>$item['item_id'],
        'onclick'=>"delete_item(this)"
    ));?></td>
		</tr>
		
		<tr style="height:3px">
		<td colspan=8 style="background-color:white"> </td>
		</tr>		</form>
	<?php endforeach; ?>
</tbody>
</table>
</div>
</div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $('#invoice_total').text(function(){
            var sum = 0.00;
            $('.item_total').each(function(){
                sum += parseFloat($(this).text());
            });
            return sum.toFixed(2);
        });
});
function save_invoice(button)
{
    if (confirm('Are you sure you want to save this invoice?'))
    {
    	$.post('<?php echo site_url("invoices/save_invoice"); ?>/' + $(button).attr('invoice_id'),
            {},
            function(response){
        		if(response.success)
    			{
    				$("#table_holder").load('<?php echo site_url("invoices/view_all"); ?>');
    				tb_remove();
    				set_feedback(response.message,response.message_class,false);
    			}
    			else
    			{
    				$("#message_bar").removeClass('error_message');
                    $("#message_bar").removeClass('warning_message');
                    $("#message_bar").removeClass('success_message');
                    $("#message_bar").addClass(response.message_class);
                    $("#message_bar").html(response.message);
                    $('#message_bar').fadeTo(5000, 1);
                    $('#message_bar').fadeTo("fast",0);
    			}
      		},'json');
    }
}

function save_item_info(input)
{
	$.post('<?php echo site_url("invoices/item_info"); ?>/' + $(input).attr('item_id') + '/' + $(input).attr('name'),
            {value: $(input).val()},
            function(response){
        		$('#total_'+$(input).attr('item_id')).html(response);

                $('#invoice_total').text(function(){
                    var sum = 0.00;
                    $('.item_total').each(function(){
                        sum += parseFloat($(this).text());
                    });
                    return sum.toFixed(2);
                });
      		}
                );
}

function delete_item(button)
{
	$("#register_wrapper").load('<?php echo site_url("invoices/remove_item"); ?>/' + $(button).attr('item_id'));
}
</script>
