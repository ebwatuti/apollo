<table width="100%" id="invoices_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Date</th>
        <th style="background-color: #0a6184; color:#FFF">Patient #</th>
        <th style="background-color: #0a6184; color:#FFF">Patient Name</th>
        <th style="background-color: #0a6184; color:#FFF">Invoiced by</th>
        <th style="background-color: #0a6184; color:#FFF">Invoice Type</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>
    </tr></thead>
    <tbody>
<?php
if($invoices->num_rows() > 0):
	foreach($invoices->result() as $invoice):
        $patient = $this->Patient->get_info($invoice->patient_id);
        $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
        $patient->patient_no = $this->Patient->patient_number($patient->patient_id);
        $invoice->date = date('j/n/Y g:i a',strtotime($invoice->invoice_time));
        $employee = $this->Employee->get_info($invoice->employee_id);
        $employee->employee_name = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $invoice->date; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->patient_no; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->patient_name; ?></td>
        <td style="background-color: #DDD"><?php echo $employee->employee_name; ?></td>
        <td style="background-color: #DDD"><?php echo $invoice->invoice_type; ?></td>
        <td style="background-color: #DDD">
			<?php echo anchor("invoices/view_invoice/$invoice->invoice_id/width:1150",
            form_button(array('name'=>'submit','content'=>'View','class'=>'DTTT_button float_left')),
            array("class"=>"thickbox vote_thickbox","title"=>"Invoice")); ?> 
            &emsp;
            <?php echo anchor("sales/select_customer/$invoice->patient_id/",
            form_button(array('name'=>'submit','content'=>'Sale','class'=>'DTTT_button float_left'))); ?>           
            &emsp;
            <?php
                echo form_button(array(
                    'name'=>'submit',
                    'onclick'=>"delete_invoice($invoice->invoice_id)",
                    'content'=>'Delete',
                    'class'=>'DTTT_button float_left')
                );
            ?>
        </td>
        </tr>
<?php
	endforeach;
endif;
?>
	</tbody>
</table>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	tb_init("a.vote_thickbox");
	$('#invoices_table').dataTable({
		//"bPaginate": false,
		//"bLengthChange": false,
		//"bFilter": false,
		"bSort": false,
		//"bInfo": false,
        "bStateSave": true,
	});
});

function delete_invoice(invoice_id)
{
    if(confirm('Are you sure you want to delete this invoice?'))
    {
        $.post('<?php echo site_url("invoices/delete_invoice"); ?>/' + invoice_id,
            {},
            function(response){
                if(response.success)
                {
                    set_feedback(response.message,response.message_class,false);
                    $("#table_holder").load('<?php echo site_url("invoices/view_all"); ?>');
                }
                else
                {
                    set_feedback(response.message,response.message_class,false);
                }
            },'json');
    }
}
</script>
