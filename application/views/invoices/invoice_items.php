<?php 
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));

echo form_open($controller_name."/add_item",array('id'=>'add_item_form','class'=>'add_item_form','onkeypress'=>'return event.keyCode != 13;'));
?>
<label id="item_label" for="item">Item</label>
<?php echo form_input(array('name'=>'item','id'=>'item','size'=>'40','palceholder'=>'Start typing item name'));?>


</form>
<table id="register">
<thead>
<tr>
<th>Name</th>
<th>Unit Price</th>
<th>Qty</th>
<th>Disc %</th>
<th>Total</th>
<th>Remarks</th>
<th></th>
</tr>
</thead>
<tbody id="prescription_contents">

<?php foreach(array_reverse($invoice_items, true) as $item): ?>
		<tr>
		<td style="align:center;"><?php echo $item['name']; ?></td>
		<td style="align:center;"><?php echo number_format($item['unit_price'],2,'.',''); ?></td>
		<td style="align:center;"><?php echo form_input(array(
									'name'=>'quantity',
									'value'=>$item['quantity'],
									'item_id'=>$item['item_id'],
									'type'=>'number',
									'length'=>'1',
									'min'=>'1',
									'onchange' => 'save_item_info(this)')); ?></td>
		<td style="align:center;"><?php echo form_input(array(
									'name'=>'discount',
									'value'=>$item['discount'],
									'item_id'=>$item['item_id'],
									'type'=>'number',
									'length'=>'1',
									'min'=>'0',
									'max'=>'100',
									'onchange' => 'save_item_info(this)')); ?></td>
		<td style="align:center;" class="item_total" id="total_<?php echo $item['item_id'];?>"><?php echo number_format(($item['quantity'] * $item['unit_price'] * (100 - $item['discount']) / 100),2,'.',''); ?></td>
        <td style="align:center;"><?php echo form_textarea(array(
									'name'=>'remarks',
									'value'=>$item['remarks'],
									'rows'=>'3',
									'cols'=>15,
									'item_id'=>$item['item_id'],
									'onchange' => 'save_item_info(this)')); ?></td>
        <td><?php echo form_button(array(
        'content'=>'Delete',
        'class'=>'submit_button float_right',
        'item_id'=>$item['item_id'],
        'onclick'=>"delete_item(this)"
    ));?></td>
		</tr>		</form>
	<?php endforeach; ?>
</tbody>
</table>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	$('#invoice_total').text(function(){
			var sum = 0.00;
			$('.item_total').each(function(){
				sum += parseFloat($(this).text());
			});
			return sum.toFixed(2);
		});
    $("#item").autocomplete('<?php echo site_url($controller_name."/item_search"); ?>',
    {
    	minChars:0,
    	max:100,
    	selectFirst: false,
       	delay:10,
    	formatItem: function(row) {
			return row[1];
		}
    });

    $("#item").result(function(event, data, formatted)
    {
		$("#add_item_form").ajaxSubmit({
			success:function(response){
				$("#message_bar").removeClass('error_message');
                $("#message_bar").removeClass('warning_message');
                $("#message_bar").removeClass('success_message');
                $("#message_bar").addClass(response.message_class);
                $("#message_bar").html(response.message);
                $('#message_bar').fadeTo(5000, 1);
                $('#message_bar').fadeTo("fast",0);
				
				if(response.success) $("#register_wrapper").load("<?php echo site_url($controller_name."/refresh_invoice"); ?>");
			},dataType:'json'});
    });
});

function save_item_info(input)
{
	$.post('<?php echo site_url("invoices/item_info"); ?>/' + $(input).attr('item_id') + '/' + $(input).attr('name'),
            {value: $(input).val()},
            function(response){
        		$('#total_'+$(input).attr('item_id')).html(response);

        		$('#invoice_total').text(function(){
					var sum = 0.00;
					$('.item_total').each(function(){
						sum += parseFloat($(this).text());
					});
					return sum.toFixed(2);
				});
      		}
                );
}

function delete_item(button)
{
	$("#register_wrapper").load('<?php echo site_url($controller_name."/remove_item"); ?>/' + $(button).attr('item_id'));
}
</script>