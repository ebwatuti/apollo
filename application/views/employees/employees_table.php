<table width="100%" id="employees_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Employee #</th>
        <th style="background-color: #0a6184; color:#FFF">Employee Name</th>
        <th style="background-color: #0a6184; color:#FFF">ID #</th>
        <th style="background-color: #0a6184; color:#FFF">Phone #</th>
        <th style="background-color: #0a6184; color:#FFF">Email</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>
    </tr></thead>
    <tbody>
<?php
	foreach($employees->result() as $employee):
        $employee->employee_name = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $employee->employee_no; ?></td>
        <td style="background-color: #DDD"><?php echo $employee->employee_name; ?></td>
        <td style="background-color: #DDD"><?php echo $employee->national_id; ?></td>
        <td style="background-color: #DDD"><?php echo $employee->phone_number; ?></td>
        <td style="background-color: #DDD"><?php echo $employee->email; ?></td>
        <td style="background-color: #DDD">
            <?php echo anchor("employees/view_employee/$employee->person_id/width:750",
                    form_button(array('name'=>'submit','content'=>'View','class'=>'DTTT_button float_left')),
                    array("class"=>"thickbox vote_thickbox","title"=>"Employee Details")); ?>
            &emsp;
            <?php
                echo form_button(array(
                    'name'=>'submit',
                    'onclick'=>'delete_employee('.$employee->person_id.')',
                    'content'=>'Delete',
                    'class'=>'DTTT_button float_left')
                );
            ?>
        </td>
        </tr>
<?php
	endforeach;
?>
	</tbody>
</table>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	tb_init("a.vote_thickbox");
    TableTools.DEFAULTS.aButtons = [ 
        "copy", 
        {
                    "sExtends": "print",
                    "sMessage": "",
        },
        {
            "sExtends":    "collection",
            "sButtonText": "Save",
            "aButtons":    [  
                {"sExtends": "xls","sTitle": "<?php echo $this->config->item('company'); ?> - Employees","mColumns": [ 0,1,2,3,4 ]}, 
                {"sExtends": "pdf","sPdfMessage": "","sPdfOrientation": "portrait","sTitle": "<?php echo $this->config->item('company'); ?> - Employees","mColumns": [ 0,1,2,3,4 ]} ]
        }, 
    ];
	$('#employees_table').dataTable({
		//"bPaginate": false,
		//"bLengthChange": false,
		//"bFilter": false,
		//"bSort": false,
		//"bInfo": false,
        "bStateSave": true,
        "sDom": 'T<"clear">lfrtip',
	});
});

function delete_employee(person_id)
{
    if(confirm('Are you sure you want to delete this employee?'))
    {
        $.post('<?php echo site_url("employees/delete_employee"); ?>/' + person_id,
            {},
            function(response){
                if(response.success)
                {
                    $("#table_holder").load('<?php echo site_url("employees/view_all"); ?>',
                        function(result){
                            set_feedback(response.message,response.message_class,false);
                        });
                }
                else
                {
                    set_feedback(response.message,response.message_class,false);
                }
            },'json');
    }
}
</script>
