<div width="100%" id="message_bar"></div>
<?php echo form_open('home/save_profile',array('id'=>'profile_form')); ?>
<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>

<div class="field_row clearfix">	
<?php echo form_label('Username:', 'username',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'username',
		'id'=>'username',
		'value'=>$employee_info->username));?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Password:', 'password',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_password(array(
		'name'=>'password',
		'id'=>'password'
	));?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('New Password:', 'new_password',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_password(array(
		'name'=>'new_password',
		'id'=>'new_password'
	));?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Repeat Password:', 'repeat_password',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_password(array(
		'name'=>'repeat_password',
		'id'=>'repeat_password'
	));?>
	</div>
</div>
<?php echo form_button(array("id"=>"profile_form_submit","content"=>"Submit","class"=>"submit_button float_right"));?>
<?php echo form_close(); ?>

<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{	
	$("#profile_form_submit").click(function(){
		$("#profile_form").ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					if(response.success)
					{
						tb_remove();
						set_feedback(response.message,response.message_class,false);						
					}
					else
					{
						$("#message_bar").removeClass('error_message');
	                    $("#message_bar").removeClass('warning_message');
	                    $("#message_bar").removeClass('success_message');
	                    $("#message_bar").addClass(response.message_class);
	                    $("#message_bar").html(response.message);
	                    $('#message_bar').fadeTo(5000, 1);
	                    $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});
});
</script>