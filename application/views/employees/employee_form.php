<div width="100%" id="message_bar"></div>
<?php echo form_open('employees/save_employee/'.$employee_info->person_id,array('id'=>'employee_form')); ?>
<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>
<fieldset id="employee_basic_info">
<legend>Employee Info</legend>
<div class="field_row clearfix">	
<?php echo form_label('First Name:', 'first_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'first_name',
		'id'=>'first_name',
		'value'=>$employee_info->first_name)
	);?>
	</div>
</div>
<div class="field_row clearfix">	
<?php echo form_label('Middle Name:', 'middle_name'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'middle_name',
		'id'=>'middle_name',
		'value'=>$employee_info->middle_name)
	);?>
	</div>
</div>
<div class="field_row clearfix">	
<?php echo form_label('Last Name:', 'last_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'last_name',
		'id'=>'last_name',
		'value'=>$employee_info->last_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Gender:', 'gender'); ?>
	<div class='form_field'>
	<?php echo form_dropdown('gender',array(
		'male'=>'male',
		'female'=>'female'),$employee_info->gender
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('National ID:', 'national_id'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'national_id',
		'id'=>'national_id',
		'value'=>$employee_info->national_id)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Phone Number:', 'phone_number'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'phone_number',
		'id'=>'phone_number',
		'value'=>$employee_info->phone_number)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Email:', 'email'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'email',
		'type'=>'email',
		'id'=>'email',
		'value'=>$employee_info->email)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Residence:', 'residence'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'residence',
		'id'=>'residence',
		'value'=>$employee_info->residence)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Address:', 'address_1'); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
            'name'=>'address_1',
			'id'=>'address_1',
            'value'=>$employee_info->address_1,
            'rows'=>'5',
			'cols'=>'17',)); ?>
	</div>
</div>
</fieldset>

<fieldset id="employee_login_info">
<legend>Account Info</legend>
<div class="field_row clearfix">	
<?php echo form_label('Employee #:', 'employee_no'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'employee_no',
		'id'=>'employee_no',
		'value'=>$employee_info->employee_no));?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Username:', 'username',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'username',
		'id'=>'username',
		'value'=>$employee_info->username));?>
	</div>
</div>

<?php
$password_label_attributes = $employee_info->person_id == "" ? array('class'=>'required'):array();
?>

<div class="field_row clearfix">	
<?php echo form_label('Password:', 'password',$password_label_attributes); ?>
	<div class='form_field'>
	<?php echo form_password(array(
		'name'=>'password',
		'id'=>'password'
	));?>
	</div>
</div>


<div class="field_row clearfix">	
<?php echo form_label('Repeat Password:', 'repeat_password',$password_label_attributes); ?>
	<div class='form_field'>
	<?php echo form_password(array(
		'name'=>'repeat_password',
		'id'=>'repeat_password'
	));?>
	</div>
</div>
</fieldset>

<fieldset id="employee_permission_info">
<legend>Permissions</legend>
<p>Check the boxes below to grant access to modules</p>

<ul id="permission_list">
<?php foreach($modules->result() as $module): ?>
<li>	
<?php echo form_checkbox("permissions[]",$module->module_id,$this->Employee->has_permission($module->module_id,$employee_info->person_id)); ?>
<span class="medium"><?php echo $module->name; ?>:</span>
<span class="small"><?php echo $module->description; ?></span>
</li>
<?php endforeach; ?>
</ul>
<?php echo form_button(array("id"=>"employee_form_submit","content"=>"Submit","class"=>"submit_button float_right"));?>
</fieldset>
<?php echo form_close(); ?>

<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{	
	$("#employee_form_submit").click(function(){
		$("#employee_form").ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					if(response.success)
					{
						$("#table_holder").load('<?php echo site_url("employees/view_all"); ?>');
						tb_remove();
						set_feedback(response.message,response.message_class,false);						
					}
					else
					{
						$("#message_bar").removeClass('error_message');
	                    $("#message_bar").removeClass('warning_message');
	                    $("#message_bar").removeClass('success_message');
	                    $("#message_bar").addClass(response.message_class);
	                    $("#message_bar").html(response.message);
	                    $('#message_bar').fadeTo(5000, 1);
	                    $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});
});
</script>