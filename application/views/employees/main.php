<?php $this->load->view("partial/header"); ?>
<div id="title_bar">
	<div id="page_title" style="margin-bottom:8px;">Employees</div>
	
</div>
<div id="sidebar">
	<?php echo anchor("employees/view_employee/-1/width:650",
	"<div class='submenu_button' style='float: left;'><span>New Employee</span></div>",
	array('title'=>'New Employee','class'=>'thickbox none',));
	?>
</div>

<div id="table_holder">

</div>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	var html = "<table width='100%' height='100px'><tr><td align='center'><img src='<?php echo base_url()?>images/loading_animation.gif' alt='spinner' /></td></tr><table>";
	$("#table_holder").html(html);
	$("#table_holder").load('<?php echo site_url("employees/view_all"); ?>');		
});

</script>
