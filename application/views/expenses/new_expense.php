<div width="100%" id="message_bar"></div>
<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>

<fieldset id="supplier_basic_info">
<legend>Expense Info</legend>
<?php echo form_open("expenses/save_expense",array('id'=>'expense_form')); ?>

<div class="field_row clearfix">
<strong><?php echo form_label('Expense #:', 'expense_no',array('class'=>'wide')); ?></strong>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'expense_no',
		'id'=>'expense_no')
	);?>
	</div>
</div>

<div class="field_row clearfix">
<strong><?php echo form_label('Description:', 'description',array('class'=>'required wide')); ?></strong>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'description',
		'id'=>'description')
	);?>
	</div>
</div>

<div class="field_row clearfix">
<strong><?php echo form_label(date('M Y').' Budget:', 'amount',array('class'=>'required wide')); ?></strong>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'amount',
		'id'=>'amount')
	);?>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_button(array("id"=>"expense_form_submit","content"=>"Submit","class"=>"submit_button float_right"));?>
</div>

<?php  echo form_close(); ?>
</fieldset>

<script type='text/javascript'>
$(document).ready(function()
{	
	$("#expense_form_submit").click(function(){
		$("#expense_form").ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					if(response.success)
					{
						$("#table_holder").load('<?php echo site_url("expenses/view_all"); ?>');
						tb_remove();
						set_feedback(response.message,response.message_class,false);
					}
					else
					{
						$("#message_bar").removeClass('error_message');
	                    $("#message_bar").removeClass('warning_message');
	                    $("#message_bar").removeClass('success_message');
	                    $("#message_bar").addClass(response.message_class);
	                    $("#message_bar").html(response.message);
	                    $('#message_bar').fadeTo(5000, 1);
	                    $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});
});
</script>