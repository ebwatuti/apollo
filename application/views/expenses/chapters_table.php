<div width="100%" id="message_bar"></div>
<div id="chapters">
<?php
    echo form_button(array(
        'name'=>'submit',
        'id'=>'new_chapter_button',
        'content'=>'New Chapter',
        'class'=>'submit_button float_right')
    );
?>
<br><br>
<table width="100%" id="chapters_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Chapter #</th>
        <th style="background-color: #0a6184; color:#FFF">Description</th>
        <th style="background-color: #0a6184; color:#FFF">Source</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>
    </tr></thead>
    <tbody>
<?php
if($chapters->num_rows() > 0):
	foreach($chapters->result() as $chapter):
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $chapter->chapter_no; ?></td>
        <td style="background-color: #DDD"><?php echo $chapter->description; ?></td>
        <td style="background-color: #DDD"><?php if($chapter->source != 'expense') echo $chapter->source; ?></td>
        <td style="background-color: #DDD">
		<?php
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'edit_chapter('.$chapter->chapter_id.')',
                'content'=>'Edit',
                'class'=>'DTTT_button float_left')
            );
        ?>
        &emsp;
        <?php
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'delete_chapter('.$chapter->chapter_id.')',
                'content'=>'Delete',
                'class'=>'DTTT_button float_left')
            );
        ?>
        </td>
        </tr>
<?php
	endforeach;
endif;
?>
	</tbody>
</table>
</div>

<div id="chapter_form_area"></div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $('#chapters_table').dataTable({
        //"bPaginate": false,
        //"bLengthChange": false,
        //"bFilter": false,
        //"bSort": false,
        //"bInfo": false,
        "bStateSave": true,
    });

    $("#new_chapter_button").click(function(){
        $("#chapter_form_area").css('display','block');
        $("#chapter_form_area").load('<?php echo site_url("expenses/chapter_info"); ?>');
        $('#chapters').css('display','none');
    });
});

function edit_chapter(chapter_id)
{
    $("#chapter_form_area").css('display','block');
    $("#chapter_form_area").load('<?php echo site_url("expenses/chapter_info"); ?>/' + chapter_id);
    $('#chapters').css('display','none');
}

function delete_chapter(chapter_id)
{
    if(confirm('Are you sure you want to delete this chapter?'))
    {
        $.post('<?php echo site_url("expenses/delete_chapter"); ?>/' + chapter_id,
            {},
            function(response){
                if(response.success)
                {
                    $("#table_holder").load('<?php echo site_url("expenses/view_all"); ?>');
                    $("#TB_ajaxContent").load('<?php echo site_url("expenses/view_chapters"); ?>',
                        function(result){
                            $("#message_bar").removeClass('error_message');
                            $("#message_bar").removeClass('warning_message');
                            $("#message_bar").removeClass('success_message');
                            $("#message_bar").addClass(response.message_class);
                            $("#message_bar").html(response.message);
                            $('#message_bar').fadeTo(5000, 1);
                            $('#message_bar').fadeTo("fast",0);
                        });
                }
                else
                {
                    $("#message_bar").removeClass('error_message');
                    $("#message_bar").removeClass('warning_message');
                    $("#message_bar").removeClass('success_message');
                    $("#message_bar").addClass(response.message_class);
                    $("#message_bar").html(response.message);
                    $('#message_bar').fadeTo(5000, 1);
                    $('#message_bar').fadeTo("fast",0);
                }
            },'json');
    }
}
</script>