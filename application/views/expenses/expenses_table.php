<?php
    $identifier = $chapter.'_'.$category.'_'.$subcategory;
    $identifier = strtolower(preg_replace('/[^a-zA-Z0-9_]/s', "_",$identifier));
    if(!$chapter):
        $label = "";
    else:
        if(!$category):
            $label = "Chapter $chapter";
        else:
            if(strpos($category, ':') !== false):
                $category = explode(':', $category);
                $category = $category[1] . ' - '.ucwords($category[0]);
            endif;
            if(!$subcategory):
                $label = "Chapter $chapter: $category";
            else:
                $label = "Chapter $chapter: $category ($subcategory)";
            endif;
        endif;
    endif;
?>
<table width="100%" id="expense_<?php echo $identifier; ?>_table" class="display">
    <thead><tr>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>
        <th style="background-color: #0a6184; color:#FFF">Expense #</th>
        <th style="background-color: #0a6184; color:#FFF">Description</th>
        <th style="background-color: #0a6184; color:#FFF">Expenditure</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>
    </tr></thead>
    <tbody>
<?php
    $total_expenditure = 0;
    $total_budget = 0;
    $total_balance_cf = 0;
    $other = false;
    foreach($expenses as $expense):
        $total_expenditure = $total_expenditure + $expense['expenditure'];
        if(!$expense['description']):
            $other = $expense;
            continue;
        endif;        
?>
        <tr>
        <?php if($expense['expand']): ?>
        <td style="background-color: #DDD; cursor: pointer;" class="control" chapter="<?php echo $expense['chapter']; ?>" 
        category="<?php echo $expense['category']; ?>" subcategory="<?php echo $expense['subcategory']; ?>">
        <img src="<?php echo base_url().'images/plus.png';?>" border="0"/>
        <?php else: ?>
        <td style="background-color: #DDD">
        <?php endif; ?>
        </td>
        <td style="background-color: #DDD"><?php echo $expense['expense_no']; ?></td>
        <td style="background-color: #DDD"><?php echo $expense['description']; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo number_format($expense['expenditure'],2,'.',','); ?></td>
        <td style="background-color: #DDD">
            <?php if($expense['expense_id']): ?>
            <?php echo anchor("expenses/view_expense/".$expense['expense_id']."/width:550",
                    form_button(array('name'=>'submit','content'=>'Edit','class'=>'DTTT_button float_left')),
                    array("class"=>"thickbox vote_thickbox","title"=>"Expense Details")); ?>
            &emsp;
            <?php echo anchor("expenses/view_expenditures/".$expense['expense_id']."/width:850",
                    form_button(array('name'=>'submit','content'=>'Payments','class'=>'DTTT_button float_left')),
                    array("class"=>"thickbox vote_thickbox","title"=>$expense['description']." Payments ($period)")); ?>
            <?php endif; ?>
        </td>
        </tr>
<?php endforeach; ?>
<?php if($other): ?>
        <tr>
        <td style="background-color: #DDD"></td>
        <td style="background-color: #DDD"></td>
        <td style="background-color: #DDD">Other</td>
        <td style="background-color: #DDD" align="right"><?php echo number_format($other['expenditure'],2,'.',','); ?></td>
        <td style="background-color: #DDD"></td>
        </tr>
<?php endif; ?>
    </tbody>
    <tfoot>
        <tr>
        <th style="background-color: #DDD">&nbsp;</th>
        <th style="background-color: #DDD">&nbsp;</th>
        <th style="background-color: #DDD" align="right">Total</th>
        <th style="background-color: #DDD" align="right"><?php echo number_format($total_expenditure,2,'.',','); ?></th>
        <th style="background-color: #DDD">&nbsp;</th>
        </tr>
    </tfoot>
</table>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    tb_init("a.vote_thickbox");
    TableTools.DEFAULTS.aButtons = [ 
        "copy", 
        {
                    "sExtends": "print",
                    "sMessage": "<?php echo $this->config->item('company'); ?> - <?php echo $label; ?> Expenditure(<?php echo $period; ?>)",
        },
        {
            "sExtends":    "collection",
            "sButtonText": "Save",
            "aButtons":    [  
                {"sExtends": "xls","sTitle": "<?php echo $this->config->item('company'); ?> - <?php echo $label; ?> Expenditure(<?php echo $period; ?>)","mColumns": [ 1,2,3 ]}, 
                {"sExtends": "pdf","sPdfMessage": "","sPdfOrientation": "portrait","sTitle": "<?php echo $this->config->item('company'); ?> - <?php echo $label; ?> Expenditure(<?php echo $period; ?>)","mColumns": [ 1,2,3 ]} ]
        }, 
    ];

    var oTable = $('#expense_<?php echo $identifier; ?>_table').dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        //"bFilter": false,
        "bSort": false,
        //"bInfo": false,
        "bStateSave": true,
        "sDom": 'Ti<"clear">lfrtp',
        "oLanguage": {
            "sInfo": "<strong><?php echo $label; ?></strong>",
            "sInfoEmpty": "<strong><?php echo $label; ?></strong>",
            "sInfoFiltered": "(filtered from _MAX_ total records)"
        }
    });

    var anOpen = [];
    $('#expense_<?php echo $identifier; ?>_table td.control').on( 'click', function(){
      var nTr = this.parentNode;
      var i = $.inArray(nTr,anOpen);
       
      if( i === -1 ){
        $('img', this).attr( 'src', "<?php echo base_url().'images/minus.png';?>" );
    
        $.post('<?php echo site_url("expenses/view_all"); ?>',
                {chapter: $(this).attr('chapter'),category: $(this).attr('category'),subcategory: $(this).attr('subcategory')},
                function(response){
                    var data = '<div class="<?php echo $identifier; ?>_innerDetails">'+ response + '</div>';
                    var nDetailsRow = oTable.fnOpen( nTr, data, 'details' );
                    $('div.innerDetails', nDetailsRow).slideDown();
                    anOpen.push( nTr );
                });
      }
      else{
        $('img', this).attr( 'src', "<?php echo base_url().'images/plus.png';?>" );
        $('div.<?php echo $identifier; ?>_innerDetails', $(nTr).next()[0]).slideUp( function(){
          oTable.fnClose( nTr );
          anOpen.splice( i, 1 );
        });
      }
    });

    $('.innerDetails').css('display','none');

});
</script>
