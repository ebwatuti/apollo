<div width="100%" id="message_bar"></div>
<div id="expenditures">
<?php
	if($period == date('Y-m-01')):
	    echo form_button(array(
	        'name'=>'submit',
	        'id'=>'new_expenditure_button',
	        'content'=>'New Expenditure',
	        'class'=>'submit_button float_right')
	    );
	endif;
?>
<br><br>
<table width="100%" id="expenditures_table" class="display">
	<thead><tr>
		<th style="background-color: #0a6184; color:#FFF">Date</th>
        <th style="background-color: #0a6184; color:#FFF">Payee</th>
        <th style="background-color: #0a6184; color:#FFF">Ref #</th>
        <th style="background-color: #0a6184; color:#FFF">Amount</th>
        <th style="background-color: #0a6184; color:#FFF">Remarks</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>
    </tr></thead>
    <tbody>
<?php
$total_expenditure = 0;
if($expenditures->num_rows() > 0):
	foreach($expenditures->result() as $expenditure):
		$total_expenditure = $total_expenditure + $expenditure->amount;
?>
        <tr>
        <td style="background-color: #DDD"><?php echo date('d/m/Y',strtotime($expenditure->payment_time)); ?></td>
        <td style="background-color: #DDD"><?php echo $expenditure->payee; ?></td>
        <td style="background-color: #DDD"><?php echo $expenditure->ref_no; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo number_format($expenditure->amount,2,'.',','); ?></td>
        <td style="background-color: #DDD"><?php echo $expenditure->remarks; ?></td>
        <td style="background-color: #DDD">
        <?php if($period == date('Y-m-01')): ?>
		<?php
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'edit_expenditure('.$expenditure->expenditure_id.')',
                'content'=>'Edit',
                'class'=>'DTTT_button float_left')
            );
        ?>
        &emsp;
        <?php
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'delete_expenditure('.$expenditure->expenditure_id.')',
                'content'=>'Delete',
                'class'=>'DTTT_button float_left')
            );
        ?>
    	<?php endif; ?>
        </td>
        </tr>
<?php
	endforeach;
endif;
?>
	</tbody>
	<tfoot>
		<tr>
        <th style="background-color: #DDD">&nbsp;</th>
        <th style="background-color: #DDD" align="right">Total</th>
        <th style="background-color: #DDD">&nbsp;</th>
        <th style="background-color: #DDD" align="right"><?php echo number_format($total_expenditure,2,'.',','); ?></th>
        <th style="background-color: #DDD">&nbsp;</th>
        <th style="background-color: #DDD">&nbsp;</th>
        </tr>
	</tfoot>
</table>
</div>

<div id="expenditure_form_area"></div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	TableTools.DEFAULTS.aButtons = [ 
        "copy", 
        {
                    "sExtends": "print",
                    "sMessage": "<?php echo $this->config->item('company'); ?> - " + $("#TB_ajaxWindowTitle").html(),
        },
        {
            "sExtends":    "collection",
            "sButtonText": "Save",
            "aButtons":    [  
                {"sExtends": "xls","sTitle": "<?php echo $this->config->item('company'); ?> - " + $("#TB_ajaxWindowTitle").html(),"mColumns": [ 0,1,2,3,4 ]}, 
                {"sExtends": "pdf","sPdfMessage": "","sPdfOrientation": "portrait","sTitle": "<?php echo $this->config->item('company'); ?> - " + $("#TB_ajaxWindowTitle").html(),"mColumns": [ 0,1,2,3,4 ]} ]
        }, 
    ];

    $('#expenditures_table').dataTable({
        //"bPaginate": false,
        //"bLengthChange": false,
        //"bFilter": false,
        //"bSort": false,
        //"bInfo": false,
        "bStateSave": true,        
        "sDom": 'T<"clear">lfrtip',
    });

    $("#new_expenditure_button").click(function(){
        $("#expenditure_form_area").css('display','block');
        $("#expenditure_form_area").load('<?php echo site_url("expenses/expenditure_info/$expense_id"); ?>');
        $('#expenditures').css('display','none');
    });
});

function edit_expenditure(expenditure_id)
{
    $("#expenditure_form_area").css('display','block');
    $("#expenditure_form_area").load('<?php echo site_url("expenses/expenditure_info/$expense_id"); ?>/' + expenditure_id);
    $('#expenditures').css('display','none');
}

function delete_expenditure(expenditure_id)
{
    if(confirm('Are you sure you want to delete this expenditure?'))
    {
        $.post('<?php echo site_url("expenses/delete_expenditure"); ?>/' + expenditure_id,
            {},
            function(response){
                if(response.success)
                {
                    $("#TB_ajaxContent").load('<?php echo site_url("expenses/view_expenditures/$expense_id"); ?>',
                        function(result){
                            $("#message_bar").removeClass('error_message');
                            $("#message_bar").removeClass('warning_message');
                            $("#message_bar").removeClass('success_message');
                            $("#message_bar").addClass(response.message_class);
                            $("#message_bar").html(response.message);
                            $('#message_bar').fadeTo(5000, 1);
                            $('#message_bar').fadeTo("fast",0);
                        });
                }
                else
                {
                    $("#message_bar").removeClass('error_message');
                    $("#message_bar").removeClass('warning_message');
                    $("#message_bar").removeClass('success_message');
                    $("#message_bar").addClass(response.message_class);
                    $("#message_bar").html(response.message);
                    $('#message_bar').fadeTo(5000, 1);
                    $('#message_bar').fadeTo("fast",0);
                }
            },'json');
    }
}
</script>