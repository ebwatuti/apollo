<div width="100%" id="message_bar"></div>
<ul id="error_message_box"></ul>

<table width="100%" id="budgets_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Period</th>
        <th style="background-color: #0a6184; color:#FFF">Balance c/f</th>
        <th style="background-color: #0a6184; color:#FFF">Budget</th>
        <th style="background-color: #0a6184; color:#FFF">Expenditure</th>
        <th style="background-color: #0a6184; color:#FFF">Balance</th>
    </tr></thead>
    <tbody>
<?php
    if(!$this->Expense->get_current_budget()):
        $balance_cf = $this->Expense->get_total_balance(date('Y-m-01'));
        $expenditure = $this->Expense->get_total_expenditure(date('Y-m-01'));
        $balance = $amount + $balance_cf - $expenditure;
?>
        <tr>
        <td style="background-color: #DDD"><?php echo date('M Y'); ?></td>
        <td style="background-color: #DDD" align="right"><?php echo number_format($balance_cf,2); ?></td>
        <td style="background-color: #DDD" align="right">
        <?php echo form_open("expenses/save_budget/",array('id'=>'current_budget_form')); ?>
        <?php echo form_input(array(
            'name'=>'amount',
            'type'=>'number',
            'min'=>0,
            'id'=>'current_budget',
            'value'=>0,
            'class'=>'float_left')
        );?>
        <?php echo form_button(array("id"=>"current_budget_submit","content"=>"Save","class"=>"submit_button float_right"));?>
        <?php echo form_close(); ?>
        </td>
        <td style="background-color: #DDD" align="right"><?php echo number_format($expenditure,2); ?></td>
        <td style="background-color: #DDD" align="right"><?php echo number_format($balance,2); ?></td>
        </tr>
<?php endif; ?>

<?php
if($budgets->num_rows() > 0):
	foreach($budgets->result() as $budget):
        $budget->balance_cf = $this->Expense->get_total_balance($budget->period);
        $budget->expenditure = $this->Expense->get_total_expenditure($budget->period);
        $budget->balance = $budget->amount + $budget->balance_cf - $budget->expenditure;
?>
        <tr>
        <td style="background-color: #DDD"><?php echo date('M Y',strtotime($budget->period)); ?></td>
        <td style="background-color: #DDD" align="right"><?php echo number_format($budget->balance_cf,2); ?></td>
        <td style="background-color: #DDD" align="right">
        <?php if($budget->period == date('Y-m-01')): ?>
        <?php echo form_open("expenses/save_budget/",array('id'=>'current_budget_form')); ?>
        <?php echo form_input(array(
            'name'=>'amount',
            'type'=>'number',
            'min'=>0,
            'id'=>'current_budget',
            'value'=>$budget->amount,
            'class'=>'float_left')
        );?>
        <?php echo form_button(array("id"=>"current_budget_submit","content"=>"Save","class"=>"submit_button float_right"));?>
        <?php echo form_close(); ?>
        <?php else: ?>
        <?php echo number_format($budget->amount,2); ?>
        <?php endif; ?>
        </td>
        <td style="background-color: #DDD" align="right"><?php echo number_format($budget->expenditure,2); ?></td>
        <td style="background-color: #DDD" align="right"><?php echo number_format($budget->balance,2); ?></td>
        </tr>
<?php
	endforeach;
endif;
?>
	</tbody>
</table>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $('#budgets_table').dataTable({
        //"bPaginate": false,
        //"bLengthChange": false,
        //"bFilter": false,
        "bSort": false,
        //"bInfo": false,
        "bStateSave": true,
    });

    $("#current_budget_submit").click(function(){
        $("#current_budget_form").ajaxSubmit({
            success:function(response)
            {
                if(response.form_validation)
                {
                    if(response.success)
                    {
                        $("#TB_ajaxContent").load('<?php echo site_url("expenses/view_budgets"); ?>',
                            function(result){
                                $("#message_bar").removeClass('error_message');
                                $("#message_bar").removeClass('warning_message');
                                $("#message_bar").removeClass('success_message');
                                $("#message_bar").addClass(response.message_class);
                                $("#message_bar").html(response.message);
                                $('#message_bar').fadeTo(5000, 1);
                                $('#message_bar').fadeTo("fast",0);
                            });
                    }
                    else
                    {
                        $("#message_bar").removeClass('error_message');
                        $("#message_bar").removeClass('warning_message');
                        $("#message_bar").removeClass('success_message');
                        $("#message_bar").addClass(response.message_class);
                        $("#message_bar").html(response.message);
                        $('#message_bar').fadeTo(5000, 1);
                        $('#message_bar').fadeTo("fast",0);
                    }
                }
                else
                {
                    $("#error_message_box").html(response.error_messages);
                }
            },
            dataType:'json',
        });
    });
});
</script>