<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>

<fieldset id="employee_login_info">
<legend>Expenditure Info</legend>
<?php echo form_open("expenses/save_expenditure/$expense_id/$expenditure_info->expenditure_id/",array('id'=>'expenditure_form')); ?>

<div class="field_row clearfix">
<strong><?php echo form_label('Payee:', 'payee',array('class'=>'wide required')); ?></strong>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'payee',
		'id'=>'payee',
		'value'=>$expenditure_info->payee)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<strong><?php echo form_label('Ref #:', 'ref_no',array('class'=>'wide')); ?></strong>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'ref_no',
		'id'=>'ref_no',
		'value'=>$expenditure_info->ref_no)
	);?>
	</div>
</div>

<div class="field_row clearfix">
	<strong><?php echo form_label('Amount:', 'amount',array('class'=>'wide required')); ?></strong>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'amount',
		'id'=>'amount',
		'value'=>$expenditure_info->amount)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
	<strong><?php echo form_label('Remarks:', 'remarks',array('class'=>'wide')); ?></strong>
	<div class='form_field'>
	<?php echo form_textarea(array(
            'name'=>'remarks',
			'id'=>'remarks',
            'value'=>$expenditure_info->remarks,
            'rows'=>'5',
			'cols'=>'17',)); ?>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_button(array('id'=>'expenditure_form_cancel','content'=>'Cancel','class'=>'submit_button float_right')); ?>&emsp;
	<?php echo form_button(array('id'=>'expenditure_form_submit','content'=>'Submit',"class"=>"submit_button float_left"));?>
</div>


<?php  echo form_close(); ?>
</fieldset>

<script type='text/javascript'>
$(document).ready(function()
{	
	$("#expenditure_form_submit").click(function(){
		$("#expenditure_form").ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					if(response.success)
					{
						$("#table_holder").load('<?php echo site_url("expenses/view_all"); ?>');
						$("#TB_ajaxContent").load('<?php echo site_url("expenses/view_expenditures/$expense_id"); ?>/',
							function(result){
								$("#message_bar").removeClass('error_message');
	                            $("#message_bar").removeClass('warning_message');
	                            $("#message_bar").removeClass('success_message');
	                            $("#message_bar").addClass(response.message_class);
	                            $("#message_bar").html(response.message);
	                            $('#message_bar').fadeTo(5000, 1);
	                            $('#message_bar').fadeTo("fast",0);
							});
					}
					else
					{
						$("#message_bar").removeClass('error_message');
	                    $("#message_bar").removeClass('warning_message');
	                    $("#message_bar").removeClass('success_message');
	                    $("#message_bar").addClass(response.message_class);
	                    $("#message_bar").html(response.message);
	                    $('#message_bar').fadeTo(5000, 1);
	                    $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});

	$("#expenditure_form_cancel").click(function(){
		$("#expenditure_form_area").css('display','none');
        $("#expenditure_form_area").html('');
        $('#expenditures').css('display','block');
	});

	$("#payee").autocomplete("<?php echo site_url('expenses/suggest_payee');?>",{max:100,minChars:0,delay:10});
    $("#payee").result(function(event, data, formatted){});
	$("#payee").search();
});
</script>