<table width="100%" id="suppliers_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Account #</th>
        <th style="background-color: #0a6184; color:#FFF">Company Name</th>
        <th style="background-color: #0a6184; color:#FFF">Phone #</th>
        <th style="background-color: #0a6184; color:#FFF">Address</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>
    </tr></thead>
    <tbody>
<?php
	foreach($suppliers->result() as $supplier):
        $supplier->supplier_name = $this->Patient->patient_name($supplier->company_name);
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $supplier->account_number; ?></td>
        <td style="background-color: #DDD"><?php echo $supplier->supplier_name; ?></td>
        <td style="background-color: #DDD"><?php echo $supplier->phone_number; ?></td>
        <td style="background-color: #DDD"><?php echo nl2br($supplier->address_1); ?></td>
        <td style="background-color: #DDD">
            <?php echo anchor("suppliers/view_supplier/$supplier->person_id/width:350",
                    form_button(array('name'=>'submit','content'=>'View','class'=>'DTTT_button float_left')),
                    array("class"=>"thickbox vote_thickbox","title"=>"Supplier Details")); ?>
            &emsp;
            <?php
                echo form_button(array(
                    'name'=>'submit',
                    'onclick'=>'delete_supplier('.$supplier->person_id.')',
                    'content'=>'Delete',
                    'class'=>'DTTT_button float_left')
                );
            ?>
        </td>
        </tr>
<?php
	endforeach;
?>
	</tbody>
</table>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	tb_init("a.vote_thickbox");
    TableTools.DEFAULTS.aButtons = [ 
        "copy", 
        {
                    "sExtends": "print",
                    "sMessage": "",
        },
        {
            "sExtends":    "collection",
            "sButtonText": "Save",
            "aButtons":    [  
                {"sExtends": "xls","sTitle": "<?php echo $this->config->item('company'); ?> - Suppliers","mColumns": [ 0,1,2,3,4 ]}, 
                {"sExtends": "pdf","sPdfMessage": "","sPdfOrientation": "portrait","sTitle": "<?php echo $this->config->item('company'); ?> - Suppliers","mColumns": [ 0,1,2,3,4 ]} ]
        }, 
    ];
	$('#suppliers_table').dataTable({
		//"bPaginate": false,
		//"bLengthChange": false,
		//"bFilter": false,
		//"bSort": false,
		//"bInfo": false,
        "bStateSave": true,
        "sDom": 'T<"clear">lfrtip',
	});
});

function delete_supplier(person_id)
{
    if(confirm('Are you sure you want to delete this supplier?'))
    {
        $.post('<?php echo site_url("suppliers/delete_supplier"); ?>/' + person_id,
            {},
            function(response){
                if(response.success)
                {
                    $("#table_holder").load('<?php echo site_url("suppliers/view_all"); ?>',
                        function(result){
                            set_feedback(response.message,response.message_class,false);
                        });
                }
                else
                {
                    set_feedback(response.message,response.message_class,false);
                }
            },'json');
    }
}
</script>
