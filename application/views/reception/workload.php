<div id="archive">
<ul id="error_message_box"></ul>
<?php echo form_open("reception/workload_date_input",array('id'=>'workload_date_form')); ?>
<table width="100%"><tfoot><tr><td colspan="10"><br><hr></td></tr></tfoot>
<tbody><tr>
<td width="20%"><strong>Start Date: &emsp;</strong></td>
<td width="50%" align="right"><?php echo form_input(array(
                'name'=>'start_date',
                'id'=>'start_date',
                'value'=>$start_date_input, 
                'class'=>'date')
        ); ?></td><td width="30%"></td>
</tr><tr>
<td width="20%"><strong>End Date: &emsp;</strong></td>
<td width="50%" align="right"><?php echo form_input(array(
                'name'=>'end_date',
                'id'=>'end_date',
                'value'=>$end_date_input, 
                'class'=>'date')
        ); ?></td>
</tr><tr>
<td colspan="2"><?php echo form_button(array("id"=>"workload_date_submit","content"=>"Submit","class"=>"submit_button float_right"));?></td>
</tr></tbody></table>
<?php echo form_close(); ?>

<div id="page_subtitle">
<?php
    if($start_date == $end_date) echo $start_date;
    else echo $start_date . "&emsp;to&emsp;" . $end_date;
?>
</div>
<div id="workload_area">
<div id="workload_table_holder">
<table width="60%" id="workload_table" class="display">
    <tr>
        <th style="background-color: #DDD">Total Visits</th>
        <td style="background-color: #DDD"><?php echo $hospital_visits; ?></td>
    </tr>
    <tr>
        <th style="background-color: #DDD">Normal Visits</th>
        <td style="background-color: #DDD"><?php echo $normal; ?></td>
    </tr>
    <tr>
        <th style="background-color: #DDD">Reviews</th>
        <td style="background-color: #DDD"><?php echo $reviews; ?></td>
    </tr>
    <tr>
        <th style="background-color: #DDD">Visits Registered by <?php echo $employee; ?></th>
        <td style="background-color: #DDD"><?php echo $employee_visits; ?></td>
    </tr>
</table>
</div>
<br><br><hr>
<div id="examination_header_bar">Clinics</div>
<div id="clinics_workload_table_holder">
<table width="100%" id="clinics_workload_table" class="display">
    <thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Clinic Name</th>
        <th style="background-color: #0a6184; color:#FFF">Normal Visits</th>
        <th style="background-color: #0a6184; color:#FFF">Reviews</th>
        <th style="background-color: #0a6184; color:#FFF">Total</th>
    </tr></thead>
    <tbody>
<?php
    $total_normal = 0;
    $total_reviews = 0;
    $total_visits = 0;
    foreach($clinics as $clinic_code=>$counts):
        $clinic = $this->Consultation->clinic_name($clinic_code);
        $total_normal = $total_normal + $counts['normal'];
        $total_reviews = $total_reviews + $counts['reviews'];
        $total_visits = $total_visits + $counts['visits'];
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $clinic; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $counts['normal']; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $counts['reviews']; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $counts['visits']; ?></td>
        </tr>
<?php
    endforeach;
?>
    </tbody>
    <tfoot>
        <tr>
            <th style="background-color: #DDD">Total</th>
            <th style="background-color: #DDD" align="right"><?php echo $total_normal; ?></th>
            <th style="background-color: #DDD" align="right"><?php echo $total_reviews; ?></th>
            <th style="background-color: #DDD" align="right"><?php echo $total_visits; ?></th>
        </tr>

        <tr><td colspan="4">&nbsp;</td></tr>
        <tr>
            <th style="background-color: #0a6184; color:#FFF" colspan="3">Other Departments</th>
            <th style="background-color: #0a6184; color:#FFF">Total</th>
        </tr>
<?php
    foreach($depts as $dept=>$count):
?>
        <tr>
        <td style="background-color: #DDD" colspan="3"><?php echo $dept; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $count; ?></td>
        </tr>
<?php
    endforeach;
?>

    </tfoot>
</table>
</div>
</div>
</div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $('#clinics_workload_table').dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        //"bSort": false,
        "bInfo": false,
        "bStateSave": true,
    });

    Date.format = 'dd-mm-yyyy';
    $('.date').datepicker({
      changeMonth: true,
      changeYear: true,
      //minDate: new Date(2014, 0, 1),
      maxDate: "0",
      dateFormat: "dd-mm-yy",
    });

    $("#workload_date_submit").click(function(){
        $("#workload_date_form").ajaxSubmit({
            success:function(response)
            {
                if(response.form_validation)
                {
                    var html = "<table width='100%' height='100px'><tr><td align='center'><img src='<?php echo base_url()?>images/loading_animation.gif' alt='spinner' /></td></tr><table>";
                    $("#workload_area").html(html);
                    $("#TB_ajaxContent").load('<?php echo site_url("reception/view_workload"); ?>');
                }
                else
                {
                    $("#error_message_box").html(response.error_messages);
                }
            },
            dataType:'json',
        });
    });
});
</script>
