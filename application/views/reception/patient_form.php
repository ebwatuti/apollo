<div width="100%" id="message_bar"></div>
<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>

<fieldset id="employee_basic_info">
<legend>Patient Info</legend>
<?php echo form_open("reception/save_patient/$patient_info->patient_id",array('id'=>'patient_form')); ?>

<?php if($patient_info->patient_id): ?>
<div class="field_row clearfix">	
<?php echo form_label('Patient #:', 'patient_number'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'patient_number',
		'id'=>'patient_number',
		'readonly'=>'readonly',
		'value'=>$this->Patient->patient_number($patient_info->patient_id))
	);?>
	</div>
</div>
<?php else: ?>
<div class="field_row clearfix">	
<?php echo form_label('Patient #:', 'patient_number'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'patient_id',
		'id'=>'patient_id',)
	);?>
	</div>
</div>
<?php endif; ?>

<div class="field_row clearfix">	
<?php echo form_label('First Name:', 'first_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'first_name',
		'id'=>'first_name',
		'required'=>'required',
		'value'=>$patient_info->first_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Middle Name:', 'middle_name'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'middle_name',
		'id'=>'middle_name',
		'value'=>$patient_info->middle_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Last Name:', 'last_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'last_name',
		'id'=>'last_name',
		'required'=>'required',
		'value'=>$patient_info->last_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Gender:', 'gender',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('gender',array(
		'male'=>'male',
		'female'=>'female'),$patient_info->gender,'id=gender required'
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Phone Number:', 'phone_number'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'phone_number',
		'id'=>'phone_number',
		'value'=>$patient_info->phone_number));?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Date of Birth:', 'dob'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
                'name'=>'dob',
                'id'=>'dob',
                'placeholder'=>'dd-mm-yyyy',
				'title'=>'dd-mm-yyyy',
               	'value'=>$patient_info->dob, 
				'class'=>'date')
        ); ?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('National ID:', 'national_id'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'national_id',
		'id'=>'national_id',
		'value'=>$patient_info->national_id));?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Email:', 'email'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'email',
		'id'=>'email',
		'value'=>$patient_info->email));?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label('Institution:', 'institution_id'); ?>
	<div class='form_field'>
	<?php echo form_dropdown('institution_id', $institutions, $patient_info->institution_id);?>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_button(array("id"=>"patient_form_submit","content"=>"Submit","class"=>"submit_button float_right"));?>
</div>

<?php  echo form_close(); ?>
</fieldset>

<?php if($patient_info->patient_id): ?>
<fieldset id="employee_login_info">
<legend>Admission Information</legend>
<?php
echo form_open("reception/allocate_patient/$patient_info->patient_id",array('id'=>'allocation_form'));
?>

<div class="field_row clearfix">
<?php
	$clinics = array();
	$clinic_obj = $this->Consultation->get_clinics();
	foreach($clinic_obj as $clinic):
		$clinics[$clinic->clinic_code] = $clinic->clinic_name;
	endforeach;
	$clinics['lab'] = 'Lab';
	$clinics['nursing'] = 'Nursing';
	$clinics['pharmacy'] = 'Pharmacy';
?>	
<?php echo form_label('Clinic:', 'clinic'); ?>
	<div class='form_field'>
	<?php 
	echo form_dropdown('clinic', $clinics, '',$clinic); ?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Date of Visit:', 'encounter_start'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
                'name'=>'encounter_date',
                'id'=>'encounter_date',
                'placeholder'=>'dd-mm-yyyy',
				'title'=>'dd-mm-yyyy', 
				'class'=>'date')
        ); ?>
	</div>
</div>
<?php /*
<div class="field_row clearfix">	
<?php echo form_label('Triage:', 'triage'); ?>
	<div class='form_field'>
	<?php 
	echo form_checkbox(array(
		'name'=>'triage',
		'id'=>'triage',
		'value'=>'1',
		'checked'=>'checked'));?>
	</div>
</div>
*/ ?>

<div class="field_row clearfix">	
<?php echo form_label('Priority:', 'priority'); ?>
	<div class='form_field'>
	<?php echo form_dropdown('priority',array(
		1=>'Normal',
		2=>'Urgent',
		3=>'Emergency'),1,'id=priority'
	);?>
	</div>
</div>

<?php 
	if($patient_info->patient_id) $patient_info->last_encounter = $this->Encounter->last_visit($patient_info->patient_id);
	if($patient_info->last_encounter):
		$patient_info->last_visit = date("M j, Y",strtotime($patient_info->last_encounter->encounter_start))
		. ' (' .$this->Consultation->clinic_name($patient_info->last_encounter->encounter_type) . ')';
?>
<div class="field_row clearfix">	
<?php echo form_label('Last Visit:',''); ?>
	<div class='form_field'>
		<?php echo $patient_info->last_visit; ?>
	</div>
</div>
<?php
	endif;
?>
	
<div class="field_row clearfix">	
<?php echo form_label('Review:', 'review'); ?>
	<div class='form_field'>
	<?php 
	echo form_checkbox(array(
		'name'=>'review',
		'id'=>'review',
		'value'=>'1',));?>
	</div>
</div>
<?php
echo form_button(array(
	'name'=>'submit',
	'id'=>'allocation_form_submit',
	'content'=>'Submit',
	'class'=>'submit_button float_right')
);
?>
<?php 
echo form_close();
?>
</fieldset>
<?php endif; ?>

<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{	
	Date.format = 'dd-mm-yyyy';
	$('#dob').datepicker({
      changeMonth: true,
      changeYear: true,
      minDate: new Date(1900, 1, 1),
      maxDate: "0",
      dateFormat: "dd-mm-yy",
    });

    $('#encounter_date').datepicker({
      changeMonth: true,
      changeYear: true,
      minDate: "0",
      dateFormat: "dd-mm-yy",
    });
	
	$("#patient_form_submit").click(function(){
		$("#patient_form").ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					//tb_remove();
					if(response.success)
					{
						$("#TB_ajaxContent").load('<?php echo site_url("reception/view_patient"); ?>/'+response.patient_id,
							function(result){
								$("#message_bar").removeClass('error_message');
	                            $("#message_bar").removeClass('warning_message');
	                            $("#message_bar").removeClass('success_message');
	                            $("#message_bar").addClass(response.message_class);
	                            $("#message_bar").html(response.message);
	                            $('#message_bar').fadeTo(5000, 1);
	                            $('#message_bar').fadeTo("fast",0);
							});
						$("#TB_ajaxWindowTitle").html('Patient Details');
						$("#table_holder").load('<?php echo site_url("reception/view_all"); ?>');
					}
					else
					{
						$("#message_bar").removeClass('error_message');
	                    $("#message_bar").removeClass('warning_message');
	                    $("#message_bar").removeClass('success_message');
	                    $("#message_bar").addClass(response.message_class);
	                    $("#message_bar").html(response.message);
	                    $('#message_bar').fadeTo(5000, 1);
	                    $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});

	$('#allocation_form_submit').click(function(){
		$('#allocation_form').ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					$("#message_bar").removeClass('error_message');
                    $("#message_bar").removeClass('warning_message');
                    $("#message_bar").removeClass('success_message');
                    $("#message_bar").addClass(response.message_class);
                    $("#message_bar").html(response.message);
                    $('#message_bar').fadeTo(5000, 1);
                    $('#message_bar').fadeTo("fast",0);
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});
});
</script>
