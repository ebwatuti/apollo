<div id="archive">
<ul id="error_message_box"></ul>
<?php echo form_open("reception/archive_date_input",array('id'=>'archive_date_form')); ?>
<table width="100%"><tfoot><tr><td colspan="10"><br><hr></td></tr></tfoot>
<tbody><tr>
<td width="20%"><strong>Start Date: &emsp;</strong></td>
<td width="50%" align="right"><?php echo form_input(array(
                'name'=>'start_date',
                'id'=>'start_date',
                'value'=>$start_date_input, 
                'class'=>'date')
        ); ?></td><td width="30%"></td>
</tr>
<tr>
<td width="20%"><strong>End Date: &emsp;</strong></td>
<td width="50%" align="right"><?php echo form_input(array(
                'name'=>'end_date',
                'id'=>'end_date',
                'value'=>$end_date_input, 
                'class'=>'date')
        ); ?></td>
</tr>
<tr>
<tr>
<td width="20%"><strong>Clinic: &emsp;</strong></td>
<?php
    $options = array('');
    foreach($clinics as $item):
        $options[$item->clinic_code] = $item->clinic_name;
    endforeach;
?>
<td width="50%" align="right"><?php echo form_dropdown('clinic', $options,$clinic); ?></td>
</tr>
<tr>
<td colspan="2"><?php echo form_button(array("id"=>"archive_date_submit","content"=>"Submit","class"=>"submit_button float_right"));?></td>
</tr></tbody></table>
<?php echo form_close(); ?>

<?php if($clinic): ?>
<div id="page_title"><?php echo $this->Consultation->clinic_name($clinic); ?></div>
<?php endif; ?>
<div id="page_subtitle">
<?php
    if($start_date == $end_date) echo $start_date;
    else echo $start_date . "&emsp;to&emsp;" . $end_date;
?>
</div>
<div id="archive_table_holder">
<table width="100%" id="archive_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Date</th>
        <th style="background-color: #0a6184; color:#FFF">Patient #</th>
        <th style="background-color: #0a6184; color:#FFF">Patient Name</th>
        <th style="background-color: #0a6184; color:#FFF">Clinic</th>
        <th style="background-color: #0a6184; color:#FFF">Visit Type</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>
    </tr></thead>
    <tbody>
<?php
if($encounters->num_rows() > 0):
	foreach($encounters->result() as $encounter):
        $patient = $this->Patient->get_info($encounter->patient_id);
        $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
        $patient->patient_no = $this->Patient->patient_number($patient->patient_id);

        $encounter->date = date('j/n/Y g:i a',strtotime($encounter->encounter_start));
        $encounter->clinic = $this->Consultation->clinic_name($encounter->encounter_type);
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $encounter->date; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->patient_no; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->patient_name; ?></td>
        <td style="background-color: #DDD"><?php echo $encounter->clinic; ?></td>
        <td style="background-color: #DDD"><?php echo ($encounter->review == 1) ? 'Review' : 'Normal'; ?></td>
        <td style="background-color: #DDD">
            <?php
                $encounter_status = array(3,5);
                if(!in_array($encounter->encounter_status, $encounter_status)):
                    echo form_button(array(
                        'name'=>'submit',
                        'onclick'=>'delete_encounter('.$encounter->encounter_id.')',
                        'content'=>'Delete',
                        'class'=>'DTTT_button float_left')
                    );
                endif;
            ?>
        </td>
        </tr>
<?php
	endforeach;
endif;
?>
	</tbody>
</table>
</div>
</div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	$('#archive_table').dataTable({
		//"bPaginate": false,
		//"bLengthChange": false,
		//"bFilter": false,
		"bSort": false,
		//"bInfo": false,
        "bStateSave": true,
	});

    Date.format = 'dd-mm-yyyy';
    $('.date').datepicker({
      changeMonth: true,
      changeYear: true,
      //minDate: new Date(2014, 0, 1),
      maxDate: "0",
      dateFormat: "dd-mm-yy",
    });

    $("#archive_date_submit").click(function(){
        $("#archive_date_form").ajaxSubmit({
            success:function(response)
            {
                if(response.form_validation)
                {
                    var html = "<table width='100%' height='100px'><tr><td align='center'><img src='<?php echo base_url()?>images/loading_animation.gif' alt='spinner' /></td></tr><table>";
                    $("#archive_table_holder").html(html);
                    $("#TB_ajaxContent").load('<?php echo site_url("reception/view_archive"); ?>');
                }
                else
                {
                    $("#error_message_box").html(response.error_messages);
                }
            },
            dataType:'json',
        });
    });
});

function delete_encounter(encounter_id)
{
    if(confirm('Are you sure you want to delete this visit?'))
    {
        $.post('<?php echo site_url("reception/delete_encounter"); ?>/' + encounter_id,
            {},
            function(response){
                $("#message_bar").removeClass('error_message');
                $("#message_bar").removeClass('warning_message');
                $("#message_bar").removeClass('success_message');
                $("#message_bar").addClass(response.message_class);
                $("#message_bar").html(response.message);
                $('#message_bar').fadeTo(5000, 1);
                $('#message_bar').fadeTo("fast",0);
                    
                if(response.success)
                {
                    var html = "<table width='100%' height='100px'><tr><td align='center'><img src='<?php echo base_url()?>images/loading_animation.gif' alt='spinner' /></td></tr><table>";
                    $("#archive_table_holder").html(html);
                    $("#TB_ajaxContent").load('<?php echo site_url("reception/view_archive"); ?>');
                }
            },'json');
    }
}
</script>
