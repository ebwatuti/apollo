<?php $this->load->view("partial/header"); ?>
<div id="title_bar">
	<div id="page_title" style="margin-bottom:8px;">Reception</div>
	
</div>
<div id="sidebar">
	<?php echo anchor("reception/view_patient/-1/width:650",
	"<div class='submenu_button' style='float: left;'><span>New Patient</span></div>",
	array('title'=>'New Patient','class'=>'thickbox none',));
	?>
	<?php echo anchor("reception/view_workload/width:400",
	"<div class='submenu_button' style='float: left;'><span>Workload</span></div>",
	array('title'=>'Patients Workload','class'=>'thickbox none',));
	?>
	<?php echo anchor("reception/view_archive/width:850",
	"<div class='submenu_button' style='float: left;'><span>Archive</span></div>",
	array('title'=>'Patient Visits Archive','class'=>'thickbox none',));
	?>
</div>

<div id="table_holder">

</div>

<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	var html = "<table width='100%' height='100px'><tr><td align='center'><img src='<?php echo base_url()?>images/loading_animation.gif' alt='spinner' /></td></tr><table>";
	$("#table_holder").html(html);
	$("#table_holder").load('<?php echo site_url("reception/view_all"); ?>');		
});

</script>
