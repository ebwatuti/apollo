<table width="100%" id="patients_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Patient #</th>
        <th style="background-color: #0a6184; color:#FFF">Patient Name</th>
        <th style="background-color: #0a6184; color:#FFF">Age</th>
        <th style="background-color: #0a6184; color:#FFF">Phone #</th>
        <th style="background-color: #0a6184; color:#FFF">Institution</th>
        <th style="background-color: #0a6184; color:#FFF">Last Visit</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>
    </tr></thead>
    <tbody>
<?php
	foreach($patients->result() as $patient):
        $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
        if($patient->dob) $patient->age = $this->Patient->patient_age($patient->dob);
        $patient->patient_no = $this->Patient->patient_number($patient->patient_id);
        $patient->last_encounter = $this->Encounter->last_visit($patient->patient_id);
        $institution = $this->Institution->get_patient_institution($patient->patient_id);
        $patient->institution = $institution ? $institution->institution_name : '';

        if($patient->last_encounter):
            $patient->last_visit = date("M j, Y",strtotime($patient->last_encounter->encounter_start));
        endif;
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $patient->patient_no; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->patient_name; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $patient->age; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->phone_number; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->institution; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $patient->last_visit; ?></td>
        <td style="background-color: #DDD">
            <?php echo anchor("reception/view_patient/$patient->patient_id/width:750",
                    form_button(array('name'=>'submit','content'=>'View','class'=>'DTTT_button float_left')),
                    array("class"=>"thickbox vote_thickbox","title"=>"Patient Details")); ?>
            &emsp;
            <?php
                echo form_button(array(
                    'name'=>'submit',
                    'onclick'=>'delete_patient('.$patient->patient_id.')',
                    'content'=>'Delete',
                    'class'=>'DTTT_button float_left')
                );
            ?>
        </td>
        </tr>
<?php
	endforeach;
?>
	</tbody>
</table>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	tb_init("a.vote_thickbox");
    TableTools.DEFAULTS.aButtons = [ 
        "copy", 
        {
                    "sExtends": "print",
                    "sMessage": "",
        },
        {
            "sExtends":    "collection",
            "sButtonText": "Save",
            "aButtons":    [  
                {"sExtends": "xls","sTitle": "<?php echo $this->config->item('company'); ?> - Patients","mColumns": [ 0,1,2,3,4 ]}, 
                {"sExtends": "pdf","sPdfMessage": "","sPdfOrientation": "portrait","sTitle": "<?php echo $this->config->item('company'); ?> - Patients","mColumns": [ 0,1,2,3,4 ]} ]
        }, 
    ];
	$('#patients_table').dataTable({
        //"bPaginate": false,
		//"bLengthChange": false,
		//"bFilter": false,
		"bSort": false,
		//"bInfo": false,
        "bStateSave": true,
        //"sDom": 'T<"clear">lfrtip',
        "bServerSide": true,
        "sAjaxSource": "<?php echo site_url('reception/refresh_table'); ?>",
        "sServerMethod": "POST",
        //"iDeferLoading": <?php echo $this->Patient->count_all(); ?>,
        "fnServerData": function(sSource, aoData, fnCallback, oSettings){
            oSettings.jqXHR = $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback,
                "complete": function(xhr, status){tb_init("a.vote_thickbox");},
            });
        },
	});
});

function delete_patient(patient_id)
{
    if(confirm('Are you sure you want to delete this patient?'))
    {
        $.post('<?php echo site_url("reception/delete_patient"); ?>/' + patient_id,
            {},
            function(response){
                if(response.success)
                {
                    $("#table_holder").load('<?php echo site_url("reception/view_all"); ?>',
                        function(result){
                            set_feedback(response.message,response.message_class,false);
                        });
                }
                else
                {
                    set_feedback(response.message,response.message_class,false);
                }
            },'json');
    }
}
</script>
