<div id="archive">
<ul id="error_message_box"></ul>
<?php echo form_open("reports/archives_date_input",array('id'=>'archives_date_form')); ?>
<table width="100%"><tfoot><tr><td colspan="10"><br><hr></td></tr></tfoot>
<tbody><tr>
<td width="20%"><strong>Start Date: &emsp;</strong></td>
<td width="50%" align="right"><?php echo form_input(array(
                'name'=>'start_date',
                'id'=>'start_date',
                'value'=>$start_date_input, 
                'class'=>'date')
        ); ?></td><td width="30%"></td>
</tr>
<tr>
<td width="20%"><strong>End Date: &emsp;</strong></td>
<td width="50%" align="right"><?php echo form_input(array(
                'name'=>'end_date',
                'id'=>'end_date',
                'value'=>$end_date_input, 
                'class'=>'date')
        ); ?></td>
</tr>
<tr>
<td width="20%"><strong>Archive Type: &emsp;</strong></td>
<?php $options = array(
        'receipts'=>'Receipts',
        'consultation_notes'=>'Consultation Notes',
        'lab_reports'=>'Lab Reports',
        'prescriptions'=>'Prescriptions',
        'nursing_services'=>'Nursing Services'
    ); ?>
<td width="50%" align="right"><?php echo form_dropdown('archive_type', $options,$archive_type); ?></td>
</tr>
<?php if($archive_type == 'consultation_notes'): ?>
<tr>
<td width="20%"><strong>Clinic: &emsp;</strong></td>
<?php
    $options = array('');
    foreach($clinics as $item):
        $options[$item->clinic_code] = $item->clinic_name;
    endforeach;
?>
<td width="50%" align="right"><?php echo form_dropdown('clinic', $options,$clinic); ?></td>
</tr>
<?php endif; ?>
<tr>
<td colspan="2"><?php echo form_button(array("id"=>"archives_date_submit","content"=>"Submit","class"=>"submit_button float_right"));?></td>
</tr></tbody></table>
<?php echo form_close(); ?>
<div id="page_title"><?php echo $title ?></div>
<div id="page_subtitle">
<?php
    if($start_date == $end_date) echo $start_date;
    else echo $start_date . "&emsp;to&emsp;" . $end_date;
?>
</div>
<div id="workload_table_holder">
<table width="100%" id="archives_workload_table" class="display">
    <thead><tr>
    <?php foreach($thead as $th): ?>
        <th style="background-color: #0a6184; color:#FFF"><?php echo $th ?></th>
    <?php endforeach; ?>
    </tr></thead>
    <tbody>
<?php
    foreach($records as $record):
?>
       <tr>
       <?php foreach ($record as $td) : ?>
            <td style="background-color: #DDD" align="right"><?php echo $td; ?></td>
        <?php endforeach; ?>
        </tr>
<?php
    endforeach;
?>
    </tbody>
</table>
</div>
</div>

<div id="view_area"></div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
   $('#archives_workload_table').dataTable({
        //"bPaginate": false,
        //"bLengthChange": false,
        //"bFilter": false,
        //"bSort": false,
        //"bInfo": false,
        //"bStateSave": true,
    });

	Date.format = 'dd-mm-yyyy';
    $('.date').datepicker({
      changeMonth: true,
      changeYear: true,
      //minDate: new Date(2014, 0, 1),
      maxDate: "0",
      dateFormat: "dd-mm-yy",
    });

    $("#archives_date_submit").click(function(){
        $("#archives_date_form").ajaxSubmit({
            success:function(response)
            {
                if(response.form_validation)
                {
                    var html = "<table width='100%' height='100px'><tr><td align='center'><img src='<?php echo base_url()?>images/loading_animation.gif' alt='spinner' /></td></tr><table>";
                    $("#workload_table_holder").html(html);
                    $("#TB_ajaxContent").load('<?php echo site_url("reports/view_archives"); ?>');
                }
                else
                {
                    $("#error_message_box").html(response.error_messages);
                }
            },
            dataType:'json',
        });
    });
});

function view_sale(sale_id){
    var html = '<?php echo form_button(array("content"=>"Close","class"=>"submit_button float_right", "onclick"=>"close_view()")); ?>';
    $("#view_area").css('display','block');
    $("#view_area").load('<?php echo site_url("reports/sale_details"); ?>/' + sale_id,
        function(response){
            $("#view_area").append(html);
        });
    $('#archive').css('display','none');
}

function view_consultation(patient_id,consultation_id){
    var html = '<?php echo form_button(array("content"=>"Close","class"=>"submit_button float_right", "onclick"=>"close_view()")); ?>';
    $("#view_area").css('display','block');
    $("#view_area").load('<?php echo site_url("reports/consultation_summary"); ?>/' + patient_id + '/' + consultation_id,
        function(response){
            $("#view_area").append(html);
        });
    $('#archive').css('display','none');
}

function view_prescription(invoice_id){
    var html = '<?php echo form_button(array("content"=>"Close","class"=>"submit_button float_right", "onclick"=>"close_view()")); ?>';
    $("#view_area").css('display','block');
    $("#view_area").load('<?php echo site_url("reports/view_prescription"); ?>/' + invoice_id,
        function(response){
            $("#view_area").append(html);
        });
    $('#archive').css('display','none');
}

function view_lab_report(invoice_id){
    var html = '<?php echo form_button(array("content"=>"Close","class"=>"submit_button float_right", "onclick"=>"close_view()")); ?>';
    $("#view_area").css('display','block');
    $("#view_area").load('<?php echo site_url("reports/view_report"); ?>/' + invoice_id,
        function(response){
            $("#view_area").append(html);
        });
    $('#archive').css('display','none');
}

function view_services(invoice_id){
    var html = '<?php echo form_button(array("content"=>"Close","class"=>"submit_button float_right", "onclick"=>"close_view()")); ?>';
    $("#view_area").css('display','block');
    $("#view_area").load('<?php echo site_url("reports/view_services"); ?>/' + invoice_id,
        function(response){
            $("#view_area").append(html);
        });
    $('#archive').css('display','none');
}

function close_view(){
    $("#view_area").css('display','none');
    $("#view_area").html('&ensp;');
    $('#archive').css('display','block');
}
</script>
