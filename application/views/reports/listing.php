
<?php $this->load->view("partial/header"); ?>
<div id="page_title" style="margin-bottom:8px;">Reports</div>
<ul id="report_list">
	<li><h3>Ngong Rd</h3>
		<ul>
			<li><?php echo anchor("reports/view_workload/width:550",'Workload',array("class"=>"thickbox vote_thickbox","title"=>"Workload")); ?></li>
			<li><?php echo anchor("reports/view_morbidity/width:550",'Morbidity',array("class"=>"thickbox vote_thickbox","title"=>"Morbidity")); ?></li>
			<li><?php echo anchor("reports/view_sales/width:550",'Sales',array("class"=>"thickbox vote_thickbox","title"=>"Sales")); ?></li>
			<li><?php echo anchor("reports/view_archives/width:1050",'Archives',array("class"=>"thickbox vote_thickbox","title"=>"Archives")); ?></li>
			<li><?php echo anchor("reports/view_institutions_workload/width:950",'Institutions',array("class"=>"thickbox vote_thickbox","title"=>"Institutions Workload")); ?></li>
			<li><?php echo anchor("reports/export_contacts/width:750",'Patient Contacts',array("class"=>"thickbox vote_thickbox","title"=>"Patient Contacts")); ?></li>
		</ul>
	</li>
	
	
</ul>

<?php $this->load->view("partial/footer"); ?>


