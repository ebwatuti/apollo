<ul id="error_message_box"></ul>
<?php echo form_open("reports/institutions_workload_date_input",array('id'=>'workload_date_form')); ?>
<table width="100%"><tfoot><tr><td colspan="10"><br><hr></td></tr></tfoot>
<tbody><tr>
<td width="10%"><strong>Month: &emsp;</strong></td>
<td width="10%" align="right"><?php echo form_input(array(
                'name'=>'month',
                'id'=>'month',
                'value'=>$month_input, 
                'class'=>'date')
        ); ?></td><td width="80%"></td>
</tr>
<tr>
<td width="10%"><strong>Institution: &emsp;</strong></td>
<?php
    $options = array('');
    foreach($institutions as $item):
        $options[$item->institution_id] = $item->institution_name;
    endforeach;
?>
<td width="10%" align="right"><?php echo form_dropdown('institution', $options,$institution); ?></td>
</tr>
<tr>
<td colspan="2"><?php echo form_button(array("id"=>"workload_date_submit","content"=>"Submit","class"=>"submit_button float_right"));?></td>
</tr></tbody></table>
<?php echo form_close(); ?>
<div id="page_title"><?php echo $title ?></div>
<div id="page_subtitle">
<?php
    echo $month;
?>
</div>
<div id="workload_table_holder">
<table width="100%" id="institutions_workload_table" class="display">
<?php if(!$institution): ?>

    <thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Institution</th>
        <th style="background-color: #0a6184; color:#FFF">Patients</th>
        <th style="background-color: #0a6184; color:#FFF">Discounts</th>
        <th style="background-color: #0a6184; color:#FFF">Value</th>
        <th style="background-color: #0a6184; color:#FFF">Outstanding Debt</th>
    </tr></thead>
    <tbody>
<?php
    $total_discounts = 0;
    $total_value = 0;
    $total_debt = 0;
    $total_patients = 0;
    foreach($workloads as $workload):
        if($workload['value'] == 0 && $workload['discount'] == 0 ) continue;
        $total_discounts = $total_discounts + $workload['discount'];
        $total_value = $total_value + $workload['value'];
        $total_debt = $total_debt + $workload['outstanding_debt'];
        $total_patients = $total_patients + $workload['patients'];
?>
       <tr>
            <th style="background-color: #DDD"><?php echo $workload['name']; ?></th>
            <td style="background-color: #DDD" align="right"><?php echo $workload['patients']; ?></td>
            <td style="background-color: #DDD" align="right"><?php echo to_currency($workload['discount']); ?></td>
            <td style="background-color: #DDD" align="right"><?php echo to_currency($workload['value']); ?></td>
            <td style="background-color: #DDD" align="right"><?php echo to_currency($workload['outstanding_debt']); ?></td>
        </tr>
<?php
    endforeach;
?>
    </tbody>
    <tfoot><tr>
        <th style="background-color: #DDD">Total</th>
        <th style="background-color: #DDD" align="right"><?php echo $total_patients; ?></td>
        <th style="background-color: #DDD" align="right"><?php echo to_currency($total_discounts); ?></td>
        <th style="background-color: #DDD" align="right"><?php echo to_currency($total_value); ?></td>
        <th style="background-color: #DDD" align="right"><?php echo to_currency($total_debt); ?></td>
    </tr></tfoot>

<?php else: ?>

     <thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Date</th>
        <th style="background-color: #0a6184; color:#FFF">Patient #</th>
        <th style="background-color: #0a6184; color:#FFF">Name</th>
        <?php 
        foreach($headers as $header):
            $totals[$header] = 0;
        ?>
        <th style="background-color: #0a6184; color:#FFF"><?php echo $header ?></th>
        <?php endforeach; ?>
        <th style="background-color: #0a6184; color:#FFF">Total</th>
    </tr></thead>
    <tbody>
<?php
    foreach($workloads as $workload):
        $workload_total = 0;
        if(array_sum($workload['categories']) == 0) continue;
?>
        <tr>
            <td style="background-color: #DDD"><?php echo $workload['date']; ?></td>
            <td style="background-color: #DDD"><?php echo $workload['patient_no']; ?></td>
            <td style="background-color: #DDD"><?php echo $workload['name']; ?></td>
            <?php
            foreach($headers as $header):
                $workload_total = $workload_total + $workload['categories'][$header];
                $totals[$header] = $totals[$header] + $workload['categories'][$header];
            ?>
            <td style="background-color: #DDD" align="right"><?php echo to_currency($workload['categories'][$header]); ?></td>
            <?php endforeach; ?>
            <td style="background-color: #DDD" align="right"><?php echo to_currency($workload_total); ?></td>
        </tr>
<?php endforeach; ?>
    </tbody>
    <tfoot><tr>
        <th style="background-color: #DDD" colspan="3">Total</th>
        <?php foreach($headers as $header): ?>
        <th style="background-color: #DDD" align="right"><?php echo to_currency($totals[$header]); ?></td>
        <?php endforeach; ?>
        <th style="background-color: #DDD" align="right"><?php echo to_currency(array_sum($totals)); ?></td>
    </tr></tfoot>

<?php endif; ?>
</table>
</div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    TableTools.DEFAULTS.aButtons = [ 
        "copy", 
        {
                    "sExtends": "print",
                    "sMessage": "<?php echo $this->config->item('company') .' - '. $title; ?>",
        },
        {
            "sExtends":    "collection",
            "sButtonText": "Save",
            "aButtons":    [  
                {"sExtends": "xls","sTitle": "<?php echo $this->config->item('company') .' - '. $title; ?>"}, 
                {"sExtends": "pdf","sPdfMessage": "<?php
    if($start_date == $end_date) echo $start_date;
    else echo $start_date . ' to ' . $end_date;
?>","sPdfOrientation": "<?php echo $institution ? 'landscape' : 'portrait'; ?>","sTitle": "<?php echo $this->config->item('company') .' - '. $title; ?>"} ]
        }, 
    ];

    $('#institutions_workload_table').dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        //"bFilter": false,
        //"bSort": false,
        //"bInfo": false,
        "bStateSave": true,
        "sDom": 'T<"clear">lfrtip',
    });

	Date.format = 'dd-mm-yyyy';
    $('.date').MonthPicker({ShowIcon: false});

    $("#workload_date_submit").click(function(){
        $("#workload_date_form").ajaxSubmit({
            success:function(response)
            {
                if(response.form_validation)
                {
                    var html = "<table width='100%' height='100px'><tr><td align='center'><img src='<?php echo base_url()?>images/loading_animation.gif' alt='spinner' /></td></tr><table>";
                    $("#workload_table_holder").html(html);
                    $("#TB_ajaxContent").load('<?php echo site_url("reports/view_institutions_workload"); ?>');
                }
                else
                {
                    $("#error_message_box").html(response.error_messages);
                }
            },
            dataType:'json',
        });
    });
});
</script>
