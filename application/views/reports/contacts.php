<table width="100%" id="contacts_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Patient #</th>
        <th style="background-color: #0a6184; color:#FFF">Patient Name</th>
        <th style="background-color: #0a6184; color:#FFF">Age</th>
        <th style="background-color: #0a6184; color:#FFF">Phone #</th>
        <th style="background-color: #0a6184; color:#FFF">Email</th>
    </tr></thead>
    <tbody>
<?php
	foreach($patients->result() as $patient):
        $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
        if($patient->dob) $patient->age = $this->Patient->patient_age($patient->dob);
        $patient->patient_no = $this->Patient->patient_number($patient->patient_id);
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $patient->patient_no; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->patient_name; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $patient->age; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->phone_number; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->email; ?></td>
        </tr>
<?php
	endforeach;
?>
	</tbody>
</table>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	TableTools.DEFAULTS.aButtons = [ 
        "copy", 
        {
                    "sExtends": "print",
                    "sMessage": "",
        },
        {
            "sExtends":    "collection",
            "sButtonText": "Save",
            "aButtons":    [  
                {"sExtends": "xls","sTitle": "<?php echo $this->config->item('company'); ?> - Patient Contacts","mColumns": [ 0,1,2,3,4 ]}, 
                {"sExtends": "pdf","sPdfMessage": "","sPdfOrientation": "portrait","sTitle": "<?php echo $this->config->item('company'); ?> - Patient Contacts","mColumns": [ 0,1,2,3,4 ]} ]
        }, 
    ];
	$('#contacts_table').dataTable({
        //"bPaginate": false,
		//"bLengthChange": false,
		//"bFilter": false,
		//"bSort": false,
		//"bInfo": false,
        //"bStateSave": true,
        "sDom": 'T<"clear">lfrtip',
	});
});
</script>
