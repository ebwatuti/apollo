<div id="archive">
<ul id="error_message_box"></ul>
<?php echo form_open("reports/morbidity_date_input",array('id'=>'morbidity_date_form')); ?>
<table width="100%"><tfoot><tr><td colspan="10"><br><hr></td></tr></tfoot>
<tbody><tr>
<td width="20%"><strong>Start Date: &emsp;</strong></td>
<td width="50%" align="right"><?php echo form_input(array(
                'name'=>'start_date',
                'id'=>'start_date',
                'value'=>$start_date_input, 
                'class'=>'date')
        ); ?></td><td width="30%"></td>
</tr><tr>
<td width="20%"><strong>End Date: &emsp;</strong></td>
<td width="50%" align="right"><?php echo form_input(array(
                'name'=>'end_date',
                'id'=>'end_date',
                'value'=>$end_date_input, 
                'class'=>'date')
        ); ?></td>
</tr><tr>
<td width="20%"><strong>Clinic: &emsp;</strong></td>
<?php
    $clinics = array('');
    $clinic_obj = $this->Consultation->get_clinics();
    foreach($clinic_obj as $clinic):
        $clinics[$clinic->clinic_code] = $clinic->clinic_name;
    endforeach;
?>
<td width="50%" align="right"><?php echo form_dropdown('morbidity_clinic', $clinics,''); ?></td>
</tr><tr>
<td width="20%"><strong>Gender: &emsp;</strong></td>
<td width="50%" align="right">
    <?php echo form_dropdown('gender', array(''=>'','male'=>'Male','female'=>'Female'), $gender); ?>
</td><td width="30%"></td>
</tr><tr>
<td width="20%"><strong>Age range: &emsp;</strong></td>
<td width="50%" align="right"><?php echo form_input(array(
                'name'=>'start_age',
                'id'=>'start_age',
                'size'=>2,
                'value'=>$start_age)
        ); ?>
        &emsp;to&emsp;
        <?php echo form_input(array(
                'name'=>'end_age',
                'id'=>'end_age',
                'size'=>2,
                'value'=>$end_age)
        ); ?></td>
</tr><tr>
<td colspan="2"><?php echo form_button(array("id"=>"morbidity_date_submit","content"=>"Submit","class"=>"submit_button float_right"));?></td>
</tr></tbody></table>
<?php echo form_close(); ?>

<div id="page_title">
<?php
    echo $title;
?>
</div>
<div id="page_subtitle">
<?php
    if($gender) echo ucwords($gender).' patients ';
    if($start_age !== '' && $end_age !== ''):
        echo ($gender) ? 'aged ' : 'Patients aged ';
        if($start_age == $end_age) echo $start_age.' yrs';
        else echo $start_age.' to '.$end_age.' yrs';
    endif;
    echo '<br />';
    if($start_date == $end_date) echo $start_date;
    else echo $start_date . "&emsp;to&emsp;" . $end_date;
?>
</div>
</div>
<div id="morbidity_table_holder">
<table width="100%" id="morbidity_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">ICD10 Code</th>
        <th style="background-color: #0a6184; color:#FFF">Description</th>
        <th style="background-color: #0a6184; color:#FFF">Cases</th>
    </tr></thead>
    <tbody>
<?php
	foreach($morbidity as $diagnosis_code=>$count):
        if($count <= 0) continue;
        $description = $this->Consultation->get_diagnosis_info($diagnosis_code)->description;
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $diagnosis_code; ?></td>
        <td style="background-color: #DDD"><?php echo $description; ?></td>
        <td style="background-color: #DDD"><?php echo $count; ?></td>
        </tr>
<?php
	endforeach;
?>
	</tbody>
</table>
</div>
</div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    TableTools.DEFAULTS.aButtons = [ 
        "copy", 
        {
                    "sExtends": "print",
                    "sMessage": "<?php echo $this->config->item('company') .' - '. $title; ?>",
        },
        {
            "sExtends":    "collection",
            "sButtonText": "Save",
            "aButtons":    [  
                {"sExtends": "xls","sTitle": "<?php echo $this->config->item('company') .' - '. $title; ?>"}, 
                {"sExtends": "pdf","sPdfMessage": "<?php
    if($gender) echo ucwords($gender).' patients ';
    if($start_age !== '' && $end_age !== ''):
        echo ($gender) ? 'aged ' : 'Patients aged ';
        if($start_age == $end_age) echo $start_age.' yrs';
        else echo $start_age.' to '.$end_age.' yrs';
    endif;
    echo '<br />';
    if($start_date == $end_date) echo $start_date;
    else echo $start_date . ' to ' . $end_date;
?>","sPdfOrientation": "portrait","sTitle": "<?php echo $this->config->item('company') .' - '. $title; ?>"} ]
        }, 
    ];
	$('#morbidity_table').dataTable({
		//"bPaginate": false,
		//"bLengthChange": false,
		//"bFilter": false,
		"bSort": false,
		//"bInfo": false,
        //"bStateSave": true,
        "sDom": 'T<"clear">lfrtip',
	});

    Date.format = 'dd-mm-yyyy';
    $('.date').datepicker({
      changeMonth: true,
      changeYear: true,
      //minDate: new Date(2014, 0, 1),
      maxDate: "0",
      dateFormat: "dd-mm-yy",
    });

    $("#morbidity_date_submit").click(function(){
        $("#morbidity_date_form").ajaxSubmit({
            success:function(response)
            {
                if(response.form_validation)
                {
                    var html = "<table width='100%' height='100px'><tr><td align='center'><img src='<?php echo base_url()?>images/loading_animation.gif' alt='spinner' /></td></tr><table>";
                    $("#morbidity_table_holder").html(html);
                    $("#TB_ajaxContent").load('<?php echo site_url("reports/view_morbidity"); ?>');
                }
                else
                {
                    $("#error_message_box").html(response.error_messages);
                }
            },
            dataType:'json',
        });
    });
});
</script>
