<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>

<fieldset id="employee_basic_info">
<legend>Chapter Info</legend>
<?php echo form_open("incomes/save_chapter/$chapter_info->chapter_id",array('id'=>'chapter_form')); ?>

<div class="field_row clearfix">	
<?php echo form_label('Chapter #:', 'chapter_no',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'chapter_no',
		'id'=>'chapter_no',
		'value'=>$chapter_info->chapter_no)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Description:', 'description',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'description',
		'id'=>'description',
		'value'=>$chapter_info->description)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Source:', 'source'); ?>
	<div class='form_field'>
	<?php
	$options = array('income'=>'');
	foreach($this->Item->get_categories()->result() as $category):
		$options['category:'.$category->category] = $category->category;
	endforeach;
	?>
	<?php echo form_dropdown('source', $options, $chapter_info->source);?>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_button(array("id"=>"chapter_form_submit","content"=>"Submit","class"=>"submit_button float_left"));?>
	<?php echo form_button(array("id"=>"chapter_form_cancel","content"=>"Cancel","class"=>"submit_button float_right"));?>
</div>

<?php  echo form_close(); ?>
</fieldset>

<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{	
	$("#chapter_form_submit").click(function(){
		$("#chapter_form").ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					//tb_remove();
					if(response.success)
					{
						$("#table_holder").load('<?php echo site_url("incomes/view_all"); ?>');
						$("#TB_ajaxContent").load('<?php echo site_url("incomes/view_chapters"); ?>',
							function(result){
								$("#message_bar").removeClass('error_message');
	                            $("#message_bar").removeClass('warning_message');
	                            $("#message_bar").removeClass('success_message');
	                            $("#message_bar").addClass(response.message_class);
	                            $("#message_bar").html(response.message);
	                            $('#message_bar').fadeTo(5000, 1);
	                            $('#message_bar').fadeTo("fast",0);
							});
					}
					else
					{
						$("#message_bar").removeClass('error_message');
	                    $("#message_bar").removeClass('warning_message');
	                    $("#message_bar").removeClass('success_message');
	                    $("#message_bar").addClass(response.message_class);
	                    $("#message_bar").html(response.message);
	                    $('#message_bar').fadeTo(5000, 1);
	                    $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});

	$("#chapter_form_cancel").click(function(){
		$("#chapter_form_area").css('display','none');
	    $("#chapter_form_area").html('&ensp;');
	    $('#chapters').css('display','block');
	});
});
</script>