<?php $this->load->view("partial/header"); ?>
<div id="title_bar">
	<div id="page_title" style="margin-bottom:8px;">Income</div>
	<div id="page_subtitle"><?php echo $period; ?></div>
</div>

<div id="sidebar">
	<?php echo form_open("incomes/incomes_date_input",array('id'=>'incomes_date_form')); ?>
	<strong>Month: &emsp;</strong><?php echo form_input(array(
                'name'=>'month',
                'id'=>'month',
                'value'=>$month_input, 
                'class'=>'date')
        ); ?>
    <?php echo form_close(); ?>

	<?php echo anchor("incomes/new_income/width:550",
	"<div class='submenu_button' style='float: left;'><span>New Income</span></div>",
	array('title'=>'New Income','class'=>'thickbox none',));
	?>
    <?php echo anchor("incomes/view_chapters/width:750",
    "<div class='submenu_button' style='float: left;'><span>Chapters</span></div>",
    array('title'=>'Income Chapters','class'=>'thickbox none',));
    ?>
</div>

<div id="table_holder">

</div>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	var html = "<table width='100%' height='100px'><tr><td align='center'><img src='<?php echo base_url()?>images/loading_animation.gif' alt='spinner' /></td></tr><table>";
	$("#table_holder").html(html);
	$("#table_holder").load('<?php echo site_url("incomes/view_all"); ?>');

	Date.format = 'dd-mm-yyyy';
    $('.date').MonthPicker({ShowIcon: false});

    $("#month").change(function(){
        $("#incomes_date_form").ajaxSubmit({
            success:function(response)
            {
                if(response.form_validation)
                {
                    $("#table_holder").html(html);
					$("#table_holder").load('<?php echo site_url("incomes/view_all"); ?>');
					$("#page_subtitle").html(response.period);
                }
                else
                {
                    $("#error_message_box").html(response.error_messages);
                }
            },
            dataType:'json',
        });
    });	
});

</script>
