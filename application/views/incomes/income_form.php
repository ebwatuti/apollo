<div width="100%" id="message_bar"></div>
<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>

<fieldset id="supplier_basic_info">
<legend>Income Info</legend>
<?php echo form_open("incomes/save_income/$income_info->income_id",array('id'=>'income_form')); ?>

<div class="field_row clearfix">
<strong><?php echo form_label('Income #:', 'income_no',array('class'=>'wide')); ?></strong>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'income_no',
		'id'=>'income_no',
		'value'=>$income_info->income_no)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<strong><?php echo form_label('Description:', 'description',array('class'=>'wide required')); ?></strong>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'description',
		'id'=>'description',
		'value'=>$income_info->description)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<strong><?php echo form_label('Chapter:', 'chapter',array('class'=>'wide required')); ?></strong>
	<div class='form_field'>
	<?php
	$options = array();
	foreach($this->Income->get_chapters()->result() as $chapter):
		if($chapter->source != 'income' ) continue;
		$options[$chapter->chapter_id] = "Chapter $chapter->chapter_no: $chapter->description";
	endforeach;
	?>
	<?php echo form_dropdown('chapter', $options, $income_info->chapter);?>
	</div>
</div>

<div class="field_row clearfix">
<strong><?php echo form_label('Category:', 'category',array('class'=>'wide')); ?></strong>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'category',
		'id'=>'category',
		'value'=>$income_info->category)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<strong><?php echo form_label('Subcategory:', 'subcategory',array('class'=>'wide')); ?></strong>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'subcategory',
		'id'=>'subcategory',
		'value'=>$income_info->subcategory)
	);?>
	</div>
</div>

<?php $income_info->amount = $this->Income->get_income_revenue(date('Y-m-01'),$income_info->chapter,$income_info->category,$income_info->subcategory,$income_info->income_id); ?>
<div class="field_row clearfix">
<strong><?php echo form_label(date('M Y').' Revenue:', 'amount',array('class'=>'wide required')); ?></strong>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'amount',
		'id'=>'amount',
		'value'=>$income_info->amount)
	);?>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_button(array("id"=>"income_form_submit","content"=>"Submit","class"=>"submit_button float_right"));?>
</div>

<?php  echo form_close(); ?>
</fieldset>

<script type='text/javascript'>
$(document).ready(function()
{	
	$('#budgets_table').dataTable({
        //"bPaginate": false,
		//"bLengthChange": false,
		//"bFilter": false,
		"bSort": false,
		//"bInfo": false,
        //"bStateSave": true,
        //"sDom": 'T<"clear">lfrtip',
	});

	$("#category").autocomplete("<?php echo site_url('incomes/suggest_category');?>",{max:100,minChars:0,delay:10});
    $("#category").result(function(event, data, formatted){});
	$("#category").search();

	$("#subcategory").autocomplete("<?php echo site_url('incomes/suggest_subcategory');?>",{max:100,minChars:0,delay:10});
    $("#subcategory").result(function(event, data, formatted){});
	$("#subcategory").search();

	$("#income_form_submit").click(function(){
		$("#income_form").ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					if(response.success)
					{
						$("#table_holder").load('<?php echo site_url("incomes/view_all"); ?>');
						tb_remove();
						set_feedback(response.message,response.message_class,false);
					}
					else
					{
						$("#message_bar").removeClass('error_message');
	                    $("#message_bar").removeClass('warning_message');
	                    $("#message_bar").removeClass('success_message');
	                    $("#message_bar").addClass(response.message_class);
	                    $("#message_bar").html(response.message);
	                    $('#message_bar').fadeTo(5000, 1);
	                    $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});
});
</script>