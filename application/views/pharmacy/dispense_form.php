<?php 
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));
?>

<div id="queue_section">
<?php
    $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
    if($patient->dob) $patient->age = $this->Patient->patient_age($patient->dob);
    $patient->patient_no = $this->Patient->patient_number($patient->patient_id);
?>
<table width="100%">
    <tr><th style="background-color: #0a6184; color:#FFF" colspan="2" align="center">Patient Info</th></tr>
    <tr><td colspan="2">
    <div class="field_row clearfix">    
    <?php echo form_label('Patient #:'); ?>
        <div class='form_field'>
        <?php echo $patient->patient_no;?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Name:'); ?>
        <div class='form_field'>
        <?php echo $patient->patient_name;?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Age:'); ?>
        <div class='form_field'>
        <?php echo $patient->age;?>
        </div>
    </div>
    </td></tr>

    <tr><td colspan="2" width="100%">
        <?php
        echo form_button(array(
            'name'=>'submit',
            'id'=>'dispense_button',
            'content'=>'Dispense',
            'class'=>'submit_button float_right')
        );
        ?>
    </td></tr>

    <tr><td colspan="2" width="100%"><br><hr></td></tr>
    
    <?php if($consultation_notes->num_rows() > 0 ): ?>
    <tr><th style="background-color: #0a6184; color:#FFF" colspan="2" align="center">Consultation History</th></tr>
    <?php foreach($consultation_notes->result() as $consultation): ?>
        <?php 
            $employee = $this->Employee->get_info($consultation->consultant_id);

        ?>
        <tr style="cursor: pointer;" onclick="view_consultation_summary(<?php echo $consultation->consultation_id; ?>)" title="Click to view summary.">
            <td><?php echo "Dr. " . ucfirst(strtolower($employee->last_name)); ?></td>
            <td><?php echo date('j/n/Y',strtotime($consultation->consultation_start)); ?></td>
        </tr>
    <?php endforeach; ?>
    <tr><td colspan="2" width="100%"><br><hr></td></tr>
    <?php endif; ?>

</table>
</div>

<div class="consultation_summary" id="consultation_summary"></div>
<div id="register_wrapper">
<div width="100%" id="message_bar"></div>

<table id="register">
<thead>
<tr>
<th>Drug Name</th>
<th>Strength</th>
<th>Dose</th>
<th>Frequency</th>
<th>Duration</th>
<th>Disp #</th>
<th>Remarks</th>
<th>Forward to Nurse</th>
</tr>
</thead>
<tbody id="dispense_contents">

<?php
    foreach(array_reverse($drugs, true) as $item):
        if($item['status'] == 1):
?>
		<tr>
		<td style="align:center;"><?php echo $item['name']; ?></td>
        <td style="align:center;"><?php echo $item['strength']; ?></td>
        <td style="align:center;"><?php echo $item['dose']; ?></td>
        <td style="align:center;"><?php echo $item['frequency']; ?></td>
        <td style="align:center;"><?php echo $item['duration']; ?></td>
        <td style="align:center;"><?php echo $item['quantity']; ?></td>
        <td style="align:center;"><?php echo $item['remarks']; ?></td>
        <td style="align:center;"><?php echo form_checkbox('nursing', '1', FALSE, 'drug_id="'.$item['drug_id'].'" onChange="save_dispense_info(this);"'); ?></td>
        </tr>
<?php
        endif;
    endforeach; 
?>
</tbody>
</table>
</div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
   $("#dispense_button").click(function()
    {
        if (confirm('Are you sure you want to dispense these drugs?'))
        {
            $.post('<?php echo site_url($controller_name."/complete_dispense/".$patient->patient_id); ?>',
                {},
                function(response){
                    if(response.success){
                        $("#table_holder").load('<?php echo site_url("$controller_name/view_all"); ?>'); 
                        tb_remove();
                        set_feedback(response.message,'success_message',false);
                    }
                    else
                    {
                        $("#message_bar").removeClass('error_message');
                        $("#message_bar").removeClass('warning_message');
                        $("#message_bar").removeClass('success_message');
                        $("#message_bar").addClass(response.message_class);
                        $("#message_bar").html(response.message);
                        $('#message_bar').fadeTo(5000, 1);
                        $('#message_bar').fadeTo("fast",0);
                    } 
              },'json');
        }
    });
});

function view_consultation_summary(consultation_id){
    var html = '<?php echo form_button(array("content"=>"Close","class"=>"submit_button float_right", "onclick"=>"close_consultation_summary()")); ?>';
    $("#consultation_summary").css('display','block');
    $("#consultation_summary").load('<?php echo site_url("$controller_name/consultation_summary/$patient->patient_id"); ?>/' + consultation_id,
        function(response){
            $("#consultation_summary").append(html);
        });
    $('#register_wrapper').css('display','none');
}

function close_consultation_summary(){
    $("#consultation_summary").css('display','none');
    $("#consultation_summary").html('&ensp;');
    $('#register_wrapper').css('display','block');
}

function save_dispense_info(input)
{
    $.post('<?php echo site_url($controller_name."/dispense_info"); ?>/' + $(input).attr('drug_id') + '/' + $(input).attr('name'),
                {value: $(input).val()}
                );
}
</script>
