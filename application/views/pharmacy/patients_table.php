<table width="100%" id="patients_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Patient #</th>
        <th style="background-color: #0a6184; color:#FFF">Patient Name</th>
        <th style="background-color: #0a6184; color:#FFF">Priority</th>
        <th style="background-color: #0a6184; color:#FFF">Comments</th>
        <th style="background-color: #0a6184; color:#FFF">Prescription</th>
    </tr></thead>
    <tbody>
<?php
if($main_queue->num_rows() > 0):
	foreach($main_queue->result() as $request):
        $patient = $this->Patient->get_info($request->patient_id);
        $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
        $patient->patient_no = $this->Patient->patient_number($patient->patient_id);
        
        switch ($request->priority) {
            case 1:
                $patient->priority = 'Normal';
                break;
            case 2:
                $patient->priority = 'Urgent';
                break;
            case 3:
                $patient->priority = 'Emergency';
                break;
            default:
                $patient->priority = 'Normal';
                break;
        }
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $patient->patient_no; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->patient_name; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->priority; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo "&emsp;"; ?></td>
        <td style="background-color: #DDD">
			<?php echo anchor("pharmacy/dispense/$patient->patient_id/width:1150",
            form_button(array('name'=>'submit','content'=>'View','class'=>'DTTT_button float_left')),
            array("class"=>"thickbox vote_thickbox","title"=>"Prescription")); ?>
        </td>
        </tr>
<?php
	endforeach;
endif;
?>

<?php
if($pending_queue->num_rows() > 0):
    foreach($pending_queue->result() as $request):
        $patient = $this->Patient->get_info($request->patient_id);
        $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
        $patient->patient_no = $this->Patient->patient_number($patient->patient_id);
        if($request->service_time) $patient->last_seen = date("g:i a",strtotime($request->service_time));
        
        switch ($request->priority) {
            case 1:
                $patient->priority = 'Normal';
                break;
            case 2:
                $patient->priority = 'Urgent';
                break;
            case 3:
                $patient->priority = 'Emergency';
                break;
            default:
                $patient->priority = 'Normal';
                break;
        }
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $patient->patient_no; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->patient_name; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->priority; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo "Last seen at: " . $patient->last_seen; ?></td>
        <td style="background-color: #DDD">
            <?php echo anchor("pharmacy/dispense/$patient->patient_id/width:1150",
            form_button(array('name'=>'submit','content'=>'View','class'=>'DTTT_button float_left')),
            array("class"=>"thickbox vote_thickbox","title"=>"Prescription")); ?>
        </td>
        </tr>
<?php
    endforeach;
endif;
?>
	</tbody>
</table>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	tb_init("a.vote_thickbox");
	$('#patients_table').dataTable({
		//"bPaginate": false,
		//"bLengthChange": false,
		//"bFilter": false,
		"bSort": false,
		//"bInfo": false,
        "bStateSave": true,
	});
});
</script>
