<?php 
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));

echo form_open($controller_name."/add_drug",array('id'=>'add_drug_form','class'=>'add_drug_form','onkeypress'=>'return event.keyCode != 13;'));
?>
<label id="item_label" for="drug_item">Drug</label>
<?php echo form_input(array('name'=>'item','id'=>'drug_item','size'=>'40','palceholder'=>'Start typing drug name'));?>


</form>
<table id="register">
<thead>
<tr>
<th>Drug Name</th>
<th>Strength</th>
<th>Dose</th>
<th>Frequency</th>
<th>Duration</th>
<th>Disp #</th>
<th>Unit Price</th>
<th>Disc %</th>
<th>Total</th>
<th style="width:15%;">Remarks</th>
<th style="width:10%;"></th>
</tr>
</thead>
<tbody id="prescription_contents">

<?php foreach(array_reverse($drugs, true) as $item): ?>
		<tr>
		<td style="align:center;"><?php echo $item['name']; ?></td>
		<td style="align:center;"><?php echo $item['strength']; ?></td>
		<td style="align:center;"><?php echo form_input(array(
									'name'=>'dose',
									'value'=>$item['dose'],
									'size'=>2,
									'drug_id'=>$item['drug_id'],
									'onchange' => 'save_drug_instructions(this)')); ?></td>
		<td style="align:center;"><?php echo form_input(array(
									'name'=>'frequency',
									'value'=>$item['frequency'],
									'size'=>2,
									'drug_id'=>$item['drug_id'],
									'onchange' => 'save_drug_instructions(this)')); ?></td>
		<td style="align:center;"><?php echo form_input(array(
									'name'=>'duration',
									'value'=>$item['duration'],
									'size'=>2,
									'drug_id'=>$item['drug_id'],
									'onchange' => 'save_drug_instructions(this)')); ?></td>
		<td style="align:center;"><?php echo form_input(array(
									'name'=>'quantity',
									'value'=>$item['quantity'],
									'drug_id'=>$item['drug_id'],
									'type'=>'number',
									'min'=>'1',
                                    'max'=>$item['stock'],
									'onchange' => 'save_drug_instructions(this)')); ?></td>
		<td style="align:center;"><?php echo number_format($item['unit_price'],2,'.',''); ?></td>
		<td style="align:center;"><?php echo $item['discount']; ?></td>
		<td style="align:center;" class="drug_total" id="total_<?php echo $item['drug_id'];?>"><?php echo number_format(($item['quantity'] * $item['unit_price'] * (100 - $item['discount']) / 100),2,'.',''); ?></td>
        <td style="align:center;"><?php echo form_textarea(array(
									'name'=>'remarks',
									'value'=>$item['remarks'],
									'rows'=>'3',
									'cols'=>15,
									'drug_id'=>$item['drug_id'],
									'onchange' => 'save_drug_instructions(this)')); ?></td>
        <td><?php echo form_button(array(
        'content'=>'Delete',
        'class'=>'submit_button float_right',
        'drug_id'=>$item['drug_id'],
        'onclick'=>"delete_drug(this)"
    ));?></td>
		</tr>
		
		<tr style="height:3px">
		<td colspan=8 style="background-color:white"> </td>
		</tr>		</form>
	<?php endforeach; ?>
</tbody>
</table>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $('#prescription_total').text(function(){
			var sum = 0.00;
			$('.drug_total').each(function(){
				sum += parseFloat($(this).text());
			});
			return sum.toFixed(2);
		});
    $("#drug_item").autocomplete('<?php echo site_url($controller_name."/drug_search"); ?>',
    {
    	minChars:0,
    	max:100,
    	selectFirst: false,
       	delay:10,
    	formatItem: function(row) {
			return row[1];
		}
    });

    $("#drug_item").result(function(event, data, formatted)
    {
		$("#add_drug_form").ajaxSubmit({
			success:function(response){
				$("#message_bar").removeClass('error_message');
                $("#message_bar").removeClass('warning_message');
                $("#message_bar").removeClass('success_message');
                $("#message_bar").addClass(response.message_class);
                $("#message_bar").html(response.message);
                $('#message_bar').fadeTo(5000, 1);
                $('#message_bar').fadeTo("fast",0);
				
				if(response.success) $("#register_wrapper").load("<?php echo site_url($controller_name."/refresh_prescription"); ?>");
			},dataType:'json'});
    });
});

function save_drug_instructions(input)
{
	$.post('<?php echo site_url($controller_name."/drug_instructions"); ?>/' + $(input).attr('drug_id') + '/' + $(input).attr('name'),
                {value: $(input).val()},
            function(response){
        		$('#total_'+$(input).attr('drug_id')).html(response);        		

                $('#prescription_total').text(function(){
					var sum = 0.00;
					$('.drug_total').each(function(){
						sum += parseFloat($(this).text());
					});
					return sum.toFixed(2);
				});
      		}
                );
}

function delete_drug(button)
{
	$("#register_wrapper").load('<?php echo site_url($controller_name."/remove_drug"); ?>/' + $(button).attr('drug_id'));
}
</script>