<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>

<fieldset id="employee_basic_info">
<legend>Patient Info</legend>
<?php echo form_open("pharmacy/save_patient/",array('id'=>'patient_form')); ?>

<div class="field_row clearfix">	
<?php echo form_label('First Name:', 'first_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'first_name',
		'id'=>'first_name',
		'required'=>'required',
		'value'=>$patient_info->first_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Last Name:', 'last_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'last_name',
		'id'=>'last_name',
		'required'=>'required',
		'value'=>$patient_info->last_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Gender:', 'gender',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('gender',array(
		'male'=>'male',
		'female'=>'female'),$patient_info->gender,'id=gender required'
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Date of Birth:', 'dob'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
                'name'=>'dob',
                'id'=>'dob',
                'placeholder'=>'dd-mm-yyyy',
				'title'=>'dd-mm-yyyy',
               	'value'=>$patient_info->dob, 
				'class'=>'date')
        ); ?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Phone Number:', 'phone_number'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'phone_number',
		'id'=>'phone_number',
		'value'=>$patient_info->phone_number));?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Email:', 'email'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'email',
		'type'=>'email',
		'id'=>'email',
		'value'=>$patient_info->email)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Address:', 'address_1'); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
            'name'=>'address_1',
			'id'=>'address_1',
            'value'=>$patient_info->address_1,
            'rows'=>'5',
			'cols'=>'17',)); ?>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_button(array("id"=>"patient_form_submit","content"=>"Submit","class"=>"submit_button float_left"));?>
	<?php echo form_button(array("id"=>"patient_form_cancel","content"=>"Cancel","class"=>"submit_button float_right"));?>
</div>

<?php  echo form_close(); ?>
</fieldset>


<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{	
	Date.format = 'dd-mm-yyyy';
	$('.date').datepicker({
      changeMonth: true,
      changeYear: true,
      minDate: new Date(1900, 1, 1),
      maxDate: "0",
      dateFormat: "dd-mm-yy",
    });
	
	$("#patient_form_submit").click(function(){
		$("#patient_form").ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					//tb_remove();
					if(response.success)
					{
						$("#patient_form_area").css('display','none');
						$('#main_area').css('display','block');
						$("#patient_form_area").html('&nbsp;');
				        $("#queue_section").load('<?php echo site_url("pharmacy/refresh_patient_info"); ?>/'+response.patient_id,
							function(result){
								$("#message_bar").removeClass('error_message');
	                            $("#message_bar").removeClass('warning_message');
	                            $("#message_bar").removeClass('success_message');
	                            $("#message_bar").addClass(response.message_class);
	                            $("#message_bar").html(response.message);
	                            $('#message_bar').fadeTo(5000, 1);
	                            $('#message_bar').fadeTo("fast",0);
							});
					}
					else
					{
						$("#message_bar").removeClass('error_message');
	                    $("#message_bar").removeClass('warning_message');
	                    $("#message_bar").removeClass('success_message');
	                    $("#message_bar").addClass(response.message_class);
	                    $("#message_bar").html(response.message);
	                    $('#message_bar').fadeTo(5000, 1);
	                    $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});

	$("#patient_form_cancel").click(function(){
		$("#patient_form_area").css('display','none');
		$('#main_area').css('display','block');
		$("#patient_form_area").html('&ensp;');
	});
});
</script>