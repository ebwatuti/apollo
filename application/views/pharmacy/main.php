<?php $this->load->view("partial/header"); ?>
<div id="title_bar">
	<div id="page_title" style="margin-bottom:8px;">Prescriptions</div>
	
</div>
<div id="sidebar">
	<?php echo anchor("pharmacy/new_prescription/width:1150",
	"<div class='submenu_button' style='float: left;'><span>New Prescription</span></div>",
	array('title'=>'New Prescription','class'=>'thickbox none',));
	?>
	<?php echo anchor("pharmacy/view_drugs_list/width:950",
	"<div class='submenu_button' style='float: left;'><span>Drugs List</span></div>",
	array('title'=>'Drugs List','class'=>'thickbox none',));
	?>
	<?php echo anchor("orders/view_all/width:1150",
	"<div class='submenu_button' style='float: left;'><span>Orders</span></div>",
	array('title'=>'Orders','class'=>'thickbox none',));
	?>
	<?php echo anchor("pharmacy/view_workload/width:450",
	"<div class='submenu_button' style='float: left;'><span>Workload</span></div>",
	array('title'=>'Pharmacy Workload','class'=>'thickbox none',));
	?>
	<?php echo anchor("pharmacy/view_archive/width:950",
	"<div class='submenu_button' style='float: left;'><span>Archive</span></div>",
	array('title'=>'Prescriptions Archive','class'=>'thickbox none',));
	?>
</div>

<div id="table_holder">

</div>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	var html = "<table width='100%' height='100px'><tr><td align='center'><img src='<?php echo base_url()?>images/loading_animation.gif' alt='spinner' /></td></tr><table>";
	$("#table_holder").html(html);
	$("#table_holder").load('<?php echo site_url("pharmacy/view_all"); ?>');	

	var table_reload = setInterval(function(){$("#table_holder").load('<?php echo site_url("pharmacy/view_all"); ?>');},30000);	
});

</script>
