<?php if($patient_id): ?>
<?php
    $patient = $this->Patient->get_info($patient_id);
    $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
    if($patient->dob) $patient->age = $this->Patient->patient_age($patient->dob);
    $patient->patient_no = $this->Patient->patient_number($patient->patient_id);
?>
<table width="100%">
    <tr><th style="background-color: #0a6184; color:#FFF; color:#FFF" colspan="2" align="center">Patient Info</th></tr>
    <tr><td colspan="2">
    <div class="field_row clearfix">    
    <?php echo form_label('Patient #:'); ?>
        <div class='form_field'>
        <?php echo $patient->patient_no;?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Name:'); ?>
        <div class='form_field'>
        <?php echo $patient->patient_name;?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Age:'); ?>
        <div class='form_field'>
        <?php echo $patient->age;?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Total:'); ?>
        <div class='form_field' id="prescription_total">
        
        </div>
    </div>
    <?php
        echo form_button(array(
            'name'=>'submit',
            'onclick'=>'remove_patient()',
            'content'=>'Remove Patient',
            'class'=>'submit_button float_left')
        );
    ?>
    <?php
        echo form_button(array(
            'name'=>'submit',
            'patient_id'=>$patient_id,
            'onclick'=>'save_prescription(this)',
            'content'=>'Save Prescription',
            'class'=>'submit_button float_right')
        );
    ?>
    </td></tr>
    <tr><td colspan="2" width="100%"><br><hr></td></tr>

</table>
<?php else: ?>
	<div class="field_row clearfix">
	<label for="patient">Patient:</label>
	<div class='form_field'>
	<?php echo form_input(array('name'=>'patient','id'=>'patient','size'=>15,'placeholder'=>'patient name/#'));?>
	</div>
	</div>
<?php endif; ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $("#patient").autocomplete('<?php echo site_url("pharmacy/patient_search"); ?>',
    {
    	minChars:0,
    	delay:10,
    	max:100,
    	formatItem: function(row) {
			return row[1];
		}
    });

    $("#patient").result(function(event, data, formatted)
    {
		$("#queue_section").load('<?php echo site_url("pharmacy/refresh_patient_info"); ?>/' + $("#patient").val(),
			function(result){
				$("#register_wrapper").load('<?php echo site_url("pharmacy/refresh_prescription"); ?>');
			});	
    });
});

function remove_patient()
{
	$("#queue_section").load('<?php echo site_url("pharmacy/refresh_patient_info"); ?>');
}

function save_prescription(button)
{
    if (confirm('Are you sure you want to save this prescription?'))
    {
    	$.post('<?php echo site_url("pharmacy/save_prescription"); ?>/' + $(button).attr('patient_id'),
            {},
            function(response){
        		if(response.success)
    			{
    				tb_remove();
    				set_feedback(response.message,response.message_class,false);
    			}
    			else
    			{
    				$("#message_bar").removeClass('error_message');
                    $("#message_bar").removeClass('warning_message');
                    $("#message_bar").removeClass('success_message');
                    $("#message_bar").addClass(response.message_class);
                    $("#message_bar").html(response.message);
                    $('#message_bar').fadeTo(5000, 1);
                    $('#message_bar').fadeTo("fast",0);
    			}
      		},'json');
    }
}
</script>