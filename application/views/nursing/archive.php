<div id="archive">
<ul id="error_message_box"></ul>
<?php echo form_open("nursing/archive_date_input",array('id'=>'archive_date_form')); ?>
<table width="100%"><tfoot><tr><td colspan="10"><br><hr></td></tr></tfoot>
<tbody><tr>
<td width="10%"><strong>Start Date: &emsp;</strong></td>
<td width="20%"><?php echo form_input(array(
                'name'=>'start_date',
                'id'=>'start_date',
                'value'=>$start_date_input, 
                'class'=>'date')
        ); ?></td>
<td width="10%"><strong>End Date: &emsp;</strong></td>
<td width="20%"><?php echo form_input(array(
                'name'=>'end_date',
                'id'=>'end_date',
                'value'=>$end_date_input, 
                'class'=>'date')
        ); ?></td>
<td width="40%"><?php echo form_button(array("id"=>"archive_date_submit","content"=>"Submit","class"=>"submit_button float_left"));?></td>
</tr></tbody></table>
<?php echo form_close(); ?>

<div id="page_subtitle">
<?php
    if($start_date == $end_date) echo $start_date;
    else echo $start_date . "&emsp;to&emsp;" . $end_date;
?>
</div>
<div id="archive_table_holder">
<table width="100%" id="archive_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Date</th>
        <th style="background-color: #0a6184; color:#FFF">Patient #</th>
        <th style="background-color: #0a6184; color:#FFF">Patient Name</th>
        <th style="background-color: #0a6184; color:#FFF">Served by</th>
        <th style="background-color: #0a6184; color:#FFF">Services</th>
    </tr></thead>
    <tbody>
<?php
if($invoices->num_rows() > 0):
	foreach($invoices->result() as $invoice):
        $patient = $this->Patient->get_info($invoice->patient_id);
        $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
        $patient->patient_no = $this->Patient->patient_number($patient->patient_id);

        $invoice->date = date('j/n/Y g:i a',strtotime($invoice->service_time));
        $employee = $this->Employee->get_info($invoice->served_by);
        $employee->employee_name = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $invoice->date; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->patient_no; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->patient_name; ?></td>
        <td style="background-color: #DDD"><?php echo $employee->employee_name; ?></td>
        <td style="background-color: #DDD"><?php
        echo form_button(array(
            'name'=>'submit',
            'onclick'=>'view_invoice('.$invoice->invoice_id.')',
            'content'=>'View',
            'class'=>'DTTT_button float_left')
        );
        ?></td>
        </tr>
<?php
	endforeach;
endif;
?>
	</tbody>
</table>
</div>
</div>

<div id="invoice"></div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	$('#archive_table').dataTable({
		//"bPaginate": false,
		//"bLengthChange": false,
		//"bFilter": false,
		"bSort": false,
		//"bInfo": false,
        "bStateSave": true,
	});

    Date.format = 'dd-mm-yyyy';
    $('.date').datepicker({
      changeMonth: true,
      changeYear: true,
      //minDate: new Date(2014, 0, 1),
      maxDate: "0",
      dateFormat: "dd-mm-yy",
    });

    $("#archive_date_submit").click(function(){
        $("#archive_date_form").ajaxSubmit({
            success:function(response)
            {
                if(response.form_validation)
                {
                    var html = "<table width='100%' height='100px'><tr><td align='center'><img src='<?php echo base_url()?>images/loading_animation.gif' alt='spinner' /></td></tr><table>";
                    $("#archive_table_holder").html(html);
                    $("#TB_ajaxContent").load('<?php echo site_url("nursing/view_archive"); ?>');
                }
                else
                {
                    $("#error_message_box").html(response.error_messages);
                }
            },
            dataType:'json',
        });
    });
});

function view_invoice(invoice_id){
    var html = '<?php echo form_button(array("content"=>"Close","class"=>"submit_button float_right", "onclick"=>"close_invoice()")); ?>';
    $("#invoice").css('display','block');
    $("#invoice").load('<?php echo site_url("nursing/view_services"); ?>/' + invoice_id,
        function(response){
            $("#invoice").append(html);
        });
    $('#archive').css('display','none');
}

function close_invoice(){
    $("#invoice").css('display','none');
    $("#invoice").html('&ensp;');
    $('#archive').css('display','block');
}
</script>
