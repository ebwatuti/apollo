<div width="100%" id="message_bar"></div>
<div id="complaints">
<?php
    echo form_button(array(
        'name'=>'submit',
        'id'=>'new_complaint_button',
        'content'=>'New Complaint',
        'class'=>'submit_button float_right')
    );
?>
<br><br>
<table width="100%" id="complaints_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Complaint</th>
        <th style="background-color: #0a6184; color:#FFF">Category</th>
        <th style="background-color: #0a6184; color:#FFF">Subcategory</th>
        <th style="background-color: #0a6184; color:#FFF">Options</th>
        <th style="background-color: #0a6184; color:#FFF">Clinics</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>       
    </tr></thead>
    <tbody>
<?php
if($complaints->num_rows() > 0):
	foreach($complaints->result() as $complaint):
        $complaint = $this->Administration->get_complaint_info($complaint->complaint_id);
        $options = str_replace(',', ', ', $complaint->options);
        $clinic_codes = explode(',', $complaint->clinic);
        $clinics = '';
        foreach($clinic_codes as $clinic_code):
            $clinic_name = $this->Consultation->clinic_name($clinic_code);
            $clinics .= ($clinic_name ? $clinic_name : $clinic_code) . ', ';
        endforeach;
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $complaint->value; ?></td>
        <td style="background-color: #DDD"><?php echo $complaint->category; ?></td>
        <td style="background-color: #DDD"><?php echo $complaint->sub_category; ?></td>
        <td style="background-color: #DDD"><?php echo character_limiter($options, 25); ?></td>
        <td style="background-color: #DDD"><?php echo character_limiter($clinics, 25); ?></td>
        <td style="background-color: #DDD">
		<?php
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'edit_complaint('.$complaint->complaint_id.')',
                'content'=>'Edit',
                'class'=>'DTTT_button float_left')
            );
        ?>
        &emsp;
        <?php
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'delete_complaint('.$complaint->complaint_id.')',
                'content'=>'Delete',
                'class'=>'DTTT_button float_left')
            );
        ?>
        </td>
        </tr>
<?php
	endforeach;
endif;
?>
	</tbody>
</table>
</div>

<div id="complaint_form_area"></div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $('#complaints_table').dataTable({
        //"bPaginate": false,
        //"bLengthChange": false,
        //"bFilter": false,
        //"bSort": false,
        //"bInfo": false,
        "bStateSave": true,
    });

    $("#new_complaint_button").click(function(){
        $("#complaint_form_area").css('display','block');
        $("#complaint_form_area").load('<?php echo site_url("admin/view_complaint"); ?>');
        $('#complaints').css('display','none');
    });
});

function edit_complaint(complaint_id)
{
    $("#complaint_form_area").css('display','block');
    $("#complaint_form_area").load('<?php echo site_url("admin/view_complaint"); ?>/' + complaint_id);
    $('#complaints').css('display','none');
}

function delete_complaint(complaint_id)
{
    if(confirm('Are you sure you want to delete this complaint?'))
    {
        $.post('<?php echo site_url("admin/delete_complaint"); ?>/' + complaint_id,
            {},
            function(response){
                if(response.success)
                {
                    $("#TB_ajaxContent").load('<?php echo site_url("admin/view_complaints"); ?>',
                        function(result){
                            $("#message_bar").removeClass('error_message');
                            $("#message_bar").removeClass('warning_message');
                            $("#message_bar").removeClass('success_message');
                            $("#message_bar").addClass(response.message_class);
                            $("#message_bar").html(response.message);
                            $('#message_bar').fadeTo(5000, 1);
                            $('#message_bar').fadeTo("fast",0);
                        });
                }
                else
                {
                    $("#message_bar").removeClass('error_message');
                    $("#message_bar").removeClass('warning_message');
                    $("#message_bar").removeClass('success_message');
                    $("#message_bar").addClass(response.message_class);
                    $("#message_bar").html(response.message);
                    $('#message_bar').fadeTo(5000, 1);
                    $('#message_bar').fadeTo("fast",0);
                }
            },'json');
    }
}
</script>