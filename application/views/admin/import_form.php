<div width="100%" id="message_bar"></div>
<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>

<?php echo form_open_multipart("admin/import_sql",array('id'=>'import_form')); ?>
<fieldset id="supplier_basic_info">
<legend>Database File</legend>

<div class="field_row clearfix">
	<div class='form_field'>
	<?php echo form_input(array(
            'name'=>'sql_file',
			'id'=>'sql_file',
            'type'=>'file')); ?>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_button(array("id"=>"import_form_submit","content"=>"Submit","class"=>"submit_button float_left"));?>
	<?php echo form_button(array("id"=>"import_form_cancel","content"=>"Cancel","class"=>"submit_button float_right"));?>
</div>
<?php  echo form_close(); ?>
</fieldset>

<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{	
	$("#import_form_submit").click(function(){
		$("#import_form").ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					tb_remove();
					set_feedback(response.message,response.message_class,false);
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});

	$("#import_form_cancel").click(function(){
		tb_remove();
	});
});
</script>