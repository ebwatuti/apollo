<div width="100%" id="message_bar"></div>
<div id="modules">
<?php
    echo form_button(array(
        'name'=>'submit',
        'id'=>'new_module_button',
        'content'=>'New Module',
        'class'=>'submit_button float_right')
    );
?>
<br><br>
<table width="100%" id="modules_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Sort Order</th>
        <th style="background-color: #0a6184; color:#FFF">Controller</th>
        <th style="background-color: #0a6184; color:#FFF">Module Name</th>
        <th style="background-color: #0a6184; color:#FFF">Description</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>
    </tr></thead>
    <tbody>
<?php $patients = array(); ?>
<?php
if($modules->num_rows() > 0):
    foreach($modules->result() as $module):
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $module->sort; ?></td>
        <td style="background-color: #DDD"><?php echo $module->module_id; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $module->name; ?></td>
        <td style="background-color: #DDD"><?php echo $module->description; ?></td>
        <td style="background-color: #DDD">
            <?php
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'edit_module(\''.$module->module_id.'\')',
                'content'=>'Edit',
                'class'=>'DTTT_button float_left')
            );
        ?>
        &emsp;
        <?php
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'delete_module(\''.$module->module_id.'\')',
                'content'=>'Delete',
                'class'=>'DTTT_button float_left')
            );
        ?>
        </td>
        </tr>
<?php
    endforeach;
endif;
?>
	</tbody>
</table>
</div>

<div id="module_form_area"></div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    tb_init("a.vote_thickbox");
    $('#modules_table').dataTable({
        //"bPaginate": false,
        //"bLengthChange": false,
        //"bFilter": false,
        "bSort": false,
        //"bInfo": false,
        "bStateSave": true,
    });

    $("#new_module_button").click(function(){
        $("#module_form_area").css('display','block');
        $("#module_form_area").load('<?php echo site_url("admin/view_module"); ?>');
        $('#modules').css('display','none');
    });
});

function edit_module(module_id)
{
    $("#module_form_area").css('display','block');
    $("#module_form_area").load('<?php echo site_url("admin/view_module"); ?>/' + module_id);
    $('#modules').css('display','none');
}

function delete_module(module_id)
{
    if(confirm('Are you sure you want to delete this module?'))
    {
        $.post('<?php echo site_url("admin/delete_module"); ?>/' + module_id,
            {},
            function(response){
                if(response.success)
                {
                    $("#TB_ajaxContent").load('<?php echo site_url("admin/view_modules"); ?>',
                        function(result){
                            $("#message_bar").removeClass('error_message');
                            $("#message_bar").removeClass('warning_message');
                            $("#message_bar").removeClass('success_message');
                            $("#message_bar").addClass(response.message_class);
                            $("#message_bar").html(response.message);
                            $('#message_bar').fadeTo(5000, 1);
                            $('#message_bar').fadeTo("fast",0);
                        });
                }
                else
                {
                    $("#message_bar").removeClass('error_message');
                    $("#message_bar").removeClass('warning_message');
                    $("#message_bar").removeClass('success_message');
                    $("#message_bar").addClass(response.message_class);
                    $("#message_bar").html(response.message);
                    $('#message_bar').fadeTo(5000, 1);
                    $('#message_bar').fadeTo("fast",0);
                }
            },'json');
    }
}
</script>
