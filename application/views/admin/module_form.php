<div width="100%" id="message_bar"></div>
<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>

<?php echo form_open_multipart("admin/save_module/$module_info->module_id",array('id'=>'module_form')); ?>
<fieldset id="employee_basic_info">
<legend>Module Info</legend>
<div class="field_row clearfix">	
<?php echo form_label('Sort Order:', 'sort',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'sort',
		'id'=>'sort',
		'required'=>'required',
		'value'=>$module_info->sort)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Controller:', 'module_id',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'module_id',
		'id'=>'module_id',
		'required'=>'required',
		'value'=>$module_info->module_id)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Module Name:', 'name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'name',
		'id'=>'name',
		'required'=>'required',
		'value'=>$module_info->name)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Description:', 'description'); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
            'name'=>'description',
			'id'=>'description',
            'value'=>$module_info->description,
            'rows'=>'5',
			'cols'=>'17',)); ?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Icon:', 'icon'); ?>
	<div class='form_field'>
	<img src="<?php echo base_url().'images/menubar/'.$module_info->module_id.'.png';?>" border="0"/><br>
	<?php echo form_input(array(
            'name'=>'icon',
			'id'=>'icon',
            'type'=>'file')); ?>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_button(array("id"=>"module_form_submit","content"=>"Submit","class"=>"submit_button float_left"));?>
	<?php echo form_button(array("id"=>"module_form_cancel","content"=>"Cancel","class"=>"submit_button float_right"));?>
</div>
<?php  echo form_close(); ?>
</fieldset>

<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{	
	$("#module_form_submit").click(function(){
		$("#module_form").ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					//tb_remove();
					if(response.success)
					{
						$("#TB_ajaxContent").load('<?php echo site_url("admin/view_modules"); ?>',
							function(result){
								$("#message_bar").removeClass('error_message');
	                            $("#message_bar").removeClass('warning_message');
	                            $("#message_bar").removeClass('success_message');
	                            $("#message_bar").addClass(response.message_class);
	                            $("#message_bar").html(response.message);
	                            $('#message_bar').fadeTo(5000, 1);
	                            $('#message_bar').fadeTo("fast",0);
							});
					}
					else
					{
						$("#message_bar").removeClass('error_message');
	                    $("#message_bar").removeClass('warning_message');
	                    $("#message_bar").removeClass('success_message');
	                    $("#message_bar").addClass(response.message_class);
	                    $("#message_bar").html(response.message);
	                    $('#message_bar').fadeTo(5000, 1);
	                    $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});

	$("#module_form_cancel").click(function(){
		$("#module_form_area").css('display','none');
	    $("#module_form_area").html('&ensp;');
	    $('#modules').css('display','block');
	});
});
</script>