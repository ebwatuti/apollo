<div width="100%" id="message_bar"></div>
<div id="examinations">
<?php
    echo form_button(array(
        'name'=>'submit',
        'id'=>'new_examination_button',
        'content'=>'New Examination',
        'class'=>'submit_button float_right')
    );
?>
<br><br>
<table width="100%" id="examinations_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Examination</th>
        <th style="background-color: #0a6184; color:#FFF">Category</th>
        <th style="background-color: #0a6184; color:#FFF">Subcategory</th>
        <th style="background-color: #0a6184; color:#FFF">Options</th>
        <th style="background-color: #0a6184; color:#FFF">Clinics</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>       
    </tr></thead>
    <tbody>
<?php
if($examinations->num_rows() > 0):
	foreach($examinations->result() as $examination):
        $examination = $this->Administration->get_examination_info($examination->examination_id);
        $options = str_replace(',', ', ', $examination->options);
        $clinic_codes = explode(',', $examination->clinic);
        $clinics = '';
        foreach($clinic_codes as $clinic_code):
            $clinic_name = $this->Consultation->clinic_name($clinic_code);
            $clinics .= ($clinic_name ? $clinic_name : $clinic_code) . ', ';
        endforeach;
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $examination->value; ?></td>
        <td style="background-color: #DDD"><?php echo $examination->category; ?></td>
        <td style="background-color: #DDD"><?php echo $examination->sub_category; ?></td>
        <td style="background-color: #DDD"><?php echo character_limiter($options, 25); ?></td>
        <td style="background-color: #DDD"><?php echo character_limiter($clinics, 25); ?></td>
        <td style="background-color: #DDD">
		<?php
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'edit_examination('.$examination->examination_id.')',
                'content'=>'Edit',
                'class'=>'DTTT_button float_left')
            );
        ?>
        &emsp;
        <?php
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'delete_examination('.$examination->examination_id.')',
                'content'=>'Delete',
                'class'=>'DTTT_button float_left')
            );
        ?>
        </td>
        </tr>
<?php
	endforeach;
endif;
?>
	</tbody>
</table>
</div>

<div id="examination_form_area"></div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $('#examinations_table').dataTable({
        //"bPaginate": false,
        //"bLengthChange": false,
        //"bFilter": false,
        //"bSort": false,
        //"bInfo": false,
        "bStateSave": true,
    });

    $("#new_examination_button").click(function(){
        $("#examination_form_area").css('display','block');
        $("#examination_form_area").load('<?php echo site_url("admin/view_examination"); ?>');
        $('#examinations').css('display','none');
    });
});

function edit_examination(examination_id)
{
    $("#examination_form_area").css('display','block');
    $("#examination_form_area").load('<?php echo site_url("admin/view_examination"); ?>/' + examination_id);
    $('#examinations').css('display','none');
}

function delete_examination(examination_id)
{
    if(confirm('Are you sure you want to delete this examination?'))
    {
        $.post('<?php echo site_url("admin/delete_examination"); ?>/' + examination_id,
            {},
            function(response){
                if(response.success)
                {
                    $("#TB_ajaxContent").load('<?php echo site_url("admin/view_examinations"); ?>',
                        function(result){
                            $("#message_bar").removeClass('error_message');
                            $("#message_bar").removeClass('warning_message');
                            $("#message_bar").removeClass('success_message');
                            $("#message_bar").addClass(response.message_class);
                            $("#message_bar").html(response.message);
                            $('#message_bar').fadeTo(5000, 1);
                            $('#message_bar').fadeTo("fast",0);
                        });
                }
                else
                {
                    $("#message_bar").removeClass('error_message');
                    $("#message_bar").removeClass('warning_message');
                    $("#message_bar").removeClass('success_message');
                    $("#message_bar").addClass(response.message_class);
                    $("#message_bar").html(response.message);
                    $('#message_bar').fadeTo(5000, 1);
                    $('#message_bar').fadeTo("fast",0);
                }
            },'json');
    }
}
</script>