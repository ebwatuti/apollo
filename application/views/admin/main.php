<?php 
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));

$this->load->view("partial/header"); ?>

<div id="title_bar">
	<div id="page_title" style="margin-bottom:8px;">
	Configuration
	</div>
</div>
<div id="sidebar">
	<?php echo anchor("admin/view_complaints/width:1050",
	"<div class='submenu_button' style='float: left;'><span>Complaints</span></div>",
	array('title'=>'Complaints','class'=>'thickbox none',));
	?>
	<?php echo anchor("admin/view_examinations/width:1050",
	"<div class='submenu_button' style='float: left;'><span>Examinations</span></div>",
	array('title'=>'Examinations','class'=>'thickbox none',));
	?>
	<?php echo anchor("admin/view_modules/width:850",
	"<div class='submenu_button' style='float: left;'><span>Modules</span></div>",
	array('title'=>'Modules','class'=>'thickbox none',));
	?>
	<?php echo anchor("admin/export_db",
	"<div class='submenu_button' style='float: left;'><span>Export Database</span></div>",
	array('title'=>'Export Database',));
	?>
	<?php echo anchor("admin/import_db/width:350",
	"<div class='submenu_button' style='float: left;'><span>Import Database</span></div>",
	array('title'=>'Import Database','class'=>'thickbox none'));
	?>
</div>

<div id="config_holder">

</div>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	var html = "<table width='100%' height='100px'><tr><td align='center'><img src='<?php echo base_url()?>images/loading_animation.gif' alt='spinner' /></td></tr><table>";
	$("#config_holder").html(html);
	$("#config_holder").load('<?php echo site_url("$controller_name/view_config"); ?>');
});

</script>
