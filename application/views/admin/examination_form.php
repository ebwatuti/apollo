<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>

<fieldset id="employee_basic_info">
<legend>examination Info</legend>
<?php echo form_open("admin/save_examination/$examination_info->examination_id",array('id'=>'examination_form')); ?>

<div class="field_row clearfix">	
<?php echo form_label('Examination:', 'value',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'value',
		'id'=>'value',
		'value'=>$examination_info->value)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Category:', 'category',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'category',
		'id'=>'category',
		'value'=>$examination_info->category)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Subcategory:', 'sub_category'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'sub_category',
		'id'=>'sub_category',
		'value'=>$examination_info->sub_category)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label("Examination Options:", 'options'); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
		'name'=>'options',
		'id'=>'options',
		'cols'=>20,
		'rows'=>5,
		'value'=>$examination_info->options)
	);?>
	<br><span style='font-style:italic'>(separate with a comma)</span>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label("Description:", 'description'); ?>
	<div class='form_field'>
	<?php echo form_checkbox(array(
		'name'=>'description',
		'id'=>'description',
		'value'=>1,
		'checked'=>$examination_info->description == 1 ? 'checked' : '')
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label("Section:", 'section'); ?>
	<div class='form_field'>
	<?php echo form_dropdown('section',array(
		'general'=>'general',
		'systemic'=>'systemic'),$examination_info->section
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php $selected_clinics = explode(',', $examination_info->clinic); ?>
<?php echo form_label("Clinics:", 'clinic'); ?>
	<div class='form_field'>
	<?php foreach($clinics as $clinic): ?>
		<label>
		<?php echo form_checkbox(array(
			'name'=>'clinic[]',
			'value'=>$clinic->clinic_code,
			'checked'=>in_array($clinic->clinic_code, $selected_clinics) ? 'checked' : '')
		);?>&emsp;<?php echo $clinic->clinic_name; ?>
		</label><br>
	<?php endforeach; ?>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_button(array("id"=>"examination_form_submit","content"=>"Submit","class"=>"submit_button float_left"));?>
	<?php echo form_button(array("id"=>"examination_form_cancel","content"=>"Cancel","class"=>"submit_button float_right"));?>
</div>

<?php  echo form_close(); ?>
</fieldset>

<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{	
	$("#examination_form_submit").click(function(){
		$("#examination_form").ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					//tb_remove();
					if(response.success)
					{
						$("#TB_ajaxContent").load('<?php echo site_url("admin/view_examinations"); ?>',
							function(result){
								$("#message_bar").removeClass('error_message');
	                            $("#message_bar").removeClass('warning_message');
	                            $("#message_bar").removeClass('success_message');
	                            $("#message_bar").addClass(response.message_class);
	                            $("#message_bar").html(response.message);
	                            $('#message_bar').fadeTo(5000, 1);
	                            $('#message_bar').fadeTo("fast",0);
							});
					}
					else
					{
						$("#message_bar").removeClass('error_message');
	                    $("#message_bar").removeClass('warning_message');
	                    $("#message_bar").removeClass('success_message');
	                    $("#message_bar").addClass(response.message_class);
	                    $("#message_bar").html(response.message);
	                    $('#message_bar').fadeTo(5000, 1);
	                    $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});

	$("#examination_form_cancel").click(function(){
		$("#examination_form_area").css('display','none');
	    $("#examination_form_area").html('&ensp;');
	    $('#examinations').css('display','block');
	});

	$("#category").autocomplete("<?php echo site_url('admin/suggest_examination_category');?>",{max:100,minChars:0,delay:10});
    $("#category").result(function(event, data, formatted){});
	$("#category").search();

	$("#sub_category").autocomplete("<?php echo site_url('admin/suggest_examination_subcategory');?>",{max:100,minChars:0,delay:10});
    $("#sub_category").result(function(event, data, formatted){});
	$("#sub_category").search();
});
</script>