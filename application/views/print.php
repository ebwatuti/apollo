<?php 
if($excel == 1){
	ob_start();
	$this->load->view("partial/header_excel");
}else{
	$this->load->view("partial/header_popup");
} 
?>

<div id="page_title" style="margin-bottom:8px;"><?php echo $title; ?></div>
<div id="page_subtitle" style="margin-bottom:8px;"><?php echo $subtitle; ?></div>
<?php echo $html; ?>

<?php 
if($excel == 1):
	$this->load->view("partial/footer_excel");
	$content = ob_end_flush();
	
	$filename = strtolower(preg_replace('/[^a-zA-Z0-9_]/s', "_",$title));
	$filename .= ".xls";
	header('Content-type: application/ms-excel');
	header('Content-Disposition: attachment; filename='.$filename);
	header('Cache-Control: max-age=0');
	echo $content;
	die();
	
else:
	$this->load->view("partial/footer"); 
endif;
?>
