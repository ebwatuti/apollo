<?php 
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));

echo form_open($controller_name."/add_prescription",array('id'=>'add_prescription_form','class'=>'add_item_form','onkeypress'=>'return event.keyCode != 13;'));?>
<label id="item_label" for="prescription_item">Drug</label>
<?php echo form_input(array('name'=>'item','id'=>'prescription_item','size'=>'40','placeholder'=>'Start typing drug name'));?>


</form>
<table id="register">
<thead>
<tr>
<th style="width:15%;">Drug Name</th>
<th style="width:10%;">Strength</th>
<th style="width:10%;">Dose</th>
<th style="width:10%;">Frequency</th>
<th style="width:10%;">Duration</th>
<th style="width:10%;">Disp #</th>
<th style="width:25%;">Remarks</th>
<th style="width:10%;"></th>
</tr>
</thead>
<tbody id="lab_request_contents">

<?php foreach(array_reverse($prescription, true) as $item): ?>
		<tr>
        <td style="align:center;"><?php echo $item['name']; ?></td>
        <td style="align:center;"><?php echo $item['strength']; ?></td>
        <td style="align:center;"><?php echo ($item['status'] == 0 && $item['invoice_status'] == 0) ? form_input(array(
                                    'name'=>'dose',
                                    'value'=>$item['dose'],
                                    'size'=>2,
                                    'drug_id'=>$item['drug_id'],
                                    'onchange' => 'save_drug_instructions(this)'))
                                    : $item['dose']; ?></td>
        <td style="align:center;"><?php echo ($item['status'] == 0 && $item['invoice_status'] == 0) ? form_input(array(
                                    'name'=>'frequency',
                                    'value'=>$item['frequency'],
                                    'size'=>2,
                                    'drug_id'=>$item['drug_id'],
                                    'onchange' => 'save_drug_instructions(this)'))
                                    : $item['frequency']; ?></td>
        <td style="align:center;"><?php echo ($item['status'] == 0 && $item['invoice_status'] == 0) ? form_input(array(
                                    'name'=>'duration',
                                    'value'=>$item['duration'],
                                    'size'=>2,
                                    'drug_id'=>$item['drug_id'],
                                    'onchange' => 'save_drug_instructions(this)'))
                                    : $item['duration']; ?></td>
        <td style="align:center;"><?php echo ($item['status'] == 0 && $item['invoice_status'] == 0) ? form_input(array(
                                    'name'=>'quantity',
                                    'value'=>$item['quantity'],
                                    'drug_id'=>$item['drug_id'],
                                    'type'=>'number',
                                    'min'=>'1',
                                    'max'=>$item['stock'],
                                    'onchange' => 'save_drug_instructions(this)'))
                                    : $item['quantity']; ?></td>
        <td style="align:center;"><?php echo ($item['status'] == 0 && $item['invoice_status'] == 0) ? form_textarea(array(
                                    'name'=>'remarks',
                                    'value'=>$item['remarks'],
                                    'rows'=>'3',
                                    'cols'=>25,
                                    'drug_id'=>$item['drug_id'],
                                    'onchange' => 'save_drug_instructions(this)'))
                                    : $item['remarks']; ?></td>
        <td><?php echo ($item['status'] == 0  && $item['invoice_status'] == 0) ? form_button(array(
        'content'=>'Delete',
        'class'=>'submit_button float_right',
        'drug_id'=>$item['drug_id'],
        'onclick'=>"delete_drug(this)"
    ))
    : '&ensp;';?></td>
        </tr>
        
        <tr style="height:3px">
        <td colspan=8 style="background-color:white"> </td>
        </tr>       </form>

	<?php endforeach; ?>
</tbody>
</table>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $("#prescription_item").autocomplete('<?php echo site_url($controller_name."/drug_search"); ?>',
    {
    	minChars:0,
    	max:100,
    	selectFirst: false,
       	delay:10,
    	formatItem: function(row) {
			return row[1];
		}
    });

    $("#prescription_item").result(function(event, data, formatted)
    {
		$("#add_prescription_form").ajaxSubmit({
			success:function(response){
				$("#message_bar").removeClass('error_message');
                $("#message_bar").removeClass('warning_message');
                $("#message_bar").removeClass('success_message');
                $("#message_bar").addClass(response.message_class);
                $("#message_bar").html(response.message);
                $('#message_bar').fadeTo(5000, 1);
                $('#message_bar').fadeTo("fast",0);
				
				if(response.success) $("#prescription_tab").load("<?php echo site_url($controller_name."/refresh_prescription"); ?>");
			},dataType:'json'});
    });
});

function save_drug_instructions(input)
{
	$.post('<?php echo site_url($controller_name."/prescription_info"); ?>/' + $(input).attr('drug_id') + '/' + $(input).attr('name'),
                {value: $(input).val()}
                );
}

function delete_drug(button)
{
	$("#prescription_tab").load('<?php echo site_url($controller_name."/delete_prescription"); ?>/' + $(button).attr('drug_id'));
}
</script>
