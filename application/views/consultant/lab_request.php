<?php 
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));

echo form_open($controller_name."/add_lab_tests",array('id'=>'lab_tests_form'));
?>
<div id="examination_header_bar" align="center">Lab Tests</div>
<ul class="lab_items_list">
    <?php foreach($all_lab_tests->Result() as $lab_test): ?>
        <li><label>
        <?php
        $options = array(
            'name'=>'lab_tests[]',
            'value'=>$lab_test->lab_test_id,
            'onchange' => 'save_lab_tests()',
            );
        if(array_key_exists($lab_test->lab_test_id, $lab_tests)):
            $options['checked'] = 'checked';
            if( $lab_tests[$lab_test->lab_test_id]['status'] != 0 || $lab_tests[$lab_test->lab_test_id]['invoice_status'] != 0):
                $options['disabled'] = 'disabled';
            endif;
        endif;
        echo form_checkbox($options); ?>
        &emsp;<?php echo $lab_test->value; ?></label></li>
    <?php endforeach; ?>
</ul>
</form>

<script type="text/javascript" language="javascript">
function save_lab_tests()
{
    $("#lab_tests_form").ajaxSubmit();
}
</script>
