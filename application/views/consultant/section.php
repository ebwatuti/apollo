<?php
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));
  
  echo form_open($controller_name.'/save_other_sections/'.$section,array('id'=>'save_section_'.$section.'_form','class'=>'consultation_form'));
?>
<table id="register">
<tbody id="cart_contents">
<?php foreach($fields as $field): ?>
	<tr>
	<td width="20%"><?php echo $field; ?></td>
	<td align="left"><?php echo form_textarea(array(
										'name'=>preg_replace('/ /s', "_",$field),
										'value'=>$other_sections[$section][$field],
										'rows'=>'3',
										'onchange' => 'save_section_'.$section.'()')); ?></td>
	</tr>
<?php endforeach; ?>
</tbody>
</table>
<?php echo form_close(); ?>


<script type="text/javascript" language="javascript">
function save_section_<?php echo $section; ?>()
{
	$("#save_section_<?php echo $section; ?>_form").ajaxSubmit();
}
</script>
