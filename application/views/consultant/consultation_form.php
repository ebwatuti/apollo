<?php 
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));
?>

<?php
    $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
    if($patient->dob) $patient->age = $this->Patient->patient_age($patient->dob);
    $patient->patient_no = $this->Patient->patient_number($patient->patient_id);
    $patient->last_encounter = $this->Encounter->last_visit($patient->patient_id);
    if($patient->last_encounter) $patient->last_visit =date("M j, Y",strtotime($patient->last_encounter->encounter_start));

    $sections = $this->Consultation->get_sections($clinic)->result();
?>

<div id="queue_section">
<table width="100%">
    <tr><th style="background-color: #0a6184; color:#FFF" colspan="2" align="center">Patient Info</th></tr>
    <tr><td colspan="2">
    <div class="field_row clearfix">    
    <?php echo form_label('Patient #:'); ?>
        <div class='form_field'>
        <?php echo $patient->patient_no;?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Name:'); ?>
        <div class='form_field'>
        <?php echo $patient->patient_name;?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Age:'); ?>
        <div class='form_field'>
        <?php echo $patient->age;?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Last Visit:'); ?>
        <div class='form_field'>
        <?php echo $patient->last_visit;?>
        </div>
    </div>
    </td></tr>

    <tr><td colspan="2" width="100%">
        <?php
        echo form_button(array(
            'name'=>'submit',
            'id'=>'save_consultation_button',
            'content'=>'Save',
            'class'=>'submit_button float_left')
        );
        ?>
        <?php
        echo form_button(array(
            'name'=>'submit',
            'id'=>'discharge_consultation_button',
            'content'=>'Discharge',
            'class'=>'submit_button float_right')
        );
        ?>
    </td></tr>

    <tr><td colspan="2" width="100%"><br><hr></td></tr>

    <?php if($triage_history->num_rows() > 0): ?>
    <tr><th style="background-color: #0a6184; color:#FFF" colspan="2" align="center">Vital Signs</th></tr>
    <?php   
        $triage = $triage_history->last_row();
        $triage_details = unserialize($triage->triage_details);
    ?>
    <tr><td colspan="2">
    <div class="field_row clearfix">    
    <?php echo form_label('Temperature:'); ?>
        <div class='form_field'>
        <?php echo $triage_details['temperature'];?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Weight:'); ?>
        <div class='form_field'>
        <?php echo $triage_details['weight'];?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Blood Pressure:'); ?>
        <div class='form_field'>
        <?php echo $triage_details['blood pressure']['systolic'] . "/" . $triage_details['blood pressure']['diastolic'];?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Pulse Rate:'); ?>
        <div class='form_field'>
        <?php echo $triage_details['pulse rate'];?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Other Remarks:'); ?>
        <div class='form_field'>
        <?php echo $triage_details['other remarks'];?>
        </div>
    </div>
    <?php
    echo form_button(array(
        'id'=>'triage_chart_button',
        'content'=>'View Chart',
        'class'=>'submit_button float_right',
        'onclick'=>"view_triage_chart('$triage->encounter_id')"
    ));
    ?>
    </td></tr>
    <tr><td colspan="2" width="100%"><br><hr></td></tr>
    <?php endif; ?>
    
    <?php if($consultation_history->num_rows() > 0 ): ?>
    <tr><th style="background-color: #0a6184; color:#FFF" colspan="2" align="center">Consultation History</th></tr>
     <tr><td colspan="2">
        <table id="consultation_history_table" width="100%">
            <thead><tr><td></td><td></td></tr></thead>
            <?php foreach($consultation_history->result() as $consultation): ?>
                <?php 
                    $employee = $this->Employee->get_info($consultation->consultant_id);
                ?>
                <tr style="cursor: pointer;" onclick="view_consultation_summary(<?php echo $consultation->consultation_id; ?>)" title="Click to view summary.">
                    <td><?php echo "Dr. " . ucfirst(strtolower($employee->last_name)); ?></td>
                    <td><?php echo date('j/n/Y',strtotime($consultation->consultation_start)); ?></td>
                </tr>        
    <?php endforeach; ?>
        </table>
    </td></tr>
    <tr><td colspan="2" width="100%"><br><hr></td></tr>
    <?php endif; ?>

</table>
</div>


<div class="consultation_summary" id="consultation_summary"></div>
<div id="register_section">

<div width="100%" id="message_bar"></div>
<ul id="error_message_box"></ul>
<div id="TabbedPanels1" class="TabbedPanels">
  <ul class="TabbedPanelsTabGroup">
    <?php foreach($sections as $section): ?>
        <?php if(!$section->displayed) continue; ?>
        <li class="TabbedPanelsTab" tabindex="0"><?php echo $section->section_name; ?></li>
    <?php endforeach; ?>
   <li class="TabbedPanelsTab" tabindex="0">Summary</li>
  </ul>
  <div class="TabbedPanelsContentGroup">
    <?php foreach($sections as $section): ?>
        <?php if(!$section->displayed) continue; ?>
        <div class="TabbedPanelsContent" id="<?php echo $section->section_id; ?>_tab">
        <?php
            if($section->view):
                $this->load->view("consultant/$section->section_id");
            else:
                $data = array('fields'=>explode(',', $section->section_fields),'section'=>$section->section_id);
                $this->load->view("consultant/section",$data);
            endif;
        ?>
        </div>
    <?php endforeach; ?>
    <div class="TabbedPanelsContent" id="summary_tab_<?php echo $patient->patient_id; ?>"><?php $this->load->view("consultant/consultation_summary"); ?></div>
  </div>
</div>

</div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    var summary_reload = setInterval(function(){$("#summary_tab_<?php echo $patient->patient_id; ?>").load('<?php echo site_url("$controller_name/refresh_summary/$patient->patient_id"); ?>');},2000);
        
    $('#consultation_history_table').dataTable({
        //"bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": false,
        "bInfo": false,
        //"bStateSave": true,
    });

    $("#save_consultation_button").click(function()
    {
        if (confirm('Are you sure you want to save this consultation?'))
        {
            $.post('<?php echo site_url($controller_name."/save_consultation"); ?>',
                {patient_id:'<?php echo $patient->patient_id; ?>'},
                function(response){
                    if(response.form_validation)
                    {
                        $("#message_bar").removeClass('error_message');
                        $("#message_bar").removeClass('warning_message');
                        $("#message_bar").removeClass('success_message');
                        $("#message_bar").addClass(response.message_class);
                        $("#message_bar").html(response.message);
                        $('#message_bar').fadeTo(5000, 1);
                        $('#message_bar').fadeTo("fast",0);   
                    }
                    else{
                        $("#error_message_box").html(response.error_messages);
                    }
              },'json');
        }
    });

    $("#discharge_consultation_button").click(function()
    {
        if (confirm('Are you sure you want to discharge this patient?'))
        {
            $.post('<?php echo site_url($controller_name."/discharge"); ?>',
                {patient_id:'<?php echo $patient->patient_id; ?>'},
                function(response){
                    if(response.form_validation)
                    {
                        if(response.success){
                            $("#table_holder").load('<?php echo site_url("$controller_name/view_all"); ?>'); 
                            tb_remove();
                            set_feedback(response.message,'success_message',false);
                        }
                        else
                        {
                            $("#message_bar").removeClass('error_message');
                            $("#message_bar").removeClass('warning_message');
                            $("#message_bar").removeClass('success_message');
                            $("#message_bar").addClass(response.message_class);
                            $("#message_bar").html(response.message);
                            $('#message_bar').fadeTo(5000, 1);
                            $('#message_bar').fadeTo("fast",0);
                        }   
                    }
                    else{
                        $("#error_message_box").html(response.error_messages);
                    } 
              },'json');
        }
    });
});

function view_consultation_summary(consultation_id){
    var html = '<?php echo form_button(array("content"=>"Close","class"=>"submit_button float_right", "onclick"=>"close_consultation_summary()")); ?>';
    $("#consultation_summary").css('display','block');
    $("#consultation_summary").load('<?php echo site_url("$controller_name/consultation_summary/$patient->patient_id"); ?>/' + consultation_id,
        function(response){
            $("#consultation_summary").append(html);
        });
    $('#register_section').css('display','none');
}

function close_consultation_summary(){
    $("#consultation_summary").css('display','none');
    $("#consultation_summary").html('&ensp;');
    $('#register_section').css('display','block');
}

function view_triage_chart(encounter_id){
    var html = '<?php echo form_button(array("content"=>"Close","class"=>"submit_button float_right", "onclick"=>"close_consultation_summary()")); ?>';
    $("#consultation_summary").css('display','block');
    $("#consultation_summary").load('<?php echo site_url("$controller_name/triage_chart"); ?>/' + encounter_id,
        function(response){
            $("#consultation_summary").append(html);
        });
    $('#register_section').css('display','none');
}

var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
</script>