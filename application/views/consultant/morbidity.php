<?php
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));
?>
<div id="archive">
<ul id="error_message_box"></ul>
<?php echo form_open("$controller_name/morbidity_date_input",array('id'=>'morbidity_date_form')); ?>
<table width="100%"><tfoot><tr><td colspan="10"><br><hr></td></tr></tfoot>
<tbody><tr>
<td width="10%"><strong>Start Date: &emsp;</strong></td>
<td width="20%"><?php echo form_input(array(
                'name'=>'start_date',
                'id'=>'start_date',
                'value'=>$start_date_input, 
                'class'=>'date')
        ); ?></td>
<td width="10%"><strong>End Date: &emsp;</strong></td>
<td width="20%"><?php echo form_input(array(
                'name'=>'end_date',
                'id'=>'end_date',
                'value'=>$end_date_input, 
                'class'=>'date')
        ); ?></td>
<td width="40%"><?php echo form_button(array("id"=>"morbidity_date_submit","content"=>"Submit","class"=>"submit_button float_left"));?></td>
</tr></tbody></table>
<?php echo form_close(); ?>

<div id="page_subtitle">
<?php
    if($start_date == $end_date) echo $start_date;
    else echo $start_date . "&emsp;to&emsp;" . $end_date;
?>
</div>
<div id="morbidity_table_holder">
<table width="100%" id="morbidity_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">ICD10 Code</th>
        <th style="background-color: #0a6184; color:#FFF">Description</th>
        <th style="background-color: #0a6184; color:#FFF"><?php echo $clinic; ?> Total</th>
        <th style="background-color: #0a6184; color:#FFF">Hospital Total</th>
    </tr></thead>
    <tbody>
<?php
	foreach($clinic_morbidity as $diagnosis_code=>$count):
        if($count <= 0 && $hospital_morbidity[$diagnosis_code] <= 0) continue;
        $description = $this->Consultation->get_diagnosis_info($diagnosis_code)->description;
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $diagnosis_code; ?></td>
        <td style="background-color: #DDD"><?php echo $description; ?></td>
        <td style="background-color: #DDD"><?php echo $count; ?></td>
        <td style="background-color: #DDD"><?php echo $hospital_morbidity[$diagnosis_code]; ?></td>
        </tr>
<?php
	endforeach;
?>
	</tbody>
</table>
</div>
</div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	$('#morbidity_table').dataTable({
		//"bPaginate": false,
		//"bLengthChange": false,
		//"bFilter": false,
		"bSort": false,
		//"bInfo": false,
        "bStateSave": true,
	});

    Date.format = 'dd-mm-yyyy';
    $('.date').datepicker({
      changeMonth: true,
      changeYear: true,
      //minDate: new Date(2014, 0, 1),
      maxDate: "0",
      dateFormat: "dd-mm-yy",
    });

    $("#morbidity_date_submit").click(function(){
        $("#morbidity_date_form").ajaxSubmit({
            success:function(response)
            {
                if(response.form_validation)
                {
                    var html = "<table width='100%' height='100px'><tr><td align='center'><img src='<?php echo base_url()?>images/loading_animation.gif' alt='spinner' /></td></tr><table>";
                    $("#morbidity_table_holder").html(html);
                    $("#TB_ajaxContent").load('<?php echo site_url("$controller_name/view_morbidity"); ?>');
                }
                else
                {
                    $("#error_message_box").html(response.error_messages);
                }
            },
            dataType:'json',
        });
    });
});
</script>
