<?php
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));
?>
<div width="100%" id="message_bar"></div>
<div id="sections">
<?php
    echo form_button(array(
        'name'=>'submit',
        'id'=>'new_section_button',
        'content'=>'New Section',
        'class'=>'submit_button float_right')
    );
?>
<br><br>
<table width="100%" id="sections_table" class="display">
    <thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Section</th>
        <th style="background-color: #0a6184; color:#FFF">Fields</th>
        <th style="background-color: #0a6184; color:#FFF">Displayed</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>       
    </tr></thead>
    <tbody>
<?php
if($sections->num_rows() > 0):
    foreach($sections->result() as $section):
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $section->section_name; ?></td>
        <td style="background-color: #DDD"><?php echo character_limiter($section->section_fields, 25); ?></td>
        <td style="background-color: #DDD"><?php echo ($section->displayed == 1) ? 'Yes' : 'No'; ?></td>
        <td style="background-color: #DDD">
        <?php
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'edit_section(\''.$section->section_id.'\')',
                'content'=>'Edit',
                'class'=>'DTTT_button float_left')
            );
        ?>
        &emsp;
        <?php
            if($section->view == 0):
                echo form_button(array(
                    'name'=>'submit',
                    'onclick'=>'delete_section(\''.$section->section_id.'\')',
                    'content'=>'Delete',
                    'class'=>'DTTT_button float_left')
                );
            endif;
        ?>
        </td>
        </tr>
<?php
    endforeach;
endif;
?>
    </tbody>
</table>
</div>

<div id="section_form_area"></div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $('#sections_table').dataTable({
        //"bPaginate": false,
        //"bLengthChange": false,
        //"bFilter": false,
        //"bSort": false,
        //"bInfo": false,
        "bStateSave": true,
    });

    $("#new_section_button").click(function(){
        $("#section_form_area").css('display','block');
        $("#section_form_area").load('<?php echo site_url("$controller_name/view_section"); ?>');
        $('#sections').css('display','none');
    });
});

function edit_section(section_id)
{
    $("#section_form_area").css('display','block');
    $("#section_form_area").load('<?php echo site_url("$controller_name/view_section"); ?>/' + section_id);
    $('#sections').css('display','none');
}

function delete_section(section_id)
{
    if(confirm('Are you sure you want to delete this section?'))
    {
        $.post('<?php echo site_url("$controller_name/delete_section"); ?>/' + section_id,
            {},
            function(response){
                if(response.success)
                {
                    $("#TB_ajaxContent").load('<?php echo site_url("$controller_name/view_sections"); ?>',
                        function(result){
                            $("#message_bar").removeClass('error_message');
                            $("#message_bar").removeClass('warning_message');
                            $("#message_bar").removeClass('success_message');
                            $("#message_bar").addClass(response.message_class);
                            $("#message_bar").html(response.message);
                            $('#message_bar').fadeTo(5000, 1);
                            $('#message_bar').fadeTo("fast",0);
                        });
                }
                else
                {
                    $("#message_bar").removeClass('error_message');
                    $("#message_bar").removeClass('warning_message');
                    $("#message_bar").removeClass('success_message');
                    $("#message_bar").addClass(response.message_class);
                    $("#message_bar").html(response.message);
                    $('#message_bar').fadeTo(5000, 1);
                    $('#message_bar').fadeTo("fast",0);
                }
            },'json');
    }
}
</script>