<?php 
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));

echo form_open($controller_name."/add_diagnosis",array('id'=>'add_item_form','onkeypress'=>'return event.keyCode != 13;'));?>
<label id="item_label" for="item">Diagnosis</label>
<?php echo form_input(array('name'=>'item','id'=>'item','size'=>'40','placeholder'=>'Start typing diagnosis'));?>


</form>
<table id="register"><tfoot><tr><td colspan="10"><br><hr><br></td></tr></tfoot>
<thead>
<tr>
<th style="width:10%;">ICD 10 Code</th>
<th style="width:35%;">Description</th>
<th style="width:30%;">Remarks</th>
<th style="width:10%;"></th>
</tr>
</thead>
<tbody id="diagnosis_contents">

<?php foreach(array_reverse($diagnosis, true) as $item): ?>
    <?php if(!$item['description']) continue; ?>
        <tr>
        <td><?php echo $item['diagnosis_code']; ?></td>
        <td style="align:center;"><?php echo $item['description']; ?></td>
        <td style="align:center;"><?php echo form_textarea(array(
                                    'name'=>'remarks',
                                    'value'=>$item['remarks'],
                                    'rows'=>'3',
                                    'diagnosis_code'=>$item['diagnosis_code'],
                                    'onchange' => 'save_diagnosis_remarks(this)')); ?></td>
        
        <td><?php echo form_button(array(
        'content'=>'Delete',
        'class'=>'submit_button float_right',
        'diagnosis_code'=>$item['diagnosis_code'],
        'onclick'=>"delete_diagnosis(this)"
    ));?></td>
        </tr>
        
        <tr style="height:3px">
        <td colspan=8 style="background-color:white"> </td>
        </tr>       </form>
    <?php endforeach; ?>
</tbody>
</table>

<?php echo form_open($controller_name."/save_diagnosis",array('id'=>'save_diagnosis_form','class'=>'consultation_form')); ?>

<table>
<tbody>
<tr>
<td valign="top"><label for="diagnosis"><strong>Remarks:</strong>&emsp;</label></td>
<td>
<?php echo form_textarea(array(
            'name'=>'diagnosis',
            'id'=>'diagnosis',
            'value'=>$diagnosis[0]['remarks'],
            'rows'=>'10',
            'cols'=>'50',
            'onchange' => 'save_diagnosis()')); ?>
</td>
</tr>
</tbody>
</table>

</form>
<script type='text/javascript'>
$(document).ready(function()
{
    $("#item").autocomplete('<?php echo site_url($controller_name."/diagnosis_search"); ?>',
    {
        minChars:0,
        max:100,
        selectFirst: false,
        delay:10,
        formatItem: function(row) {
            return row[1];
        }
    });

    $("#item").result(function(event, data, formatted)
    {
        $("#add_item_form").ajaxSubmit({
            success:function(response){
                $("#message_bar").removeClass('error_message');
                $("#message_bar").removeClass('warning_message');
                $("#message_bar").removeClass('success_message');
                $("#message_bar").addClass(response.message_class);
                $("#message_bar").html(response.message);
                $('#message_bar').fadeTo(5000, 1);
                $('#message_bar').fadeTo("fast",0);
                
                if(response.success) $("#diagnosis_tab").load("<?php echo site_url($controller_name."/refresh_diagnosis"); ?>");
            },dataType:'json'});
    });
});

function save_diagnosis_remarks(input)
{
    $.post('<?php echo site_url($controller_name."/diagnosis_remarks"); ?>/' + $(input).attr('diagnosis_code'),
                {remarks: $(input).val()}
                );
}

function delete_diagnosis(button)
{
    $("#diagnosis_tab").load('<?php echo site_url($controller_name."/delete_diagnosis"); ?>/' + $(button).attr('diagnosis_code'));
}
function save_diagnosis()
{
    $("#save_diagnosis_form").ajaxSubmit();
}
</script>
