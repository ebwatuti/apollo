<?php
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));

 echo form_open($controller_name."/save_treatment_plan",array('id'=>'save_treatment_plan_form','class'=>'consultation_form')); ?>

<table>
<tbody>
<tr>
<td valign="top"><label for="treatment_plan"><strong>Remarks:</strong>&emsp;</label></td>
<td>
<?php echo form_textarea(array(
            'name'=>'treatment_plan',
			'id'=>'treatment_plan',
            'value'=>$treatment_plan['treatment_plan'],
            'rows'=>'10',
			'cols'=>'50',
            'onchange' => 'save_treatment_plan()')); ?>
</td>
</tr>
</tbody>
</table>

</form>
<script type='text/javascript'>
function enable_text(source,target)
{
	if(source.checked == "checked") $("#"+target).attr("readonly","readonly");
	
}

function save_treatment_plan()
{
	$("#save_treatment_plan_form").ajaxSubmit();
}
</script>
