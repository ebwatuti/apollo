<?php
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));
?>
<table width="100%" id="patients_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Patient #</th>
        <th style="background-color: #0a6184; color:#FFF">Patient Name</th>
        <th style="background-color: #0a6184; color:#FFF">Age</th>
        <th style="background-color: #0a6184; color:#FFF">Priority</th>
        <th style="background-color: #0a6184; color:#FFF">Comments</th>
        <th style="background-color: #0a6184; color:#FFF">Consultation</th>
    </tr></thead>
    <tbody>
<?php $patients = array(); ?>
<?php
if($lab_queue->num_rows() > 0):
    foreach($lab_queue->result() as $encounter):
        $patient = $this->Patient->get_info($encounter->patient_id);
        $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
        if($patient->dob) $patient->age = $this->Patient->patient_age($patient->dob);
        $patient->patient_no = $this->Patient->patient_number($patient->patient_id);
        $patients[$patient->patient_id] = $patient->patient_no;        
        
        $patient->lab_results = "Lab results completed at: " . date("g:i a",strtotime($encounter->service_time));

        switch ($encounter->priority) {
            case 1:
                $patient->priority = 'Normal';
                break;
            case 2:
                $patient->priority = 'Urgent';
                break;
            case 3:
                $patient->priority = 'Emergency';
                break;
            default:
                $patient->priority = 'Normal';
                break;
        }
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $patient->patient_no; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->patient_name; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $patient->age; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->priority; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $patient->lab_results; ?></td>
        <td style="background-color: #DDD">
            <?php echo anchor("$controller_name/view_consultation/$encounter->patient_id/width:1150",
            form_button(array('name'=>'submit','content'=>'Open','class'=>'DTTT_button float_left')),
            array("class"=>"thickbox vote_thickbox","title"=>"Consultation")); ?>
        </td>
        </tr>
<?php
    endforeach;
endif;
?>

<?php
    $employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
    $clinic = $this->session->userdata('clinic');
if($main_queue->num_rows() > 0):	
    foreach($main_queue->result() as $encounter):
        $patient = $this->Patient->get_info($encounter->patient_id);
        $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
        if($patient->dob) $patient->age = $this->Patient->patient_age($patient->dob);
        $patient->patient_no = $this->Patient->patient_number($patient->patient_id);
        $patient->last_encounter = $this->Encounter->last_visit($encounter->patient_id);
        $patients[$patient->patient_id] = $patient->patient_no;

        switch ($encounter->priority) {
            case 1:
                $patient->priority = 'Normal';
                break;
            case 2:
                $patient->priority = 'Urgent';
                break;
            case 3:
                $patient->priority = 'Emergency';
                break;
            default:
                $patient->priority = 'Normal';
                break;
        }
        
        if($patient->last_encounter) $patient->last_visit = "Last visit: " . date("M j, Y",strtotime($patient->last_encounter->encounter_start));
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $patient->patient_no; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->patient_name; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $patient->age; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->priority; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $patient->last_visit; ?></td>
        <td style="background-color: #DDD">
			<?php echo anchor("$controller_name/view_consultation/$encounter->patient_id/width:1150",
            form_button(array('name'=>'submit','content'=>'Open','class'=>'DTTT_button float_left')),
            array("class"=>"thickbox vote_thickbox","title"=>"Consultation")); ?>
        </td>
        </tr>
<?php
	endforeach;
endif;
?>

<?php
if($returning_queue->num_rows() > 0):
    foreach($returning_queue->result() as $encounter):
        if(array_key_exists($encounter->patient_id, $patients)) continue;
        $patient = $this->Patient->get_info($encounter->patient_id);
        $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
        if($patient->dob) $patient->age = $this->Patient->patient_age($patient->dob);
        $patient->patient_no = $this->Patient->patient_number($patient->patient_id);
        $patient->returning = $this->Consultation->returning($encounter->patient_id, $employee_id, $clinic);

        switch ($encounter->priority) {
            case 1:
                $patient->priority = 'Normal';
                break;
            case 2:
                $patient->priority = 'Urgent';
                break;
            case 3:
                $patient->priority = 'Emergency';
                break;
            default:
                $patient->priority = 'Normal';
                break;
        }
        
        if($patient->returning) $patient->last_seen = "Last seen at: " . date("g:i a",strtotime($patient->returning->consultation_end));
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $patient->patient_no; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->patient_name; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $patient->age; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->priority; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $patient->last_seen; ?></td>
        <td style="background-color: #DDD">
            <?php echo anchor("$controller_name/view_consultation/$encounter->patient_id/width:1150",
            form_button(array('name'=>'submit','content'=>'Open','class'=>'DTTT_button float_left')),
            array("class"=>"thickbox vote_thickbox","title"=>"Consultation")); ?>
        </td>
        </tr>
<?php
    endforeach;
endif;
?>
	</tbody>
</table>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    tb_init("a.vote_thickbox");
    $('#patients_table').dataTable({
        //"bPaginate": false,
        //"bLengthChange": false,
        //"bFilter": false,
        "bSort": false,
        //"bInfo": false,
        "bStateSave": true,
    });
});
</script>
