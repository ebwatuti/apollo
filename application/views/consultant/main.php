<?php 
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));

$this->load->view("partial/header"); ?>

<div id="title_bar">
	<div id="page_title" style="margin-bottom:8px;">
	<?php
		$clinic = $this->Consultation->clinic_name($this->session->userdata('clinic'));
		echo $clinic;
	?>
	</div>
	
</div>

<div id="sidebar">
	<?php echo anchor("$controller_name/view_procedures/width:550",
	"<div class='submenu_button' style='float: left;'><span>Procedures</span></div>",
	array('title'=>$clinic.' Procedures','class'=>'thickbox none',));
	?>
	<?php echo anchor("$controller_name/view_sections/width:550",
	"<div class='submenu_button' style='float: left;'><span>Sections</span></div>",
	array('title'=>$clinic.' Sections','class'=>'thickbox none',));
	?>
	<?php echo anchor("$controller_name/view_workload/width:450",
	"<div class='submenu_button' style='float: left;'><span>Workload</span></div>",
	array('title'=>$clinic.' Workload','class'=>'thickbox none',));
	?>
	<?php echo anchor("$controller_name/view_morbidity/width:850",
	"<div class='submenu_button' style='float: left;'><span>Morbidity</span></div>",
	array('title'=>$clinic.' Morbidity','class'=>'thickbox none',));
	?>
	<?php echo anchor("$controller_name/view_archive/width:950",
	"<div class='submenu_button' style='float: left;'><span>Archive</span></div>",
	array('title'=>$clinic.' Archive','class'=>'thickbox none',));
	?>
</div>
<div id="table_holder">

</div>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	var html = "<table width='100%' height='100px'><tr><td align='center'><img src='<?php echo base_url()?>images/loading_animation.gif' alt='spinner' /></td></tr><table>";
	$("#table_holder").html(html);
	$("#table_holder").load('<?php echo site_url("$controller_name/view_all"); ?>');	

	var table_reload = setInterval(function(){$("#table_holder").load('<?php echo site_url("$controller_name/view_all"); ?>');},30000);	
});

</script>
