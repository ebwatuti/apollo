<?php
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));

 echo form_open($controller_name."/save_medical_history",array('id'=>'save_medical_history_form','class'=>'consultation_form')); ?>

<table id="register">
<tbody id="cart_contents">
<tr>
<td width="20%">Previous Admissions</td>
<td align="left"><?php echo form_textarea(array(
									'name'=>'previous_admission',
									'id'=>'previous_admission',
									'value'=>$medical_history["previous_admission"],
									'rows'=>'5',
									'onchange' => 'save_medical_history()')); ?></td>
</tr>
<tr>
<td width="10%">Medication</td>
<td align="left"><?php echo form_textarea(array(
									'name'=>'medication',
									'id'=>'medication',
									'value'=>$medical_history["medication"],
									'rows'=>'5',
									'onchange' => 'save_medical_history()')); ?></td>
</tr>
<tr>
<td width="10%">Allergies</td>
<td align="left"><?php echo form_textarea(array(
									'name'=>'allergies',
									'id'=>'allergies',
									'value'=>$medical_history["allergies"],
									'rows'=>'5',
									'onchange' => 'save_medical_history()')); ?></td>
</tr>
<tr>
<td width="10%">Chronic Illness
<p style='font-size:smaller; font-style:italic'>(include TB, HIV, HTN, Epilepsy, Asthma etc)</p>
</td>
<td align="left"><?php echo form_textarea(array(
									'name'=>'chronic_illness',
									'id'=>'chronic_illness',
									'value'=>$medical_history["chronic_illness"],
									'rows'=>'5',
									'onchange' => 'save_medical_history()')); ?></td>
</tr>
<tr>
<td width="10%">Previous Surgery</td>
<td align="left"><?php echo form_textarea(array(
									'name'=>'previous_surgery',
									'id'=>'previous_surgery',
									'value'=>$medical_history["previous_surgery"],
									'rows'=>'5',
									'onchange' => 'save_medical_history()')); ?></td>
</tr>

</tbody>
</table>

</form>
<script type='text/javascript'>
function save_medical_history()
{
	$("#save_medical_history_form").ajaxSubmit();
}
</script>