<div>
<?php echo form_button(array("content"=>"Print","onclick"=>"print_doc(this)","html"=>"summary_area","class"=>"print_button float_right")); ?>
<?php echo form_button(array("content"=>"PDF","onclick"=>"pdf_doc(this)","html"=>"summary_area","class"=>"print_button float_right")); ?>
<?php echo form_open('home/print_doc',array('id'=>'print_doc_form','base_url'=>site_url("home/print_doc"))); ?>
<input type="hidden" name="title" id="doc_title"  />
<input type="hidden" name="subtitle" id="doc_subtitle"  />
<input type="hidden" name="html" id="doc_html"  />
<?php echo form_close(); ?>
</div>
<div id="summary_area">
<table>
<tr><td><b>Patient #:&emsp;</b></td><td><?php echo $patient->patient_no; ?></td></tr>
<tr><td><b>Patient Name:&emsp;</b></td><td><?php echo $patient->patient_name; ?></td></tr>
<tr><td><b>Age:&emsp;</b></td><td><?php echo $patient->age; ?></td></tr>
<tr><td><b>Diagnosed by:&emsp;</b></td><td>Dr. <?php echo $consultant->employee_name; ?></td></tr>
<tr><td><b>Date:&emsp;</b></td><td><?php echo $consultation_date ? $consultation_date : date('j/n/Y g:i a'); ?></td></tr>
</table>
<hr />

<?php if(!empty($complaints)){ ?>
<div id="examination_header_bar">Chief Complaints</div>
<table><tfoot><tr><td>&nbsp;</td></tr></tfoot>
<tbody valign="top">
<tr>
<td><?php echo nl2br($complaints["complaints"]); ?></td>
</tr>
</tbody></table>
<hr />
<?php } ?>

<?php if(!empty($medical_history)){ ?>
<div id="examination_header_bar">Medical History</div>
<table><tfoot><tr><td>&nbsp;</td></tr></tfoot>
<tbody valign="top">
<?php if(!empty($medical_history['previous_admission'])){ ?>
<tr><th>Previous Admissions:&emsp;</th><td><?php echo nl2br($medical_history['previous_admission']); ?></td></tr>
<?php } ?>
<?php if(!empty($medical_history['medication'])){ ?>
<tr><th>Medication:&emsp;</th><td><?php echo nl2br($medical_history['medication']); ?></td></tr>
<?php } ?>
<?php if(!empty($medical_history['allergies'])){ ?>
<tr><th>Allergies:&emsp;</th><td><?php echo nl2br($medical_history['allergies']); ?></td></tr>
<?php } ?>
<?php if(!empty($medical_history["chronic_illness"])){ ?>
<tr><th>Chronic Illness:&emsp;</th><td><?php echo nl2br($medical_history["chronic_illness"]); ?></td></tr>
<?php } ?>
<?php if(!empty($medical_history["previous_surgery"])){ ?>
<tr><th>Previous Surgery:&emsp;</th><td><?php echo nl2br($medical_history["previous_surgery"]); ?></td></tr>
<?php } ?>
</tbody></table>
<hr />
<?php } ?>

<?php if(!empty($family_history)){ ?>
<div id="examination_header_bar">Family/Social History</div>
<table><tfoot><tr><td>&nbsp;</td></tr></tfoot>
<tbody valign="top">
<?php if(!empty($family_history['occupation'])){ ?>
<tr><th>Occupation:&emsp;</th><td><?php echo nl2br($family_history['occupation']); ?></td></tr>
<?php } ?>
<?php if(!empty($family_history['alcohol'])){ ?>
<tr><th>Alcohol:&emsp;</th><td><?php echo nl2br($family_history['alcohol']); ?></td></tr>
<?php } ?>
<?php if(!empty($family_history['cigarettes'])){ ?>
<tr><th>Cigarettes:&emsp;</th><td><?php echo nl2br($family_history['cigarettes']); ?></td></tr>
<?php } ?>
<?php if(!empty($family_history["familial_diseases"])){ ?>
<tr><th>Familial Diseases:&emsp;</th><td><?php echo nl2br($family_history["familial_diseases"]); ?></td></tr>
<?php } ?>
</tbody></table>
<hr />
<?php } ?>

<?php if(!empty($obst_gyn) && $patient->gender == 'female'){ ?>
<div id="examination_header_bar">Obstetrics and Gynaecology History</div>
<table><tfoot><tr><td>&nbsp;</td></tr></tfoot>
<tbody valign="top">
<?php if(!empty($obst_gyn['parity'])){ ?>
<tr><th>Parity:&emsp;</th><td><?php echo nl2br($obst_gyn['parity']); ?></td></tr>
<?php } ?>
<?php if(!empty($obst_gyn['gravida'])){ ?>
<tr><th>Gravida:&emsp;</th><td><?php echo nl2br($obst_gyn['gravida']); ?></td></tr>
<?php } ?>
<?php if(!empty($obst_gyn['lmp_start'])){ ?>
<tr><th>LMP:&emsp;</th><td><?php echo nl2br($obst_gyn['lmp_start'])." to ".nl2br($obst_gyn['lmp_end']); ?></td></tr>
<?php } ?>
<?php if(!empty($obst_gyn["menarche"])){ ?>
<tr><th>Menarche:&emsp;</th><td><?php echo nl2br($obst_gyn["menarche"]); ?></td></tr>
<?php } ?>
<?php if(!empty($obst_gyn["menses"])){ ?>
<tr><th>Menses:&emsp;</th><td><?php echo nl2br($obst_gyn["menses"]); ?></td></tr>
<?php } ?>
</tbody></table>
<hr />
<?php } ?>

<?php if(!empty($other_sections)){ ?>
<?php
	foreach($other_sections as $section=>$fields):
		$section = $this->Consultation->section_info($section);
?>
<div id="examination_header_bar"><?php echo $section->section_name; ?></div>
<table><tfoot><tr><td>&nbsp;</td></tr></tfoot>
<tbody valign="top">
<?php
	foreach($fields as $field=>$value):
		if(!$value) continue;
?>
<tr><th><?php echo preg_replace('/_/s', " ",$field); ?>:&emsp;</th><td><?php echo nl2br($value); ?></td></tr>
<?php endforeach; ?>
</tbody></table>
<hr />
<?php endforeach; ?>
<?php } ?>

<?php if(!empty($examination)){ ?>
<?php
	$examination_values = array();
	$this->db->distinct();
	$this->db->select('section,category,sub_category,value');
	$this->db->from('examinations');
	$values = $this->db->get();
	
	foreach($values->result() as $value):
		$var = $value->section."_".$value->category."_".$value->sub_category."_".$value->value;
        $var = strtolower(preg_replace('/[^a-zA-Z0-9_]/s', "_",$var));
		if(isset($examination["$var"]) && !empty($examination["$var"])):
			$examination_values["$value->section"]["$value->category"]["$value->sub_category"]["$value->value"] = $examination["$var"];
			if(isset($examination[$var."_describe"]) && !empty($examination[$var."_describe"])) 
				$examination_values["$value->section"]["$value->category"]["$value->sub_category"]["$value->value"] .= " (" . $examination[$var."_describe"] . ")";
		endif;
		$var = $value->section."_".$value->category."_examination_remarks";
		$var = strtolower(preg_replace('/[^a-zA-Z0-9_]/s', "_",$var));
		if(isset($examination["$var"]) && !empty($examination["$var"])):
			unset($examination_values["$value->section"]["$value->category"]["Other Remarks"]);
			$examination_values["$value->section"]["$value->category"]["Other Remarks"] = $examination["$var"];
		endif;
	endforeach;
	
	foreach ($examination_values as $section => $categories):
?>
		<div id="examination_header_bar"><?php echo ucfirst(strtolower($section)); ?> Examinations</div>
        <?php
		foreach($categories as $category=>$sub_categories):
		?>
        	<table><thead><tr><th style="font-size:14px" colspan='2'><?php echo $category; ?></th></tr></thead><tfoot><tr><td>&nbsp;</td></tr></tfoot>
				<tbody valign="top">
                	<?php
					foreach($sub_categories as $sub_category=>$values):
						if(is_array($values)):
					?>
                    		<tr><th colspan="2" align="center"><?php echo $sub_category; ?></th></tr>
                        	<?php
							foreach($values as $value=>$result):
							?>
                        	<tr>
                            <?php if(!empty($value)): ?><td  style="font-style:italic;"><?php echo $value; ?>:&emsp;</td><?php endif; ?>
                            <td><?php echo nl2br($result); ?></td></tr>
                    <?php
							endforeach;
						else:
					?>
                    		<tr><th><?php echo $sub_category; ?>:&emsp;</td><td><?php echo nl2br($values); ?></td></tr>
                    	<?php
						endif;
						?>
                    	<tr><td>&ensp;</td></tr>	
					<?php
                    endforeach;
					?>
                </tbody>
            </table>
            <hr />      
<?php
		endforeach;
	endforeach;
?>
<?php } ?>

<?php if(!empty($lab_tests)){ ?>
<div id="examination_header_bar">Lab Tests</div>
<table width="90%"><tfoot><tr><td>&nbsp;</td></tr></tfoot>
	<thead><tr><th style="background-color: #0a6184; color:#FFF">Test</th><th style="background-color: #0a6184; color:#FFF">Results</th></tr></thead>
    <tbody>
<?php
foreach(array_reverse($lab_tests, true) as $line=>$item):
?>
<tr><td style="background-color: #DDD"><?php echo $item['value']; ?></td>
<td style="background-color: #DDD">
<?php foreach($item['results'] as $option=>$value): ?>
<strong><?php if(is_string($option)) echo "$option:&emsp;"; ?></strong><?php echo nl2br($value); ?><br>
<?php endforeach; ?>
</td></tr>
<?php endforeach; ?>
</tbody></table>
<hr />
<?php } ?>

<?php if(!empty($diagnosis)){ ?>
<div id="examination_header_bar">Diagnosis</div>
<table width="90%"><tfoot><tr><td>&nbsp;</td></tr></tfoot>
	<tbody>
<?php foreach(array_reverse($diagnosis, true) as $line=>$item): ?>
	<tr><td><?php if($item['description']): ?><strong><?php echo $item['description']; ?>:&emsp;</strong><?php endif; ?>
	<?php echo nl2br($item['remarks']); ?></td><tr><td>&ensp;</td></tr>
<?php endforeach; ?>
</tbody></table>
<hr />
<?php } ?>

<?php if(!empty($procedures)){ ?>
<div id="examination_header_bar">Procedures</div>
<table width="90%"><tfoot><tr><td>&nbsp;</td></tr></tfoot>
	<thead><tr><th style="background-color: #0a6184; color:#FFF">Procedure</th>
	<th style="background-color: #0a6184; color:#FFF">Qty</th>
	<th style="background-color: #0a6184; color:#FFF">Remarks</th>
	<th style="background-color: #0a6184; color:#FFF">Results</th></tr></thead>
    <tbody>
<?php
foreach(array_reverse($procedures, true) as $line=>$item):
?>
<tr><td style="background-color: #DDD"><?php echo $item['name']; ?></td>
<td style="background-color: #DDD" align="right"><?php echo $item['quantity']; ?></td>
<td style="background-color: #DDD"><?php echo nl2br($item['remarks']); ?></td>
<td style="background-color: #DDD"><?php echo nl2br($item['results']); ?></td></tr>
<?php endforeach; ?>
</tbody></table>
<hr />
<?php } ?>

<?php if(!empty($treatment_plan)){ ?>
<div id="examination_header_bar">Treatment Plan</div>
<table><tfoot><tr><td>&nbsp;</td></tr></tfoot>
<tbody valign="top">
<tr>
<td><?php echo nl2br($treatment_plan["treatment_plan"]); ?></td>
</tr>
</tbody></table>
<hr />
<?php } ?>

<?php if(!empty($prescription)){ ?>
<div id="examination_header_bar">Prescription</div>
<table width="90%"><tfoot><tr><td>&nbsp;</td></tr></tfoot>
	<thead><tr><th style="background-color: #0a6184; color:#FFF">Drug</th>
	<th style="background-color: #0a6184; color:#FFF">Strength</th>
	<th style="background-color: #0a6184; color:#FFF">Dose</th>
	<th style="background-color: #0a6184; color:#FFF">Frequency</th>
	<th style="background-color: #0a6184; color:#FFF">Duration</th>
	<th style="background-color: #0a6184; color:#FFF">Disp #</th>
	<th style="background-color: #0a6184; color:#FFF">Remarks</th></tr></thead>
    <tbody>
<?php
foreach(array_reverse($prescription, true) as $line=>$item):
?>
<tr><td style="background-color: #DDD"><?php echo $item['name']; ?></td>
<td style="background-color: #DDD"><?php echo $item['strength']; ?></td>
<td style="background-color: #DDD"><?php echo $item['dose']; ?></td>
<td style="background-color: #DDD"><?php echo $item['frequency']; ?></td>
<td style="background-color: #DDD"><?php echo $item['duration']; ?></td>
<td style="background-color: #DDD"><?php echo $item['quantity']; ?></td>
<td style="background-color: #DDD"><?php echo nl2br($item['remarks']); ?></td></tr>
<?php endforeach; ?>
</tbody></table>
<hr />
<?php } ?>

</div>

<script type="text/javascript">
function print_doc(button)
{
	var title = "<?php echo $this->config->item('company'); ?>";
	var subtitle = "Consultation Notes";
	var content = $(button).attr("html");
	var html = $("#"+content).html();
	$.post("<?php echo site_url('home/print_doc'); ?>",{title:title,subtitle:subtitle,html:html},function(result){
		CallPrint(result,true);
	  });
}

function pdf_doc(button)
{
	var title = "<?php echo $this->config->item('company'); ?>";
	var subtitle = "Consultation Notes";
	var content = $(button).attr("html");
	var html = $("#"+content).html();
	
	$("#doc_title").val(title);
	$("#doc_subtitle").val(subtitle);
	$("#doc_html").val(html);
	
	var action = $("#print_doc_form").attr("base_url");
	action += "/pdf";
	$("#print_doc_form").attr("action",action);
	$("#print_doc_form").submit();
}
</script>