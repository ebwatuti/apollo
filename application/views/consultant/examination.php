<?php
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));
  
  echo form_open($controller_name.'/save_examination',array('id'=>'save_examination_form','class'=>'consultation_form')); 
?>
<?php
	$this->db->distinct();
	$this->db->select('section');
	$this->db->like("clinic",$clinic);
	$this->db->from('examinations');
	$sections = $this->db->get();
	
	foreach($sections->result() as $section):
		$this->db->distinct();
		$this->db->select('category');
		$this->db->where('section',$section->section);
		$this->db->like("clinic",$clinic);
		$this->db->from('examinations');
		$categories = $this->db->get();
?>

<div id="examination_header_bar"><?php echo ucfirst(strtolower($section->section)); ?> Examinations</div>
<div id="Accordion_<?php echo strtolower($section->section); ?>_examination" class="Accordion" tabindex="0">

<?php
	foreach($categories->result() as $category):
		$this->db->distinct();
		$this->db->select('sub_category');
		$this->db->where('category',$category->category);
		$this->db->where('section',$section->section);
		$this->db->like("clinic",$clinic);
		$this->db->from('examinations');
		$sub_categories = $this->db->get();		
?>	
		<div class="AccordionPanel">
            <div class="AccordionPanelTab"><?php echo $category->category; ?></div>
            <div class="AccordionPanelContent">
            <table height="200px">
    			<tr><td valign="top" width="58%">
<?php	
		    foreach($sub_categories->result() as $sub_category):
                $this->db->select('value,options,description');
                $this->db->where('category',$category->category);
                $this->db->where('sub_category',$sub_category->sub_category);
                $this->db->where('section',$section->section);
				$this->db->like("clinic",$clinic);
                $this->db->from('examinations');
                //$this->db->order_by('value','ASC');
                $values = $this->db->get();
?>	
                <table>
                <?php if ($sub_category->sub_category): ?><thead bgcolor="#BBBBBB"><tr><th colspan="3"><?php echo $sub_category->sub_category; ?></th></tr></thead><?php endif; ?>
                <tbody>
<?php			
                foreach($values->result() as $value):
					$other_remarks = true;
					$options = split(',',$value->options);
                    $var = $section->section."_".$category->category."_".$sub_category->sub_category."_".$value->value;
                    $var = strtolower(preg_replace('/[^a-zA-Z0-9_]/s', "_",$var));
					
					if ($value->value):
    ?>	
                        <tr><td valign="top" align="right" style="border-bottom:thin; border-bottom-style: dashed"><strong><?php echo $value->value ?></strong>&emsp;</td>
                        <td style="border-bottom:thin; border-bottom-style:dashed">
<?php
                        if (empty($options[0])):
?>
                            <input type="text" name="<?php echo $var; ?>" value="<?php echo $examination["$var"]; ?>" onchange="save_examination()" />
<?php
                        else:
                            foreach($options as $option):
?>
                                <label><input type="radio" name="<?php echo $var; ?>" value="<?php echo $option; ?>" <?php echo ($examination["$var"]==$option) ? 'checked="checked"':''; ?> onchange="save_examination()" />&emsp;
                                <?php echo $option; ?></label>
                                <br />
<?php
                            endforeach;
                        endif;
                        
                        if ($value->description == '1'):
?>
                            <span style='font-size:smaller; font-style:italic'>Describe:&emsp;
                            <input type="text" name='<?php echo $var."_describe"; ?>' value="<?php echo $examination[$var."_describe"]; ?>" onchange="save_examination()" />
                            </span>
<?php  					endif;?>
                        </td></tr>
<?php
					else:
						$other_remarks = false;
						$data = array(
								  "id"       => $var,
								  "name"       => $var,
								  "cols"   => "50",
								  "rows"        => "10",
								  "onchange" => "save_examination()",
								  "value" => $examination[$var],
								);
					
						echo form_textarea($data);
?>
<?php
                     endif;
				endforeach;
?>	
                </tbody>
                <tfoot><tr><td colspan="3" style="border-bottom:thick; border-bottom-style: ridge">&nbsp;</td></tr></tfoot>
                </table>
<?php
            endforeach;
            $var = strtolower(preg_replace('/[^a-zA-Z0-9_]/s', "_",$section->section."_".$category->category));
?>	
                </td><?php if($other_remarks): ?><td valign="top">
                     <table><tr><td valign="top"><b>Other Remarks</b></td>
                         <td><?php
                            
                            $data = array(
                                  "id"       => $var."_examination_remarks",
                                  "name"       => $var."_examination_remarks",
                                  "cols"   => "30",
                                  "rows"        => "8",
                                  "onchange" => "save_examination()",
                                  "value" => $examination[$var.'_examination_remarks'],
                                );
                    
                        echo form_textarea($data);
                    
                         ?></td><?php endif; ?></tr>
                     </table>
                 </td>
               </tr></table>
			</div>
		</div>
<?php
	endforeach;
?>
</div>

<script type="text/javascript" language="javascript">
var Accordion_<?php echo strtolower($section->section); ?>_examination = new Spry.Widget.Accordion("Accordion_<?php echo strtolower($section->section); ?>_examination");
</script>
<?php
	endforeach;
	
	echo form_close();
?>


<script type="text/javascript" language="javascript">
function save_examination()
{
	$("#save_examination_form").ajaxSubmit();
}
</script>
