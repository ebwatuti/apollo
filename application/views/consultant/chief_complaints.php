<?php
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));

 echo form_open($controller_name."/save_complaints",array('id'=>'save_complaints_form','class'=>'consultation_form')); ?>

<table>
<tbody>
<tr>
<td valign="top"><label for="complaints"><strong>Remarks:</strong>&emsp;</label></td>
<td>
<?php echo form_textarea(array(
            'name'=>'complaints',
			'id'=>'complaints',
            'value'=>$complaints['complaints'],
            'rows'=>'10',
			'cols'=>'50',
            'onchange' => 'save_complaints()')); ?>
</td>
</tr>
</tbody>
</table>

</form>
<script type='text/javascript'>
function enable_text(source,target)
{
	if(source.checked == "checked") $("#"+target).attr("readonly","readonly");
	
}

function save_complaints()
{
	$("#save_complaints_form").ajaxSubmit();
}
</script>
