<?php 
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));
 ?>
<?php if(!empty($possible_diagnosis)): ?>
<div id="examination_header_bar">Possible Diagnosis</div>
<table width="100%" id="possible_diagnosis_table">
<thead>
<tr>
<th style="background-color: #0a6184; color:#FFF">ICD 10 Code</th>
<th style="background-color: #0a6184; color:#FFF">Description</th>
<th style="background-color: #0a6184; color:#FFF">Probability</th>
<th style="background-color: #0a6184; color:#FFF"></th>
</tr>
</thead>
<tbody id="possible_diagnosis_contents">
<?php foreach($possible_diagnosis as $item): ?>
	<?php if(array_key_exists($item['diagnosis_code'], $diagnosis)) continue; ?>
		<tr>
		<td style="background-color: #DDD"><?php echo $item['diagnosis_code']; ?></td>
		<td style="background-color: #DDD"><?php echo $item['description']; ?></td>
        <td style="background-color: #DDD"><?php echo round($item['probability'] * 100 , 3) . '%'; ?></td>
        
		<td style="background-color: #DDD; align: center; "><?php echo form_button(array(
        'content'=>'Add',
        'class'=>'submit_button float_right',
        'diagnosis_code'=>$item['diagnosis_code'],
        'onclick'=>"add_possible_diagnosis(this)"
    ));?></td>
		</tr>
	<?php endforeach; ?>
</tbody>
</table>
<?php endif; ?>
<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $('#possible_diagnosis_table').dataTable({
        //"bPaginate": false,
        //"bLengthChange": false,
        "bFilter": false,
        "bSort": false,
        //"bInfo": false,
        "bStateSave": true,
    });
});
function add_possible_diagnosis(button)
{
	$.post('<?php echo site_url($controller_name."/add_diagnosis"); ?>',
                {item: $(button).attr('diagnosis_code')},
                function(response){
                	$("#message_bar").removeClass('error_message');
	                $("#message_bar").removeClass('warning_message');
	                $("#message_bar").removeClass('success_message');
	                $("#message_bar").addClass(response.message_class);
	                $("#message_bar").html(response.message);
	                $('#message_bar').fadeTo(5000, 1);
	                $('#message_bar').fadeTo("fast",0);
					
					if(response.success) $("#diagnosis_tab").load("<?php echo site_url($controller_name."/refresh_diagnosis"); ?>");
                },'json');	
}
</script>