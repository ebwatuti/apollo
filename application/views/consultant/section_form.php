<?php
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));
?>
<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>

<fieldset id="supplier_basic_info">
<legend>Section Info</legend>
<?php echo form_open("$controller_name/save_section/$section_info->section_id",array('id'=>'section_form')); ?>

<div class="field_row clearfix">	
<?php echo form_label('Section Name:', 'section_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'section_name',
		'id'=>'section_name',
		'value'=>$section_info->section_name)
	);?>
	</div>
</div>
<?php if($section_info->view == 0): ?>
<div class="field_row clearfix">	
<?php echo form_label("Section Fields:", 'section_fields'); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
		'name'=>'section_fields',
		'id'=>'section_fields',
		'rows'=>5,
		'value'=>$section_info->section_fields)
	);?>
	<br><span style='font-style:italic'>(separate with a comma)</span>
	</div>
</div>
<?php endif; ?>
<div class="field_row clearfix wide">
<?php echo form_label("Displayed:", 'displayed'); ?>
	<div class='form_field'>
	<?php echo form_checkbox(array(
		'name'=>'displayed',
		'value'=>1,
		'checked'=>($section_info->displayed == 1) ? true : false)
	);?>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_button(array("id"=>"section_form_submit","content"=>"Submit","class"=>"submit_button float_left"));?>
	<?php echo form_button(array("id"=>"section_form_cancel","content"=>"Cancel","class"=>"submit_button float_right"));?>
</div>

<?php  echo form_close(); ?>
</fieldset>

<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{	
	$("#section_form_submit").click(function(){
		$("#section_form").ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					//tb_remove();
					if(response.success)
					{
						$("#TB_ajaxContent").load('<?php echo site_url("$controller_name/view_sections"); ?>',
							function(result){
								$("#message_bar").removeClass('error_message');
	                            $("#message_bar").removeClass('warning_message');
	                            $("#message_bar").removeClass('success_message');
	                            $("#message_bar").addClass(response.message_class);
	                            $("#message_bar").html(response.message);
	                            $('#message_bar').fadeTo(5000, 1);
	                            $('#message_bar').fadeTo("fast",0);
							});
					}
					else
					{
						$("#message_bar").removeClass('error_message');
	                    $("#message_bar").removeClass('warning_message');
	                    $("#message_bar").removeClass('success_message');
	                    $("#message_bar").addClass(response.message_class);
	                    $("#message_bar").html(response.message);
	                    $('#message_bar').fadeTo(5000, 1);
	                    $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});

	$("#section_form_cancel").click(function(){
		$("#section_form_area").css('display','none');
	    $("#section_form_area").html('&ensp;');
	    $('#sections').css('display','block');
	});
});
</script>