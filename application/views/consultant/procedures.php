<?php 
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));

echo form_open($controller_name."/add_procedure",array('id'=>'add_procedure_form','class'=>'add_item_form','onkeypress'=>'return event.keyCode != 13;'));?>
<label id="item_label" for="procedure_item">Procedure</label>
<?php echo form_input(array('name'=>'item','id'=>'procedure_item','size'=>'40','placeholder'=>'Start typing procedure name'));?>


</form>
<table id="register">
<thead>
<tr>
<th>Procedure Name</th>
<th>Unit Price</th>
<th>Qty</th>
<th>Disc %</th>
<th>Total</th>
<th>Remarks</th>
<th>Results</th>
<th></th>
</tr>
</thead>
<tbody id="procedures_contents">

<?php foreach(array_reverse($procedures, true) as $item): ?>
		<tr>
        <td style="align:center;"><?php echo $item['name']; ?></td>
        <td style="align:center;"><?php echo ($item['status'] == 0 && $item['invoice_status'] == 0) ? form_input(array(
                                    'name'=>'unit_price',
                                    'value'=>$item['unit_price'],
                                    'procedure_id'=>$item['procedure_id'],
                                    'type'=>'number',
                                    'length'=>'1',
                                    'min'=>'0',
                                    'max'=>$item['original_unit_price'],
                                    'onchange' => 'save_procedure_instructions(this)'))
                                    : $item['unit_price']; ?></td>
        <td style="align:center;"><?php echo ($item['status'] == 0 && $item['invoice_status'] == 0) ? form_input(array(
                                    'name'=>'quantity',
                                    'value'=>$item['quantity'],
                                    'procedure_id'=>$item['procedure_id'],
                                    'type'=>'number',
                                    'length'=>'1',
                                    'min'=>'1',
                                    'onchange' => 'save_procedure_instructions(this)'))
                                    : $item['quantity']; ?></td>
        <td style="align:center;"><?php echo $item['discount']; ?></td>
        <td style="align:center;" id="total_<?php echo $item['procedure_id'];?>"><?php echo to_currency($item['quantity'] * $item['unit_price'] * (100 - $item['discount']) / 100); ?></td>
        <td style="align:center;"><?php echo ($item['status'] == 0 && $item['invoice_status'] == 0) ? form_textarea(array(
                                    'name'=>'remarks',
                                    'value'=>$item['remarks'],
                                    'rows'=>'3',
                                    'cols'=>15,
                                    'procedure_id'=>$item['procedure_id'],
                                    'onchange' => 'save_procedure_instructions(this)'))
                                    : $item['remarks']; ?></td>
        <td style="align:center;"><?php echo ($item['status'] == 1 && $item['invoice_status'] == 1) ? form_textarea(array(
                                    'name'=>'results',
                                    'value'=>$item['results'],
                                    'rows'=>'3',
                                    'cols'=>15,
                                    'procedure_id'=>$item['procedure_id'],
                                    'onchange' => 'save_procedure_instructions(this)'))
                                    : $item['results']; ?></td>
        <td><?php echo ($item['status'] == 0  && $item['invoice_status'] == 0) ? form_button(array(
        'content'=>'Delete',
        'class'=>'submit_button float_right',
        'procedure_id'=>$item['procedure_id'],
        'onclick'=>"delete_procedure(this)"
    ))
    : '&ensp;';?></td>
        </tr>
        
        <tr style="height:3px">
        <td colspan=8 style="background-color:white"> </td>
        </tr>       </form>

	<?php endforeach; ?>
</tbody>
</table>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $("#procedure_item").autocomplete('<?php echo site_url($controller_name."/procedure_search"); ?>',
    {
    	minChars:0,
    	max:100,
    	selectFirst: false,
       	delay:10,
    	formatItem: function(row) {
			return row[1];
		}
    });

    $("#procedure_item").result(function(event, data, formatted)
    {
		$("#add_procedure_form").ajaxSubmit({
			success:function(response){
				$("#message_bar").removeClass('error_message');
                $("#message_bar").removeClass('warning_message');
                $("#message_bar").removeClass('success_message');
                $("#message_bar").addClass(response.message_class);
                $("#message_bar").html(response.message);
                $('#message_bar').fadeTo(5000, 1);
                $('#message_bar').fadeTo("fast",0);
				
				if(response.success) $("#procedures_tab").load("<?php echo site_url($controller_name."/refresh_procedures"); ?>");
			},dataType:'json'});
    });
});

function save_procedure_instructions(input)
{
	$.post('<?php echo site_url($controller_name."/procedure_info"); ?>/' + $(input).attr('procedure_id') + '/' + $(input).attr('name'),
                {value: $(input).val()},
            function(response){
                $('#total_'+$(input).attr('procedure_id')).html(response);
            }
                );
}

function delete_procedure(button)
{
	$("#procedures_tab").load('<?php echo site_url($controller_name."/remove_procedure"); ?>/' + $(button).attr('procedure_id'));
}
</script>
