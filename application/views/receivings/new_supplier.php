<div width="100%" id="message_bar"></div>
<?php echo form_open('receivings/save_supplier/'.$supplier_info->person_id,array('id'=>'supplier_form')); ?>
<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>
<fieldset id="supplier_basic_info">
<legend>Supplier Info</legend>
<div class="field_row clearfix">	
<?php echo form_label('Company Name:', 'company_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'company_name',
		'id'=>'company_name',
		'value'=>$supplier_info->company_name)
	);?>
	</div>
</div>
<div class="field_row clearfix">	
<?php echo form_label('Account #:', 'account_number'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'account_number',
		'id'=>'account_number',
		'value'=>$supplier_info->account_number)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Phone Number:', 'phone_number'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'phone_number',
		'id'=>'phone_number',
		'value'=>$supplier_info->phone_number)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Address:', 'address_1'); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
            'name'=>'address_1',
			'id'=>'address_1',
            'value'=>$supplier_info->address_1,
            'rows'=>'5',
			'cols'=>'17',)); ?>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_button(array("id"=>"supplier_form_submit","content"=>"Submit","class"=>"submit_button float_left"));?>
	<?php echo form_button(array("id"=>"supplier_form_cancel","content"=>"Cancel","class"=>"submit_button float_right"));?>
</div>
</fieldset>

<?php echo form_close(); ?>

<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{	
	Date.format = 'dd-mm-yyyy';
	$('.date').datepicker({
      changeMonth: true,
      changeYear: true,
      minDate: new Date(1900, 1, 1),
      maxDate: "0",
      dateFormat: "dd-mm-yy",
    });
	
	$("#supplier_form_submit").click(function(){
		$("#supplier_form").ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					tb_remove();
					if(response.success)
					{
						$("#supplier").attr("value",response.supplier_id);
						$("#select_supplier_form").submit();
					}
					else
					{
						set_feedback(response.message,response.message_class,false);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});

	$("#supplier_form_cancel").click(function(){
		tb_remove();
	});
});
</script>