<?php $this->load->view("partial/header"); ?>

<div id="title_bar">
	<div id="page_title" style="margin-bottom:8px;">Purchases Register</div>
	
</div>
<?php
if(isset($error))
{
	echo "<div class='error_message'>".$error."</div>";
}

if (isset($warning))
{
	echo "<div class='warning_mesage'>".$warning."</div>";
}

if (isset($success))
{
	echo "<div class='success_message'>".$success."</div>";
}
?>
<div id="sidebar">
	<?php echo anchor("receivings/view_workload/width:550",
	"<div class='submenu_button' style='float: left;'><span>Workload</span></div>",
	array('title'=>'Receivings Workload','class'=>'thickbox none',));
	?>
	<?php echo anchor("receivings/view_archive/width:950",
	"<div class='submenu_button' style='float: left;'><span>Archive</span></div>",
	array('title'=>'Receivings Archive','class'=>'thickbox none',));
	?>
</div>
<div id="register_wrapper">
<?php echo form_open("receivings/change_mode",array('id'=>'mode_form')); ?>
	<span>Purchase Mode</span>
<?php echo form_dropdown('mode',$modes,$mode,'onchange="$(\'#mode_form\').submit();"'); ?>

</form>
<?php echo form_open("receivings/add",array('id'=>'add_item_form')); ?>
<label id="item_label" for="item">

<?php
if($mode=='receive')
{
	echo $this->lang->line('sales_find_or_scan_item');
}
else
{
	echo $this->lang->line('sales_find_or_scan_item_or_receipt');
}
?>
</label>
<?php echo form_input(array('name'=>'item','id'=>'item','size'=>'40','placeholder'=>'item name/#'));?>

</form>
<table id="register">
<thead>
<tr>
<th style="width:30%;">Item Name</th>
<th style="width:10%;">Price</th>
<th style="width:10%;">Qty</th>
<th style="width:10%;">Disc %</th>
<th style="width:15%;">Total</th>
<th style="width:25%;">&emsp;</th>
</tr>
</thead>
<tbody id="cart_contents">
<?php
if(count($cart)==0)
{
?>
<tr><td colspan='8'>
<div class='warning_message' style='padding:7px;'><?php echo $this->lang->line('sales_no_items_in_cart'); ?></div>
</tr></tr>
<?php
}
else
{
	foreach(array_reverse($cart, true) as $line=>$item)
	{
		$cur_item_info = $this->Item->get_info($item['item_id']);
		echo form_open("receivings/edit_item/$line");
	?>
		<tr>
		<td style="align:center;"><?php echo $item['name']; ?></td>



		<?php if ($items_module_allowed)
		{
		?>
			<td><?php echo form_input(array('name'=>'price','value'=>$item['price'],'size'=>'3'));?></td>
		<?php
		}
		else
		{
		?>
			<td><?php echo $item['price']; ?></td>
			<?php echo form_hidden('price',$item['price']); ?>
		<?php
		}
		?>

		<td>
		<?php
        	if($item['is_serialized']==1)
        	{
        		echo $item['quantity'];
        		echo form_hidden('quantity',$item['quantity']);
        	}
        	else
        	{
        		echo form_input(array('name'=>'quantity','value'=>$item['quantity'],'size'=>'2'));
        	}
		?>
		</td>

		<td><?php echo form_input(array('name'=>'discount','value'=>$item['discount'],'size'=>'3'));?></td>
		<td><?php echo to_currency($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100); ?></td>
		<td><?php echo form_submit(array('name'=>'edit_item', 'value'=>'Edit','class'=>'submit_button float_left'));?>
		&emsp;
		<?php echo anchor("receivings/delete_item/$line",
		"<div class='submit_button' style='float: left;'><span>Delete</span></div>");?></td>
		</tr>
		
		<tr style="height:3px">
		<td colspan=8 style="background-color:white"> </td>
		</tr>		</form>
	<?php
	}
}
?>
</tbody>
</table>
</div>


<div id="overall_sale">
	<?php
	if(isset($supplier))
	{
?>
<table width="100%">
    <tr><td colspan="2">
    <div class="field_row clearfix">    
    <?php echo form_label('Supplier Name:'); ?>
        <div class='form_field'>
        <?php echo $supplier;?>
        </div>
    </div>
    </td></tr>

    <tr><td colspan="2" width="100%">
        <?php
        echo anchor("receivings/remove_supplier",form_button(array('content'=>'Remove Supplier','class'=>'submit_button float_right')));
        ?>
    </td></tr>
</table>
<?php
	}
	else
	{
		echo form_open("receivings/select_supplier",array('id'=>'select_supplier_form')); ?>
		<label id="supplier_label" for="supplier">Select Supplier (optional)</label>
		<?php echo form_input(array('name'=>'supplier','id'=>'supplier','size'=>'30','placeholder'=>'supplier name'));?>
		</form>
		<div class="clearfix">&nbsp;</div>
		<div style="margin-top:5px;text-align:center;">
		<h3 style="margin: 5px 0 5px 0">or</h3>
		<?php
	        echo anchor("receivings/new_supplier/width:350",
	        	form_button(array('content'=>'New Supplier','class'=>'submit_button')),
	        	array('title'=>'New Supplier','class'=>'thickbox none',));
	    ?>
		</div>
	<?php
	}
	?>

	<div id='sale_details'>
		<div class="float_left" style='width:55%;'><?php echo $this->lang->line('sales_total'); ?>:</div>
		<div class="float_left" style="width:45%;font-weight:bold;"><?php echo to_currency($total); ?></div>
	</div>




	<?php
	// Only show this part if there are Items already in the sale.
	if(count($cart) > 0)
	{
	?>		
	<div class="clearfix">&nbsp;</div>
	<?php echo form_open("receivings/complete",array('id'=>'finish_receiving_form')); ?>
	<table width="100%">
	<tr>
	<td>
		<?php echo $this->lang->line('sales_payment').':   ';?>
	</td>
	<td>
		<?php echo form_dropdown( 'payment_type', $payment_options, array(), 'id="payment_types"' ); ?>
	</td>
	</tr>
	<tr>
	<td>
		<span id="amount_tendered_label"><?php echo $this->lang->line( 'sales_amount_tendered' ).': '; ?></span>
	</td>
	<td>
		<?php echo form_input( array( 'name'=>'amount_tendered', 'id'=>'amount_tendered', 'size'=>'10' ) );	?>
	</td>
	</tr>
	</table>
	<div class="clearfix">&nbsp;</div>
	<label id="comment_label" for="comment"><?php echo $this->lang->line('common_comments'); ?>:</label>
	<?php echo form_textarea(array('name'=>'comment','value'=>'','rows'=>'4','cols'=>'23'));?>
	<div class="clearfix">&nbsp;</div>
	<div id="finish_sale">
	<?php echo form_button(array('content'=>'Complete','class'=>'submit_button float_left','id'=>'finish_receiving_button')); ?>
	</form>
		<?php echo form_open("receivings/cancel_receiving",array('id'=>'cancel_receiving_form')); ?>
		<?php echo form_button(array('content'=>'Cancel','class'=>'submit_button float_right','id'=>'cancel_receiving_button')); ?>
    	</form>
	</div>

	<?php
	}
	?>
</div>
<div class="clearfix" style="margin-bottom:30px;">&nbsp;</div>


<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $("#item").autocomplete('<?php echo site_url("receivings/item_search"); ?>',
    {
    	minChars:0,
    	max:100,
    	selectFirst: false,
       	delay:10,
    	formatItem: function(row) {
			return row[1];
		}
    });

    $("#item").result(function(event, data, formatted)
    {
		$("#add_item_form").submit();
    });

	$('#item').focus();

	$("#supplier").autocomplete('<?php echo site_url("receivings/supplier_search"); ?>',
    {
    	minChars:0,
    	delay:10,
    	max:100,
    	formatItem: function(row) {
			return row[1];
		}
    });

    $("#supplier").result(function(event, data, formatted)
    {
		$("#select_supplier_form").submit();
    });

    $("#finish_receiving_button").click(function()
    {
    	if (confirm('Are you sure you want to save this purchase?'))
    	{
    		$('#finish_receiving_form').submit();
    	}
    });

    $("#cancel_receiving_button").click(function()
    {
    	if (confirm('Are yu sure you want to cancel this purchase?'))
    	{
    		$('#cancel_receiving_form').submit();
    	}
    });
});

function post_item_form_submit(response)
{
	if(response.success)
	{
		$("#item").attr("value",response.item_id);
		$("#add_item_form").submit();
	}
}

function post_person_form_submit(response)
{
	if(response.success)
	{
		$("#supplier").attr("value",response.person_id);
		$("#select_supplier_form").submit();
	}
}

</script>
