<div width="100%" id="message_bar"></div>
<div id="lab_tests">
<?php
    echo form_button(array(
        'name'=>'submit',
        'id'=>'new_lab_test_button',
        'content'=>'New Test',
        'class'=>'submit_button float_right')
    );
?>
<br><br>
<table width="100%" id="lab_tests_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Category</th>
        <th style="background-color: #0a6184; color:#FFF">Test Name</th>
        <th style="background-color: #0a6184; color:#FFF">Options</th>
        <th style="background-color: #0a6184; color:#FFF">Price</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>
    </tr></thead>
    <tbody>
<?php
if($lab_tests->num_rows() > 0):
	foreach($lab_tests->result() as $lab_test):
        $lab_test = $this->Lab->get_lab_test_info($lab_test->lab_test_id);
        $options = str_replace(',', ', ', $lab_test->options);
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $lab_test->category; ?></td>
        <td style="background-color: #DDD"><?php echo $lab_test->value; ?></td>
        <td style="background-color: #DDD"><?php echo character_limiter($options, 25); ?></td>
        <td style="background-color: #DDD"><?php echo $lab_test->item_info->unit_price; ?></td>
        <td style="background-color: #DDD">
		<?php
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'edit_lab_test('.$lab_test->lab_test_id.')',
                'content'=>'Edit',
                'class'=>'DTTT_button float_left')
            );
        ?>
        &emsp;
        <?php
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'delete_lab_test('.$lab_test->lab_test_id.')',
                'content'=>'Delete',
                'class'=>'DTTT_button float_left')
            );
        ?>
        </td>
        </tr>
<?php
	endforeach;
endif;
?>
	</tbody>
</table>
</div>

<div id="lab_test_form_area"></div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $('#lab_tests_table').dataTable({
        //"bPaginate": false,
        //"bLengthChange": false,
        //"bFilter": false,
        //"bSort": false,
        //"bInfo": false,
        "bStateSave": true,
    });

    $("#new_lab_test_button").click(function(){
        $("#lab_test_form_area").css('display','block');
        $("#lab_test_form_area").load('<?php echo site_url("labs/view_lab_test"); ?>');
        $('#lab_tests').css('display','none');
    });
});

function edit_lab_test(lab_test_id)
{
    $("#lab_test_form_area").css('display','block');
    $("#lab_test_form_area").load('<?php echo site_url("labs/view_lab_test"); ?>/' + lab_test_id);
    $('#lab_tests').css('display','none');
}

function delete_lab_test(lab_test_id)
{
    if(confirm('Are you sure you want to delete this lab test?'))
    {
        $.post('<?php echo site_url("labs/delete_lab_test"); ?>/' + lab_test_id,
            {},
            function(response){
                if(response.success)
                {
                    $("#TB_ajaxContent").load('<?php echo site_url("labs/view_lab_tests"); ?>',
                        function(result){
                            $("#message_bar").removeClass('error_message');
                            $("#message_bar").removeClass('warning_message');
                            $("#message_bar").removeClass('success_message');
                            $("#message_bar").addClass(response.message_class);
                            $("#message_bar").html(response.message);
                            $('#message_bar').fadeTo(5000, 1);
                            $('#message_bar').fadeTo("fast",0);
                        });
                }
                else
                {
                    $("#message_bar").removeClass('error_message');
                    $("#message_bar").removeClass('warning_message');
                    $("#message_bar").removeClass('success_message');
                    $("#message_bar").addClass(response.message_class);
                    $("#message_bar").html(response.message);
                    $('#message_bar').fadeTo(5000, 1);
                    $('#message_bar').fadeTo("fast",0);
                }
            },'json');
    }
}
</script>