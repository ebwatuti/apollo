<?php 
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));

echo form_open($controller_name."/add_lab_tests",array('id'=>'lab_tests_form'));
?>
<div id="examination_header_bar" align="center">Lab Tests</div>
<ul class="lab_items_list">
    <?php foreach($lab_tests->Result() as $lab_test): ?>
        <li><label>
        <?php echo form_checkbox(array(
            'name'=>'lab_tests[]',
            'value'=>$lab_test->lab_test_id,
            'onchange' => 'save_lab_tests()',
            'checked'=>array_key_exists($lab_test->lab_test_id, $selected_lab_tests) ? 'checked' : '',
            )); ?>
        &emsp;<?php echo $lab_test->value; ?></label></li>
    <?php endforeach; ?>
</ul>
</form>

<script type="text/javascript" language="javascript">
function save_lab_tests()
{
    $("#lab_tests_form").ajaxSubmit();
}
</script>
