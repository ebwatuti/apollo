<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>

<fieldset id="employee_basic_info">
<legend>Lab Test Info</legend>
<?php echo form_open("labs/save_lab_test/$lab_test->lab_test_id",array('id'=>'lab_test_form')); ?>

<div class="field_row clearfix">	
<?php echo form_label('Test Name:', 'test_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'test_name',
		'id'=>'test_name',
		'required'=>'required',
		'value'=>$lab_test->value)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Category:', 'category'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'category',
		'id'=>'category',
		'value'=>$lab_test->category)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label("Test Options:", 'options'); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
		'name'=>'options',
		'id'=>'options',
		'cols'=>20,
		'rows'=>5,
		'value'=>$lab_test->options)
	);?>
	<br><span style='font-style:italic'>(separate with a comma)</span>
	</div>	

</div>

<div class="field_row clearfix">	
<?php echo form_label('Price:', 'price',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'price',
		'id'=>'price',
		'value'=>$lab_test->item_info->unit_price)
	);?>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_button(array("id"=>"lab_test_form_submit","content"=>"Submit","class"=>"submit_button float_left"));?>
	<?php echo form_button(array("id"=>"lab_test_form_cancel","content"=>"Cancel","class"=>"submit_button float_right"));?>
</div>

<?php  echo form_close(); ?>
</fieldset>

<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{	
	$("#lab_test_form_submit").click(function(){
		$("#lab_test_form").ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					//tb_remove();
					if(response.success)
					{
						$("#TB_ajaxContent").load('<?php echo site_url("labs/view_lab_tests"); ?>',
							function(result){
								$("#message_bar").removeClass('error_message');
	                            $("#message_bar").removeClass('warning_message');
	                            $("#message_bar").removeClass('success_message');
	                            $("#message_bar").addClass(response.message_class);
	                            $("#message_bar").html(response.message);
	                            $('#message_bar').fadeTo(5000, 1);
	                            $('#message_bar').fadeTo("fast",0);
							});
					}
					else
					{
						$("#message_bar").removeClass('error_message');
	                    $("#message_bar").removeClass('warning_message');
	                    $("#message_bar").removeClass('success_message');
	                    $("#message_bar").addClass(response.message_class);
	                    $("#message_bar").html(response.message);
	                    $('#message_bar').fadeTo(5000, 1);
	                    $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});

	$("#lab_test_form_cancel").click(function(){
		$("#lab_test_form_area").css('display','none');
	    $("#lab_test_form_area").html('&ensp;');
	    $('#lab_tests').css('display','block');
	});

	$("#category").autocomplete("<?php echo site_url('labs/suggest_category');?>",{max:100,minChars:0,delay:10});
    $("#category").result(function(event, data, formatted){});
	$("#category").search();
});
</script>