<div>
<?php echo form_button(array("content"=>"Print","onclick"=>"print_doc(this)","html"=>"lab_report_holder","class"=>"print_button float_right")); ?>
<?php echo form_button(array("content"=>"PDF","onclick"=>"pdf_doc(this)","html"=>"lab_report_holder","class"=>"print_button float_right")); ?>
<?php echo form_open('home/print_doc',array('id'=>'print_doc_form','base_url'=>site_url("home/print_doc"))); ?>
<input type="hidden" name="title" id="doc_title"  />
<input type="hidden" name="subtitle" id="doc_subtitle"  />
<input type="hidden" name="html" id="doc_html"  />
<?php echo form_close(); ?>
</div>
<div id="lab_report_holder">
<?php
    $patient = $this->Patient->get_info($lab_report->patient_id);
    $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
    if($patient->dob) $patient->age = $this->Patient->patient_age($patient->dob);
    $patient->patient_no = $this->Patient->patient_number($patient->patient_id);

    $employee = $this->Employee->get_info($lab_report->served_by);
    $served_by = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
    $employee = $this->Employee->get_info($lab_report->employee_id);
    $requested_by = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
?>
<table>
<tr><td><b>Patient #:&emsp;</b></td><td><?php echo $patient->patient_no; ?></td></tr>
<tr><td><b>Patient Name:&emsp;</b></td><td><?php echo $patient->patient_name; ?></td></tr>
<tr><td><b>Age:&emsp;</b></td><td><?php echo $patient->age; ?></td></tr>
<tr><td><b>Requested by:&emsp;</b></td><td><?php echo $requested_by; ?></td></tr>
<tr><td><b>Served by:&emsp;</b></td><td><?php echo $served_by; ?></td></tr>
<tr><td><b>Date:&emsp;</b></td><td><?php echo date("M j, Y",strtotime($lab_report->service_time));?></td></tr>
</table>
<hr />

<table width="100%">
<thead>
<tr>
<th width="30%" style="background-color: #0a6184; color:#FFF">Test</th>
<th width="50%" style="background-color: #0a6184; color:#FFF">Results</th>
</tr>
</thead>
<tbody id="lab_report_contents">

<?php
    foreach($lab_report->invoice_items as $item):
        if($item->status == 1):
            $lab_test_id = $this->Lab->get_lab_test_id($item->item_id);
            $item->value = $this->Lab->get_lab_test_info($lab_test_id)->value;
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $item->value; ?></td>
        <td style="background-color: #DDD">
        <table>
        <?php
            $results = unserialize($item->results);
            foreach($results as $option=>$value):
        ?>
        <tr>    
        <?php if(is_string($option)) echo '<td>'.form_label($option).':&emsp;</td>'; ?>
            <td>
            <?php echo nl2br($value);?>
            </td>
        </tr>
        <?php endforeach; ?>
        </table>
        </td>
        </tr>
        
        <tr style="height:3px">
        <td colspan=8 style="background-color:white"> </td>
        </tr>       
<?php
        endif;
    endforeach; 
?>
</tbody>
</table>
</div>

<script type="text/javascript">
function print_doc(button)
{
    var title = "<?php echo $this->config->item('company'); ?>";
    var subtitle = "Lab Report";
    var content = $(button).attr("html");
    var html = $("#"+content).html();
    $.post("<?php echo site_url('home/print_doc'); ?>",{title:title,subtitle:subtitle,html:html},function(result){
        CallPrint(result,true);
      });
}

function pdf_doc(button)
{
    var title = "<?php echo $this->config->item('company'); ?>";
    var subtitle = "Lab Report";
    var content = $(button).attr("html");
    var html = $("#"+content).html();
    
    $("#doc_title").val(title);
    $("#doc_subtitle").val(subtitle);
    $("#doc_html").val(html);
    
    var action = $("#print_doc_form").attr("base_url");
    action += "/pdf";
    $("#print_doc_form").attr("action",action);
    $("#print_doc_form").submit();
}
</script>
