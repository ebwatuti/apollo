<?php $this->load->view("partial/header"); ?>
<div id="title_bar">
	<div id="page_title" style="margin-bottom:8px;">Lab Requests</div>
	
</div>
<div id="sidebar">
	<?php echo anchor("labs/new_request/width:950",
	"<div class='submenu_button' style='float: left;'><span>New Request</span></div>",
	array('title'=>'New Lab Request','class'=>'thickbox none',));
	?>
	<?php echo anchor("labs/view_lab_tests/width:650",
	"<div class='submenu_button' style='float: left;'><span>Lab Tests</span></div>",
	array('title'=>'Lab Tests','class'=>'thickbox none',));
	?>
	<?php echo anchor("labs/view_workload/width:450",
	"<div class='submenu_button' style='float: left;'><span>Workload</span></div>",
	array('title'=>'Lab Workload','class'=>'thickbox none',));
	?>
	<?php echo anchor("labs/view_archive/width:950",
	"<div class='submenu_button' style='float: left;'><span>Archive</span></div>",
	array('title'=>'Lab Reports Archive','class'=>'thickbox none',));
	?>
</div>

<div id="table_holder">

</div>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	var html = "<table width='100%' height='100px'><tr><td align='center'><img src='<?php echo base_url()?>images/loading_animation.gif' alt='spinner' /></td></tr><table>";
	$("#table_holder").html(html);
	$("#table_holder").load('<?php echo site_url("labs/view_all"); ?>');	

	var table_reload = setInterval(function(){$("#table_holder").load('<?php echo site_url("labs/view_all"); ?>');},30000);	
});

</script>
