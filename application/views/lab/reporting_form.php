<?php 
  //get the controller name 
  $CI =& get_instance();
  $controller_name=strtolower(get_class($CI));
?>

<div id="queue_section">
<?php
    $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
    if($patient->dob) $patient->age = $this->Patient->patient_age($patient->dob);
    $patient->patient_no = $this->Patient->patient_number($patient->patient_id);
?>
<table width="100%">
    <tr><th style="background-color: #0a6184; color:#FFF" colspan="2" align="center">Patient Info</th></tr>
    <tr><td colspan="2">
    <div class="field_row clearfix">    
    <?php echo form_label('Patient #:'); ?>
        <div class='form_field'>
        <?php echo $patient->patient_no;?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Name:'); ?>
        <div class='form_field'>
        <?php echo $patient->patient_name;?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Age:'); ?>
        <div class='form_field'>
        <?php echo $patient->age;?>
        </div>
    </div>
    </td></tr>

    <tr><td colspan="2" width="100%">
        <?php
        echo form_button(array(
            'name'=>'submit',
            'id'=>'save_lab_report_button',
            'content'=>'Save',
            'class'=>'submit_button float_left')
        );
        ?>
        <?php
        echo form_button(array(
            'name'=>'submit',
            'id'=>'complete_lab_report_button',
            'content'=>'Complete',
            'class'=>'submit_button float_right')
        );
        ?>
    </td></tr>

    <tr><td colspan="2" width="100%"><br><hr></td></tr>
    
    <?php if($consultation_notes->num_rows() > 0 ): ?>
    <tr><th style="background-color: #0a6184; color:#FFF" colspan="2" align="center">Consultation History</th></tr>
    <?php foreach($consultation_notes->result() as $consultation): ?>
        <?php 
            $employee = $this->Employee->get_info($consultation->consultant_id);

        ?>
        <tr style="cursor: pointer;" onclick="view_consultation_summary(<?php echo $consultation->consultation_id; ?>)" title="Click to view summary.">
            <td><?php echo "Dr. " . ucfirst(strtolower($employee->last_name)); ?></td>
            <td><?php echo date('j/n/Y',strtotime($consultation->consultation_start)); ?></td>
        </tr>
    <?php endforeach; ?>
    <tr><td colspan="2" width="100%"><br><hr></td></tr>
    <?php endif; ?>

</table>
</div>

<div class="consultation_summary" id="consultation_summary"></div>
<div id="register_wrapper">
<div width="100%" id="message_bar"></div>

<table id="register">
<thead>
<tr>
<th style="width:30%;">Test</th>
<th style="width:50%;">Results</th>
</tr>
</thead>
<tbody id="lab_report_contents">

<?php
    foreach(array_reverse($lab_tests, true) as $item):
        if($item['status'] == 1):
?>
		<tr>
		<td style="align:center;"><?php echo $item['value']; ?></td>
        <td style="align:center;">
        <table>
        <?php
            foreach($item['results'] as $option=>$value):
        ?>
        <tr>    
        <?php if(is_string($option)) echo '<td>'.form_label($option).':&emsp;</td>'; ?>
            <td>
            <?php echo form_textarea(array(
                                    'name'=>'results',
                                    'value'=>$item['results'][$option],
                                    'rows'=>'2',
                                    'cols'=>25,
                                    'lab_item_id'=>$item['lab_test_id'],
                                    'option'=>$option,
                                    'onchange' => 'save_lab_test_results(this)'));?>
            </td>
        </tr>
        <?php endforeach; ?>
        </table>
        </td>
		</tr>
		
		<tr style="height:3px">
		<td colspan=8 style="background-color:white"> </td>
		</tr>		</form>
<?php
        endif;
    endforeach; 
?>
</tbody>
</table>
</div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $("#save_lab_report_button").click(function()
    {
        if (confirm('Are you sure you want to save these results?'))
        {
            $.post('<?php echo site_url($controller_name."/save_lab_report"); ?>',
                {patient_id:'<?php echo $patient->patient_id; ?>'},
                function(response){
                    $("#message_bar").removeClass('error_message');
                    $("#message_bar").removeClass('warning_message');
                    $("#message_bar").removeClass('success_message');
                    $("#message_bar").addClass(response.message_class);
                    $("#message_bar").html(response.message);
                    $('#message_bar').fadeTo(5000, 1);
                    $('#message_bar').fadeTo("fast",0);

                    $("#table_holder").load('<?php echo site_url("$controller_name/view_all"); ?>'); 
              },'json');
        }
    });

    $("#complete_lab_report_button").click(function()
    {
        if (confirm('Are you sure you want to complete these results?'))
        {
            $.post('<?php echo site_url($controller_name."/complete_lab_report"); ?>',
                {patient_id:'<?php echo $patient->patient_id; ?>'},
                function(response){
                    if(response.success){
                        $("#table_holder").load('<?php echo site_url("$controller_name/view_all"); ?>'); 
                        tb_remove();
                        set_feedback(response.message,'success_message',false);
                    }
                    else
                    {
                        $("#message_bar").removeClass('error_message');
                        $("#message_bar").removeClass('warning_message');
                        $("#message_bar").removeClass('success_message');
                        $("#message_bar").addClass(response.message_class);
                        $("#message_bar").html(response.message);
                        $('#message_bar').fadeTo(5000, 1);
                        $('#message_bar').fadeTo("fast",0);
                    } 
              },'json');
        }
    });
});

function view_consultation_summary(consultation_id){
    var html = '<?php echo form_button(array("content"=>"Close","class"=>"submit_button float_right", "onclick"=>"close_consultation_summary()")); ?>';
    $("#consultation_summary").css('display','block');
    $("#consultation_summary").load('<?php echo site_url("$controller_name/consultation_summary/$patient->patient_id"); ?>/' + consultation_id,
        function(response){
            $("#consultation_summary").append(html);
        });
    $('#register_wrapper').css('display','none');
}

function close_consultation_summary(){
    $("#consultation_summary").css('display','none');
    $("#consultation_summary").html('&ensp;');
    $('#register_wrapper').css('display','block');
}

function save_lab_test_results(input)
{
	$.post('<?php echo site_url($controller_name."/lab_test_results"); ?>/' + $(input).attr('lab_item_id'),
                {results: $(input).val(),option: $(input).attr('option')}
                );
}
</script>
