<div width="100%" id="message_bar"></div>
<div id="drugs_list">
<?php
    echo form_button(array(
        'name'=>'submit',
        'id'=>'new_drug_button',
        'content'=>'New Drug',
        'class'=>'submit_button float_right')
    );
?>
<br><br>
<table width="100%" id="drugs_list_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Generic Name</th>
        <th style="background-color: #0a6184; color:#FFF">Brand Name</th>
        <th style="background-color: #0a6184; color:#FFF">Class</th>
        <th style="background-color: #0a6184; color:#FFF">Cost Price</th>
        <th style="background-color: #0a6184; color:#FFF">Unit Price</th>
        <th style="background-color: #0a6184; color:#FFF">Qty in Store</th>
        <th style="background-color: #0a6184; color:#FFF">Issued Qty</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>
    </tr></thead>
    <tbody>
<?php
if($drugs_list->num_rows() > 0):
	foreach($drugs_list->result() as $drug):
        $drug = $this->Pharm->get_drug_info($drug->drug_id);
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $drug->generic_name; ?></td>
        <td style="background-color: #DDD"><?php echo $drug->brand_name; ?></td>
        <td style="background-color: #DDD"><?php echo $drug->class; ?></td>
        <td style="background-color: #DDD"><?php echo $drug->item_info->cost_price; ?></td>
        <td style="background-color: #DDD"><?php echo $drug->item_info->unit_price; ?></td>
        <td style="background-color: #DDD"><?php echo (int)$drug->item_info->store_quantity; ?></td>
        <td style="background-color: #DDD"><?php echo (int)$drug->item_info->quantity; ?></td>
        <td style="background-color: #DDD">
		<?php
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'edit_drug('.$drug->drug_id.')',
                'content'=>'Edit',
                'class'=>'DTTT_button float_left')
            );
        ?>
        &emsp;
        <?php
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'inventory('.$drug->item_info->item_id.')',
                'content'=>'Trail',
                'class'=>'DTTT_button float_left')
            );
        ?>
        &emsp;
        <?php
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'delete_drug('.$drug->drug_id.')',
                'content'=>'Delete',
                'class'=>'DTTT_button float_left')
            );
        ?>
        </td>
        </tr>
<?php
	endforeach;
endif;
?>
	</tbody>
</table>
</div>

<div id="drug_form_area"></div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    TableTools.DEFAULTS.aButtons = [ 
        "copy", 
        {
                    "sExtends": "print",
                    "sMessage": "",
        },
        {
            "sExtends":    "collection",
            "sButtonText": "Save",
            "aButtons":    [  
                {"sExtends": "xls","sTitle": "<?php echo $this->config->item('company'); ?> - Drugs","mColumns": [ 0,1,2,3,4,5 ]}, 
                {"sExtends": "pdf","sPdfMessage": "","sPdfOrientation": "portrait","sTitle": "<?php echo $this->config->item('company'); ?> - Drugs","mColumns": [ 0,1,2,3,4,5 ]} ]
        }, 
    ];
    $('#drugs_list_table').dataTable({
        //"bPaginate": false,
        //"bLengthChange": false,
        //"bFilter": false,
        //"bSort": false,
        //"bInfo": false,
        "bStateSave": true,
        "sDom": 'T<"clear">lfrtip',
        "bServerSide": true,
        "sAjaxSource": "<?php echo site_url('items/refresh_drugs_list'); ?>",
        "sServerMethod": "POST",
        "fnServerData": function(sSource, aoData, fnCallback, oSettings){
            oSettings.jqXHR = $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback,
                "complete": function(xhr, status){tb_init("a.vote_thickbox");},
            });
        },
        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    });

    $("#new_drug_button").click(function(){
        $("#drug_form_area").css('display','block');
        $("#drug_form_area").load('<?php echo site_url("items/view_drug"); ?>');
        $('#drugs_list').css('display','none');
    });
});

function edit_drug(drug_id)
{
    $("#drug_form_area").css('display','block');
    $("#drug_form_area").load('<?php echo site_url("items/view_drug"); ?>/' + drug_id);
    $('#drugs_list').css('display','none');
}

function inventory(item_id)
{
    $("#drug_form_area").css('display','block');
    $("#drug_form_area").load('<?php echo site_url("items/inventory_trail"); ?>/' + item_id);
    $('#drugs_list').css('display','none');
}

function delete_drug(drug_id)
{
    if(confirm('Are you sure you want to delete this drug?'))
    {
        $.post('<?php echo site_url("items/delete_drug"); ?>/' + drug_id,
            {},
            function(response){
                if(response.success)
                {
                    $("#TB_ajaxContent").load('<?php echo site_url("items/view_drugs_list"); ?>',
                        function(result){
                            $("#message_bar").removeClass('error_message');
                            $("#message_bar").removeClass('warning_message');
                            $("#message_bar").removeClass('success_message');
                            $("#message_bar").addClass(response.message_class);
                            $("#message_bar").html(response.message);
                            $('#message_bar').fadeTo(5000, 1);
                            $('#message_bar').fadeTo("fast",0);
                        });
                }
                else
                {
                    $("#message_bar").removeClass('error_message');
                    $("#message_bar").removeClass('warning_message');
                    $("#message_bar").removeClass('success_message');
                    $("#message_bar").addClass(response.message_class);
                    $("#message_bar").html(response.message);
                    $('#message_bar').fadeTo(5000, 1);
                    $('#message_bar').fadeTo("fast",0);
                }
            },'json');
    }
}
</script>