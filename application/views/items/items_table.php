<table width="100%" id="items_table">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Item Name</th>
        <th style="background-color: #0a6184; color:#FFF">Category</th>
        <th style="background-color: #0a6184; color:#FFF">Sub-category</th>
        <th style="background-color: #0a6184; color:#FFF">Cost Price</th>
        <th style="background-color: #0a6184; color:#FFF">Unit Price</th>
        <th style="background-color: #0a6184; color:#FFF">Qty in Store</th>
        <th style="background-color: #0a6184; color:#FFF">Issued Qty</th>
        <th style="background-color: #0a6184; color:#FFF" width="25%">&nbsp;</th>
    </tr></thead>
    <tbody>
<?php
	foreach($items->result() as $item):
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $item->name; ?></td>
        <td style="background-color: #DDD"><?php echo $item->category; ?></td>
        <td style="background-color: #DDD"><?php echo $item->sub_category; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $item->cost_price; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $item->unit_price; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo (int)$item->store_quantity; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo (int)$item->quantity; ?></td>
        <td style="background-color: #DDD">
            <?php echo anchor("items/view_item/$item->item_id/width:750",
                    form_button(array('name'=>'submit','content'=>'View','class'=>'DTTT_button float_left')),
                    array("class"=>"thickbox vote_thickbox","title"=>"Item Details")); ?>
            &emsp;
            <?php echo anchor("items/inventory_trail/$item->item_id/width:600",
                    form_button(array('name'=>'submit','content'=>'Trail','class'=>'DTTT_button float_left')),
                    array("class"=>"thickbox vote_thickbox","title"=>"Inventory Trail")); ?>
            &emsp;
            <?php
                echo form_button(array(
                    'name'=>'submit',
                    'onclick'=>'delete_item('.$item->item_id.')',
                    'content'=>'Delete',
                    'class'=>'DTTT_button float_left')
                );
            ?>
        </td>
        </tr>
<?php
	endforeach;
?>
	</tbody>
</table>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	tb_init("a.vote_thickbox");
    TableTools.DEFAULTS.aButtons = [ 
        "copy", 
        {
                    "sExtends": "print",
                    "sMessage": "",
        },
        {
            "sExtends":    "collection",
            "sButtonText": "Save",
            "aButtons":    [  
                {"sExtends": "xls","sTitle": "<?php echo $this->config->item('company'); ?> - Items","mColumns": [ 0,1,2,3,4,5,6 ]}, 
                {"sExtends": "pdf","sPdfMessage": "","sPdfOrientation": "portrait","sTitle": "<?php echo $this->config->item('company'); ?> - Items","mColumns": [ 0,1,2,3,4,5,6 ]} ]
        }, 
    ];
	$('#items_table').dataTable({
		//"bPaginate": false,
		//"bLengthChange": false,
		//"bFilter": false,
		"bSort": false,
		//"bInfo": false,
        "bStateSave": true,
        "sDom": 'T<"clear">lfrtip',
        "bServerSide": true,
        "sAjaxSource": "<?php echo site_url('items/refresh_table'); ?>",
        "sServerMethod": "POST",
        "fnServerData": function(sSource, aoData, fnCallback, oSettings){
            oSettings.jqXHR = $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback,
                "complete": function(xhr, status){tb_init("a.vote_thickbox");},
            });
        },
        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
	});
});

function delete_item(item_id)
{
    if(confirm('Are you sure you want to delete this item?'))
    {
        $.post('<?php echo site_url("items/delete_item"); ?>/' + item_id,
            {},
            function(response){
                if(response.success)
                {
                    $("#table_holder").load('<?php echo site_url("items/view_all"); ?>',
                        function(result){
                            set_feedback(response.message,response.message_class,false);
                        });
                }
                else
                {
                    set_feedback(response.message,response.message_class,false);
                }
            },'json');
    }
}
</script>
