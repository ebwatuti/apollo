<table width="100%">
    <tr><td colspan="2" align="center"><div id="examination_header_bar">Item Info</div></th></tr>
    <tr><td colspan="2">
		<div class="field_row clearfix">    
		<?php echo form_label('Item #:'); ?>
		    <div class='form_field'>
		    <?php echo $item_info->item_number;?>
		    </div>
		</div>
		<div class="field_row clearfix">    
		<?php echo form_label('Item Name:'); ?>
		    <div class='form_field'>
		    <?php echo $item_info->name;?>
		    </div>
		</div>
		<div class="field_row clearfix">    
		<?php echo form_label('Category:'); ?>
		    <div class='form_field'>
		    <?php echo $item_info->category;?>
		    </div>
		</div>
		<div class="field_row clearfix">    
		<?php echo form_label('Sub-category:'); ?>
		    <div class='form_field'>
		    <?php echo $item_info->sub_category;?>
		    </div>
		</div>
		<div class="field_row clearfix">    
		<?php echo form_label('Quantity in store:'); ?>
		    <div class='form_field'>
		    <?php echo $item_info->store_quantity;?>
		    </div>
		</div>
		<div class="field_row clearfix">    
		<?php echo form_label('Quantity issued:'); ?>
		    <div class='form_field'>
		    <?php echo $item_info->quantity;?>
		    </div>
		</div>
	</td></tr>
	<tr><td colspan="2" width="100%"><br><hr></td></tr>
    <tr><td colspan="2" align="center"><div id="examination_header_bar">Inventory Trail</div></th></tr>
    <tr><td colspan="2">
    	<table id="inventory_trail_table" width="100%">
    	<thead><tr>
    		<th style="background-color: #0a6184; color:#FFF" align="center">Date</td>
    		<th style="background-color: #0a6184; color:#FFF" align="center">Employee</td>
    		<th style="background-color: #0a6184; color:#FFF" align="center">Qty In/Out</td>
    		<th style="background-color: #0a6184; color:#FFF" align="center">Comments</td>
    	</tr></thead>
    	<tbody>
    		<?php
			foreach($this->Inventory->get_inventory_data_for_item($item_info->item_id)->result_array() as $row):
			?>
			<tr>
			<td style="background-color: #DDD"><?php echo date("M j, Y",strtotime($row['trans_date']));?></td>
			<td style="background-color: #DDD"><?php
				$person_id = $row['trans_user'];
				$employee = $this->Employee->get_info($person_id);
				echo $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
				?>
			</td>
			<td style="background-color: #DDD" align="right"><?php echo $row['trans_inventory'];?></td>
			<td style="background-color: #DDD"><?php echo nl2br($row['trans_comment']);?></td>
			</tr>
			<?php endforeach; ?>
    	</tbody>
    	</table>
    </td></tr>
</table>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $('#inventory_trail_table').dataTable({
        //"bPaginate": false,
        //"bLengthChange": false,
        //"bFilter": false,
        "bSort": false,
        //"bInfo": false,
        "bStateSave": true,
    });
});
</script>