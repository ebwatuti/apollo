<?php $this->load->view("partial/header"); ?>
<div id="title_bar">
	<div id="page_title" style="margin-bottom:8px;">Items</div>
</div>
<div id="sidebar">		
	<?php echo anchor("items/view_item/-1/width:750",
	"<div class='submenu_button' style='float: left;'><span>New Item</span></div>",
	array('title'=>'New Item','class'=>'thickbox none',));
	?>
	<?php echo anchor("items/view_clinics/width:850",
	"<div class='submenu_button' style='float: left;'><span>Clinics</span></div>",
	array('title'=>'Clinics','class'=>'thickbox none',));
	?>
	<?php echo anchor("items/view_procedures/width:750",
	"<div class='submenu_button' style='float: left;'><span>Procedures</span></div>",
	array('title'=>'Procedures','class'=>'thickbox none',));
	?>
	<?php echo anchor("items/view_lab_tests/width:650",
	"<div class='submenu_button' style='float: left;'><span>Lab Tests</span></div>",
	array('title'=>'Lab Tests','class'=>'thickbox none',));
	?>	
	<?php echo anchor("items/view_drugs_list/width:950",
	"<div class='submenu_button' style='float: left;'><span>Drugs</span></div>",
	array('title'=>'Drugs','class'=>'thickbox none',));
	?>	
	<?php echo anchor("items/view_orders/width:1150",
	"<div class='submenu_button' style='float: left;'><span>Orders</span></div>",
	array('title'=>'Orders','class'=>'thickbox none',));
	?>
</div>

<div id="table_holder">

</div>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	var html = "<table width='100%' height='100px'><tr><td align='center'><img src='<?php echo base_url()?>images/loading_animation.gif' alt='spinner' /></td></tr><table>";
	$("#table_holder").html(html);
	$("#table_holder").load('<?php echo site_url("items/view_all"); ?>');		
});

</script>
