<div width="100%" id="message_bar"></div>
<div id="orders">
<table width="100%" id="orders_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Order #</th>
        <th style="background-color: #0a6184; color:#FFF">Date</th>
        <th style="background-color: #0a6184; color:#FFF">Employee</th>
        <th style="background-color: #0a6184; color:#FFF">Remarks</th>
        <th style="background-color: #0a6184; color:#FFF">Status</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>
    </tr></thead>
    <tbody>
<?php
if($pending_orders->num_rows() > 0):
	foreach($pending_orders->result() as $order):
        $order = $this->Order->get_info($order->order_id);
        $employee = $this->Employee->get_info($order->employee_id);
        $order->employee = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
        switch ($order->order_status) {
            case 0:
                $order->status = 'Pending';
                break;
            case 1:
                $order->status = 'Rejected on '.date('j/n/Y g:i a',strtotime($order->service_time));
                break;
            case 2:
                $order->status = 'Approved on '.date('j/n/Y g:i a',strtotime($order->service_time));
                break;
            default:
                $order->status = 'Pending';
                break;
        }
?>
        <tr>
        <td style="background-color: #DDD" align="right"><?php echo $order->order_id; ?></td>
        <td style="background-color: #DDD"><?php echo date('j/n/Y g:i a',strtotime($order->order_time)); ?></td>
        <td style="background-color: #DDD"><?php echo $order->employee; ?></td>
        <td style="background-color: #DDD"><?php echo $order->remarks; ?></td>
        <td style="background-color: #DDD"><?php echo $order->status; ?></td>
        <td style="background-color: #DDD">
		<?php            
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'view_order('.$order->order_id.')',
                'content'=>'View',
                'class'=>'DTTT_button float_left')
            );
        ?>
        </td>
        </tr>
<?php
	endforeach;
endif;
?>

<?php
if($processed_orders->num_rows() > 0):
    foreach($processed_orders->result() as $order):
        $order = $this->Order->get_info($order->order_id);
        $employee = $this->Employee->get_info($order->employee_id);
        $order->employee = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
        
        switch ($order->order_status) {
            case 0:
                $order->status = 'Pending';
                break;
            case 1:
                $order->status = 'Rejected on '.date('j/n/Y g:i a',strtotime($order->service_time));
                break;
            case 2:
                $order->status = 'Approved on '.date('j/n/Y g:i a',strtotime($order->service_time));
                break;
            default:
                $order->status = 'Pending';
                break;
        }
?>
        <tr>
        <td style="background-color: #DDD" align="right"><?php echo $order->order_id; ?></td>
        <td style="background-color: #DDD"><?php echo date('j/n/Y g:i a',strtotime($order->order_time)); ?></td>
        <td style="background-color: #DDD"><?php echo $order->employee; ?></td>
        <td style="background-color: #DDD"><?php echo $order->remarks; ?></td>
        <td style="background-color: #DDD"><?php echo $order->status; ?></td>
        <td style="background-color: #DDD">
        <?php            
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'view_order('.$order->order_id.')',
                'content'=>'View',
                'class'=>'DTTT_button float_left')
            );
        ?>
        </td>
        </tr>
<?php
    endforeach;
endif;
?>
	</tbody>
</table>
</div>

<div id="order_form_area"></div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $('#orders_table').dataTable({
        //"bPaginate": false,
        //"bLengthChange": false,
        //"bFilter": false,
        "bSort": false,
        //"bInfo": false,
        "bStateSave": true,
    });
});

function view_order(order_id)
{
    $("#order_form_area").css('display','block');
    $("#order_form_area").load('<?php echo site_url("items/view_order"); ?>/' + order_id);
    $('#orders').css('display','none');
}
</script>