<div width="100%" id="message_bar"></div>
<div id="procedures">
<?php
    echo form_button(array(
        'name'=>'submit',
        'id'=>'new_procedure_button',
        'content'=>'New Procedure',
        'class'=>'submit_button float_right')
    );
?>
<br><br>
<table width="100%" id="procedures_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Procedure</th>
        <th style="background-color: #0a6184; color:#FFF">Price</th>
        <th style="background-color: #0a6184; color:#FFF">Clinic</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>       
    </tr></thead>
    <tbody>
<?php
if($procedures->num_rows() > 0):
	foreach($procedures->result() as $procedure):
        $procedure = $this->Consultation->get_procedure_info($procedure->procedure_id);
        $clinic_name = ($procedure->clinic == 'nursing') ? 'Nursing' : $this->Consultation->clinic_name($procedure->clinic);
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $procedure->name; ?></td>
        <td style="background-color: #DDD"><?php echo $procedure->item_info->unit_price; ?></td>
        <td style="background-color: #DDD"><?php echo $clinic_name; ?></td>
        <td style="background-color: #DDD">
		<?php
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'edit_procedure('.$procedure->procedure_id.')',
                'content'=>'Edit',
                'class'=>'DTTT_button float_left')
            );
        ?>
        &emsp;
        <?php
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'delete_procedure('.$procedure->procedure_id.')',
                'content'=>'Delete',
                'class'=>'DTTT_button float_left')
            );
        ?>
        </td>
        </tr>
<?php
	endforeach;
endif;
?>
	</tbody>
</table>
</div>

<div id="procedure_form_area"></div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $('#procedures_table').dataTable({
        //"bPaginate": false,
        //"bLengthChange": false,
        //"bFilter": false,
        //"bSort": false,
        //"bInfo": false,
        "bStateSave": true,
    });

    $("#new_procedure_button").click(function(){
        $("#procedure_form_area").css('display','block');
        $("#procedure_form_area").load('<?php echo site_url("items/view_procedure"); ?>');
        $('#procedures').css('display','none');
    });
});

function edit_procedure(procedure_id)
{
    $("#procedure_form_area").css('display','block');
    $("#procedure_form_area").load('<?php echo site_url("items/view_procedure"); ?>/' + procedure_id);
    $('#procedures').css('display','none');
}

function delete_procedure(procedure_id)
{
    if(confirm('Are you sure you want to delete this procedure?'))
    {
        $.post('<?php echo site_url("items/delete_procedure"); ?>/' + procedure_id,
            {},
            function(response){
                if(response.success)
                {
                    $("#TB_ajaxContent").load('<?php echo site_url("items/view_procedures"); ?>',
                        function(result){
                            $("#message_bar").removeClass('error_message');
                            $("#message_bar").removeClass('warning_message');
                            $("#message_bar").removeClass('success_message');
                            $("#message_bar").addClass(response.message_class);
                            $("#message_bar").html(response.message);
                            $('#message_bar').fadeTo(5000, 1);
                            $('#message_bar').fadeTo("fast",0);
                        });
                }
                else
                {
                    $("#message_bar").removeClass('error_message');
                    $("#message_bar").removeClass('warning_message');
                    $("#message_bar").removeClass('success_message');
                    $("#message_bar").addClass(response.message_class);
                    $("#message_bar").html(response.message);
                    $('#message_bar').fadeTo(5000, 1);
                    $('#message_bar').fadeTo("fast",0);
                }
            },'json');
    }
}
</script>