<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>

<fieldset id="employee_basic_info">
<legend>Procedure Info</legend>
<?php echo form_open("items/save_procedure/$procedure_info->procedure_id",array('id'=>'procedure_form')); ?>

<div class="field_row clearfix">	
<?php echo form_label('Procedure Name:', 'procedure_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'procedure_name',
		'id'=>'procedure_name',
		'value'=>$procedure_info->name)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Price:', 'price',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'price',
		'id'=>'price',
		'value'=>$procedure_info->item_info->unit_price)
	);?>
	</div>
</div>

<div class="field_row clearfix wide">
<?php $selected_clinics = explode(',', $procedure_info->clinic); ?>
<?php echo form_label("Clinics:", 'clinic'); ?>
	<div class='form_field'>
	<?php foreach($clinics as $clinic): ?>
		<label>
		<?php echo form_radio(array(
			'name'=>'clinic',
			'value'=>$clinic->clinic_code,
			'checked'=>($clinic->clinic_code == $procedure_info->clinic) ? 'checked' : '')
		);?>&emsp;<?php echo $clinic->clinic_name; ?>
		</label><br>
	<?php endforeach; ?>
		<label>
		<?php echo form_radio(array(
			'name'=>'clinic',
			'value'=>'nursing',
			'checked'=>($procedure_info->clinic == 'nursing') ? 'checked' : '')
		);?>&emsp;Nursing
		</label><br>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_button(array("id"=>"procedure_form_submit","content"=>"Submit","class"=>"submit_button float_left"));?>
	<?php echo form_button(array("id"=>"procedure_form_cancel","content"=>"Cancel","class"=>"submit_button float_right"));?>
</div>

<?php  echo form_close(); ?>
</fieldset>

<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{	
	$("#procedure_form_submit").click(function(){
		$("#procedure_form").ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					//tb_remove();
					if(response.success)
					{
						$("#TB_ajaxContent").load('<?php echo site_url("items/view_procedures"); ?>',
							function(result){
								$("#message_bar").removeClass('error_message');
	                            $("#message_bar").removeClass('warning_message');
	                            $("#message_bar").removeClass('success_message');
	                            $("#message_bar").addClass(response.message_class);
	                            $("#message_bar").html(response.message);
	                            $('#message_bar').fadeTo(5000, 1);
	                            $('#message_bar').fadeTo("fast",0);
							});
					}
					else
					{
						$("#message_bar").removeClass('error_message');
	                    $("#message_bar").removeClass('warning_message');
	                    $("#message_bar").removeClass('success_message');
	                    $("#message_bar").addClass(response.message_class);
	                    $("#message_bar").html(response.message);
	                    $('#message_bar').fadeTo(5000, 1);
	                    $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});

	$("#procedure_form_cancel").click(function(){
		$("#procedure_form_area").css('display','none');
	    $("#procedure_form_area").html('&ensp;');
	    $('#procedures').css('display','block');
	});
});
</script>