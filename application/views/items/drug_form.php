<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>

<fieldset id="employee_basic_info">
<legend>Drug Info</legend>
<?php echo form_open("items/save_drug/$drug->drug_id",array('id'=>'drug_form')); ?>

<div class="field_row clearfix">	
<?php echo form_label('Generic Name:', 'generic_name'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'generic_name',
		'id'=>'generic_name',
		'value'=>$drug->generic_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Brand Name:', 'brand_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'brand_name',
		'id'=>'brand_name',
		'value'=>$drug->brand_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Class:', 'class'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'class',
		'id'=>'class',
		'value'=>$drug->class)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Strength:', 'strength'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'strength',
		'id'=>'strength',
		'value'=>$drug->strength)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label('Supplier:', 'supplier'); ?>
	<div class='form_field'>
	<?php echo form_dropdown('supplier_id', $suppliers, $drug->item_info->supplier_id);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Cost Price:', 'cost_price'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'cost_price',
		'id'=>'cost_price',
		'value'=>$drug->item_info->cost_price)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Unit Price:', 'unit_price',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'unit_price',
		'id'=>'unit_price',
		'value'=>$drug->item_info->unit_price)
	);?>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_button(array("id"=>"drug_form_submit","content"=>"Submit","class"=>"submit_button float_left"));?>
	<?php echo form_button(array("id"=>"drug_form_cancel","content"=>"Cancel","class"=>"submit_button float_right"));?>
</div>

<?php  echo form_close(); ?>
</fieldset>

<?php if($drug->item_info->item_id): ?>
<fieldset id="employee_login_info">
<legend>Inventory</legend>
<?php
echo form_open("items/save_inventory/".$drug->item_info->item_id,array('id'=>'inventory_form'));
?>
<div class="field_row clearfix">
<?php echo form_label('Qty in store:', 'cur_quantity',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'cur_quantity',
		'id'=>'cur_quantity',
		'readonly'=>'readonly',
		'value'=>(int)$drug->item_info->store_quantity)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Qty in store to add/subtract:', 'store_quantity',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'store_quantity',
		'id'=>'store_quantity'
		)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label('Issued qty:', 'cur_quantity',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'readonly'=>'readonly',
		'value'=>(int)$drug->item_info->quantity)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Issued qty to add/subtract:', 'quantity',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'quantity',
		'id'=>'quantity'
		)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Comments:', 'trans_comment',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
		'name'=>'trans_comment',
		'id'=>'trans_comment',
		'rows'=>'3',
		'cols'=>'17')		
	);?>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_button(array("id"=>"inventory_form_submit","content"=>"Submit","class"=>"submit_button float_left"));?>
	<?php echo form_button(array("id"=>"inventory_form_cancel","content"=>"Cancel","class"=>"submit_button float_right"));?>
</div>
<?php 
echo form_close();
?>
</fieldset>
<?php endif; ?>

<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{	
	$("#drug_form_submit").click(function(){
		$("#drug_form").ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					//tb_remove();
					if(response.success)
					{
						$("#TB_ajaxContent").load('<?php echo site_url("items/view_drugs_list"); ?>',
							function(result){
								$("#drug_form_area").css('display','block');
							    $("#drug_form_area").load('<?php echo site_url("items/view_drug"); ?>/' + response.drug_id);
							    $('#drugs_list').css('display','none');
								$("#message_bar").removeClass('error_message');
	                            $("#message_bar").removeClass('warning_message');
	                            $("#message_bar").removeClass('success_message');
	                            $("#message_bar").addClass(response.message_class);
	                            $("#message_bar").html(response.message);
	                            $('#message_bar').fadeTo(5000, 1);
	                            $('#message_bar').fadeTo("fast",0);
							});
					}
					else
					{
						$("#message_bar").removeClass('error_message');
	                    $("#message_bar").removeClass('warning_message');
	                    $("#message_bar").removeClass('success_message');
	                    $("#message_bar").addClass(response.message_class);
	                    $("#message_bar").html(response.message);
	                    $('#message_bar').fadeTo(5000, 1);
	                    $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});

	$('#inventory_form_submit').click(function(){
		$('#inventory_form').ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					if(response.success)
					{
						$("#TB_ajaxContent").load('<?php echo site_url("items/view_drugs_list"); ?>',
							function(result){
								$("#drug_form_area").css('display','block');
							    $("#drug_form_area").load('<?php echo site_url("items/view_drug/".$drug->drug_id); ?>/');
							    $('#drugs_list').css('display','none');
								$("#message_bar").removeClass('error_message');
	                            $("#message_bar").removeClass('warning_message');
	                            $("#message_bar").removeClass('success_message');
	                            $("#message_bar").addClass(response.message_class);
	                            $("#message_bar").html(response.message);
	                            $('#message_bar').fadeTo(5000, 1);
	                            $('#message_bar').fadeTo("fast",0);
							});
					}
					else
					{
						$("#message_bar").removeClass('error_message');
	                    $("#message_bar").removeClass('warning_message');
	                    $("#message_bar").removeClass('success_message');
	                    $("#message_bar").addClass(response.message_class);
	                    $("#message_bar").html(response.message);
	                    $('#message_bar').fadeTo(5000, 1);
	                    $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});

	$("#drug_form_cancel,#inventory_form_cancel").click(function(){
		$("#drug_form_area").css('display','none');
	    $("#drug_form_area").html('&ensp;');
	    $('#drugs_list').css('display','block');
	});

	$("#class").autocomplete("<?php echo site_url('items/suggest_class');?>",{max:100,minChars:0,delay:10});
    $("#class").result(function(event, data, formatted){});
	$("#class").search();
});
</script>