<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>

<fieldset id="employee_basic_info">
<legend>Clinic Info</legend>
<?php echo form_open("items/save_clinic/$clinic_info->clinic_id",array('id'=>'clinic_form')); ?>

<div class="field_row clearfix">	
<?php echo form_label('Clinic Code:', 'clinic_code'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'clinic_code',
		'id'=>'clinic_code',
		'readonly'=>'readonly',
		'value'=>$clinic_info->clinic_code)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Clinic Name:', 'clinic_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'clinic_name',
		'id'=>'clinic_name',
		'value'=>$clinic_info->clinic_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Consultation Fee:', 'price',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'price',
		'id'=>'price',
		'value'=>$clinic_info->item_info->unit_price)
	);?>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_button(array("id"=>"clinic_form_submit","content"=>"Submit","class"=>"submit_button float_left"));?>
	<?php echo form_button(array("id"=>"clinic_form_cancel","content"=>"Cancel","class"=>"submit_button float_right"));?>
</div>

<?php  echo form_close(); ?>
</fieldset>

<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{	
	$("#clinic_form_submit").click(function(){
		$("#clinic_form").ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					//tb_remove();
					if(response.success)
					{
						$("#TB_ajaxContent").load('<?php echo site_url("items/view_clinics"); ?>',
							function(result){
								$("#message_bar").removeClass('error_message');
	                            $("#message_bar").removeClass('warning_message');
	                            $("#message_bar").removeClass('success_message');
	                            $("#message_bar").addClass(response.message_class);
	                            $("#message_bar").html(response.message);
	                            $('#message_bar').fadeTo(5000, 1);
	                            $('#message_bar').fadeTo("fast",0);
							});
					}
					else
					{
						$("#message_bar").removeClass('error_message');
	                    $("#message_bar").removeClass('warning_message');
	                    $("#message_bar").removeClass('success_message');
	                    $("#message_bar").addClass(response.message_class);
	                    $("#message_bar").html(response.message);
	                    $('#message_bar').fadeTo(5000, 1);
	                    $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});

	$("#clinic_form_cancel").click(function(){
		$("#clinic_form_area").css('display','none');
	    $("#clinic_form_area").html('&ensp;');
	    $('#clinics').css('display','block');
	});
});
</script>