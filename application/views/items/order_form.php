<div width="100%" id="message_bar"></div>

<div id="main_area">
<div id="queue_section">

<table width="100%">
    <tr><th style="background-color: #0a6184; color:#FFF; color:#FFF" colspan="2" align="center">Order Info</th></tr>
    <tr><td colspan="2">
    <div class="field_row clearfix">    
    <?php echo form_label('Order #:'); ?>
        <div class='form_field'>
        <?php echo $order->order_id; ?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Employee:'); ?>
        <div class='form_field'>
        <?php
            $employee = $this->Employee->get_info($order->employee_id);
            $order->employee = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
            echo $order->employee;
        ?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Remarks:'); ?>
        <div class='form_field'>
        <?php echo nl2br($order->remarks); ?>
        </div>
    </div>
    <?php if($order->order_status == 0): ?>
    <div class="field_row clearfix">    
    <?php echo form_label('Issuer Remarks:'); ?>
        <div class='form_field'>
        <?php echo form_textarea(array(
            'name'=>'remarks',
            'id'=>'remarks',
            'value'=>$order->service_remarks,
            'rows'=>'3',
            'cols'=>'25')); ?>
        </div>
    </div>
    <?php
            echo form_button(array(
                'name'=>'submit',
                'order_id'=>$order_id,
                'onclick'=>'save_order(this)',
                'content'=>'Save',
                'class'=>'submit_button float_left')
            );
        else:
            switch ($order->order_status) {
                case 0:
                    $order->status = 'Pending';
                    break;
                case 1:
                    $order->status = 'Rejected';
                    break;
                case 2:
                    $order->status = 'Approved';
                    break;
                default:
                    $order->status = 'Pending';
                    break;
            }
    ?>
    <div class="field_row clearfix">    
    <?php echo form_label('Status:'); ?>
        <div class='form_field'>
        <?php echo $order->status; ?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Issuer Remarks:'); ?>
        <div class='form_field'>
        <?php echo nl2br($order->service_remarks); ?>
        </div>
    </div>
    <?php
        endif;

        echo form_button(array(
            'name'=>'cancel',
            'onclick'=>'cancel_order(this)',
            'content'=>'Cancel',
            'class'=>'submit_button float_right')
        );
    ?>
    </td></tr>
    <tr><td colspan="2" width="100%"><br><hr></td></tr>
    
</table>
</div>

<div id="register_wrapper">
<table id="register">
<thead>
<tr>
<th>Name</th>
<th>Requested Qty</th>
<th>Issued Qty</th>
</tr>
</thead>
<tbody>

<?php foreach(array_reverse($order_items, true) as $item): ?>
        <tr>
        <td style="align:center;"><?php echo $item['name']; ?></td>
        <td style="align:center;"><?php echo $item['quantity']; ?></td>
        <td style="align:center;"><?php echo ($item['order_status'] == 0) ? form_input(array(
                                    'name'=>'issued',
                                    'value'=>$item['issued'],
                                    'item_id'=>$item['item_id'],
                                    'type'=>'number',
                                    'length'=>'1',
                                    'min'=>'1',
                                    'onchange' => 'save_item_info(this)')) : $item['issued']; ?></td>        
        </tr>
    <?php endforeach; ?>
</tbody>
</table>
</div>
</div>

<script type="text/javascript" language="javascript">
function save_order(button)
{
    if (confirm('Are you sure you want to save this order?'))
    {
    	$.post('<?php echo site_url("items/save_order"); ?>/' + $(button).attr('order_id'),
            {remarks: $('#remarks').val(), status: $('#status').val()},
            function(response){
        		if(response.success)
    			{
    				$("#TB_ajaxContent").load('<?php echo site_url("items/view_orders"); ?>',
                        function(result){
                            $("#message_bar").removeClass('error_message');
                            $("#message_bar").removeClass('warning_message');
                            $("#message_bar").removeClass('success_message');
                            $("#message_bar").addClass(response.message_class);
                            $("#message_bar").html(response.message);
                            $('#message_bar').fadeTo(5000, 1);
                            $('#message_bar').fadeTo("fast",0);
                        });
    			}
    			else
    			{
    				$("#message_bar").removeClass('error_message');
                    $("#message_bar").removeClass('warning_message');
                    $("#message_bar").removeClass('success_message');
                    $("#message_bar").addClass(response.message_class);
                    $("#message_bar").html(response.message);
                    $('#message_bar').fadeTo(5000, 1);
                    $('#message_bar').fadeTo("fast",0);
    			}
      		},'json');
    }
}

function cancel_order()
{
    $("#TB_ajaxContent").load('<?php echo site_url("items/view_orders"); ?>');   
}

function save_item_info(input)
{
    $.post('<?php echo site_url("items/order_item_info"); ?>/' + $(input).attr('item_id') + '/' + $(input).attr('name'),
            {value: $(input).val()});
}
</script>
