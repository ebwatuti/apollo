<div width="100%" id="message_bar"></div>
<div id="clinics">
<?php
    echo form_button(array(
        'name'=>'submit',
        'id'=>'new_clinic_button',
        'content'=>'New Clinic',
        'class'=>'submit_button float_right')
    );
?>
<br><br>
<table width="100%" id="clinics_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Clinic Code</th>
        <th style="background-color: #0a6184; color:#FFF">Clinic Name</th>
        <th style="background-color: #0a6184; color:#FFF">Consultation Fee</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>
    </tr></thead>
    <tbody>
<?php
if($clinics->num_rows() > 0):
	foreach($clinics->result() as $clinic):
        $clinic = $this->Administration->get_clinic_info($clinic->clinic_id);
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $clinic->clinic_code; ?></td>
        <td style="background-color: #DDD"><?php echo $clinic->clinic_name; ?></td>
        <td style="background-color: #DDD"><?php echo $clinic->item_info->unit_price; ?></td>
        <td style="background-color: #DDD">
		<?php
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'edit_clinic('.$clinic->clinic_id.')',
                'content'=>'Edit',
                'class'=>'DTTT_button float_left')
            );
        ?>
        &emsp;
        <?php
            echo form_button(array(
                'name'=>'submit',
                'onclick'=>'delete_clinic('.$clinic->clinic_id.')',
                'content'=>'Delete',
                'class'=>'DTTT_button float_left')
            );
        ?>
        </td>
        </tr>
<?php
	endforeach;
endif;
?>
	</tbody>
</table>
</div>

<div id="clinic_form_area"></div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $('#clinics_table').dataTable({
        //"bPaginate": false,
        //"bLengthChange": false,
        //"bFilter": false,
        //"bSort": false,
        //"bInfo": false,
        "bStateSave": true,
    });

    $("#new_clinic_button").click(function(){
        $("#clinic_form_area").css('display','block');
        $("#clinic_form_area").load('<?php echo site_url("items/view_clinic"); ?>');
        $('#clinics').css('display','none');
    });
});

function edit_clinic(clinic_id)
{
    $("#clinic_form_area").css('display','block');
    $("#clinic_form_area").load('<?php echo site_url("items/view_clinic"); ?>/' + clinic_id);
    $('#clinics').css('display','none');
}

function delete_clinic(clinic_id)
{
    if(confirm('Are you sure you want to delete this clinic?'))
    {
        $.post('<?php echo site_url("items/delete_clinic"); ?>/' + clinic_id,
            {},
            function(response){
                if(response.success)
                {
                    $("#TB_ajaxContent").load('<?php echo site_url("items/view_clinics"); ?>',
                        function(result){
                            $("#message_bar").removeClass('error_message');
                            $("#message_bar").removeClass('warning_message');
                            $("#message_bar").removeClass('success_message');
                            $("#message_bar").addClass(response.message_class);
                            $("#message_bar").html(response.message);
                            $('#message_bar').fadeTo(5000, 1);
                            $('#message_bar').fadeTo("fast",0);
                        });
                }
                else
                {
                    $("#message_bar").removeClass('error_message');
                    $("#message_bar").removeClass('warning_message');
                    $("#message_bar").removeClass('success_message');
                    $("#message_bar").addClass(response.message_class);
                    $("#message_bar").html(response.message);
                    $('#message_bar').fadeTo(5000, 1);
                    $('#message_bar').fadeTo("fast",0);
                }
            },'json');
    }
}
</script>