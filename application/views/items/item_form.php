<div width="100%" id="message_bar"></div>
<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>

<fieldset id="employee_basic_info">
<legend>Item Info</legend>
<?php echo form_open("items/save_item/$item_info->item_id",array('id'=>'item_form')); ?>

<div class="field_row clearfix">
<?php echo form_label('Item #:', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'item_number',
		'id'=>'item_number',
		'value'=>$item_info->item_number)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label('Item Name:', 'name',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'name',
		'id'=>'name',
		'value'=>$item_info->name)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label('Category:', 'category',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'category',
		'id'=>'category',
		'value'=>$item_info->category)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label('Sub-category:', 'sub_category',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'sub_category',
		'id'=>'sub_category',
		'value'=>$item_info->sub_category)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label('Supplier:', 'supplier',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('supplier_id', $suppliers, $item_info->supplier_id);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label('Cost Price:', 'cost_price',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'cost_price',
		'id'=>'cost_price',
		'value'=>$item_info->cost_price)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label('Unit Price:', 'unit_price',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'unit_price',
		'size'=>'8',
		'id'=>'unit_price',
		'value'=>$item_info->unit_price)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label('Reorder Level:', 'reorder_level',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'reorder_level',
		'id'=>'reorder_level',
		'value'=>(int)$item_info->reorder_level)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Location:', 'location',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'location',
		'id'=>'location',
		'value'=>$item_info->location)
	);?>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_button(array("id"=>"item_form_submit","content"=>"Submit","class"=>"submit_button float_right"));?>
</div>

<?php  echo form_close(); ?>
</fieldset>

<?php if($item_info->item_id): ?>
<fieldset id="employee_login_info">
<legend>Inventory</legend>
<?php
echo form_open("items/save_inventory/$item_info->item_id",array('id'=>'inventory_form'));
?>
<div class="field_row clearfix">
<?php echo form_label('Qty in store:', 'cur_quantity',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'cur_quantity',
		'id'=>'cur_quantity',
		'readonly'=>'readonly',
		'value'=>(int)$item_info->store_quantity)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Qty in store to add/subtract:', 'store_quantity',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'store_quantity',
		'id'=>'store_quantity'
		)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label('Issued qty:', 'cur_quantity',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'readonly'=>'readonly',
		'value'=>(int)$item_info->quantity)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Issued qty to add/subtract:', 'quantity',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'quantity',
		'id'=>'quantity'
		)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Comments:', 'trans_comment',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
		'name'=>'trans_comment',
		'id'=>'trans_comment',
		'rows'=>'3',
		'cols'=>'17')		
	);?>
	</div>
</div>
<?php
echo form_button(array(
	'name'=>'submit',
	'id'=>'inventory_form_submit',
	'content'=>'Submit',
	'class'=>'submit_button float_right')
);
?>
<?php 
echo form_close();
?>
</fieldset>
<?php endif; ?>

<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{	
	$("#category").autocomplete("<?php echo site_url('items/suggest_category');?>",{max:100,minChars:0,delay:10});
    $("#category").result(function(event, data, formatted){});
	$("#category").search();

	$("#sub_category").autocomplete("<?php echo site_url('items/suggest_subcategory');?>",{max:100,minChars:0,delay:10});
    $("#sub_category").result(function(event, data, formatted){});
	$("#sub_category").search();

	$("#location").autocomplete("<?php echo site_url('items/suggest_location');?>",{max:100,minChars:0,delay:10});
    $("#location").result(function(event, data, formatted){});
	$("#location").search();
	
	$("#item_form_submit").click(function(){
		$("#item_form").ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					//tb_remove();
					if(response.success)
					{
						$("#TB_ajaxContent").load('<?php echo site_url("items/view_item"); ?>/'+response.item_id,
							function(result){
								$("#message_bar").removeClass('error_message');
	                            $("#message_bar").removeClass('warning_message');
	                            $("#message_bar").removeClass('success_message');
	                            $("#message_bar").addClass(response.message_class);
	                            $("#message_bar").html(response.message);
	                            $('#message_bar').fadeTo(5000, 1);
	                            $('#message_bar').fadeTo("fast",0);
							});
						$("#TB_ajaxWindowTitle").html('Item Details');
						$("#table_holder").load('<?php echo site_url("items/view_all"); ?>');
					}
					else
					{
						$("#message_bar").removeClass('error_message');
	                    $("#message_bar").removeClass('warning_message');
	                    $("#message_bar").removeClass('success_message');
	                    $("#message_bar").addClass(response.message_class);
	                    $("#message_bar").html(response.message);
	                    $('#message_bar').fadeTo(5000, 1);
	                    $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});

	$('#inventory_form_submit').click(function(){
		$('#inventory_form').ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					if(response.success)
					{
						$("#TB_ajaxContent").load('<?php echo site_url("items/view_item"); ?>/'+response.item_id,
							function(result){
								$("#message_bar").removeClass('error_message');
	                            $("#message_bar").removeClass('warning_message');
	                            $("#message_bar").removeClass('success_message');
	                            $("#message_bar").addClass(response.message_class);
	                            $("#message_bar").html(response.message);
	                            $('#message_bar').fadeTo(5000, 1);
	                            $('#message_bar').fadeTo("fast",0);
							});
						$("#TB_ajaxWindowTitle").html('Item Details');
						$("#table_holder").load('<?php echo site_url("items/view_all"); ?>');
					}
					else
					{
						$("#message_bar").removeClass('error_message');
	                    $("#message_bar").removeClass('warning_message');
	                    $("#message_bar").removeClass('success_message');
	                    $("#message_bar").addClass(response.message_class);
	                    $("#message_bar").html(response.message);
	                    $('#message_bar').fadeTo(5000, 1);
	                    $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});
});
</script>