<div id="archive">
<ul id="error_message_box"></ul>
<?php echo form_open("sales/workload_date_input",array('id'=>'workload_date_form')); ?>
<table width="100%"><tfoot><tr><td colspan="10"><br><hr></td></tr></tfoot>
<tbody><tr>
<td width="20%"><strong>Start Date: &emsp;</strong></td>
<td width="50%" align="right"><?php echo form_input(array(
                'name'=>'start_date',
                'id'=>'start_date',
                'value'=>$start_date_input, 
                'class'=>'date')
        ); ?></td><td width="30%"></td>
</tr>
<tr>
<td width="20%"><strong>End Date: &emsp;</strong></td>
<td width="50%" align="right"><?php echo form_input(array(
                'name'=>'end_date',
                'id'=>'end_date',
                'value'=>$end_date_input, 
                'class'=>'date')
        ); ?></td>
</tr>
<tr>
<td width="20%"><strong>Report Type: &emsp;</strong></td>
<?php $options = array(
        'categories'=>'Categories',
        'items'=>'Items'
    ); ?>
<td width="50%" align="right"><?php echo form_dropdown('report_type', $options,$report_type); ?></td>
</tr>
<?php if($report_type == 'categories'): ?>
<tr>
<td width="20%"><strong>Category: &emsp;</strong></td>
<?php
    $options = array('');
    foreach($categories as $item):
        $options[$item->category] = $item->category;
    endforeach;
?>
<td width="50%" align="right"><?php echo form_dropdown('category', $options,$category); ?></td>
</tr>
<?php endif; ?>
<tr>
<td colspan="2"><?php echo form_button(array("id"=>"workload_date_submit","content"=>"Submit","class"=>"submit_button float_right"));?></td>
</tr></tbody></table>
<?php echo form_close(); ?>

<div id="page_title"><?php echo $title ?></div>
<div id="page_subtitle">
<?php
    if($start_date == $end_date) echo $start_date;
    else echo $start_date . "&emsp;to&emsp;" . $end_date;
?>
</div>
<div id="workload_table_holder">
<table width="100%" id="sales_workload_table" class="display">
    <thead><tr>
        <th style="background-color: #0a6184; color:#FFF"><?php echo $th ?></th>
        <?php if($report_type == 'items'): ?>
        <th style="background-color: #0a6184; color:#FFF">Unit Price</th>
        <th style="background-color: #0a6184; color:#FFF">Quantity</th>
        <?php endif; ?>
        <?php if($report_type == 'employees'): ?>
        <th style="background-color: #0a6184; color:#FFF">Sales</th>
        <?php endif; ?>
        <?php if($discounts): ?><th style="background-color: #0a6184; color:#FFF">Discounts</th><?php endif; ?>
        <th style="background-color: #0a6184; color:#FFF">Value</th>
    </tr></thead>
    <tbody>
<?php
    $total_discounts = 0;
    $total_value = 0;
    $total_sales = 0;
    foreach($sales as $name=>$workload):
        //if($workload['value'] == 0 && $workload['discount'] == 0 ) continue;
        $total_discounts = $total_discounts + $workload['discount'];
        $total_value = $total_value + $workload['value'];
        $total_sales = $total_sales + $workload['sales'];
?>
       <tr>
            <th style="background-color: #DDD"><?php echo $workload['name']; ?></th>
            <?php if($report_type == 'items'): ?>
            <td style="background-color: #DDD" align="right"><?php echo to_currency($workload['price']); ?></td>
            <td style="background-color: #DDD" align="right"><?php echo $workload['quantity']; ?></td>
            <?php endif; ?>
            <?php if($report_type == 'employees'): ?>
            <td style="background-color: #DDD" align="right"><?php echo $workload['sales']; ?></td>
            <?php endif; ?>
            <?php if($discounts): ?><td style="background-color: #DDD" align="right"><?php echo to_currency($workload['discount']); ?></td><?php endif; ?>
            <td style="background-color: #DDD" align="right"><?php echo to_currency($workload['value']); ?></td>
        </tr>
<?php
    endforeach;
?>
    </tbody>
    <tfoot><tr>
        <th style="background-color: #DDD" <?php if($report_type == 'items'): ?>colspan="3"<?php endif; ?>>Total</th>
        <?php if($discounts): ?><th style="background-color: #DDD" align="right"><?php echo to_currency($total_discounts); ?></td><?php endif; ?>
        <th style="background-color: #DDD" align="right"><?php echo to_currency($total_value); ?></td>
    </tr></tfoot>
</table>
</div>
</div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $('#sales_workload_table').dataTable({
        //"bPaginate": false,
        //"bLengthChange": false,
        //"bFilter": false,
        //"bSort": false,
        //"bInfo": false,
        //"bStateSave": true,
    });

	Date.format = 'dd-mm-yyyy';
    $('.date').datepicker({
      changeMonth: true,
      changeYear: true,
      //minDate: new Date(2014, 0, 1),
      maxDate: "0",
      dateFormat: "dd-mm-yy",
    });

    $("#workload_date_submit").click(function(){
        $("#workload_date_form").ajaxSubmit({
            success:function(response)
            {
                if(response.form_validation)
                {
                    var html = "<table width='100%' height='100px'><tr><td align='center'><img src='<?php echo base_url()?>images/loading_animation.gif' alt='spinner' /></td></tr><table>";
                    $("#workload_table_holder").html(html);
                    $("#TB_ajaxContent").load('<?php echo site_url("sales/view_workload"); ?>');
                }
                else
                {
                    $("#error_message_box").html(response.error_messages);
                }
            },
            dataType:'json',
        });
    });
});
</script>
