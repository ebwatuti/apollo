<div id="archive">
<ul id="error_message_box"></ul>
<?php echo form_open("sales/archive_date_input",array('id'=>'archive_date_form')); ?>
<table width="100%"><tfoot><tr><td colspan="10"><br><hr></td></tr></tfoot>
<tbody><tr>
<td width="10%"><strong>Start Date: &emsp;</strong></td>
<td width="20%"><?php echo form_input(array(
                'name'=>'start_date',
                'id'=>'start_date',
                'value'=>$start_date_input, 
                'class'=>'date')
        ); ?></td>
<td width="10%"><strong>End Date: &emsp;</strong></td>
<td width="20%"><?php echo form_input(array(
                'name'=>'end_date',
                'id'=>'end_date',
                'value'=>$end_date_input, 
                'class'=>'date')
        ); ?></td>
<td width="40%"><?php echo form_button(array("id"=>"archive_date_submit","content"=>"Submit","class"=>"submit_button float_left"));?></td>
</tr></tbody></table>
<?php echo form_close(); ?>

<div id="page_subtitle">
<?php
    if($start_date == $end_date) echo $start_date;
    else echo $start_date . "&emsp;to&emsp;" . $end_date;
?>
</div>
<div id="archive_table_holder">
<table width="100%" id="archive_table" class="display">
    <thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Date</th>
        <th style="background-color: #0a6184; color:#FFF">Receipt #</th>
        <th style="background-color: #0a6184; color:#FFF">Patient #</th>
        <th style="background-color: #0a6184; color:#FFF">Patient Name</th>
        <th style="background-color: #0a6184; color:#FFF">Served by</th>
        <th style="background-color: #0a6184; color:#FFF">Discount</th>
        <th style="background-color: #0a6184; color:#FFF">Value</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>
    </tr></thead>
    <tbody>
<?php
if($sales->num_rows() > 0):
    foreach($sales->result() as $sale):
        $sale = $this->Sale->get_sale_info($sale->sale_id);
        $patient = $this->Patient->get_info($sale->customer_id);
        $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
        $patient->patient_no = $this->Patient->patient_number($patient->patient_id);

        $sale->date = date('j/n/Y g:i a',strtotime($sale->sale_time));
        $sale->receipt_no = $this->Sale->receipt_no($sale->sale_id);
        $sale->value = 0;
        $sale->discount = 0;

        foreach($sale->sale_items as $sale_item):
            $sale->value = $sale->value + $sale_item->quantity_purchased * $sale_item->item_unit_price * (100 - $sale_item->discount_percent) / 100;
            $sale->discount = $sale->discount + $sale_item->quantity_purchased * $sale_item->item_unit_price * $sale_item->discount_percent / 100;
        endforeach;

        $employee = $this->Employee->get_info($sale->employee_id);
        $employee->employee_name = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $sale->date; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $sale->receipt_no; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $patient->patient_no; ?></td>
        <td style="background-color: #DDD"><?php echo $patient->patient_name; ?></td>
        <td style="background-color: #DDD"><?php echo $employee->employee_name; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo to_currency($sale->discount); ?></td>
        <td style="background-color: #DDD" align="right"><?php echo to_currency($sale->value); ?></td>
        <td style="background-color: #DDD"><?php
        echo form_button(array(
            'name'=>'submit',
            'onclick'=>'view_sale('.$sale->sale_id.')',
            'content'=>'View',
            'class'=>'DTTT_button float_left')
        );
        ?></td>
        </tr>
<?php
    endforeach;
endif;
?>
    </tbody>
</table>
</div>
</div>

<div id="sale"></div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $('#archive_table').dataTable({
        //"bPaginate": false,
        //"bLengthChange": false,
        //"bFilter": false,
        "bSort": false,
        //"bInfo": false,
        "bStateSave": true,
    });

    Date.format = 'dd-mm-yyyy';
    $('.date').datepicker({
      changeMonth: true,
      changeYear: true,
      //minDate: new Date(2014, 0, 1),
      maxDate: "0",
      dateFormat: "dd-mm-yy",
    });

    $("#archive_date_submit").click(function(){
        $("#archive_date_form").ajaxSubmit({
            success:function(response)
            {
                if(response.form_validation)
                {
                    var html = "<table width='100%' height='100px'><tr><td align='center'><img src='<?php echo base_url()?>images/loading_animation.gif' alt='spinner' /></td></tr><table>";
                    $("#archive_table_holder").html(html);
                    $("#TB_ajaxContent").load('<?php echo site_url("sales/view_archive"); ?>');
                }
                else
                {
                    $("#error_message_box").html(response.error_messages);
                }
            },
            dataType:'json',
        });
    });
});

function view_sale(sale_id){
    var html = '<?php echo form_button(array("content"=>"Close","class"=>"submit_button float_right", "onclick"=>"close_sale()")); ?>';
    $("#sale").css('display','block');
    $("#sale").load('<?php echo site_url("sales/sale_details"); ?>/' + sale_id,
        function(response){
            $("#sale").append(html);
        });
    $('#archive').css('display','none');
}

function close_sale(){
    $("#sale").css('display','none');
    $("#sale").html('&ensp;');
    $('#archive').css('display','block');
}
</script>
