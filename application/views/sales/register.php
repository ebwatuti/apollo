<?php $this->load->view("partial/header"); ?>

<div id="title_bar">
	<div id="page_title" style="margin-bottom:8px;">Sales Register</div>
	
</div>
<?php
if(isset($error))
{
	echo "<div class='error_message'>".$error."</div>";
}

if (isset($warning))
{
	echo "<div class='warning_mesage'>".$warning."</div>";
}

if (isset($success))
{
	echo "<div class='success_message'>".$success."</div>";
}
?>
<div id="sidebar">
	<?php echo anchor("sales/view_workload/width:550",
	"<div class='submenu_button' style='float: left;'><span>Workload</span></div>",
	array('title'=>'Sales Workload','class'=>'thickbox none',));
	?>
	<?php echo anchor("sales/view_archive/width:950",
	"<div class='submenu_button' style='float: left;'><span>Archive</span></div>",
	array('title'=>'Sales Archive','class'=>'thickbox none',));
	?>
</div>
<div id="register_wrapper">
<?php echo form_open("sales/change_mode",array('id'=>'mode_form')); ?>
	<span>Sales Mode</span>
<?php echo form_dropdown('mode',$modes,$mode,'onchange="$(\'#mode_form\').submit();"'); ?>

</form>
<?php echo form_open("sales/add",array('id'=>'add_item_form')); ?>
<label id="item_label" for="item">

<?php
if($mode=='sale')
{
	//echo $this->lang->line('sales_find_or_scan_item');
}
else
{
	echo $this->lang->line('sales_find_or_scan_item_or_receipt');
}
?>
</label>

<?php if($mode !='sale') echo form_input(array('name'=>'item','id'=>'item','size'=>'40','placeholder'=>'item name/#'));?>

</form>
<table id="register">
<thead>
<tr>
<th style="width:30%;">Item Name</th>
<th style="width:10%;">Price</th>
<th style="width:10%;">Qty</th>
<th style="width:10%;">Disc %</th>
<th style="width:15%;">Total</th>
<th style="width:25%;">&emsp;</th>
</tr>
</thead>
<tbody id="cart_contents">
<?php
if(count($cart)==0)
{
?>
<tr><td colspan='8'>
<div class='warning_message' style='padding:7px;'><?php echo $this->lang->line('sales_no_items_in_cart'); ?></div>
</tr></tr>
<?php
}
else
{
	foreach(array_reverse($cart, true) as $line=>$item)
	{
		$cur_item_info = $this->Item->get_info($item['item_id']);
		echo form_open("sales/edit_item/$line");
	?>
		<tr>
		<td style="align:center;"><?php echo $item['name']; ?></td>



		<td><?php echo $item['price']; ?></td>
		<?php echo form_hidden('price',$item['price']); ?>

		<td>
		<?php
        	if($item['is_serialized']==1)
        	{
        		echo $item['quantity'];
        		echo form_hidden('quantity',$item['quantity']);
        	}
        	else
        	{
        		echo form_input(array('name'=>'quantity','value'=>$item['quantity'],'size'=>'2'));
        	}
		?>
		</td>

		<td><?php echo $item['discount']; ?></td>
		<?php echo form_hidden('discount',$item['discount']); ?>
		<td><?php echo to_currency($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100); ?></td>
		<td><?php echo form_submit(array('name'=>'edit_item', 'value'=>'Edit','class'=>'submit_button float_left'));?>
		&emsp;
		<?php echo anchor("sales/delete_item/$line",
		"<div class='submit_button' style='float: left;'><span>Delete</span></div>");?></td>
		</tr>
		
		<tr style="height:3px">
		<td colspan=8 style="background-color:white"> </td>
		</tr>		</form>
	<?php
	}
}
?>
</tbody>
</table>
</div>


<div id="overall_sale">
	<?php
	if(isset($patient_no))
	{
?>
<table width="100%">
    <tr><td colspan="2">
    <div class="field_row clearfix">    
    <?php echo form_label('Patient #:'); ?>
        <div class='form_field'>
        <?php echo $patient_no;?>
        </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Name:'); ?>
        <div class='form_field'>
        <?php echo $patient_name;?>
        </div>
    </div>
    </td></tr>

    <tr><td colspan="2" width="100%">
        <?php
        echo anchor("sales/remove_customer",form_button(array('content'=>'Remove Patient','class'=>'submit_button float_right')));
        ?>
    </td></tr>
</table>
<?php
	}
	else
	{
		echo form_open("sales/select_customer",array('id'=>'select_customer_form')); ?>
		<label id="customer_label" for="customer">Select Patient (optional)</label>
		<?php echo form_input(array('name'=>'customer','id'=>'customer','size'=>'30','placeholder'=>'patient name/#'));?>
		</form>
		<div class="clearfix">&nbsp;</div>
<?php
	}
	?>

	<div id='sale_details'>
		<div class="float_left" style="width:55%;"><?php echo $this->lang->line('sales_sub_total'); ?>:</div>
		<div class="float_left" style="width:45%;font-weight:bold;"><?php echo to_currency($subtotal); ?></div>

		<?php foreach($taxes as $name=>$value) { ?>
		<div class="float_left" style='width:55%;'><?php echo $name; ?>:</div>
		<div class="float_left" style="width:45%;font-weight:bold;"><?php echo to_currency($value); ?></div>
		<?php }; ?>

		<div class="float_left" style='width:55%;'><?php echo $this->lang->line('sales_total'); ?>:</div>
		<div class="float_left" style="width:45%;font-weight:bold;"><?php echo to_currency($total); ?></div>
	</div>




	<?php
	// Only show this part if there are Items already in the sale.
	if(count($cart) > 0)
	{
	?>		
	<div class="clearfix">&nbsp;</div>
	<div id="Payment_Types" >

		<div style="height:100px;">

			<?php echo form_open("sales/add_payment",array('id'=>'add_payment_form')); ?>
			<table width="100%">
			<tr>
			<td>
				<?php echo $this->lang->line('sales_payment').':   ';?>
			</td>
			<td>
				<?php echo form_dropdown( 'payment_type', $payment_options, 'Cash', 'id="payment_types" onchange="checkPaymentType(this)"' ); ?>
			</td>
			</tr>
			<tr id="payment_remarks_row" style="display: none;">
			<td>
				<span id="payment_remarks_label">Remarks</span>
			</td>
			<td>
				<?php echo form_input( array( 'name'=>'payment_remarks', 'id'=>'payment_remarks', 'size'=>'10' ) );	?>
			</td>
			</tr>
			<tr>
			<td>
				<span id="amount_tendered_label"><?php echo $this->lang->line( 'sales_amount_tendered' ).': '; ?></span>
			</td>
			<td>
				<?php echo form_input( array( 'name'=>'amount_tendered', 'id'=>'amount_tendered', 'value'=>to_currency_no_money($amount_due), 'size'=>'10' ) );	?>
			</td>
			</tr>
        	</table>
			<div>
				<?php echo form_button(array('content'=>'Add Payment','class'=>'submit_button float_right','id'=>'add_payment_button')); ?>
			</div>
		</div>
		</form>

		<?php
		// Only show this part if there is at least one payment entered.
		if(count($payments) > 0)
		{
		?>
	    	<table id="register">
	    	<thead>
			<tr>
			<th><?php echo 'Type'; ?></th>
			<th><?php echo 'Amount'; ?></th>
			<th>&nbsp;</th>


			</tr>
			</thead>
			<tbody id="payment_contents">
			<?php
				foreach($payments as $payment_id=>$payment)
				{
				echo form_open("sales/edit_payment/$payment_id",array('id'=>'edit_payment_form'.$payment_id));
				?>
	            <tr>
				<td><?php echo $payment['payment_type']; ?><?php echo $payment['payment_remarks'] ? ': '.$payment['payment_remarks'] : ''; ?></td>
				<td style="text-align:right;"><?php echo to_currency( $payment['payment_amount'] ); ?></td>
	            <td><?php echo anchor( "sales/delete_payment/$payment_id", form_button(array('content'=>'Delete','class'=>'submit_button float_right'))); ?></td>
				</tr>
				</form>
				<?php
				}
				?>
			</tbody>
			</table>
		    <br />
		<?php
		}
		?>
	</div>
	<div class="clearfix">&nbsp;</div>
	<table width="100%"><tr>
    <td style="width:55%; "><div class="float_left"><?php echo 'Payments Total:' ?></div></td>
    <td style="width:45%; text-align:right;"><div class="float_left" style="text-align:right;font-weight:bold;"><?php echo to_currency($payments_total); ?></div></td>
	</tr>
	<tr>
	<td style="width:55%; "><div class="float_left" ><?php echo 'Amount Due:' ?></div></td>
	<td style="width:45%; text-align:right; "><div class="float_left" style="text-align:right;font-weight:bold;"><?php echo to_currency($amount_due); ?></div></td>
	</tr></table>
	<div class="clearfix">&nbsp;</div>
	<div id="finish_sale">
		<?php
		// Only show this part if there is at least one payment entered.
		if(count($payments) > 0)
		{
		?>
				<?php echo form_open("sales/complete",array('id'=>'finish_sale_form')); 
								 
				if ($payments_cover_total)
				{
					echo form_button(array('content'=>'Complete Sale','class'=>'submit_button float_left','id'=>'finish_sale_button'));
				}
				?>
			</form>
		<?php
		}
		?>
		<?php echo form_open("sales/cancel_sale",array('id'=>'cancel_sale_form')); ?>
		<?php echo form_button(array('content'=>'Cancel Sale','class'=>'submit_button float_right','id'=>'cancel_sale_button')); ?>
    	</form>
	</div>

	<?php
	}
	?>
</div>
<div class="clearfix" style="margin-bottom:30px;">&nbsp;</div>


<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $("#item").autocomplete('<?php echo site_url("sales/item_search"); ?>',
    {
    	minChars:0,
    	max:100,
    	selectFirst: false,
       	delay:10,
    	formatItem: function(row) {
			return row[1];
		}
    });

    $("#item").result(function(event, data, formatted)
    {
		$("#add_item_form").submit();
    });

	$('#item').focus();

	$("#customer").autocomplete('<?php echo site_url("sales/customer_search"); ?>',
    {
    	minChars:0,
    	delay:10,
    	max:100,
    	formatItem: function(row) {
			return row[1];
		}
    });

    $("#customer").result(function(event, data, formatted)
    {
		$("#select_customer_form").submit();
    });

    $("#finish_sale_button").click(function()
    {
    	if (confirm('<?php echo $this->lang->line("sales_confirm_finish_sale"); ?>'))
    	{
    		$('#finish_sale_form').submit();
    	}
    });

	$("#suspend_sale_button").click(function()
	{
		if (confirm('<?php echo $this->lang->line("sales_confirm_suspend_sale"); ?>'))
    	{
			$('#finish_sale_form').attr('action', '<?php echo site_url("sales/suspend"); ?>');
    		$('#finish_sale_form').submit();
    	}
	});

    $("#cancel_sale_button").click(function()
    {
    	if (confirm('<?php echo $this->lang->line("sales_confirm_cancel_sale"); ?>'))
    	{
    		$('#cancel_sale_form').submit();
    	}
    });

	$("#add_payment_button").click(function()
	{
	   $('#add_payment_form').submit();
    });
});

function post_item_form_submit(response)
{
	if(response.success)
	{
		$("#item").attr("value",response.item_id);
		$("#add_item_form").submit();
	}
}

function post_person_form_submit(response)
{
	if(response.success)
	{
		$("#customer").attr("value",response.person_id);
		$("#select_customer_form").submit();
	}
}

function checkPaymentType(input)
{
	if ($(input).val() == "<?php echo $this->lang->line('sales_giftcard'); ?>")
	{
		$("#amount_tendered_label").html("<?php echo $this->lang->line('sales_giftcard_number'); ?>");
		$("#amount_tendered").val('');
		$("#amount_tendered").focus();
	}
	else if ($(input).val() == "<?php echo $this->lang->line('sales_check'); ?>")
	{
		$("#amount_tendered_label").html("<?php echo $this->lang->line('sales_amount_tendered'); ?>:");
		$("#payment_remarks_row").css('display','block');
		$("#payment_remarks_label").html("<?php echo $this->lang->line('sales_check'); ?> No(s):");
		$("#payment_remarks").focus();
	}
	else
	{
		$("#payment_remarks_row").css('display','none');
		$("#payment_remarks").val('');
		$("#amount_tendered_label").html("<?php echo $this->lang->line('sales_amount_tendered'); ?>:");		
	}
}

</script>
