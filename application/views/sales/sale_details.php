<?php
//get the controller name 
$CI =& get_instance();
$controller_name=strtolower(get_class($CI));
?>
<div>
    <?php echo form_button(array("content"=>"Print Receipt","onclick"=>"print_receipt($sale->sale_id)","class"=>"print_button float_right")); ?>
</div>
<div id="sale_holder">
<?php
    $patient = $this->Patient->get_info($sale->customer_id);
    $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
    $patient->age = $this->Patient->patient_age($patient->dob);
    $patient->patient_no = $this->Patient->patient_number($patient->patient_id);

    $employee = $this->Employee->get_info($sale->employee_id);
    $served_by = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);

    $sale->value = 0;
    $sale->discount = 0;
    foreach($sale->sale_items as $sale_item):
        $sale->value = $sale->value + $sale_item->quantity_purchased * $sale_item->item_unit_price * (100 - $sale_item->discount_percent) / 100;
        $sale->discount = $sale->discount + $sale_item->quantity_purchased * $sale_item->item_unit_price * $sale_item->discount_percent / 100;
    endforeach;
?>
<table>
<?php if($sale->customer_id): ?>
<tr><td><b>Patient #:&emsp;</b></td><td><?php echo $patient->patient_no; ?></td></tr>
<tr><td><b>Patient Name:&emsp;</b></td><td><?php echo $patient->patient_name; ?></td></tr>
<?php endif; ?>
<tr><td><b>Served by:&emsp;</b></td><td><?php echo $served_by; ?></td></tr>
<tr><td><b>Receipt #:&emsp;</b></td><td><?php echo $this->Sale->receipt_no($sale->sale_id); ?></td></tr>
<tr><td><b>Value:&emsp;</b></td><td><?php echo to_currency($sale->value); ?></td></tr>
<tr><td><b>Payments:&emsp;</b></td><td><?php echo $sale->payment_type; ?></td></tr>
<tr><td><b>Date:&emsp;</b></td><td><?php echo date("M j, Y",strtotime($sale->sale_time));?></td></tr>
</table>
<hr />

<table width="100%">
<thead>
<tr>
<th style="background-color: #0a6184; color:#FFF">Item</th>
<th style="background-color: #0a6184; color:#FFF">Price</th>
<th style="background-color: #0a6184; color:#FFF">Qty</th>
<th style="background-color: #0a6184; color:#FFF">Disc %</th>
<th style="background-color: #0a6184; color:#FFF">Total</th>
</tr>
</thead>
<tbody id="sale_contents">

<?php
    foreach($sale->sale_items as $sale_item):
        $item_info = $this->Item->get_info($sale_item->item_id);
    	$sale_item->total = $sale_item->quantity_purchased * $sale_item->item_unit_price * (100 - $sale_item->discount_percent) / 100;
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $item_info->name; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo to_currency($sale_item->item_unit_price); ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $sale_item->quantity_purchased; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo $sale_item->discount_percent; ?>%</td>
        <td style="background-color: #DDD" align="right"><?php echo to_currency($sale_item->total); ?></td>
        </tr>      
<?php
    endforeach; 
?>
</tbody>
</table>
</div>

<script type="text/javascript">
function print_receipt(sale_id)
{
    $.get("<?php echo site_url($controller_name.'/sale_receipt'); ?>/" + sale_id,{},function(result){
        CallPrint(result,true);
      });
}
</script>

