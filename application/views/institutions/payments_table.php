<div width="100%" id="message_bar"></div>
<div id="payments">
<?php
    echo form_button(array(
        'name'=>'submit',
        'id'=>'new_payment_button',
        'content'=>'New Payment',
        'class'=>'submit_button float_right')
    );
?>
<div id="page_title"><?php echo $institution->institution_name; ?> Payments</div>
<table width="100%" id="payments_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Date</th>
        <th style="background-color: #0a6184; color:#FFF">Employee</th>
        <th style="background-color: #0a6184; color:#FFF">Payment Type</th>
        <th style="background-color: #0a6184; color:#FFF">Amount</th>
        <th style="background-color: #0a6184; color:#FFF">Remarks</th>
    </tr></thead>
    <tbody>
<?php
if($payments->num_rows() > 0):
    foreach($payments->result() as $payment):
        $payment->date = date('j/n/Y g:i a',strtotime($payment->payment_time));
        $employee = $this->Employee->get_info($payment->employee_id);
        $employee->employee_name = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $payment->date; ?></td>
        <td style="background-color: #DDD"><?php echo $employee->employee_name; ?></td>
        <td style="background-color: #DDD"><?php echo $payment->payment_type; ?></td>
        <td style="background-color: #DDD" align="right"><?php echo to_currency($payment->amount); ?></td>
        <td style="background-color: #DDD"><?php echo $payment->remarks; ?></td>
        </tr>
<?php
    endforeach;
endif;
?>
	</tbody>
</table>
</div>

<div id="payment_form_area"></div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    TableTools.DEFAULTS.aButtons = [ 
        "copy", 
        {
                    "sExtends": "print",
                    "sMessage": "<?php echo $this->config->item('company') .' - '. $institution->institution_name . ' Payments'; ?>",
        },
        {
            "sExtends":    "collection",
            "sButtonText": "Save",
            "aButtons":    [  
                {"sExtends": "xls","sTitle": "<?php echo $this->config->item('company') .' - '. $institution->institution_name . ' Payments'; ?>"}, 
                {"sExtends": "pdf","sPdfMessage": "<?php echo $month; ?>","sPdfOrientation": "landscape","sTitle": "<?php echo $this->config->item('company') .' - '. $institution->institution_name . ' Payments'; ?>"} ]
        }, 
    ];
    $('#payments_table').dataTable({
        //"bPaginate": false,
        //"bLengthChange": false,
        //"bFilter": false,
        "bSort": false,
        //"bInfo": false,
        "bStateSave": true,
        "sDom": 'T<"clear">lfrtip',
    });

    $("#new_payment_button").click(function(){
        $("#payment_form_area").css('display','block');
        $("#payment_form_area").load('<?php echo site_url("institutions/new_payment/$institution->institution_id"); ?>');
        $('#payments').css('display','none');
    });
});
</script>
