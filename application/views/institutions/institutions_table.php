<table width="100%" id="institutions_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Institution Name</th>
        <th style="background-color: #0a6184; color:#FFF">Phone #</th>
        <th style="background-color: #0a6184; color:#FFF">Address</th>
        <th style="background-color: #0a6184; color:#FFF">% Discount</th>
        <th style="background-color: #0a6184; color:#FFF">% Liability</th>
        <th style="background-color: #0a6184; color:#FFF">Outstanding Debt</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>
    </tr></thead>
    <tbody>
<?php
	foreach($institutions->result() as $institution):
        $institution->institution_name = $this->Patient->patient_name($institution->institution_name);
        $institution->outstanding_debt = $this->Institution->get_outstanding_debt($institution->institution_id);
?>
        <tr>
        <td style="background-color: #DDD"><?php echo $institution->institution_name; ?></td>
        <td style="background-color: #DDD"><?php echo $institution->phone_number; ?></td>
        <td style="background-color: #DDD"><?php echo nl2br($institution->address_1); ?></td>
        <td style="background-color: #DDD"><?php echo $institution->institution_discount; ?>%</td>
        <td style="background-color: #DDD"><?php echo $institution->institution_liability; ?>%</td>
        <td style="background-color: #DDD"><?php echo to_currency($institution->outstanding_debt); ?></td>
        <td style="background-color: #DDD">
            <?php echo anchor("institutions/view_institution/$institution->institution_id/width:350",
                    form_button(array('name'=>'submit','content'=>'Edit','class'=>'DTTT_button float_left')),
                    array("class"=>"thickbox vote_thickbox","title"=>"Institution Details")); ?>
            &emsp;
            <?php echo anchor("institutions/view_invoice/$institution->institution_id/width:950",
                    form_button(array('name'=>'submit','content'=>'Invoice','class'=>'DTTT_button float_left')),
                    array("class"=>"thickbox vote_thickbox","title"=>"Institution Invoice")); ?>
            &emsp;
            <?php echo anchor("institutions/view_payments/$institution->institution_id/width:850",
                    form_button(array('name'=>'submit','content'=>'Payments','class'=>'DTTT_button float_left')),
                    array("class"=>"thickbox vote_thickbox","title"=>"Institution Payments")); ?>
            &emsp;            
            <?php echo form_button(array('name'=>'submit','content'=>'Delete','class'=>'DTTT_button float_left','onclick'=>'delete_institution('.$institution->institution_id.')')); ?>
        </td>
        </tr>
<?php
	endforeach;
?>
	</tbody>
</table>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	tb_init("a.vote_thickbox");
    TableTools.DEFAULTS.aButtons = [ 
        "copy", 
        {
                    "sExtends": "print",
                    "sMessage": "",
        },
        {
            "sExtends":    "collection",
            "sButtonText": "Save",
            "aButtons":    [  
                {"sExtends": "xls","sTitle": "<?php echo $this->config->item('company'); ?> - Institutions","mColumns": [ 0,1,2,3,4 ]}, 
                {"sExtends": "pdf","sPdfMessage": "","sPdfOrientation": "portrait","sTitle": "<?php echo $this->config->item('company'); ?> - Institutions","mColumns": [ 0,1,2,3,4 ]} ]
        }, 
    ];
	$('#institutions_table').dataTable({
		//"bPaginate": false,
		//"bLengthChange": false,
		//"bFilter": false,
		//"bSort": false,
		//"bInfo": false,
        "bStateSave": true,
        "sDom": 'T<"clear">lfrtip',
	});
});

function delete_institution(institution_id)
{
    if(confirm('Are you sure you want to delete this institution?'))
    {
        $.post('<?php echo site_url("institutions/delete_institution"); ?>/' + institution_id,
            {},
            function(response){
                if(response.success)
                {
                    $("#table_holder").load('<?php echo site_url("institutions/view_all"); ?>',
                        function(result){
                            set_feedback(response.message,response.message_class,false);
                        });
                }
                else
                {
                    set_feedback(response.message,response.message_class,false);
                }
            },'json');
    }
}
</script>
