<ul id="error_message_box"></ul>
<?php echo form_open("institutions/invoice_date_input",array('id'=>'invoice_date_form')); ?>
<table>
<tbody><tr>
<td><strong>Month: &emsp;</strong></td>
<td align="right"><?php echo form_input(array(
                'name'=>'month',
                'id'=>'month',
                'value'=>$month_input, 
                'class'=>'date')
        ); ?></td>
<td><?php echo form_button(array("id"=>"invoice_date_submit","content"=>"Submit","class"=>"submit_button float_left"));?></td>
</tr></tbody></table>
<table width="100%"><tfoot><tr><td colspan="10"><br><hr></td></tr></tfoot></table>
<?php echo form_close(); ?>
<div id="page_title"><?php echo $title ?></div>
<div id="page_subtitle">
<?php echo $month; ?>
</div>
<div id="invoice_table_holder">
<table width="100%" id="invoice_table" class="display">
    <thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Date</th>
        <th style="background-color: #0a6184; color:#FFF">Patient #</th>
        <th style="background-color: #0a6184; color:#FFF">Name</th>
        <?php 
        foreach($headers as $header):
            $totals[$header] = 0;
        ?>
        <th style="background-color: #0a6184; color:#FFF"><?php echo $header ?></th>
        <?php endforeach; ?>
        <th style="background-color: #0a6184; color:#FFF">Total</th>
    </tr></thead>
    <tbody>
<?php
    foreach($workloads as $workload):
        $workload_total = 0;
        if(array_sum($workload['categories']) == 0) continue;
?>
        <tr>
            <td style="background-color: #DDD"><?php echo $workload['date']; ?></td>
            <td style="background-color: #DDD"><?php echo $workload['patient_no']; ?></td>
            <td style="background-color: #DDD"><?php echo $workload['name']; ?></td>
            <?php
            foreach($headers as $header):
                $workload_total = $workload_total + $workload['categories'][$header];
                $totals[$header] = $totals[$header] + $workload['categories'][$header];
            ?>
            <td style="background-color: #DDD" align="right"><?php echo to_currency($workload['categories'][$header]); ?></td>
            <?php endforeach; ?>
            <td style="background-color: #DDD" align="right"><?php echo to_currency($workload_total); ?></td>
        </tr>
<?php endforeach; ?>
    </tbody>
    <tfoot><tr>
        <th style="background-color: #DDD" colspan="3">Total</th>
        <?php foreach($headers as $header): ?>
        <th style="background-color: #DDD" align="right"><?php echo to_currency($totals[$header]); ?></td>
        <?php endforeach; ?>
        <th style="background-color: #DDD" align="right"><?php echo to_currency(array_sum($totals)); ?></td>
    </tr></tfoot>
</table>
</div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    TableTools.DEFAULTS.aButtons = [ 
        "copy", 
        {
                    "sExtends": "print",
                    "sMessage": "<?php echo $this->config->item('company') .' - '. $title; ?>",
        },
        {
            "sExtends":    "collection",
            "sButtonText": "Save",
            "aButtons":    [  
                {"sExtends": "xls","sTitle": "<?php echo $this->config->item('company') .' - '. $title; ?>"}, 
                {"sExtends": "pdf","sPdfMessage": "<?php echo $month; ?>","sPdfOrientation": "landscape","sTitle": "<?php echo $this->config->item('company') .' - '. $title; ?>"} ]
        }, 
    ];

    $('#invoice_table').dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        //"bFilter": false,
        //"bSort": false,
        //"bInfo": false,
        "bStateSave": true,
        "sDom": 'T<"clear">lfrtip',
    });

	Date.format = 'dd-mm-yyyy';
    $('.date').MonthPicker({ShowIcon: false});

    $("#invoice_date_submit").click(function(){
        $("#invoice_date_form").ajaxSubmit({
            success:function(response)
            {
                if(response.form_validation)
                {
                    var html = "<table width='100%' height='100px'><tr><td align='center'><img src='<?php echo base_url()?>images/loading_animation.gif' alt='spinner' /></td></tr><table>";
                    $("#invoice_table_holder").html(html);
                    $("#TB_ajaxContent").load('<?php echo site_url("institutions/view_invoice/$institution->institution_id"); ?>');
                }
                else
                {
                    $("#error_message_box").html(response.error_messages);
                }
            },
            dataType:'json',
        });
    });
});
</script>
