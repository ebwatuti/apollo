<div width="100%" id="message_bar"></div>
<?php echo form_open('institutions/save_institution/'.$institution_info->institution_id,array('id'=>'institution_form')); ?>
<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>
<fieldset id="supplier_basic_info">
<legend>Institution Info</legend>
<div class="field_row clearfix">	
<?php echo form_label('Institution Name:', 'institution_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'institution_name',
		'id'=>'institution_name',
		'value'=>$institution_info->institution_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Phone Number:', 'phone_number'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'phone_number',
		'id'=>'phone_number',
		'value'=>$institution_info->phone_number)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Address:', 'address_1'); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
            'name'=>'address_1',
			'id'=>'address_1',
            'value'=>$institution_info->address_1,
            'rows'=>'5',
			'cols'=>'17',)); ?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('% Discount:', 'institution_discount'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'institution_discount',		
		'type'=>'number',
		'min'=>0,
		'max'=>100,
		'id'=>'institution_discount',
		'value'=>$institution_info->institution_discount ? $institution_info->institution_discount : 0)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('% Liability:', 'institution_liability',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'institution_liability',
		'type'=>'number',
		'min'=>0,
		'max'=>100,
		'id'=>'institution_liability',
		'value'=>$institution_info->institution_liability ? $institution_info->institution_liability : 100)
	);?>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_button(array("id"=>"institution_form_submit","content"=>"Submit","class"=>"submit_button float_right"));?>
</div>
</fieldset>

<?php echo form_close(); ?>

<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{	
	$("#institution_form_submit").click(function(){
		$("#institution_form").ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					if(response.success)
					{
						$("#table_holder").load('<?php echo site_url("institutions/view_all"); ?>');
						tb_remove();
						set_feedback(response.message,response.message_class,false);						
					}
					else
					{
						$("#message_bar").removeClass('error_message');
	                    $("#message_bar").removeClass('warning_message');
	                    $("#message_bar").removeClass('success_message');
	                    $("#message_bar").addClass(response.message_class);
	                    $("#message_bar").html(response.message);
	                    $('#message_bar').fadeTo(5000, 1);
	                    $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});
});
</script>