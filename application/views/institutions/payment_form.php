<div width="100%" id="message_bar"></div>
<div id="required_fields_message">The fields marked red are required.</div>
<ul id="error_message_box"></ul>

<?php echo form_open("institutions/save_payment/$institution->institution_id",array('id'=>'payment_form')); ?>
<fieldset id="employee_basic_info">
<legend>Payment Info</legend>
<div class="field_row clearfix">	
<?php echo form_label('Institution:'); ?>
	<div class='form_field'>
	<?php echo $institution->institution_name;?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Payment Type:', 'payment_type',array('class'=>'required')); ?>
	<div class='form_field'>
	<select name="payment_type" id="payment_type" onchange="check_payment_type(this)">
		<option value="Cash">Cash</option>
		<option value="Cheque">Cheque</option>
	</select>
	
	</div>
</div>

<div class="field_row clearfix" id="payment_info_div">	
<?php echo form_label('Cheque #:', 'payment_info'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'payment_info',
		'id'=>'payment_info')
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Amount:', 'amount',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'amount',
		'id'=>'amount',
		'required'=>'required')
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Remarks:', 'remarks'); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
            'name'=>'remarks',
			'id'=>'remarks',
            'rows'=>'5',
			'cols'=>'17',)); ?>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_button(array("id"=>"payment_form_submit","content"=>"Submit","class"=>"submit_button float_left"));?>
	<?php echo form_button(array("id"=>"payment_form_cancel","content"=>"Cancel","class"=>"submit_button float_right"));?>
</div>
<?php  echo form_close(); ?>
</fieldset>

<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{	
	$("#payment_info_div").css('display','none');

	$("#payment_form_submit").click(function(){
		$("#payment_form").ajaxSubmit({
			success:function(response)
			{
				if(response.form_validation)
				{
					//tb_remove();
					if(response.success)
					{
						$("#TB_ajaxContent").load('<?php echo site_url("institutions/view_payments/$institution->institution_id"); ?>',
							function(result){
								$("#message_bar").removeClass('error_message');
	                            $("#message_bar").removeClass('warning_message');
	                            $("#message_bar").removeClass('success_message');
	                            $("#message_bar").addClass(response.message_class);
	                            $("#message_bar").html(response.message);
	                            $('#message_bar').fadeTo(5000, 1);
	                            $('#message_bar').fadeTo("fast",0);
	                            $("#table_holder").load('<?php echo site_url("institutions/view_all"); ?>');
							});
					}
					else
					{
						$("#message_bar").removeClass('error_message');
	                    $("#message_bar").removeClass('warning_message');
	                    $("#message_bar").removeClass('success_message');
	                    $("#message_bar").addClass(response.message_class);
	                    $("#message_bar").html(response.message);
	                    $('#message_bar').fadeTo(5000, 1);
	                    $('#message_bar').fadeTo("fast",0);
					}
				}
				else
				{
					$("#error_message_box").html(response.error_messages);
				}
			},
			dataType:'json',
		});
	});

	$("#payment_form_cancel").click(function(){
		$("#payment_form_area").css('display','none');
	    $("#payment_form_area").html('&ensp;');
	    $('#payments').css('display','block');
	});
});

function check_payment_type(input)
{
	if($(input).val() == 'Cash'){
		$("#payment_info_div").css('display','none');
	    $("#payment_info").val('');
	}

	else{
		$("#payment_info_div").css('display','block');
	}
}
</script>