<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<base href="<?php echo base_url();?>" />
	<title><?php echo $this->config->item('company'); ?></title>
	<link rel="icon" href="<?php echo base_url();?>images/favicon.ico" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/ospos.css" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/ospos_print.css"  media="print"/>
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/jquery-ui.css" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/MonthPicker.2.1.css" />
	<style type="text/css" title="currentStyle">
		@import "<?php echo base_url();?>media/css/demo_page.css";
		@import "<?php echo base_url();?>media/css/demo_table.css";
		@import "<?php echo base_url();?>media/css/TableTools.css";
	</style>
	<script>BASE_URL = '<?php echo site_url(); ?>';</script>
	<script src="<?php echo base_url();?>js/jquery-1.9.1.min.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery-migrate-1.2.1.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery-ui.min.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/MonthPicker.2.1.min.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.color.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.metadata.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.form.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.tablesorter.min.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.ajax_queue.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.bgiframe.min.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.autocomplete.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.validate.min.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.jkey-1.1.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/thickbox.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/common.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/manage_tables.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/swfobject.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/date.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	
	<script src="<?php echo base_url();?>media/js/jquery.dataTables.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>media/js/TableTools.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>media/js/ZeroClipboard.js" type="text/javascript" language="javascript" charset="UTF-8"></script>

	<script src="<?php echo base_url();?>js/SpryAccordion.js" type="text/javascript"></script>
	<link href="<?php echo base_url();?>css/SpryAccordion.css" rel="stylesheet" type="text/css" />
    <script src="<?php echo base_url();?>js/SpryTabbedPanels.js" type="text/javascript"></script>
	<link href="<?php echo base_url();?>css/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />

	<script src="<?php echo base_url();?>js/modaldbox.js" type="text/javascript"></script>
	<link href="<?php echo base_url();?>css/modaldbox.css" rel="stylesheet" type="text/css" />

	<script src="<?php echo base_url();?>js/highcharts.js" type="text/javascript"></script>
	
<style type="text/css">
html {
    overflow: auto;
}
</style>

</head>
<body>
<div id="menubar">
	<div id="menubar_container">
		<div id="menubar_company_info">
		<span id="company_title"><?php echo $this->config->item('company'); ?></span><br />
		</div>

		<div id="menubar_navigation">
			<div class="menu_item">
				<a href="<?php echo site_url("home");?>" >
				<img src="<?php echo base_url().'images/menubar/home.png';?>" border="0" alt="Menubar Image" /></a><br />
				<a href="<?php echo site_url("$module->module_id");?>">Home</a>
			</div>
			<?php
			foreach($allowed_modules->result() as $module)
			{
			?>
			<div class="menu_item">
				<a href="<?php echo site_url("$module->module_id");?>">
				<img src="<?php echo base_url().'images/menubar/'.$module->module_id.'.png';?>" border="0" alt="Menubar Image" /></a><br />
				<a href="<?php echo site_url("$module->module_id");?>"><?php echo $module->name ?></a>
			</div>
			<?php
			}
			?>
			<div class="menu_item">
				<a href="<?php echo base_url()."chat/index.php/site_admin/";?>" target="new">
				<img src="<?php echo base_url().'images/menubar/chat.png';?>" border="0" alt="Menubar Image" /></a><br />
				<a href="<?php echo base_url()."chat/index.php/site_admin/";?>" target="new">Live Chat</a>
			</div>
		</div>

		<div id="menubar_footer">
		<?php echo "Welcome ".anchor("home/view/width:350", "$user_info->first_name $user_info->last_name ",array('class'=>'thickbox none','title'=>'Profile'))."! | "; ?>
		<?php echo anchor("home/logout",'Logout'); ?>
		</div>

		<div id="menubar_date">
		<?php echo date('M j, Y g:i a') ?>
		</div>

	</div>
</div>
<div id="content_area_wrapper">
<div id="content_area">
