<div width="100%" id="message_bar"></div>
<div id="orders">
<?php
    echo form_button(array(
        'name'=>'submit',
        'id'=>'new_order_button',
        'content'=>'New Order',
        'class'=>'submit_button float_right')
    );
?>
<br><br>
<table width="100%" id="orders_table" class="display">
	<thead><tr>
        <th style="background-color: #0a6184; color:#FFF">Order #</th>
        <th style="background-color: #0a6184; color:#FFF">Date</th>
        <th style="background-color: #0a6184; color:#FFF">Remarks</th>
        <th style="background-color: #0a6184; color:#FFF">Status</th>
        <th style="background-color: #0a6184; color:#FFF">&nbsp;</th>
    </tr></thead>
    <tbody>
<?php
if($pending_orders->num_rows() > 0):
	foreach($pending_orders->result() as $order):
        $order = $this->Order->get_info($order->order_id);
        switch ($order->order_status) {
            case 0:
                $order->status = 'Pending';
                break;
            case 1:
                $order->status = 'Rejected on '.date('j/n/Y g:i a',strtotime($order->service_time));
                break;
            case 2:
                $order->status = 'Approved on '.date('j/n/Y g:i a',strtotime($order->service_time));
                break;
            default:
                $order->status = 'Pending';
                break;
        }
?>
        <tr>
        <td style="background-color: #DDD" align="right"><?php echo $order->order_id; ?></td>
        <td style="background-color: #DDD"><?php echo date('j/n/Y g:i a',strtotime($order->order_time)); ?></td>
        <td style="background-color: #DDD"><?php echo $order->remarks; ?></td>
        <td style="background-color: #DDD"><?php echo $order->status; ?></td>
        <td style="background-color: #DDD">
		<?php
            
                echo form_button(array(
                    'name'=>'submit',
                    'onclick'=>'edit_order('.$order->order_id.')',
                    'content'=>($order->order_status == 0) ? 'Edit' : 'View',
                    'class'=>'DTTT_button float_left')
                );
        ?>
        &emsp;
        <?php
            if($order->order_status == 0):
                echo form_button(array(
                    'name'=>'submit',
                    'onclick'=>'delete_order('.$order->order_id.')',
                    'content'=>'Delete',
                    'class'=>'DTTT_button float_left')
                );
            endif;
        ?>
        </td>
        </tr>
<?php
	endforeach;
endif;
?>

<?php
if($processed_orders->num_rows() > 0):
    foreach($processed_orders->result() as $order):
        $order = $this->Order->get_info($order->order_id);
        switch ($order->order_status) {
            case 0:
                $order->status = 'Pending';
                break;
            case 1:
                $order->status = 'Rejected on '.date('j/n/Y g:i a',strtotime($order->service_time));
                break;
            case 2:
                $order->status = 'Approved on '.date('j/n/Y g:i a',strtotime($order->service_time));
                break;
            default:
                $order->status = 'Pending';
                break;
        }
?>
        <tr>
        <td style="background-color: #DDD" align="right"><?php echo $order->order_id; ?></td>
        <td style="background-color: #DDD"><?php echo date('j/n/Y g:i a',strtotime($order->order_time)); ?></td>
        <td style="background-color: #DDD"><?php echo $order->remarks; ?></td>
        <td style="background-color: #DDD"><?php echo $order->status; ?></td>
        <td style="background-color: #DDD">
        <?php
            
                echo form_button(array(
                    'name'=>'submit',
                    'onclick'=>'edit_order('.$order->order_id.')',
                    'content'=>($order->order_status == 0) ? 'Edit' : 'View',
                    'class'=>'DTTT_button float_left')
                );
        ?>
        &emsp;
        <?php
            if($order->order_status == 0):
                echo form_button(array(
                    'name'=>'submit',
                    'onclick'=>'delete_order('.$order->order_id.')',
                    'content'=>'Delete',
                    'class'=>'DTTT_button float_left')
                );
            endif;
        ?>
        </td>
        </tr>
<?php
    endforeach;
endif;
?>
	</tbody>
</table>
</div>

<div id="order_form_area"></div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
    $('#orders_table').dataTable({
        //"bPaginate": false,
        //"bLengthChange": false,
        //"bFilter": false,
        "bSort": false,
        //"bInfo": false,
        "bStateSave": true,
    });

    $("#new_order_button").click(function(){
        $("#order_form_area").css('display','block');
        $("#order_form_area").load('<?php echo site_url("orders/view_order"); ?>');
        $('#orders').css('display','none');
    });
});

function edit_order(order_id)
{
    $("#order_form_area").css('display','block');
    $("#order_form_area").load('<?php echo site_url("orders/view_order"); ?>/' + order_id);
    $('#orders').css('display','none');
}

function delete_order(order_id)
{
    if(confirm('Are you sure you want to delete this order?'))
    {
        $.post('<?php echo site_url("orders/delete_order"); ?>/' + order_id,
            {},
            function(response){
                if(response.success)
                {
                    $("#TB_ajaxContent").load('<?php echo site_url("orders/view_all"); ?>',
                        function(result){
                            $("#message_bar").removeClass('error_message');
                            $("#message_bar").removeClass('warning_message');
                            $("#message_bar").removeClass('success_message');
                            $("#message_bar").addClass(response.message_class);
                            $("#message_bar").html(response.message);
                            $('#message_bar').fadeTo(5000, 1);
                            $('#message_bar').fadeTo("fast",0);
                        });
                }
                else
                {
                    $("#message_bar").removeClass('error_message');
                    $("#message_bar").removeClass('warning_message');
                    $("#message_bar").removeClass('success_message');
                    $("#message_bar").addClass(response.message_class);
                    $("#message_bar").html(response.message);
                    $('#message_bar').fadeTo(5000, 1);
                    $('#message_bar').fadeTo("fast",0);
                }
            },'json');
    }
}
</script>