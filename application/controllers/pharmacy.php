<?php
require_once ("secure_area.php");
class Pharmacy extends Secure_area
{
	function __construct()
	{
		parent::__construct('pharmacy');
		//error_reporting(E_ALL);
		//$this->output->enable_profiler(TRUE);
	}

	function index()
	{
		$this->load->view('pharmacy/main');
	}

	function view_all()
	{
		$data['main_queue'] = $this->Pharm->get_main_queue();
		$data['pending_queue'] = $this->Pharm->get_pending_queue();

		$this->load->view('pharmacy/patients_table',$data);
	}

	function view_drugs_list()
	{
		$data['drugs_list'] = $this->Pharm->drugs_list();

		$this->load->view('pharmacy/drugs_list',$data);
	}

	function view_drug($drug_id = -1)
	{
		if($drug_id) $data['drug'] = $this->Pharm->get_drug_info($drug_id);
		$suppliers = array('' => '');
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$row['person_id']] = $row['company_name'] ;
		}

		$data['suppliers']=$suppliers;

		$this->load->view('pharmacy/drug_form',$data);
	}

	function save_drug($drug_id = null)
	{
		$this->form_validation->set_rules('brand_name', 'Brand Name', 'required');
		$this->form_validation->set_rules('unit_price', 'Unit Price', 'required|numeric');
		$this->form_validation->set_rules('cost_price', 'Cost Price', 'numeric');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$drug_data = array(
			'generic_name'=>$this->input->post('generic_name'),
			'brand_name'=>$this->input->post('brand_name'),
			'class'=>$this->input->post('class'),
			'strength'=>$this->input->post('strength')
			);
			$item_data = array(
			'name'=>$this->input->post('generic_name') ? $this->input->post('brand_name').'('.$this->input->post('generic_name').')' : $this->input->post('brand_name'),
			'category'=>'Pharmacy',
			'sub_category'=>$this->input->post('class'),
			'supplier_id'=>$this->input->post('supplier_id') ? $this->input->post('supplier_id') : null,
			'quantifiable'=>1,
			'unit_price'=>$this->input->post('unit_price'),
			'cost_price'=>$this->input->post('cost_price'),
			);

			$drug_id = $this->Pharm->save_drug($drug_data,$item_data,$drug_id);
			if($drug_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Drug information saved successfully.','message_class'=>'success_message','drug_id'=>$drug_id));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save drug information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function save_inventory($item_id)
	{	
		$this->form_validation->set_rules('quantity', 'Qty to add/subtract', 'required|integer');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
			$cur_item_info = $this->Item->get_info($item_id);
			
			$item_data = array(
			'quantity'=>$cur_item_info->quantity + $this->input->post('quantity')
			);
			$item_id = $this->Item->save($item_data,$item_id);
			if($item_id):
				$inv_data = array
				(
					'trans_date'=>date('Y-m-d H:i:s'),
					'trans_items'=>$item_id,
					'trans_user'=>$employee_id,
					'trans_comment'=>trim($this->input->post('trans_comment')) ? $this->input->post('trans_comment') : 'Manual edit of quantity.',
					'trans_inventory'=>$this->input->post('quantity')
				);
				$this->Inventory->insert($inv_data);
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Drug information saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save drug information.','message_class'=>'error_message'));
			endif;
		endif;

	}

	function inventory_trail($item_id=-1)
	{
		$data['item_info']=$this->Item->get_info($item_id);
		$this->load->view("pharmacy/inventory_trail",$data);
	}

	function suggest_class()
	{
		$suggestions = $this->Pharm->get_class_suggestions($this->input->post('q'));
		echo implode("\n",$suggestions);
	}

	function delete_drug($drug_id)
	{
		$drug_id = $this->Pharm->delete_drug($drug_id);
		if($drug_id):
			echo json_encode(array('success'=>true,'message'=>'Drug deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('success'=>false,'message'=>'Unable to delete drug.','message_class'=>'error_message'));
		endif;
	}

	function view_archive()
	{
		$date = ($this->session->userdata('prescription_archive_date')) ? $this->session->userdata('prescription_archive_date') : array("start"=>date("Y-m-d 00:00:00"),"end"=>date("Y-m-d 23:59:59"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));
		$data['prescriptions'] = $this->Pharm->archive_prescriptions($start_date,$end_date);
		$data["start_date"] = date("j/n/Y",strtotime($date["start"]));
		$data["end_date"] = date("j/n/Y",strtotime($date["end"]));
		
		$data["start_date_input"] = date("d-m-Y",strtotime($date["start"]));
		$data["end_date_input"] = date("d-m-Y",strtotime($date["end"]));

		$this->load->view("pharmacy/archive",$data);
	}

	function archive_date_input()
	{
		$this->form_validation->set_rules('start_date', 'Start Date', 'callback_date_check|required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$start_date = isset($start_date) ? date("Y-m-d H:i:s",strtotime($start_date)) : date("Y-m-d H:i:s");
			$end_date = isset($end_date) ? date("Y-m-d H:i:s",strtotime($end_date)) : $start_date;
			$date = array("start"=>$start_date,"end"=>$end_date);
			
			$this->session->set_userdata('prescription_archive_date', $date);
			echo json_encode(array('form_validation'=>true));
		endif;
	}

	public function date_check()
	{
		$start_date = strtotime($this->input->post('start_date'));
		$end_date = strtotime($this->input->post('end_date'));
		if ($start_date > $end_date):
			$this->form_validation->set_message('date_check', 'Start date cannot be later than end date');
			return FALSE;
		else:
			return TRUE;
		endif;
	}

	function view_prescription($invoice_id)
	{
		$data['prescription'] = $this->Pharm->get_prescription($invoice_id);

		$this->load->view('pharmacy/prescription',$data);
	}

	function new_prescription()
	{
		$this->session->unset_userdata('new_prescription');
		$this->load->view('pharmacy/new_prescription_form');
	}

	function drug_search()
	{
		$suggestions = $this->Pharm->drug_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function add_drug()
	{
		$drug_id = $this->input->post("item");
		$drug_info = $this->Pharm->get_drug_info($drug_id);
		$prescription = $this->session->userdata('new_prescription');

		if($drug_info):
			if(array_key_exists($drug_info->drug_id, $prescription)):
				echo json_encode(array('success'=>false,'message_class'=>'warning_message','message'=>"Drug already added"));
			elseif($drug_info->item_info->quantity <= 0):
				echo json_encode(array('success'=>false,'message_class'=>'warning_message','message'=>"Drug is out of stock"));
			else:
				$prescription[$drug_info->drug_id] = array(
					'drug_id'=>$drug_info->drug_id,
					'name'=>$drug_info->item_info->name,
					'strength'=>$drug_info->strength,
					'remarks'=>'',
					'dose'=>'',
					'frequency'=>'',
					'duration'=>'',
					'quantity'=>1,
					'discount'=>0,
					'unit_price'=>$drug_info->item_info->unit_price,
					'stock'=>$drug_info->item_info->quantity,
					'item_id'=>$this->Item->get_info('PHARM'.$drug_info->drug_id)->item_id,
					'status'=>0,
					'invoice_status'=>0,
					);
				$this->session->set_userdata('new_prescription',$prescription);
				echo json_encode(array('success'=>true,'message_class'=>'success_message','message'=>"Drug added successfully"));
			endif;
		else:
			echo json_encode(array('success'=>false,'message_class'=>'error_message','message'=>"Please select a valid drug from the list of suggestions"));
		endif;
	}

	function remove_drug($drug_id)
	{
		$prescription = $this->session->userdata('new_prescription');
		unset($prescription[$drug_id]);
		$this->session->set_userdata('new_prescription',$prescription);

		$data['drugs'] = $prescription;
		$this->load->view("pharmacy/new_prescription",$data);
	}

	function drug_instructions($drug_id,$var)
	{
		$prescription = $this->session->userdata('new_prescription');
		$prescription[$drug_id][$var] = $this->input->post('value');

		$this->session->set_userdata('new_prescription',$prescription);

		if($var == 'quantity'):
			$prescription[$drug_id][$var] = is_numeric($this->input->post('value')) && $this->input->post('value') > 0 ? (int)$this->input->post('value') : 1;
		else:
			$prescription[$drug_id][$var] = $this->input->post('value');
		endif;

		$this->session->set_userdata('new_prescription',$prescription);
		$quantity = $prescription[$drug_id]['quantity'];
		$unit_price = $prescription[$drug_id]['unit_price'];
		$discount = $prescription[$drug_id]['discount'];
		echo number_format(($quantity * $unit_price * (100 - $discount) / 100),2,'.','');
	}

	function refresh_prescription()
	{
		$data['drugs'] = $this->session->userdata('new_prescription');
		$this->load->view("pharmacy/new_prescription",$data);
	}

	function patient_search()
	{
		$suggestions = $this->Patient->get_patient_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function refresh_patient_info($patient_id = null)
	{
		$new_prescription = $this->session->userdata('new_prescription') ? $this->session->userdata('new_prescription') : array();
		$drugs = $patient_id ? $this->Pharm->get_invoice_drugs($patient_id) : array();
		foreach($drugs as $drug):
			$drug_id = $this->Pharm->get_drug_id($drug->item_id);
			$drug_info = $this->Pharm->get_drug_info($drug_id);
			$drug->instructions = unserialize($drug->instructions);
			$new_prescription[$drug_id] = array(
				'drug_id'=>$drug_id,
				'name'=>$drug_info->item_info->name,
				'strength'=>$drug_info->strength,
				'remarks'=>$drug->remarks,
				'dose'=>$drug->instructions['dose'],
				'frequency'=>$drug->instructions['frequency'],
				'duration'=>$drug->instructions['duration'],
				'quantity'=>$drug->quantity,
				'discount'=>$drug->discount,
				'unit_price'=>$drug_info->item_info->unit_price,
				'item_id'=>$drug->item_id,
				'status'=>$drug->status,
				'invoice_id'=>$drug->invoice_id,
				'invoice_status'=>$drug->invoice_status,
				);
		endforeach;

		$this->session->set_userdata('new_prescription',$new_prescription);

		$data['patient_id'] = $patient_id;
		$this->load->view("pharmacy/patient_info",$data);
	}

	function new_patient()
	{
		$this->load->view('pharmacy/new_patient');
	}

	function save_patient()
	{
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('email', 'Email', 'valid_email');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$patient_data = array(
			'first_name'=>$this->input->post('first_name'),
			'last_name'=>$this->input->post('last_name'),
			'middle_name'=>$this->input->post('middle_name'),
			'gender'=>$this->input->post('gender'),
			'residence'=>$this->input->post('residence'),
			'national_id'=>$this->input->post('national_id'),
			'dob'=>$this->input->post('dob'),
			'phone_number'=>$this->input->post('phone_number'),
			'email'=>$this->input->post('email'),
			'address_1'=>$this->input->post('address_1'),
			'address_2'=>$this->input->post('address_2'),
			'city'=>$this->input->post('city'),
			'state'=>$this->input->post('state'),
			'zip'=>$this->input->post('zip'),
			'country'=>$this->input->post('country'),
			);

			$patient_id = $this->Patient->save($patient_data);
			if($patient_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Patient information saved successfully.','message_class'=>'success_message','patient_id'=>$patient_id));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save patient information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function save_prescription($patient_id)
	{
		$drugs = $this->session->userdata('new_prescription');
		$employee = $this->Employee->get_logged_in_employee_info();
		$invoice_data = array();
		$invoice_item_data = array();
		$invoice_id = $this->Invoice->already_invoiced($patient_id,$employee->person_id,'Pharmacy');

		if(count($drugs) > 0):
			foreach($drugs as $item):
				$invoice_item_data[] = array(
					'remarks'=>$item['remarks'],
					'status'=>$item['status'],
					'item_id'=>$item['item_id'],
					'quantity'=>$item['quantity'],
					'instructions'=>serialize(array('dose'=>$item['dose'],'frequency'=>$item['frequency'],'duration'=>$item['duration'])),
					);
			endforeach;

			$invoice_data = array(
					'patient_id'=>$patient_id,
					'employee_id'=>$employee->person_id,
					'invoice_type'=>'Pharmacy',
					);
			if($this->Pharm->save_prescription($invoice_data,$invoice_item_data,$invoice_id)):
				echo json_encode(array('success'=>true,'message'=>'Prescription saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('success'=>false,'message'=>'Unable to save prescription.','message_class'=>'success_message'));
			endif;
		else:
			echo json_encode(array('success'=>false,'message'=>'No drugs have been selected.','message_class'=>'error_message'));
		endif;
	}

	function dispense($patient_id)
	{
		$this->session->unset_userdata('prescription');
		$prescription = $this->Pharm->get_prescriptions($patient_id);
		$data['consultation_notes'] = $this->Pharm->get_consultation_notes($patient_id);
		$data['patient'] = $this->Patient->get_info($patient_id);

		$data['drugs'] = array();
		foreach($prescription as $drug):
			$drug_id = $this->Pharm->get_drug_id($drug->item_id);
			$drug_info = $this->Pharm->get_drug_info($drug_id);
			$drug->instructions = unserialize($drug->instructions);
			$data['drugs'][$drug_id] = array(
				'drug_id'=>$drug_id,
				'name'=>$drug_info->item_info->name,
				'strength'=>$drug_info->strength,
				'remarks'=>$drug->remarks,
				'dose'=>$drug->instructions['dose'],
				'frequency'=>$drug->instructions['frequency'],
				'duration'=>$drug->instructions['duration'],
				'quantity'=>$drug->quantity,
				'item_id'=>$drug->item_id,
				'status'=>$drug->status,
				'invoice_status'=>$drug->invoice_status,
				'invoice_id'=>$drug->invoice_id,
				'line'=>$drug->line,
				'nursing'=>0,
				);
		endforeach;
		$this->session->set_userdata('prescription',$data['drugs']);

		$this->load->view('pharmacy/dispense_form',$data);
	}

	function dispense_info($drug_id,$var)
	{
		$prescription = $this->session->userdata('prescription');
		$prescription[$drug_id][$var] = $this->input->post('value');

		$this->session->set_userdata('prescription',$prescription);
	}

	function complete_dispense($patient_id)
	{
		$drugs = $this->session->userdata('prescription');
		$employee = $this->Employee->get_logged_in_employee_info();
		$invoice_data = array();
		$invoice_item_data = array();

		$nursing_invoice_data = array(
			'patient_id'=>$patient_id,
			'employee_id'=>$employee->person_id,					
			'invoice_type'=>'Nursing',
			'invoice_status'=>1
			);
		$nursing_invoice_items_data = array();

		if(count($drugs) > 0):
			foreach($drugs as $item):
				if($item['nursing'] == 1):
					$nursing_invoice_items_data[] = array(
						'remarks'=>$item['remarks'],
						'quantity'=>$item['quantity'],
						'status'=>$item['status'],
						'item_id'=>$item['item_id'],
						);
				endif;

				if($item['status'] == 1):
					if(!$invoice_data[$item['invoice_id']]):
						$invoice_data[$item['invoice_id']] = array(
							'invoice_status'=>3,
							'service_time'=>date('Y-m-d H:i:s'),
							'served_by'=>$employee->person_id);
					endif;
					$invoice_item_data[] = array(
						'results' => serialize($item['results']),
						'invoice_id'=> $item['invoice_id'],
						'line'=>$item['line'],
						'item_id'=>$item['item_id'],
						);
				endif;
			endforeach;

			if($this->Pharm->save_dispense($invoice_data,$invoice_item_data)):
				if(!empty($nursing_invoice_items_data)) $this->Invoice->save($nursing_invoice_data,$nursing_invoice_items_data);
				echo json_encode(array('success'=>true,'message'=>'Drugs dispensed successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('success'=>false,'message'=>'Unable to dispense drugs.','message_class'=>'error_message'));
			endif;
		endif;		
	}

	function consultation_summary($patient_id,$consultation_id)
	{
		$patient = $this->Patient->get_info($patient_id);
		$patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
    	if($patient->dob) $patient->age = $this->Patient->patient_age($patient->dob);
    	$patient->patient_no = $this->Patient->patient_number($patient->patient_id);
		$data['patient'] = $patient;

		$consultation_info = $this->Consultation->get_info($consultation_id);
		$data['complaints'] = unserialize($consultation_info->consultation_complaints);
		$data['examination'] = unserialize($consultation_info->consultation_examination);
		$data['obst_gyn'] = unserialize($consultation_info->consultation_obst_gyn);
		$data['family_history'] = unserialize($consultation_info->consultation_family_history);
		$data['medical_history'] = unserialize($consultation_info->consultation_medical_history);
		$data['treatment_plan'] = unserialize($consultation_info->consultation_treatment_plan);

		$employee = $this->Employee->get_info($consultation_info->consultant_id);
		$employee->employee_name = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
		$data['consultant'] = $employee;

		foreach($consultation_info->diagnosis as $diagnosis):
			$data['diagnosis'][$diagnosis->diagnosis_code] = array(
				'diagnosis_code'=>$diagnosis->diagnosis_code,
				'remarks'=>$diagnosis->remarks,
				'description'=>$this->Consultation->get_diagnosis_info($diagnosis->diagnosis_code)->description,
				);
		endforeach;

		foreach($consultation_info->lab_tests as $lab_test):
			$lab_test_id = $this->Lab->get_lab_test_id($lab_test->item_id);
			$data['lab_tests'][$lab_test_id] = array(
				'lab_test_id'=>$lab_test_id,
				'value'=>$this->Lab->get_lab_test_info($lab_test_id)->value,
				'remarks'=>$lab_test->remarks,
				'results'=>unserialize($lab_test->results),
				'item_id'=>$lab_test->item_id,
				'status'=>$lab_test->status,
				'invoice_status'=>$lab_test->invoice_status,
				);
		endforeach;

		foreach($consultation_info->prescription as $drug):
			$drug_id = $this->Pharm->get_drug_id($drug->item_id);
			$drug_info = $this->Pharm->get_drug_info($drug_id);
			$drug->instructions = unserialize($drug->instructions);
			$data['prescription'][$drug_id] = array(
				'drug_id'=>$drug_id,
				'name'=>$drug_info->item_info->name,
				'strength'=>$drug_info->strength,
				'remarks'=>$drug->remarks,
				'dose'=>$drug->instructions['dose'],
				'frequency'=>$drug->instructions['frequency'],
				'duration'=>$drug->instructions['duration'],
				'quantity'=>$drug->quantity,
				'item_id'=>$lab_test->item_id,
				'status'=>$lab_test->status,
				'invoice_status'=>$lab_test->invoice_status,
				);
		endforeach;

		$data['consultation_date'] = date('j/n/Y',strtotime($consultation_info->consultation_start));

		$this->load->view("consultant/consultation_summary",$data);
	}

	function view_workload()
	{
		$date = ($this->session->userdata('pharmacy_workload_date')) ? $this->session->userdata('pharmacy_workload_date') : array("start"=>date("Y-m-d 00:00:00"),"end"=>date("Y-m-d 23:59:59"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));

		$employee = $this->Employee->get_logged_in_employee_info();
		$data['employee'] = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);

		$data['hospital_workload'] = $this->Pharm->workload($start_date,$end_date);
		$data['employee_workload'] = $this->Pharm->workload($start_date,$end_date,$employee->person_id);
		$drugs_workload = $this->Pharm->drugs_workload($start_date,$end_date);
		arsort($drugs_workload);
		$data['drugs_workload'] = $drugs_workload;

		$data["start_date"] = date("j/n/Y",strtotime($date["start"]));
		$data["end_date"] = date("j/n/Y",strtotime($date["end"]));
		
		$data["start_date_input"] = date("d-m-Y",strtotime($date["start"]));
		$data["end_date_input"] = date("d-m-Y",strtotime($date["end"]));

		$this->load->view("pharmacy/workload",$data);
	}

	function workload_date_input()
	{
		$this->form_validation->set_rules('start_date', 'Start Date', 'callback_date_check|required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$start_date = isset($start_date) ? date("Y-m-d H:i:s",strtotime($start_date)) : date("Y-m-d H:i:s");
			$end_date = isset($end_date) ? date("Y-m-d H:i:s",strtotime($end_date)) : $start_date;
			$date = array("start"=>$start_date,"end"=>$end_date);
			
			$this->session->set_userdata('pharmacy_workload_date', $date);
			echo json_encode(array('form_validation'=>true));
		endif;
	}
}
?>