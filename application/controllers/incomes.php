<?php
require_once ("secure_area.php");
class Incomes extends Secure_area
{
	function __construct()
	{
		parent::__construct('incomes');
		//$this->output->enable_profiler(TRUE);
		//error_reporting(E_ALL);
	}

	function index()
	{
		$period = $this->session->userdata('incomes_period') ? $this->session->userdata('incomes_period') : date('Y-m-01');
		$data['period'] = date('M Y',strtotime($period));
		$data['month_input'] = date('m/Y',strtotime($period));
		$this->load->view('incomes/main',$data);
	}

	function view_all()
	{
		$period = $this->session->userdata('incomes_period') ? $this->session->userdata('incomes_period') : date('Y-m-01');
		$chapter = $this->input->post('chapter');
		$category = $this->input->post('category');
		$subcategory = $this->input->post('subcategory');		
		
		$data['incomes'] = $this->Income->get_all($period,$chapter,$category,$subcategory);
		$data['period'] = date('M Y',strtotime($period));
		$data['chapter'] = $chapter;
		$data['category'] = $category;
		$data['subcategory'] = $subcategory;

		$this->load->view('incomes/incomes_table',$data);
	}
	
	function new_income()
	{
		$this->load->view('incomes/income_form');	
	}

	function view_income($income_id)
	{
		$data['income_info'] = $this->Income->get_info($income_id);

		$this->load->view('incomes/income_form',$data);
	}
	
	function save_income($income_id=null)
	{
		$this->form_validation->set_rules('income_no', 'Income #', 'callback_income_no_check['.$income_id.']');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_rules('chapter', 'Chapter', 'required');
		$this->form_validation->set_rules('amount', date('M Y').' Revenue', 'required|is_numeric');
		if($this->input->post('subcategory')) $this->form_validation->set_rules('category', 'Category', 'required');
		
		if($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$income_data = array(
				'income_no'=>$this->input->post('income_no'),
				'description'=>$this->input->post('description'),
				'chapter'=>$this->input->post('chapter'),
				'category'=>$this->input->post('category'),
				'subcategory'=>$this->input->post('subcategory'),
			);

			$revenue_data = array(
				'amount'=>$this->input->post('amount'),
				'period'=>date('Y-m-01'),
			);
			
			$income_id = $this->Income->save($income_data,$revenue_data,$income_id);
			if($income_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Income information saved successfully.','message_class'=>'success_message','income_id'=>$income_id));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save income information.','message_class'=>'error_message'));
			endif;
		endif;		
	}

	public function income_no_check($income_no,$income_id)
	{
		if ($this->Income->income_no_exists($income_no,$income_id)):
			$this->form_validation->set_message('income_no_check', 'Income # already exists');
			return FALSE;
		else:
			return TRUE;
		endif;
	}

	function delete_income($income_id)
	{
		$income_id = $this->Income->delete($income_id);
		if($income_id):
			echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Income information deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to delete income information.','message_class'=>'error_message'));
		endif;
	}

	function incomes_date_input()
	{
		$this->form_validation->set_rules('month', 'Month', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$month = '01/'.$this->input->post('month');
			$month = preg_replace('/[\/]/s', '-',$month);
			$period = isset($month) ? date("Y-m-01",strtotime($month)) : date("Y-m-01");
			
			$this->session->set_userdata('incomes_period', $period);
			echo json_encode(array('form_validation'=>true,'period'=>date('M Y',strtotime($month))));
		endif;
	}

	function suggest_category()
	{
		$suggestions = $this->Income->get_category_suggestions($this->input->post('q'));
		echo implode("\n",$suggestions);
	}

	function suggest_subcategory()
	{
		$suggestions = $this->Income->get_subcategory_suggestions($this->input->post('q'));
		echo implode("\n",$suggestions);
	}

	function view_chapters()
	{
		$data['chapters'] = $this->Income->get_chapters();

		$this->load->view('incomes/chapters_table',$data);
	}

	function chapter_info($chapter_id = null)
	{
		if($chapter_id) $data['chapter_info'] = $this->Income->get_chapter_info($chapter_id);

		$this->load->view('incomes/chapter_form',$data);
	}

	function save_chapter($chapter_id=null)
	{
		$this->form_validation->set_rules('chapter_no', 'Chapter #', "required|callback_chapter_no_check[$chapter_id]");
		$this->form_validation->set_rules('description', 'Description', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$chapter_data = array(
				'chapter_no'=>$this->input->post('chapter_no'),
				'description'=>$this->input->post('description'),
				'source'=>$this->input->post('source'),
				'type'=>'income',
			);
			
			$chapter_id = $this->Income->save_chapter($chapter_data,$chapter_id);
			if($chapter_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Chapter information saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save chapter information.','message_class'=>'error_message'));
			endif;
		endif;		
	}

	function delete_chapter($chapter_id)
	{
		$chapter_id = $this->Income->delete_chapter($chapter_id);
		if($chapter_id):
			echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Chapter information deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to delete chapter information.','message_class'=>'error_message'));
		endif;
	}

	public function chapter_no_check($chapter_no,$chapter_id)
	{
		if ($this->Income->chapter_no_exists($chapter_no,$chapter_id)):
			$this->form_validation->set_message('chapter_no_check', 'Chapter # already exists');
			return FALSE;
		else:
			return TRUE;
		endif;
	}
}
?>
