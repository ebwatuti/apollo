<?php
require_once ("secure_area.php");
class Consultant extends Secure_area
{
	function __construct($module = 'consultant', $clinic = 'clinic1')
	{
		parent::__construct($module);
		$this->session->set_userdata('clinic',$clinic);
		//error_reporting(E_ALL);
		//$this->output->enable_profiler(TRUE);
	}

	function index()
	{
		$this->load->view('consultant/main');
	}

	function view_consultation($patient_id)
	{
		$this->clear_consultation();

		$employee = $this->Employee->get_logged_in_employee_info();
		$employee->employee_name = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
		$employee_id = $employee->person_id;
    	$clinic = $this->session->userdata('clinic');
    	$data['clinic'] = $this->session->userdata('clinic');
    	$encounter_id = $this->Encounter->check_encounter($patient_id);
		$consultation_id = $this->Consultation->returning($patient_id,$employee_id,$clinic)->consultation_id;
		if($consultation_id) $data['consultation_info'] = $this->Consultation->get_info($consultation_id);

		$patient_liability = 100;
		$institution = $this->Institution->get_patient_institution($patient_id);
		if($institution):
			$patient_liability = 100 - ($institution->institution_discount + $institution->institution_liability);
		endif;

		$data['consultant'] = $employee;
		$data['patient'] = $this->Patient->get_info($patient_id);
		$data['consultation_history'] = $this->Consultation->consultation_history($patient_id);
		$data['triage_history'] = $this->Triaging->get_info($encounter_id);
		$data['all_lab_tests'] = $this->Lab->lab_tests();
		if($consultation_id):
			$consultation_info = $this->Consultation->get_info($consultation_id);
			$data['complaints'] = unserialize($consultation_info->consultation_complaints);
			$data['examination'] = unserialize($consultation_info->consultation_examination);
			$data['obst_gyn'] = unserialize($consultation_info->consultation_obst_gyn);
			$data['family_history'] = unserialize($consultation_info->consultation_family_history);
			$data['medical_history'] = unserialize($consultation_info->consultation_medical_history);
			$data['treatment_plan'] = unserialize($consultation_info->consultation_treatment_plan);
			$data['other_sections'] = unserialize($consultation_info->consultation_other_sections);

			foreach($consultation_info->diagnosis as $diagnosis):
				$data['diagnosis'][$diagnosis->diagnosis_code] = array(
					'diagnosis_code'=>$diagnosis->diagnosis_code,
					'remarks'=>$diagnosis->remarks,
					'description'=>$this->Consultation->get_diagnosis_info($diagnosis->diagnosis_code)->description,
					);
			endforeach;

			foreach($consultation_info->lab_tests as $lab_test):
				$lab_test_id = $this->Lab->get_lab_test_id($lab_test->item_id);
				$data['lab_tests'][$lab_test_id] = array(
					'lab_test_id'=>$lab_test_id,
					'value'=>$this->Lab->get_lab_test_info($lab_test_id)->value,
					'remarks'=>$lab_test->remarks,
					'results'=>unserialize($lab_test->results),
					'item_id'=>$lab_test->item_id,
					'status'=>($patient_liability == 0 && $lab_test->invoice_status == 1) ? 0 : $lab_test->status,
					'invoice_status'=>($patient_liability == 0 && $lab_test->invoice_status == 1) ? 0 : $lab_test->invoice_status,
					);
			endforeach;

			foreach($consultation_info->prescription as $drug):
				$drug_id = $this->Pharm->get_drug_id($drug->item_id);
				$drug_info = $this->Pharm->get_drug_info($drug_id);
				$drug->instructions = unserialize($drug->instructions);
				$data['prescription'][$drug_id] = array(
					'drug_id'=>$drug_id,
					'name'=>$drug_info->item_info->name,
					'strength'=>$drug_info->strength,
					'remarks'=>$drug->remarks,
					'dose'=>$drug->instructions['dose'],
					'frequency'=>$drug->instructions['frequency'],
					'duration'=>$drug->instructions['duration'],
					'quantity'=>$drug->quantity,
					'item_id'=>$drug->item_id,
					'status'=>($patient_liability == 0 && $drug->invoice_status == 1) ? 0 : $drug->status,
					'invoice_status'=>($patient_liability == 0 && $drug->invoice_status == 1) ? 0 : $drug->invoice_status,
					);
			endforeach;

			foreach($consultation_info->procedures as $procedure):
				$procedure_id = $this->Consultation->get_procedure_id($procedure->item_id);
				$procedure_info = $this->Consultation->get_procedure_info($procedure_id);
				$data['procedures'][$procedure_id] = array(
					'procedure_id'=>$procedure_id,
					'name'=>$procedure_info->name,
					'clinic'=>$procedure_info->clinic,
					'remarks'=>$procedure->remarks,
					'results'=>$procedure->results,
					'item_id'=>$procedure->item_id,
					'quantity'=>$procedure->quantity,
					'discount'=>$procedure->discount,
					'unit_price'=>$procedure->unit_price,
					'original_unit_price'=>$procedure_info->item_info->unit_price,
					'status'=>($patient_liability == 0 && $procedure->invoice_status == 1) ? 0 : $procedure->status,
					'invoice_status'=>($patient_liability == 0 && $procedure->invoice_status == 1) ? 0 : $procedure->invoice_status,
					);
			endforeach;

			$this->session->set_userdata('complaints',$data['complaints']);
			$this->session->set_userdata('examination',$data['examination']);
			$this->session->set_userdata('obst_gyn',$data['obst_gyn']);
			$this->session->set_userdata('family_history',$data['family_history']);
			$this->session->set_userdata('medical_history',$data['medical_history']);
			$this->session->set_userdata('treatment_plan',$data['treatment_plan']);
			$this->session->set_userdata('diagnosis',$data['diagnosis']);
			$this->session->set_userdata('lab_tests',$data['lab_tests']);
			$this->session->set_userdata('prescription',$data['prescription']);
			$this->session->set_userdata('procedures',$data['procedures']);
			$this->session->set_userdata('other_sections',$data['other_sections']);
		endif;

		$this->load->view('consultant/consultation_form',$data);
	}

	function save($consultation_status, $encounter_status)
	{
		$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
		$patient_id = $this->input->post('patient_id');
		$encounter_id = $this->Encounter->check_encounter($patient_id);

		$consultation_data = array(
			'patient_id'=>$patient_id,
			'consultant_id'=>$employee_id,
			'consultation_complaints'=>serialize($this->session->userdata('complaints')),
			'consultation_obst_gyn'=>serialize($this->session->userdata('obst_gyn')),
			'consultation_medical_history'=>serialize($this->session->userdata('medical_history')),
			'consultation_family_history'=>serialize($this->session->userdata('family_history')),
			'consultation_examination'=>serialize($this->session->userdata('examination')),
			'consultation_treatment_plan'=>serialize($this->session->userdata('treatment_plan')),
			'consultation_other_sections'=>serialize($this->session->userdata('other_sections')),
			'consultation_status'=>$consultation_status,
			'consultation_type'=>$this->session->userdata('clinic'),
			'consultation_end'=>date('Y-m-d H:i:s'),
		);

		$diagnosis = $this->session->userdata('diagnosis');
		$diagnosis_data = array();

		foreach($diagnosis as $item):
			$diagnosis_data[] = array(
				'diagnosis_code' => $item['diagnosis_code'],
				'remarks'=> $item['remarks']
				);
		endforeach;

		$lab_tests = $this->session->userdata('lab_tests');
		$lab_test_data = array();
		$lab_request_data = array();

		if(!empty($lab_tests)):
			$consultation_data['lab_request'] = 1;
			foreach($lab_tests as $item):
				if($item['status'] == 0 && $item['invoice_status'] == 0):
					$lab_test_data[] = array(
						'item_id' => $item['item_id'],
						'remarks'=> $item['remarks'],						
						'status'=>$item['status'],
						);
				endif;
			endforeach;

			if(!empty($lab_test_data)):
				$lab_request_data = array(
					'invoice_id'=>$this->Invoice->already_invoiced($patient_id,$employee_id,'Lab'),
					'invoice_data'=>array(
						'patient_id'=>$patient_id,
						'employee_id'=>$employee_id,
						'encounter_id'=>$encounter_id,
						'invoice_type'=>'Lab',
						),
					'invoice_item_data'=>$lab_test_data,
					);
			endif;
		endif;

		$prescription = $this->session->userdata('prescription');
		$drug_data = array();
		$prescription_data = array();

		if(!empty($prescription)):
			foreach($prescription as $item):
				if($item['status'] == 0 && $item['invoice_status'] == 0):
					$drug_data[] = array(
						'remarks'=>$item['remarks'],
						'status'=>$item['status'],
						'item_id'=>$item['item_id'],
						'quantity'=>$item['quantity'],
						'instructions'=>serialize(array('dose'=>$item['dose'],'frequency'=>$item['frequency'],'duration'=>$item['duration'])),
						);
				endif;
			endforeach;

			if(!empty($drug_data)):
				$prescription_data = array(
					'invoice_id'=>$this->Invoice->already_invoiced($patient_id,$employee_id,'Pharmacy'),
					'invoice_data'=>array(
						'patient_id'=>$patient_id,
						'employee_id'=>$employee_id,
						'encounter_id'=>$encounter_id,
						'invoice_type'=>'Pharmacy',
						),
					'invoice_item_data'=>$drug_data,
					);
			endif;
		endif;

		$procedures = $this->session->userdata('procedures');
		$procedure_data = array();
		$procedures_data = array();
		$nursing_procedure_data = array();
		$nursing_data = array();

		if(!empty($procedures)):
			foreach($procedures as $item):
				if($item['status'] == 0 && $item['invoice_status'] == 0):
					if($item['clinic'] == 'nursing'):
						$nursing_procedure_data[] = array(
							'remarks'=>$item['remarks'],
							'results'=>$item['results'],
							'status'=>$item['status'],
							'item_id'=>$item['item_id'],
							'quantity'=>$item['quantity'],
							'unit_price'=>$item['unit_price'],
							);
					else:
						$procedure_data[] = array(
							'remarks'=>$item['remarks'],
							'results'=>$item['results'],
							'status'=>$item['status'],
							'item_id'=>$item['item_id'],
							'quantity'=>$item['quantity'],
							'unit_price'=>$item['unit_price'],
							);
					endif;
				endif;
			endforeach;

			if(!empty($procedure_data)):
				$procedures_data = array(
					'invoice_id'=>$this->Invoice->already_invoiced($patient_id,$employee_id,$this->Consultation->clinic_name($this->session->userdata('clinic')).' Procedures'),
					'invoice_data'=>array(
						'patient_id'=>$patient_id,
						'employee_id'=>$employee_id,
						'encounter_id'=>$encounter_id,
						'invoice_type'=>$this->Consultation->clinic_name($this->session->userdata('clinic')).' Procedures',
						'service_time'=>date('Y-m-d H:i:s'),
						),
					'invoice_item_data'=>$procedure_data,
					);
			endif;
			if(!empty($nursing_procedure_data)):
				$nursing_data = array(
					'invoice_id'=>$this->Invoice->already_invoiced($patient_id,$employee_id,'Nursing'),
					'invoice_data'=>array(
						'patient_id'=>$patient_id,
						'employee_id'=>$employee_id,
						'encounter_id'=>$encounter_id,
						'invoice_type'=>'Nursing',
						),
					'invoice_item_data'=>$nursing_procedure_data,
					);
			endif;
		endif;

		$consultation_id = $this->Consultation->returning($patient_id,$employee_id,$this->session->userdata('clinic'))->consultation_id;

		return $this->Consultation->save($consultation_data,$diagnosis_data,$lab_request_data,$prescription_data,$procedures_data,$nursing_data,$encounter_status,$patient_id,$consultation_id);
	}

	function save_consultation()
	{
		$this->form_validation->set_rules('patient_id', 'Patient', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$consultation_status = 1;
			$encounter_status = 3;
			$consultation_id = $this->save($consultation_status, $encounter_status);

			if($consultation_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Consultation information saved successfully.','message_class'=>'success_message','patient_id'=>$patient_id));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save consultation information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function discharge()
	{
		$this->form_validation->set_rules('patient_id', 'Patient', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$consultation_status = 3;
			$encounter_status = 5;
			$consultation_id = $this->save($consultation_status, $encounter_status);

			if($consultation_id):
				$this->Consultation->discharge($consultation_id);
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Consultation information saved successfully.','message_class'=>'success_message','patient_id'=>$patient_id));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save consultation information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function consultation_summary($patient_id,$consultation_id)
	{
		$patient = $this->Patient->get_info($patient_id);
		$patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
    	if($patient->dob) $patient->age = $this->Patient->patient_age($patient->dob);
    	$patient->patient_no = $this->Patient->patient_number($patient->patient_id);
		$data['patient'] = $patient;

		$consultation_info = $this->Consultation->get_info($consultation_id);
		$data['complaints'] = unserialize($consultation_info->consultation_complaints);
		$data['examination'] = unserialize($consultation_info->consultation_examination);
		$data['obst_gyn'] = unserialize($consultation_info->consultation_obst_gyn);
		$data['family_history'] = unserialize($consultation_info->consultation_family_history);
		$data['medical_history'] = unserialize($consultation_info->consultation_medical_history);
		$data['treatment_plan'] = unserialize($consultation_info->consultation_treatment_plan);
		$data['other_sections'] = unserialize($consultation_info->consultation_other_sections);

		$employee = $this->Employee->get_info($consultation_info->consultant_id);
		$employee->employee_name = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
		$data['consultant'] = $employee;

		foreach($consultation_info->diagnosis as $diagnosis):
			$data['diagnosis'][$diagnosis->diagnosis_code] = array(
				'diagnosis_code'=>$diagnosis->diagnosis_code,
				'remarks'=>$diagnosis->remarks,
				'description'=>$this->Consultation->get_diagnosis_info($diagnosis->diagnosis_code)->description,
				);
		endforeach;

		foreach($consultation_info->lab_tests as $lab_test):
			$lab_test_id = $this->Lab->get_lab_test_id($lab_test->item_id);
			$data['lab_tests'][$lab_test_id] = array(
				'lab_test_id'=>$lab_test_id,
				'value'=>$this->Lab->get_lab_test_info($lab_test_id)->value,
				'remarks'=>$lab_test->remarks,
				'results'=>unserialize($lab_test->results),
				'item_id'=>$lab_test->item_id,
				'status'=>$lab_test->status,
				'invoice_status'=>$lab_test->invoice_status,
				);
		endforeach;

		foreach($consultation_info->prescription as $drug):
			$drug_id = $this->Pharm->get_drug_id($drug->item_id);
			$drug_info = $this->Pharm->get_drug_info($drug_id);
			$drug->instructions = unserialize($drug->instructions);
			$data['prescription'][$drug_id] = array(
				'drug_id'=>$drug_id,
				'name'=>$drug_info->item_info->name,
				'strength'=>$drug_info->strength,
				'remarks'=>$drug->remarks,
				'dose'=>$drug->instructions['dose'],
				'frequency'=>$drug->instructions['frequency'],
				'duration'=>$drug->instructions['duration'],
				'quantity'=>$drug->quantity,
				'item_id'=>$lab_test->item_id,
				'status'=>$lab_test->status,
				'invoice_status'=>$lab_test->invoice_status,
				);
		endforeach;

		foreach($consultation_info->procedures as $procedure):
			$procedure_id = $this->Consultation->get_procedure_id($procedure->item_id);
			$data['procedures'][$procedure_id] = array(
				'procedure_id'=>$procedure_id,
				'name'=>$this->Consultation->get_procedure_info($procedure_id)->name,
				'remarks'=>$procedure->remarks,
				'results'=>$procedure->results,
				'item_id'=>$procedure->item_id,
				'status'=>($patient_liability == 0 && $procedure->invoice_status == 1) ? 0 : $procedure->status,
				'invoice_status'=>($patient_liability == 0 && $procedure->invoice_status == 1) ? 0 : $procedure->invoice_status,
				);
		endforeach;

		$data['consultation_date'] = date('j/n/Y',strtotime($consultation_info->consultation_start));

		$this->load->view("consultant/consultation_summary",$data);
	}

	function refresh_summary($patient_id)
	{
		$patient = $this->Patient->get_info($patient_id);
		$patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
    	if($patient->dob) $patient->age = $this->Patient->patient_age($patient->dob);
    	$patient->patient_no = $this->Patient->patient_number($patient->patient_id);
		$data['patient'] = $patient;

		$employee = $this->Employee->get_logged_in_employee_info();
		$employee->employee_name = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
		$data['consultant'] = $employee;

		$data['complaints'] = $this->session->userdata('complaints');
		$data['examination'] = $this->session->userdata('examination');
		$data['obst_gyn'] = $this->session->userdata('obst_gyn');
		$data['family_history'] = $this->session->userdata('family_history');
		$data['medical_history'] = $this->session->userdata('medical_history');
		$data['treatment_plan'] = $this->session->userdata('treatment_plan');
		$data['other_sections'] = $this->session->userdata('other_sections');

		$data['diagnosis'] = $this->session->userdata('diagnosis');

		$data['lab_tests'] = $this->session->userdata('lab_tests');
		$data['prescription'] = $this->session->userdata('prescription');
		$data['procedures'] = $this->session->userdata('procedures');

		$this->load->view("consultant/consultation_summary",$data);
	}

	function triage_chart($encounter_id){
		$data['triage_history'] = $this->Triaging->get_info($encounter_id);

		$this->load->view('triage/triage_chart',$data);
	}

	function view_all()
	{
		$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
		$data['main_queue'] = $this->Consultation->get_main_queue($this->session->userdata('clinic'));
		$data['returning_queue'] = $this->Consultation->get_returning_queue($this->session->userdata('clinic'));
		$data['lab_queue'] = $this->Consultation->get_lab_queue($this->session->userdata('clinic'));

		$this->load->view('consultant/patients_table',$data);
	}

	function diagnosis_search()
	{
		$suggestions = $this->Consultation->diagnosis_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function lab_test_search()
	{
		$suggestions = $this->Lab->lab_test_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function drug_search()
	{
		$suggestions = $this->Pharm->drug_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function procedure_search()
	{
		$suggestions = $this->Consultation->procedure_suggestions($this->session->userdata('clinic'),$this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function save_complaints()
	{
		$complaints =  $this->input->post();
				
		$this->session->set_userdata('complaints', $complaints);
	}
	
	function save_obst_gyn()
	{
		$obst_gyn = $this->input->post();
		
		$this->session->set_userdata('obst_gyn', $obst_gyn);
	}
	
	function save_medical_history()
	{
		$medical_history = $this->input->post();
		
		$this->session->set_userdata('medical_history', $medical_history);
	}
	
	function save_family_history()
	{
		$family_history = $this->input->post();
		
		$this->session->set_userdata('family_history', $family_history);
	}
	
	function save_examination()
	{
		$examination = $this->input->post();
		
		$this->session->set_userdata('examination', $examination);
	}
	
	function save_treatment_plan()
	{
		$treatment_plan = $this->input->post();
		
		$this->session->set_userdata('treatment_plan', $treatment_plan);
	}

	function save_other_sections($section)
	{
		$other_sections = $this->session->userdata('other_sections');
		$other_sections[$section] = $this->input->post();
		
		$this->session->set_userdata('other_sections', $other_sections);
	}

	function save_diagnosis()
	{
		$diagnosis = $this->session->userdata('diagnosis');
		$diagnosis[0] = array(
			'diagnosis_code'=>0,
			'remarks'=>$this->input->post('diagnosis'),
			);
		
		$this->session->set_userdata('diagnosis',$diagnosis);
	}

	function add_diagnosis()
	{
		$diagnosis_code = $this->input->post("item");
		$diagnosis_info = $this->Consultation->get_diagnosis_info($diagnosis_code);
		$diagnosis = $this->session->userdata('diagnosis');

		if($diagnosis_info):
			if(array_key_exists($diagnosis_info->diagnosis_code, $diagnosis)):
				echo json_encode(array('success'=>false,'message_class'=>'warning_message','message'=>"Diagnosis already added"));
			else:
				$diagnosis[$diagnosis_info->diagnosis_code] = array(
					'diagnosis_code'=>$diagnosis_info->diagnosis_code,
					'remarks'=>'',
					'description'=>$diagnosis_info->description,
					);
				$this->session->set_userdata('diagnosis',$diagnosis);
				echo json_encode(array('success'=>true,'message_class'=>'success_message','message'=>"Diagnosis added successfully"));
			endif;
		else:
			echo json_encode(array('success'=>false,'message_class'=>'error_message','message'=>"Please select a valid diagnosis from the list of suggestions"));
		endif;
	}

	function delete_diagnosis($diagnosis_code)
	{
		$diagnosis = $this->session->userdata('diagnosis');
		unset($diagnosis[$diagnosis_code]);
		$this->session->set_userdata('diagnosis',$diagnosis);

		$data['diagnosis'] = $diagnosis;
		$this->load->view("consultant/diagnosis",$data);
	}

	function diagnosis_remarks($diagnosis_code)
	{
		$diagnosis = $this->session->userdata('diagnosis');
		$diagnosis[$diagnosis_code]['remarks'] = $this->input->post('remarks');

		$this->session->set_userdata('diagnosis',$diagnosis);
	}

	function refresh_diagnosis()
	{
		$data['diagnosis'] = $this->session->userdata('diagnosis');
		$this->load->view("consultant/diagnosis",$data);
	}

	function add_lab_tests()
	{
		$lab_test_ids = $this->input->post("lab_tests");		
		$session_lab_tests = $this->session->userdata('lab_tests');
		$lab_tests = array();

		foreach($session_lab_tests as $lab_test_id=>$lab_test):
			if($lab_test['status'] != 0 && $lab_test['invoice_status'] != 0):
				$lab_tests[ $lab_test_id ] = $session_lab_tests[ $lab_test_id ];
			endif;
		endforeach;

		foreach($lab_test_ids as $lab_test_id):
			$lab_test_info = $this->Lab->get_lab_test_info($lab_test_id);
			if($lab_test_info && !array_key_exists($lab_test_id, $lab_tests)):
				$lab_tests[$lab_test_info->lab_test_id] = array(
					'lab_test_id'=>$lab_test_info->lab_test_id,
					'value'=>$lab_test_info->value,
					'item_id'=>$this->Item->get_info('LAB'.$lab_test_info->lab_test_id)->item_id,
					'status'=>0,
					'invoice_status'=>0,
					);
			endif;
		endforeach;

		$this->session->set_userdata('lab_tests',$lab_tests);
	}

	function refresh_lab_request()
	{
		$data['all_lab_tests'] = $this->Lab->lab_tests();
		$data['lab_tests'] = $this->session->userdata('lab_tests');
		$this->load->view("consultant/lab_request",$data);
	}

	function add_prescription()
	{
		$drug_id = $this->input->post("item");
		$drug_info = $this->Pharm->get_drug_info($drug_id);
		$prescription = $this->session->userdata('prescription');

		if($drug_info):
			if(array_key_exists($drug_info->drug_id, $prescription)):
				echo json_encode(array('success'=>false,'message_class'=>'warning_message','message'=>"Drug already added"));
			elseif($drug_info->item_info->quantity <= 0):
				echo json_encode(array('success'=>false,'message_class'=>'warning_message','message'=>"Drug is out of stock"));
			else:
				$prescription[$drug_info->drug_id] = array(
					'drug_id'=>$drug_info->drug_id,
					'name'=>$drug_info->item_info->name,
					'strength'=>$drug_info->strength,
					'remarks'=>'',
					'dose'=>'',
					'frequency'=>'',
					'duration'=>'',
					'quantity'=>1,
					'stock'=>$drug_info->item_info->quantity,
					'item_id'=>$this->Item->get_info('PHARM'.$drug_info->drug_id)->item_id,
					'status'=>0,
					'invoice_status'=>0,
					);
				$this->session->set_userdata('prescription',$prescription);
				echo json_encode(array('success'=>true,'message_class'=>'success_message','message'=>"Drug added successfully"));
			endif;
		else:
			echo json_encode(array('success'=>false,'message_class'=>'error_message','message'=>"Please select a valid drug from the list of suggestions"));
		endif;
	}

	function delete_prescription($drug_id)
	{
		$prescription = $this->session->userdata('prescription');
		unset($prescription[$drug_id]);
		$this->session->set_userdata('prescription',$prescription);

		$data['prescription'] = $prescription;
		$this->load->view("consultant/prescription",$data);
	}

	function prescription_info($drug_id,$var)
	{
		$prescription = $this->session->userdata('prescription');
		$prescription[$drug_id][$var] = $this->input->post('value');

		$this->session->set_userdata('prescription',$prescription);
	}

	function refresh_prescription()
	{
		$data['prescription'] = $this->session->userdata('prescription');
		$this->load->view("consultant/prescription",$data);
	}

	function add_procedure()
	{
		$procedure_id = $this->input->post("item");
		$procedure_info = $this->Consultation->get_procedure_info($procedure_id);
		$procedures = $this->session->userdata('procedures');

		if($procedure_info):
			if(array_key_exists($procedure_info->procedure_id, $procedures)):
				echo json_encode(array('success'=>false,'message_class'=>'warning_message','message'=>"Procedure already added"));
			else:
				$procedures[$procedure_info->procedure_id] = array(
					'procedure_id'=>$procedure_info->procedure_id,
					'name'=>$procedure_info->name,
					'clinic'=>$procedure_info->clinic,
					'strength'=>$drug_info->strength,
					'quantity'=>1,
					'discount'=>0,
					'item_id'=>$procedure_info->item_info->item_id,
					'unit_price'=>$procedure_info->item_info->unit_price,
					'status'=>0,
					'invoice_status'=>0,
					);
				$this->session->set_userdata('procedures',$procedures);
				echo json_encode(array('success'=>true,'message_class'=>'success_message','message'=>"Procedure added successfully"));
			endif;
		else:
			echo json_encode(array('success'=>false,'message_class'=>'error_message','message'=>"Please select a valid procedure from the list of suggestions"));
		endif;
	}

	function remove_procedure($procedure_id)
	{
		$procedures = $this->session->userdata('procedures');
		unset($procedures[$procedure_id]);
		$this->session->set_userdata('procedures',$procedures);

		$data['procedures'] = $procedures;
		$this->load->view("consultant/procedures",$data);
	}

	function procedure_info($procedure_id,$var)
	{
		$procedures = $this->session->userdata('procedures');
		if($var == 'quantity'):
			$procedures[$procedure_id][$var] = is_numeric($this->input->post('value')) && $this->input->post('value') > 0 ? (int)$this->input->post('value') : 1;
		elseif($var == 'unit_price'):
			$procedures[$procedure_id][$var] = is_numeric($this->input->post('value')) && $this->input->post('value') >= 0 && $this->input->post('value') <= $procedures[$procedure_id]['original_unit_price'] ? $this->input->post('value') : $procedures[$procedure_id]['original_unit_price'];
		else:
			$procedures[$procedure_id][$var] = $this->input->post('value');
		endif;
		$procedures[$procedure_id][$var] = $this->input->post('value');

		$this->session->set_userdata('procedures',$procedures);
		$quantity = $procedures[$procedure_id]['quantity'];
		$unit_price = $procedures[$procedure_id]['unit_price'];
		$discount = $procedures[$procedure_id]['discount'];
		echo to_currency($quantity * $unit_price * (100 - $discount) / 100);
	}

	function refresh_procedures()
	{
		$data['procedures'] = $this->session->userdata('procedures');
		$this->load->view("consultant/procedures",$data);
	}
	
	function clear_consultation()
    {
		$consultation_info = array(
			'complaints' => '',
			'obst_gyn' => '',
			'medical_history' => '',
			'family_history' => '',
			'examination' => '',
			'treatment_plan' => '',
			'diagnosis'=>'',
			'possible_diagnosis'=>'',
			'lab_tests'=>'',
			'prescription'=>'',
			'procedures'=>'',
			);

		$this->session->unset_userdata($consultation_info);
    }

    function view_archive()
	{
		$date = ($this->session->userdata('consultation_archive_date')) ? $this->session->userdata('consultation_archive_date') : array("start"=>date("Y-m-d 00:00:00"),"end"=>date("Y-m-d 23:59:59"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));
		$data['consultations'] = $this->Consultation->archive_consultations($start_date,$end_date,$this->session->userdata('clinic'));
		$data["start_date"] = date("j/n/Y",strtotime($date["start"]));
		$data["end_date"] = date("j/n/Y",strtotime($date["end"]));
		
		$data["start_date_input"] = date("d-m-Y",strtotime($date["start"]));
		$data["end_date_input"] = date("d-m-Y",strtotime($date["end"]));

		$this->load->view("consultant/archive",$data);
	}

	function archive_date_input()
	{
		$this->form_validation->set_rules('start_date', 'Start Date', 'callback_date_check|required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$start_date = isset($start_date) ? date("Y-m-d H:i:s",strtotime($start_date)) : date("Y-m-d H:i:s");
			$end_date = isset($end_date) ? date("Y-m-d H:i:s",strtotime($end_date)) : $start_date;
			$date = array("start"=>$start_date,"end"=>$end_date);
			
			$this->session->set_userdata('consultation_archive_date', $date);
			echo json_encode(array('form_validation'=>true));
		endif;
	}

	public function date_check()
	{
		$start_date = strtotime($this->input->post('start_date'));
		$end_date = strtotime($this->input->post('end_date'));
		if ($start_date > $end_date):
			$this->form_validation->set_message('date_check', 'Start date cannot be later than end date');
			return FALSE;
		else:
			return TRUE;
		endif;
	}

	function view_workload()
	{
		$date = ($this->session->userdata('consultation_workload_date')) ? $this->session->userdata('consultation_workload_date') : array("start"=>date("Y-m-d 00:00:00"),"end"=>date("Y-m-d 23:59:59"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));

		$clinic = $this->session->userdata('clinic');
		$employee = $this->Employee->get_logged_in_employee_info();
		$data['consultant'] = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
		$data['clinic'] = $this->Consultation->clinic_name($clinic);

		$data['hospital_workload'] = $this->Consultation->workload($start_date,$end_date);
		$where = 'consultant_id = "'.$employee->person_id.'"';
		$data['consultant_workload'] = $this->Consultation->workload($start_date,$end_date,$where);
		$where = 'consultation_type = "'.$clinic.'"';
		$data['clinic_workload'] = $this->Consultation->workload($start_date,$end_date,$where);
		$where = 'consultant_id = "'.$employee->person_id.'" AND consultation_type = "'.$clinic.'"';
		$data['consultant_clinic_workload'] = $this->Consultation->workload($start_date,$end_date,$where);

		$procedures_workload = $this->Consultation->procedures_workload($clinic,$start_date,$end_date);
		arsort($procedures_workload);
		$data['procedures_workload'] = $procedures_workload;

		$data["start_date"] = date("j/n/Y",strtotime($date["start"]));
		$data["end_date"] = date("j/n/Y",strtotime($date["end"]));
		
		$data["start_date_input"] = date("d-m-Y",strtotime($date["start"]));
		$data["end_date_input"] = date("d-m-Y",strtotime($date["end"]));

		$this->load->view("consultant/workload",$data);
	}

	function workload_date_input()
	{
		$this->form_validation->set_rules('start_date', 'Start Date', 'callback_date_check|required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$start_date = isset($start_date) ? date("Y-m-d H:i:s",strtotime($start_date)) : date("Y-m-d H:i:s");
			$end_date = isset($end_date) ? date("Y-m-d H:i:s",strtotime($end_date)) : $start_date;
			$date = array("start"=>$start_date,"end"=>$end_date);
			
			$this->session->set_userdata('consultation_workload_date', $date);
			echo json_encode(array('form_validation'=>true));
		endif;
	}

	function view_morbidity()
	{
		$date = ($this->session->userdata('consultation_morbidity_date')) ? $this->session->userdata('consultation_morbidity_date') : array("start"=>date("Y-m-d 00:00:00"),"end"=>date("Y-m-d 23:59:59"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));

		$clinic = $this->session->userdata('clinic');
		$employee = $this->Employee->get_logged_in_employee_info();
		$data['clinic'] = $this->Consultation->clinic_name($clinic);

		$hospital_morbidity = $this->Consultation->morbidity($start_date,$end_date);
		$data['hospital_morbidity'] = $hospital_morbidity;
		$clinic_morbidity = $this->Consultation->morbidity($start_date,$end_date,$clinic);
		arsort($clinic_morbidity);
		$data['clinic_morbidity'] = $clinic_morbidity;

		$data["start_date"] = date("j/n/Y",strtotime($date["start"]));
		$data["end_date"] = date("j/n/Y",strtotime($date["end"]));
		
		$data["start_date_input"] = date("d-m-Y",strtotime($date["start"]));
		$data["end_date_input"] = date("d-m-Y",strtotime($date["end"]));

		$this->load->view("consultant/morbidity",$data);
	}

	function morbidity_date_input()
	{
		$this->form_validation->set_rules('start_date', 'Start Date', 'callback_date_check|required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$start_date = isset($start_date) ? date("Y-m-d H:i:s",strtotime($start_date)) : date("Y-m-d H:i:s");
			$end_date = isset($end_date) ? date("Y-m-d H:i:s",strtotime($end_date)) : $start_date;
			$date = array("start"=>$start_date,"end"=>$end_date);
			
			$this->session->set_userdata('consultation_morbidity_date', $date);
			echo json_encode(array('form_validation'=>true));
		endif;
	}

	function view_procedures()
	{
		$data['procedures'] = $this->Consultation->get_procedures($this->session->userdata('clinic'));
		$this->load->view('consultant/procedures_table',$data);
	}

	function view_procedure($procedure_id = -1)
	{
		if($procedure_id != -1) $data['procedure_info'] = $this->Consultation->get_procedure_info($procedure_id);

		$this->load->view('consultant/procedure_form',$data);
	}

	function save_procedure($procedure_id = null)
	{
		$this->form_validation->set_rules('procedure_name', 'Procedure Name', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required|numeric');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$procedure_data = array(
			'name'=>$this->input->post('procedure_name'),
			'clinic'=>$this->session->userdata('clinic')
			);
			$item_data = array(
			'name'=>$this->input->post('procedure_name'),
			'category'=>($this->session->userdata('clinic') == 'clinic4') ? 'Dental' : 'Medical Services',
			'sub_category'=>($this->session->userdata('clinic') == 'clinic4') ? '' : $this->Consultation->clinic_name($this->session->userdata('clinic')),
			'quantity'=>1,
			'quantifiable'=>0,
			'unit_price'=>$this->input->post('price'),
			);

			$procedure_id = $this->Consultation->save_procedure($procedure_data,$item_data,$procedure_id);
			if($procedure_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Procedure information saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save procedure information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function delete_procedure($procedure_id)
	{
		$procedure_id = $this->Consultation->delete_procedure($procedure_id);
		if($procedure_id):
			echo json_encode(array('success'=>true,'message'=>'Procedure deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('success'=>false,'message'=>'Unable to delete procedure.','message_class'=>'error_message'));
		endif;
	}

	function view_sections()
	{
		$data['sections'] = $this->Consultation->get_sections($this->session->userdata('clinic'));
		$this->load->view('consultant/sections_table',$data);
	}

	function view_section($section_id = -1)
	{
		if($section_id != -1) $data['section_info'] = $this->Consultation->section_info($section_id,$this->session->userdata('clinic'));

		$this->load->view('consultant/section_form',$data);
	}

	function save_section($section_id = null)
	{
		$this->form_validation->set_rules('section_name', 'Section Name', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$section_data = array(
			'clinic'=>$this->session->userdata('clinic'),
			'section_name'=>$this->input->post('section_name'),
			'section_fields'=>$this->input->post('section_fields') ? $this->input->post('section_fields') : null,
			'displayed'=>$this->input->post('displayed') ? 1 : 0,
			);

			if(!$section_id) $section_data['section_id'] = strtolower(preg_replace('/[^a-zA-Z0-9_]/s', "_",$this->input->post('section_name')));

			$section_id = $this->Consultation->save_section($section_data,$this->session->userdata('clinic'),$section_id);
			if($section_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Section information saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save section information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function delete_section($section_id)
	{
		$section_id = $this->Consultation->delete_section($this->session->userdata('clinic'),$section_id);
		if($section_id):
			echo json_encode(array('success'=>true,'message'=>'Section deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('success'=>false,'message'=>'Unable to delete section.','message_class'=>'error_message'));
		endif;
	}
}
?>