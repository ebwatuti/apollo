<?php
require_once ("secure_area.php");
class Orders extends Secure_area
{
	function __construct()
	{
		parent::__construct();
		//error_reporting(E_ALL);
		//$this->output->enable_profiler(TRUE);
	}

	function index()
	{
		$this->load->view('orders/main');
	}

	function view_all()
	{
		$employee = $this->Employee->get_logged_in_employee_info();
		$data['pending_orders'] = $this->Order->get_pending_orders($employee->person_id);
		$data['processed_orders'] = $this->Order->get_processed_orders($employee->person_id);

		$this->load->view('orders/orders_table',$data);
	}

	function view_order($order_id)
	{
		$this->session->unset_userdata('order_items');
		$employee = $this->Employee->get_logged_in_employee_info();
		if(!$order_id) $order_id = $this->Order->already_ordered($employee->person_id);

		if($order_id) $order = $this->Order->get_info($order_id);
		foreach($order->order_items as $item):
			$item_info = $this->Item->get_info($item->item_id);
			$order_items[$item->item_id] = array(
				'name'=>$item_info->name,
				'quantity'=>$item->quantity,
				'issued'=>$item->issued,
				'item_id'=>$item->item_id,
				'order_id'=>$order->order_id,
				'order_status'=>$order->order_status,
				);
		endforeach;

		$this->session->set_userdata('order_items',$order_items);
		$data['order_id'] = $order_id;
		$data['order'] = $order;
		$data['order_items'] = $order_items;
		
		$this->load->view('orders/order_form',$data);
	}

	function delete_order($order_id)
	{
		$order_id = $this->Order->delete_order($order_id);
		if($order_id):
			echo json_encode(array('success'=>true,'message'=>'Order deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('success'=>false,'message'=>'Unable to delete order.','message_class'=>'error_message'));
		endif;
	}

	function new_order()
	{
		$this->session->unset_userdata('order_items');
		$this->load->view('orders/order_form',$data);
	}

	function item_search()
	{
		$suggestions = $this->Item->get_item_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function add_item()
	{
		$item_id = $this->input->post("item");
		$item_info = $this->Item->get_info($item_id);
		$order_items = $this->session->userdata('order_items');

		if($item_info):
			if(array_key_exists($item_info->item_id, $order_items)):
				echo json_encode(array('success'=>false,'message_class'=>'warning_message','message'=>"Item already added"));
			else:
				$order_items[$item_info->item_id] = array(
					'item_id'=>$item_info->item_id,
					'name'=>$item_info->name,
					'quantity'=>1,
					'issued'=>0,
					'order_status'=>0,
					);
				$this->session->set_userdata('order_items',$order_items);
				echo json_encode(array('success'=>true,'message_class'=>'success_message','message'=>"Item added successfully"));
			endif;
		else:
			echo json_encode(array('success'=>false,'message_class'=>'error_message','message'=>"Please select a valid item from the list of suggestions"));
		endif;
	}

	function remove_item($item_id)
	{
		$order_items = $this->session->userdata('order_items');
		unset($order_items[$item_id]);
		$this->session->set_userdata('order_items',$order_items);

		$data['order_items'] = $order_items;
		$this->load->view("orders/order_items",$data);
	}

	function item_info($item_id,$var)
	{
		$order_items = $this->session->userdata('order_items');
		if($var == 'quantity'):
			$order_items[$item_id][$var] = is_numeric($this->input->post('value')) && $this->input->post('value') > 0 ? (int)$this->input->post('value') : 1;
		else:
			$order_items[$item_id][$var] = $this->input->post('value');
		endif;

		$this->session->set_userdata('order_items',$order_items);
	}

	function refresh_order()
	{
		$data['order_items'] = $this->session->userdata('order_items');
		
		$this->load->view("orders/order_items",$data);
	}

	function save_order($order_id)
	{
		$order_items = $this->session->userdata('order_items');
		$employee = $this->Employee->get_logged_in_employee_info();
		$order_data = array();
		$order_item_data = array();

		if(count($order_items) > 0):
			foreach($order_items as $item):
				$order_item_data[] = array(
					'quantity'=>$item['quantity'],
					'item_id'=>$item['item_id'],
					);
			endforeach;

			$order_data = array(
					'employee_id'=>$employee->person_id,					
					'remarks'=>$this->input->post('remarks'),
					);
			if($this->Order->save($order_data,$order_item_data,$order_id)):
				echo json_encode(array('success'=>true,'message'=>'Order saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('success'=>false,'message'=>'Unable to save order.','message_class'=>'success_message'));
			endif;
		else:
			echo json_encode(array('success'=>false,'message'=>'No items have been selected.','message_class'=>'error_message'));
		endif;
	}
}
?>