<?php
require_once ("secure_area.php");
class Employees extends Secure_area
{
	function __construct()
	{
		parent::__construct('employees');		
		//error_reporting(E_ALL);
		//$this->output->enable_profiler(TRUE);
	}
	
	function index()
	{
		$this->load->view('employees/main');
	}
	
	function view_employee($employee_id=-1)
	{
		if($employee_id != -1):
			$data['employee_info'] = $this->Employee->get_info($employee_id);
		endif;
		$data['modules'] = $this->Module->get_all_modules();

		$this->load->view('employees/employee_form',$data);
	}

	function view_all()
	{
		$data['employees'] = $this->Employee->get_all();

		$this->load->view('employees/employees_table',$data);
	}
	
	function save_employee($employee_id = null)
	{
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'valid_email');
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[5]|callback_username_check['.$employee_id.']');
		$this->form_validation->set_rules('employee_no', 'Employee #', 'callback_employee_no_check['.$employee_id.']');
		if(!$employee_id):
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
		endif;
		if($this->input->post('password')):
			$this->form_validation->set_rules('repeat_password', 'Repeat Password', 'trim|required|matches[password]');
		endif;
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$employee_data = array(
				'first_name'=>$this->input->post('first_name'),
				'middle_name'=>$this->input->post('middle_name'),
				'last_name'=>$this->input->post('last_name'),
				'gender'=>$this->input->post('gender'),
				'residence'=>$this->input->post('residence'),
				'national_id'=>$this->input->post('national_id'),
				'email'=>$this->input->post('email'),
				'phone_number'=>$this->input->post('phone_number'),
				'address_1'=>$this->input->post('address_1'),
				'address_2'=>$this->input->post('address_2'),
				'city'=>$this->input->post('city'),
				'zip'=>$this->input->post('zip'),
				'country'=>$this->input->post('country'),
				'employee_no'=>$this->input->post('employee_no'),
				'username'=>$this->input->post('username')
			);

			$permission_data = $this->input->post("permissions") ? $this->input->post("permissions") : array();
			
			if($this->input->post('password')):
				$employee_data['password'] = md5($this->input->post('password'));
			endif;
			
			$employee_id = $this->Employee->save($employee_data,$permission_data,$employee_id);
			if($employee_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Employee information saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save employee information.','message_class'=>'error_message'));
			endif;
		endif;
	}
	
	function delete_employee($employee_id)
	{
		$employee_id = $this->Employee->delete($employee_id);
		if($employee_id):
			echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Employee information deleted successfully.','message_class'=>'success_message','patient_id'=>$patient_id));
		else:
			echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to delete employee information.','message_class'=>'error_message'));
		endif;
	}

	public function username_check($username,$employee_id)
	{
		if ($this->Employee->username_exists($username,$employee_id)):
			$this->form_validation->set_message('username_check', 'Username already exists');
			return FALSE;
		else:
			return TRUE;
		endif;
	}

	public function employee_no_check($employee_no,$employee_id)
	{
		if ($this->Employee->employee_no_exists($employee_no,$employee_id)):
			$this->form_validation->set_message('employee_no_check', 'Employee # already exists');
			return FALSE;
		else:
			return TRUE;
		endif;
	}

	function get_employee_search_suggestions($search,$limit=10)
	{
		$suggestions = array();
		
		$this->db->from('employees');
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or middle_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%')");
		$this->db->order_by("last_name", "asc");		
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[] = $row->person_id.'|'. $this->patient_name($row->first_name,$row->middle_name,$row->last_name);		
		}
		
		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}
}
?>