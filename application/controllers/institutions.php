<?php
require_once ("secure_area.php");
class Institutions extends Secure_area
{
	function __construct()
	{
		parent::__construct('institutions');
		//error_reporting(E_ALL);
		//$this->output->enable_profiler(TRUE);
	}
	
	function index()
	{
		$this->load->view('institutions/main');
	}
	
	function view_institution($institution_id=-1)
	{
		if($institution_id != -1):
			$data['institution_info'] = $this->Institution->get_info($institution_id);
		endif;

		$this->load->view('institutions/institution_form',$data);
	}

	function view_all()
	{
		$data['institutions'] = $this->Institution->get_all();

		$this->load->view('institutions/institutions_table',$data);
	}
	
	function save_institution($institution_id = null)
	{
		$this->form_validation->set_rules('institution_name', 'Institution Name', 'required');
		$this->form_validation->set_rules('institution_liability', 'Percentage Liability', 'required|numeric|callback_liability_check');
		$this->form_validation->set_rules('institution_discount', 'Percentage Discount', 'numeric');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$institution_data = array(
				'institution_name'=>$this->input->post('institution_name'),
				'institution_discount'=>$this->input->post('institution_discount'),
				'institution_liability'=>$this->input->post('institution_liability'),
				'phone_number'=>$this->input->post('phone_number'),
				'address_1'=>$this->input->post('address_1'),
			);
			
			$institution_id = $this->Institution->save($institution_data,$institution_id);
			if($institution_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Institution information saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save institution information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	public function liability_check($liability)
	{
		if($liability > 100 || $liability < 0):
			$this->form_validation->set_message('liability_check', '&#37; Liability must be between 0 and 100');
			return FALSE;
		elseif($this->input->post('institution_discount') < 0 || $this->input->post('institution_discount') > 100):
			$this->form_validation->set_message('liability_check', '&#37; Discount must be between 0 and 100');
			return FALSE;
		elseif(($liability + $this->input->post('institution_discount')) > 100):
			$this->form_validation->set_message('liability_check', 'The sum of the liability and discount cannot be more than 100');
			return FALSE;
		else:
			return TRUE;
		endif;
	}
	
	function delete_institution($institution_id)
	{
		$institution_id = $this->Institution->delete($institution_id);
		if($institution_id):
			echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Institution information deleted successfully.','message_class'=>'success_message','patient_id'=>$patient_id));
		else:
			echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to delete institution information.','message_class'=>'error_message'));
		endif;
	}

	function view_payments($institution_id)
	{
		$data['institution'] = $this->Institution->get_info($institution_id);
		$data['payments'] = $this->Institution->get_payments($institution_id);

		$this->load->view('institutions/payments_table',$data);
	}

	function new_payment($institution_id)
	{
		$data['institution'] = $this->Institution->get_info($institution_id);

		$this->load->view('institutions/payment_form',$data);
	}

	function save_payment($institution_id)
	{
		$this->form_validation->set_rules('payment_type', 'Payment Type', 'required');
		$this->form_validation->set_rules('amount', 'Amount', 'required|numeric|greater_than[0]');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$payment_type = $this->input->post('payment_info') ? $this->input->post('payment_type') .': '.$this->input->post('payment_info') : $this->input->post('payment_type');
			$payment_data=array(
				'institution_id'=>$institution_id,
				'payment_type'=>$payment_type,
				'amount'=>$this->input->post('amount'),
				'remarks'=>$this->input->post('remarks'),
				'employee_id'=>$this->Employee->get_logged_in_employee_info()->person_id,
			);
			
			if( $this->Institution->save_payment($payment_data) ):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Payment information saved successfully.','message_class'=>'success_message',));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save payment information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function view_invoice($institution_id)
	{
		$date = ($this->session->userdata('institution_invoice_date')) ? $this->session->userdata('institution_invoice_date') : array("start"=>date("Y-m-01"),"end"=>date("Y-m-t"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));

		$institution = $this->Institution->get_info($institution_id);
		$invoice = $this->Institution->get_invoice($start_date,$end_date,$institution_id);
		$data['workloads'] = $invoice['workload'];
		$data['headers'] = $invoice['headers'];
		$data['title'] = $this->Patient->patient_name($institution->institution_name).' Invoice';
	    $data['institution'] = $institution;
	    $data["month"] = date("F Y",strtotime($date["start"]));
		$data["month_input"] = date("m/Y",strtotime($date["start"]));

		$this->load->view("institutions/invoice",$data);
	}

	function invoice_date_input()
	{
		$this->form_validation->set_rules('month', 'Month', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$month = '01/'.$this->input->post('month');
			$month = preg_replace('/[\/]/s', '-',$month);
			$start_date = isset($month) ? date("Y-m-01",strtotime($month)) : date("Y-m-01");
			$end_date = isset($month) ? date("Y-m-t",strtotime($month)) : date("Y-m-t");
			$date = array("start"=>$start_date,"end"=>$end_date);
			
			$this->session->set_userdata('institution_invoice_date', $date);
			echo json_encode(array('form_validation'=>true));
		endif;
	}
}
?>