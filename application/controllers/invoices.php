<?php
require_once ("secure_area.php");
class Invoices extends Secure_area
{
	function __construct()
	{
		parent::__construct('invoices');
		//error_reporting(E_ALL);
		//$this->output->enable_profiler(TRUE);
	}

	function index()
	{
		$this->load->view('invoices/main');
	}

	function view_all()
	{
		$data['invoices'] = $this->Invoice->get_pending_invoices();

		$this->load->view('invoices/invoices_table',$data);
	}

	function view_invoice($invoice_id)
	{
		$this->session->unset_userdata('invoice_items');
		$invoice = $this->Invoice->get_info($invoice_id);
		foreach($invoice->invoice_items as $item):
			$item_info = $this->Item->get_info($item->item_id);
			$invoice_items[$item->item_id] = array(
				'name'=>$item_info->name,
				'remarks'=>$item->remarks,
				'quantity'=>$item->quantity,
				'unit_price'=>$item->unit_price,
				'discount'=>$item->discount,
				'item_id'=>$item->item_id,
				'status'=>$item->status,
				'invoice_id'=>$invoice->invoice_id,
				'invoice_status'=>$invoice->invoice_status,
				);
		endforeach;

		$this->session->set_userdata('invoice_items',$invoice_items);
		$data['patient_id'] = $invoice->patient_id;
		$data['invoice_id'] = $invoice_id;
		$data['invoice_items'] = $invoice_items;
		
		$this->load->view('invoices/invoice_form',$data);
	}

	function delete_invoice($invoice_id)
	{
		$invoice_id = $this->Invoice->delete_invoice($invoice_id);
		if($invoice_id):
			echo json_encode(array('success'=>true,'message'=>'Invoice deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('success'=>false,'message'=>'Unable to delete invoice.','message_class'=>'error_message'));
		endif;
	}

	function new_invoice()
	{
		$this->session->unset_userdata('invoice_items');
		$this->load->view('invoices/new_invoice_form',$data);
	}

	function item_search()
	{
		$suggestions = $this->Item->get_item_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function add_item()
	{
		$item_id = $this->input->post("item");
		$item_info = $this->Item->get_info($item_id);
		$invoice_items = $this->session->userdata('invoice_items');

		if($item_info):
			if(array_key_exists($item_info->item_id, $invoice_items)):
				echo json_encode(array('success'=>false,'message_class'=>'warning_message','message'=>"Item already added"));
			else:
				$invoice_items[$item_info->item_id] = array(
					'item_id'=>$item_info->item_id,
					'name'=>$item_info->name,
					'remarks'=>'',
					'quantity'=>1,
					'unit_price'=>$item_info->unit_price,
					'discount'=>0,
					'status'=>0,
					'invoice_status'=>0,
					);
				$this->session->set_userdata('invoice_items',$invoice_items);
				echo json_encode(array('success'=>true,'message_class'=>'success_message','message'=>"Item added successfully"));
			endif;
		else:
			echo json_encode(array('success'=>false,'message_class'=>'error_message','message'=>"Please select a valid item from the list of suggestions"));
		endif;
	}

	function remove_item($item_id)
	{
		$invoice_items = $this->session->userdata('invoice_items');
		unset($invoice_items[$item_id]);
		$this->session->set_userdata('invoice_items',$invoice_items);

		$data['invoice_items'] = $invoice_items;
		$this->load->view("invoices/invoice_items",$data);
	}

	function item_info($item_id,$var)
	{
		$invoice_items = $this->session->userdata('invoice_items');
		if($var == 'quantity'):
			$invoice_items[$item_id][$var] = is_numeric($this->input->post('value')) && $this->input->post('value') > 0 ? (int)$this->input->post('value') : 1;
		elseif($var == 'discount'):
			$invoice_items[$item_id][$var] = is_numeric($this->input->post('value')) && $this->input->post('value') >= 0 && $this->input->post('value') <= 100 ? $this->input->post('value') : 0;
		else:
			$invoice_items[$item_id][$var] = $this->input->post('value');
		endif;

		$this->session->set_userdata('invoice_items',$invoice_items);
		$quantity = $invoice_items[$item_id]['quantity'];
		$unit_price = $invoice_items[$item_id]['unit_price'];
		$discount = $invoice_items[$item_id]['discount'];
		echo number_format(($quantity * $unit_price * (100 - $discount) / 100),2,'.','');
	}

	function refresh_invoice()
	{
		$data['invoice_items'] = $this->session->userdata('invoice_items');
		
		$this->load->view("invoices/invoice_items",$data);
	}

	function patient_search()
	{
		$suggestions = $this->Patient->get_patient_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function refresh_patient_info($patient_id = null)
	{
		$invoice_items = $this->session->userdata('invoice_items') ? $this->session->userdata('invoice_items') : array();
		$invoice_id = $this->Invoice->already_invoiced($patient_id,$this->Employee->get_logged_in_employee_info()->person_id);
		$invoice = $this->Invoice->get_info($invoice_id);
		foreach($invoice->invoice_items as $item):
			$item_info = $this->Item->get_info($item->item_id);
			$invoice_items[$item->item_id] = array(
				'name'=>$item_info->name,
				'remarks'=>$item->remarks,
				'quantity'=>$item->quantity,
				'unit_price'=>$item->unit_price,
				'discount'=>$item->discount,
				'item_id'=>$item->item_id,
				'status'=>$item->status,
				'invoice_id'=>$invoice->invoice_id,
				'invoice_status'=>$invoice->invoice_status,
				);
		endforeach;

		$this->session->set_userdata('invoice_items',$invoice_items);

		$data['patient_id'] = $patient_id;
		$this->load->view("invoices/patient_info",$data);
	}

	function new_patient()
	{
		$this->load->view('invoice/new_patient');
	}

	function save_patient()
	{
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('email', 'Email', 'valid_email');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$patient_data = array(
			'first_name'=>$this->input->post('first_name'),
			'last_name'=>$this->input->post('last_name'),
			'middle_name'=>$this->input->post('middle_name'),
			'gender'=>$this->input->post('gender'),
			'residence'=>$this->input->post('residence'),
			'national_id'=>$this->input->post('national_id'),
			'dob'=>$this->input->post('dob'),
			'phone_number'=>$this->input->post('phone_number'),
			'email'=>$this->input->post('email'),
			'address_1'=>$this->input->post('address_1'),
			'address_2'=>$this->input->post('address_2'),
			'city'=>$this->input->post('city'),
			'state'=>$this->input->post('state'),
			'zip'=>$this->input->post('zip'),
			'country'=>$this->input->post('country'),
			);

			$patient_id = $this->Patient->save($patient_data);
			if($patient_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Patient information saved successfully.','message_class'=>'success_message','patient_id'=>$patient_id));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save patient information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function save_new_invoice($patient_id)
	{
		$invoice_items = $this->session->userdata('invoice_items');
		$employee = $this->Employee->get_logged_in_employee_info();
		$invoice_data = array();
		$invoice_item_data = array();
		$invoice_id = $this->Invoice->already_invoiced($patient_id,$employee->person_id,'Normal');

		if(count($invoice_items) > 0):
			foreach($invoice_items as $item):
				$invoice_item_data[] = array(
					'remarks'=>$item['remarks'],
					'quantity'=>$item['quantity'],
					'discount'=>$item['discount'],
					'status'=>$item['status'],
					'item_id'=>$item['item_id'],
					);
			endforeach;

			$invoice_data = array(
					'patient_id'=>$patient_id,
					'employee_id'=>$employee->person_id,					
					'invoice_type'=>'Normal',
					);
			if($this->Invoice->save($invoice_data,$invoice_item_data,$invoice_id)):
				echo json_encode(array('success'=>true,'message'=>'Invoice saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('success'=>false,'message'=>'Unable to save Invoice.','message_class'=>'success_message'));
			endif;
		else:
			echo json_encode(array('success'=>false,'message'=>'No items have been selected.','message_class'=>'error_message'));
		endif;
	}

	function save_invoice($invoice_id)
	{
		$invoice_items = $this->session->userdata('invoice_items');
		$employee = $this->Employee->get_logged_in_employee_info();
		$invoice_data = array();
		$invoice_item_data = array();


		if(count($invoice_items) > 0):
			foreach($invoice_items as $item):
				$invoice_item_data[] = array(
					'remarks'=>$item['remarks'],
					'quantity'=>$item['quantity'],
					'discount'=>$item['discount'],
					'status'=>$item['status'],
					'item_id'=>$item['item_id'],
					);
			endforeach;

			$invoice_data = array(
					'employee_id'=>$employee->person_id,
					);
			if($this->Invoice->save($invoice_data,$invoice_item_data,$invoice_id)):
				echo json_encode(array('success'=>true,'message'=>'Invoice saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('success'=>false,'message'=>'Unable to save Invoice.','message_class'=>'success_message'));
			endif;
		else:
			echo json_encode(array('success'=>false,'message'=>'No items have been selected.','message_class'=>'error_message'));
		endif;
	}
}
?>