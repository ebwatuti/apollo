<?php
require_once ("secure_area.php");
class Sales extends Secure_area
{
	function __construct()
	{
		parent::__construct('sales');
		$this->load->library('sale_lib');
		//error_reporting(E_ALL);
		//$this->output->enable_profiler(TRUE);
	}

	function index()
	{
		$this->_reload();
	}

	function item_search()
	{
		$suggestions = $this->Item->get_item_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		$suggestions = array_merge($suggestions, $this->Item_kit->get_item_kit_search_suggestions($this->input->post('q'),$this->input->post('limit')));
		echo implode("\n",$suggestions);
	}

	function customer_search()
	{
		$suggestions = $this->Patient->get_patient_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function new_patient()
	{
		$this->load->view('sales/new_patient');
	}

	function save_patient()
	{
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('email', 'Email', 'valid_email');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$patient_data = array(
			'first_name'=>$this->input->post('first_name'),
			'last_name'=>$this->input->post('last_name'),
			'middle_name'=>$this->input->post('middle_name'),
			'gender'=>$this->input->post('gender'),
			'residence'=>$this->input->post('residence'),
			'national_id'=>$this->input->post('national_id'),
			'dob'=>$this->input->post('dob'),
			'phone_number'=>$this->input->post('phone_number'),
			'email'=>$this->input->post('email'),
			'address_1'=>$this->input->post('address_1'),
			'address_2'=>$this->input->post('address_2'),
			'city'=>$this->input->post('city'),
			'state'=>$this->input->post('state'),
			'zip'=>$this->input->post('zip'),
			'country'=>$this->input->post('country'),
			);

			$patient_id = $this->Patient->save($patient_data);
			if($patient_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Patient information saved successfully.','message_class'=>'success_message','patient_id'=>$patient_id));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save patient information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function select_customer($customer_id = null)
	{
		$customer_id = $customer_id ? $customer_id : $this->input->post("customer");
		$this->sale_lib->set_customer($customer_id);
		$this->sale_lib->get_invoice($customer_id);
		$this->_reload();
	}

	function change_mode()
	{
		$mode = $this->input->post("mode");
		$this->sale_lib->set_mode($mode);
		$this->_reload();
	}
	
	function set_comment() 
	{
 	  $this->sale_lib->set_comment($this->input->post('comment'));
	}
	
	function set_email_receipt()
	{
 	  $this->sale_lib->set_email_receipt($this->input->post('email_receipt'));
	}

	//Alain Multiple Payments
	function add_payment()
	{		
		$data = array();
		$this->form_validation->set_rules( 'amount_tendered', 'lang:sales_amount_tendered', 'numeric' );
		
		if ( $this->form_validation->run() == FALSE )
		{
			if ( $this->input->post( 'payment_type' ) == $this->lang->line( 'sales_gift_card' ) )
				$data['error']=$this->lang->line('sales_must_enter_numeric_giftcard');
			else
				$data['error']=$this->lang->line('sales_must_enter_numeric');
				
 			$this->_reload( $data );
 			return;
		}
		
		$payment_type = $this->input->post( 'payment_type' );
		$payment_remarks = $this->input->post( 'payment_remarks' );
		if ( $payment_type == $this->lang->line( 'sales_giftcard' ) )
		{
			$payments = $this->sale_lib->get_payments();
			$payment_type = $this->input->post( 'payment_type' ) . ':' . $payment_amount = $this->input->post( 'amount_tendered' );
			$current_payments_with_giftcard = isset( $payments[$payment_type] ) ? $payments[$payment_type]['payment_amount'] : 0;
			$cur_giftcard_value = $this->Giftcard->get_giftcard_value( $this->input->post( 'amount_tendered' ) ) - $current_payments_with_giftcard;
			
			if ( $cur_giftcard_value <= 0 )
			{
				$data['error'] = 'Giftcard balance is ' . to_currency( $this->Giftcard->get_giftcard_value( $this->input->post( 'amount_tendered' ) ) ) . ' !';
				$this->_reload( $data );
				return;
			}

			$new_giftcard_value = $this->Giftcard->get_giftcard_value( $this->input->post( 'amount_tendered' ) ) - $this->sale_lib->get_amount_due( );
			$new_giftcard_value = ( $new_giftcard_value >= 0 ) ? $new_giftcard_value : 0;
			$data['warning'] = 'Giftcard ' . $this->input->post( 'amount_tendered' ) . ' balance is ' . to_currency( $new_giftcard_value ) . ' !';
			$payment_amount = min( $this->sale_lib->get_amount_due( ), $this->Giftcard->get_giftcard_value( $this->input->post( 'amount_tendered' ) ) );
		}
		else
		{
			$payment_amount = $this->input->post( 'amount_tendered' );
		}
		
		if( !$this->sale_lib->add_payment( $payment_type, $payment_amount, $payment_remarks  ) )
		{
			$data['error']='Unable to Add Payment! Please try again!';
		}
		
		$this->_reload($data);
	}

	//Alain Multiple Payments
	function delete_payment( $payment_id )
	{
		$this->sale_lib->delete_payment( $payment_id );
		$this->_reload();
	}

	function add()
	{
		$data=array();
		$mode = $this->sale_lib->get_mode();
		$item_id_or_number_or_item_kit_or_receipt = $this->input->post("item");
		$quantity = $mode=="sale" ? 1:-1;

		if($this->sale_lib->is_valid_receipt($item_id_or_number_or_item_kit_or_receipt) && $mode=='return')
		{
			$this->sale_lib->return_entire_sale($item_id_or_number_or_item_kit_or_receipt);
		}
		elseif($this->sale_lib->is_valid_item_kit($item_id_or_number_or_item_kit_or_receipt))
		{
			$this->sale_lib->add_item_kit($item_id_or_number_or_item_kit_or_receipt);
		}
		elseif(!$this->sale_lib->add_item($item_id_or_number_or_item_kit_or_receipt,$quantity))
		{
			$data['error']=$this->lang->line('sales_unable_to_add_item');
		}
		
		if($this->sale_lib->out_of_stock($item_id_or_number_or_item_kit_or_receipt))
		{
			$data['warning'] = $this->lang->line('sales_quantity_less_than_zero');
		}
		$this->_reload($data);
	}

	function edit_item($line)
	{
		$data= array();

		$this->form_validation->set_rules('price', 'Price', 'required|numeric');
		$this->form_validation->set_rules('quantity', 'Qty', 'required|numeric');
		$this->form_validation->set_rules('discount', 'Disc %', 'numeric');

        $description = $this->input->post("description");
        $serialnumber = $this->input->post("serialnumber");
		$price = $this->input->post("price");
		$quantity = $this->input->post("quantity");
		$discount = $this->input->post("discount");


		if ($this->form_validation->run() != FALSE)
		{
			$this->sale_lib->edit_item($line,$description,$serialnumber,$quantity,$discount,$price);
		}
		else
		{
			$data['error'] = validation_errors();
		}
		
		if($this->sale_lib->out_of_stock($this->sale_lib->get_item_id($line)))
		{
			$data['warning'] = $this->lang->line('sales_quantity_less_than_zero');
		}


		$this->_reload($data);
	}

	function delete_item($item_number)
	{
		$this->sale_lib->delete_item($item_number);
		$this->_reload();
	}

	function remove_customer()
	{
		$this->sale_lib->remove_customer();
		$this->_reload();
	}

	function complete()
	{
		$data['cart']=$this->sale_lib->get_cart();
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes();
		$data['total']=$this->sale_lib->get_total();
		$data['receipt_title']=$this->lang->line('sales_receipt');
		$data['transaction_time']= date('d/m/Y h:i a');
		$patient_id=$this->sale_lib->get_customer();
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$comment = $this->sale_lib->get_comment();
		$emp_info=$this->Employee->get_info($employee_id);
		$data['payments']=$this->sale_lib->get_payments();
		$data['amount_change']=to_currency($this->sale_lib->get_amount_due() * -1);
		$data['employee']=$this->Patient->patient_name($emp_info->first_name,$emp_info->middle_name,$emp_info->last_name);

		if($patient_id!=-1)
		{
			$patient_info=$this->Patient->get_info($patient_id);
			$data['patient_no']=$this->Patient->patient_number($patient_info->patient_id);
			$data['patient_name'] = $this->Patient->patient_name($patient_info->first_name,$patient_info->middle_name,$patient_info->last_name);
		}

		$invoice_data = array();
		foreach($data['cart'] as $item):
			if($item['invoice']):
				if(!$invoice_data[$item['invoice']]):
					$invoice_data[$item['invoice']] = array(
						'invoices_data'=>array('invoice_status'=>1),
						'invoices_items_data'=>array()
						);
				endif;
				$invoice_data[$item['invoice']]['invoices_items_data'][] = array(
					'invoice_id'=>$item['invoice'],
					'item_id'=>$item['item_id'],
					'line'=>$item['invoice_item_line'],
					'status'=>$item['status']);
			endif;
		endforeach;

		//SAVE sale to database
		$sale_id = $this->Sale->save($data['cart'], $patient_id,$employee_id,$comment,$data['payments'],false,$invoice_data);

		$data['sale_id']=$sale_id;
		$data['receipt_no']= $this->Sale->receipt_no($sale_id);
		if (!$sale_id)
		{
			$data['error_message'] = $this->lang->line('sales_transaction_failed');
		}

		$this->load->view("sales/receipt",$data);
		$this->sale_lib->clear_all();
	}
	
	function receipt($sale_id)
	{
		$sale_info = $this->Sale->get_info($sale_id)->row_array();
		$this->sale_lib->copy_entire_sale($sale_id);
		$data['cart']=$this->sale_lib->get_cart();
		$data['payments']=$this->sale_lib->get_payments();
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes();
		$data['total']=$this->sale_lib->get_total();
		$data['receipt_title']=$this->lang->line('sales_receipt');
		$data['transaction_time']= date('d/m/Y h:i a', strtotime($sale_info['sale_time']));
		$patient_id=$this->sale_lib->get_customer();
		$emp_info=$this->Employee->get_info($sale_info['employee_id']);
		$data['payment_type']=$sale_info['payment_type'];
		$data['amount_change']=to_currency($this->sale_lib->get_amount_due() * -1);
		$data['employee']=$this->Patient->patient_name($emp_info->first_name,$emp_info->middle_name,$emp_info->last_name);

		if($patient_id!=-1)
		{
			$patient_info=$this->Patient->get_info($patient_id);
			$data['patient_no']=$this->Patient->patient_number($patient_info->patient_id);
			$data['patient_name'] = $this->Patient->patient_name($patient_info->first_name,$patient_info->middle_name,$patient_info->last_name);
		}

		$data['sale_id']=$sale_id;
		$data['receipt_no']= $this->Sale->receipt_no($sale_id);
		$this->load->view("sales/receipt",$data);
		$this->sale_lib->clear_all();

	}
	
	function edit($sale_id)
	{
		$data = array();

		$data['customers'] = array('' => 'No Customer');
		foreach ($this->Customer->get_all()->result() as $customer)
		{
			$data['customers'][$customer->person_id] = $customer->first_name . ' '. $customer->last_name;
		}

		$data['employees'] = array();
		foreach ($this->Employee->get_all()->result() as $employee)
		{
			$data['employees'][$employee->person_id] = $employee->first_name . ' '. $employee->last_name;
		}

		$data['sale_info'] = $this->Sale->get_info($sale_id)->row_array();
				
		
		$this->load->view('sales/edit', $data);
	}
	
	function delete($sale_id)
	{
		$data = array();
		
		if ($this->Sale->delete($sale_id))
		{
			$data['success'] = true;
		}
		else
		{
			$data['success'] = false;
		}
		
		$this->load->view('sales/delete', $data);
		
	}
	
	function save($sale_id)
	{
		$sale_data = array(
			'sale_time' => date('Y-m-d', strtotime($this->input->post('date'))),
			'customer_id' => $this->input->post('customer_id') ? $this->input->post('customer_id') : null,
			'employee_id' => $this->input->post('employee_id'),
			'comment' => $this->input->post('comment')
		);
		
		if ($this->Sale->update($sale_data, $sale_id))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('sales_successfully_updated')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('sales_unsuccessfully_updated')));
		}
	}
	
	function _payments_cover_total()
	{
		$total_payments = 0;

		foreach($this->sale_lib->get_payments() as $payment)
		{
			$total_payments += $payment['payment_amount'];
		}

		/* Changed the conditional to account for floating point rounding */
		if ( ( $this->sale_lib->get_mode() == 'sale' ) && ( ( to_currency_no_money( $this->sale_lib->get_total() ) - $total_payments ) > 1e-6 ) )
		{
			return false;
		}
		
		return true;
	}
	
	function _reload($data=array())
	{
		$person_info = $this->Employee->get_logged_in_employee_info();
		$data['cart']=$this->sale_lib->get_cart();
		$data['modes']=array('sale'=>$this->lang->line('sales_sale'),'return'=>$this->lang->line('sales_return'));
		$data['mode']=$this->sale_lib->get_mode();
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes();
		$data['total']=$this->sale_lib->get_total();
		$data['items_module_allowed'] = $this->Employee->has_permission('items', $person_info->person_id);
		$data['comment'] = $this->sale_lib->get_comment();
		$data['email_receipt'] = $this->sale_lib->get_email_receipt();
		$data['payments_total']=$this->sale_lib->get_payments_total();
		$data['amount_due']=$this->sale_lib->get_amount_due();
		$data['payments']=$this->sale_lib->get_payments();
		$data['payment_options']=array(
			$this->lang->line('sales_cash') => $this->lang->line('sales_cash'),
			$this->lang->line('sales_check') => $this->lang->line('sales_check'),
		);

		$patient_id=$this->sale_lib->get_customer();
		if($patient_id!=-1)
		{
			$patient_info=$this->Patient->get_info($patient_id);
			$data['patient_no']=$this->Patient->patient_number($patient_info->patient_id);
			$data['patient_name'] = $this->Patient->patient_name($patient_info->first_name,$patient_info->middle_name,$patient_info->last_name);
		}
		$data['payments_cover_total'] = $this->_payments_cover_total();
		$this->load->view("sales/register",$data);
	}

    function cancel_sale()
    {
    	$this->sale_lib->clear_all();
    	$this->_reload();

    }
	
	function suspend()
	{
		$data['cart']=$this->sale_lib->get_cart();
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes();
		$data['total']=$this->sale_lib->get_total();
		$data['receipt_title']=$this->lang->line('sales_receipt');
		$data['transaction_time']= date('m/d/Y h:i:s a');
		$customer_id=$this->sale_lib->get_customer();
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$comment = $this->input->post('comment');
		$emp_info=$this->Employee->get_info($employee_id);
		$payment_type = $this->input->post('payment_type');
		$data['payment_type']=$this->input->post('payment_type');
		//Alain Multiple payments
		$data['payments']=$this->sale_lib->get_payments();
		$data['amount_change']=to_currency($this->sale_lib->get_amount_due() * -1);
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;

		if($customer_id!=-1)
		{
			$cust_info=$this->Customer->get_info($customer_id);
			$data['customer']=$cust_info->first_name.' '.$cust_info->last_name;
		}

		$total_payments = 0;

		foreach($data['payments'] as $payment)
		{
			$total_payments += $payment['payment_amount'];
		}

		//SAVE sale to database
		$data['sale_id']='POS '.$this->Sale_suspended->save($data['cart'], $customer_id,$employee_id,$comment,$data['payments']);
		if ($data['sale_id'] == 'POS -1')
		{
			$data['error_message'] = $this->lang->line('sales_transaction_failed');
		}
		$this->sale_lib->clear_all();
		$this->_reload(array('success' => $this->lang->line('sales_successfully_suspended_sale')));
	}
	
	function suspended()
	{
		$data = array();
		$data['suspended_sales'] = $this->Sale_suspended->get_all()->result_array();
		$this->load->view('sales/suspended', $data);
	}
	
	function unsuspend()
	{
		$sale_id = $this->input->post('suspended_sale_id');
		$this->sale_lib->clear_all();
		$this->sale_lib->copy_entire_suspended_sale($sale_id);
		$this->Sale_suspended->delete($sale_id);
    	$this->_reload();
	}

	function view_workload()
	{
		$date = ($this->session->userdata('sales_workload_date')) ? $this->session->userdata('sales_workload_date') : array("start"=>date("Y-m-d 00:00:00"),"end"=>date("Y-m-d 23:59:59"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));
		$category = $this->session->userdata('sales_sales_category');
		$report_type = ($this->session->userdata('sales_report_type')) ? $this->session->userdata('sales_report_type') : 'categories';

		$employee = $this->Employee->get_logged_in_employee_info();
		$employee_id = $employee->person_id;

		switch ($report_type) {
			case 'categories':
				$data['sales'] = $category ? $this->Sale->subcategories_workload($start_date,$end_date,$category,$employee_id) : $this->Sale->categories_workload($start_date,$end_date,$employee_id);
				$data['title'] = $category ? ucwords($category).' Category' : 'Item Categories';
				$data['th'] = $category ? 'Sub-category' : 'Category';
				$data['discounts'] = TRUE;
				$data['categories'] = $this->Item->get_categories()->Result();
				$data['category'] = $category;
				break;
			case 'items':
				$data['sales'] = $this->Sale->items_workload($start_date,$end_date,$employee_id);
				$data['title'] = 'Items';
				$data['th'] = 'Item';
				$data['discounts'] = TRUE;
				break;
			default:
				$data['sales'] = $category ? $this->Sale->subcategories_workload($start_date,$end_date,$category,$employee_id) : $this->Sale->categories_workload($start_date,$end_date,$employee_id);
				$data['title'] = $category ? ucwords($category).' Category' : 'Item Categories';
				$data['th'] = $category ? 'Sub-category' : 'Category';
				$data['discounts'] = TRUE;
				$data['categories'] = $this->Item->get_categories()->Result();
				$data['category'] = $category;
		}
		$data['report_type'] = $report_type;

		$data["start_date"] = date("j/n/Y",strtotime($date["start"]));
		$data["end_date"] = date("j/n/Y",strtotime($date["end"]));
		
		$data["start_date_input"] = date("d-m-Y",strtotime($date["start"]));
		$data["end_date_input"] = date("d-m-Y",strtotime($date["end"]));

		$this->load->view("sales/workload",$data);
	}

	function workload_date_input()
	{
		$this->form_validation->set_rules('start_date', 'Start Date', 'callback_date_check|required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$start_date = isset($start_date) ? date("Y-m-d H:i:s",strtotime($start_date)) : date("Y-m-d H:i:s");
			$end_date = isset($end_date) ? date("Y-m-d H:i:s",strtotime($end_date)) : $start_date;
			$date = array("start"=>$start_date,"end"=>$end_date);
			
			$this->session->set_userdata('sales_workload_date', $date);
			$this->session->set_userdata('sales_report_type', $this->input->post('report_type'));
			$this->session->set_userdata('sales_sales_category', $this->input->post('category'));
			
			echo json_encode(array('form_validation'=>true));
		endif;
	}

	function view_archive()
	{
		$date = ($this->session->userdata('sales_archive_date')) ? $this->session->userdata('sales_archive_date') : array("start"=>date("Y-m-d 00:00:00"),"end"=>date("Y-m-d 23:59:59"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));
		$data['sales'] = $this->Sale->sales_archive($start_date,$end_date);
		$data["start_date"] = date("j/n/Y",strtotime($date["start"]));
		$data["end_date"] = date("j/n/Y",strtotime($date["end"]));
		
		$data["start_date_input"] = date("d-m-Y",strtotime($date["start"]));
		$data["end_date_input"] = date("d-m-Y",strtotime($date["end"]));

		$this->load->view("sales/archive",$data);
	}

	function archive_date_input()
	{
		$this->form_validation->set_rules('start_date', 'Start Date', 'callback_date_check|required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$start_date = isset($start_date) ? date("Y-m-d H:i:s",strtotime($start_date)) : date("Y-m-d H:i:s");
			$end_date = isset($end_date) ? date("Y-m-d H:i:s",strtotime($end_date)) : $start_date;
			$date = array("start"=>$start_date,"end"=>$end_date);
			
			$this->session->set_userdata('sales_archive_date', $date);
			echo json_encode(array('form_validation'=>true));
		endif;
	}

	public function date_check()
	{
		$start_date = strtotime($this->input->post('start_date'));
		$end_date = strtotime($this->input->post('end_date'));
		if ($start_date > $end_date):
			$this->form_validation->set_message('date_check', 'Start date cannot be later than end date');
			return FALSE;
		else:
			return TRUE;
		endif;
	}

	function sale_details($sale_id)
	{
		$data['sale'] = $this->Sale->get_sale_info($sale_id);

		$this->load->view('sales/sale_details',$data);
	}

	function sale_receipt($sale_id)
	{
		$sale_info = $this->Sale->get_info($sale_id)->row_array();
		$this->sale_lib->copy_entire_sale($sale_id);
		$data['cart']=$this->sale_lib->get_cart();
		$data['payments']=$this->sale_lib->get_payments();
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes();
		$data['total']=$this->sale_lib->get_total();
		$data['receipt_title']=$this->lang->line('sales_receipt');
		$data['transaction_time']= date('d/m/Y h:i a', strtotime($sale_info['sale_time']));
		$patient_id=$this->sale_lib->get_customer();
		$emp_info=$this->Employee->get_info($sale_info['employee_id']);
		$data['payment_type']=$sale_info['payment_type'];
		$data['amount_change']=to_currency($this->sale_lib->get_amount_due() * -1);
		$data['employee']=$this->Patient->patient_name($emp_info->first_name,$emp_info->middle_name,$emp_info->last_name);

		if($patient_id!=-1)
		{
			$patient_info=$this->Patient->get_info($patient_id);
			$data['patient_no']=$this->Patient->patient_number($patient_info->patient_id);
			$data['patient_name'] = $this->Patient->patient_name($patient_info->first_name,$patient_info->middle_name,$patient_info->last_name);
		}

		$data['sale_id']=$sale_id;
		$data['receipt_no']= $this->Sale->receipt_no($sale_id);
		$this->load->view("sales/receipt_page",$data);
		$this->sale_lib->clear_all();
	}
}
?>