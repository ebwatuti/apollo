<?php
require_once ("secure_area.php");

class Home extends Secure_area 
{
	function __construct()
	{
		parent::__construct();	
	}
	
	function index()
	{
		$this->load->view("home");
	}
	
	function logout()
	{
		$this->Employee->logout();
	}

	function view()
	{
		$data['employee_info']=$this->Employee->get_logged_in_employee_info();
		$this->load->view("employees/profile_form",$data);
	}
	
	public function username_check($username)
	{
		if($this->Employee->username_exists($username,$this->Employee->get_logged_in_employee_info()->person_id)):
			$this->form_validation->set_message('username_check', 'Username already exists');
			return FALSE;
		else:
			return TRUE;
		endif;
	}
	
	public function password_check($password)
	{
		if($this->Employee->get_logged_in_employee_info()->password != md5($password)):
			$this->form_validation->set_message('password_check', 'Password does not match existing password');
			return FALSE;
		else:
			return TRUE;
		endif;
	}
	
	function save_profile()
	{
		$this->form_validation->set_rules('username', 'Username', 'callback_username_check|min_length[5]|required');
		$this->form_validation->set_rules('password', 'Password', 'callback_password_check|required');
		$this->form_validation->set_rules('new_password', 'New Password', 'trim|min_length[6]');
		if($this->input->post('new_password')):
			$this->form_validation->set_rules('repeat_password', 'Repeat Password', 'trim|required|matches[new_password]');
		endif;
		
		if($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
			$employee_data = array('username'=>$this->input->post('username'));
			if($this->input->post('new_password')) $employee_data['password'] = md5($this->input->post('new_password'));
			
			if($this->Employee->save_profile($employee_data,$employee_id)):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Profile information saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save profile information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function print_doc($doc_type="")
	{
		$data["title"] = $this->input->post('title');
		$data["subtitle"] = $this->input->post('subtitle');
		$data["html"] = $this->input->post('html');
		$doc_title = strtolower(preg_replace('/[^a-zA-Z0-9_]/s', "_",$data["title"]."_".$data["subtitle"]));
		switch($doc_type)
		{
			case "pdf":
				$this->load->library('mpdf');
				$file = $this->load->view("print",$data,true);
				$this->mpdf->WriteHTML($file);
				$this->mpdf->Output($doc_title.".pdf","D");
				exit;
			break;
			case "xls":
				$data["excel"] = 1;
				$file = $this->load->view("print",$data,true);
				echo $file;
			break;
			default:
				$file = $this->load->view("print",$data,true);
				echo $file;
			break;			
		}
	}
}
?>