<?php
require_once ("secure_area.php");
class Labs extends Secure_area
{
	function __construct()
	{
		parent::__construct('labs');
		//error_reporting(E_ALL);
		//$this->output->enable_profiler(TRUE);
	}

	function index()
	{
		$this->load->view('lab/main');
	}

	function view_request($patient_id)
	{
		$this->session->unset_userdata('lab_tests');
		$lab_tests = $this->Lab->get_lab_tests($patient_id);
		$data['consultation_notes'] = $this->Lab->get_consultation_notes($patient_id);
		$data['patient'] = $this->Patient->get_info($patient_id);

		$data['lab_tests'] = array();
		foreach($lab_tests as $lab_test):
			$lab_test_id = $this->Lab->get_lab_test_id($lab_test->item_id);
			$data['lab_tests'][$lab_test_id] = array(
				'lab_test_id'=>$lab_test_id,
				'value'=>$this->Lab->get_lab_test_info($lab_test_id)->value,
				'results'=>$lab_test->results ? unserialize($lab_test->results) : $this->Lab->lab_test_options($lab_test_id),
				'remarks'=>$lab_test->remarks,
				'item_id'=>$lab_test->item_id,
				'status'=>$lab_test->status,
				'invoice_status'=>$lab_test->invoice_status,
				'invoice_id'=>$lab_test->invoice_id,
				'line'=>$lab_test->line,
				);
		endforeach;
		$this->session->set_userdata('lab_tests',$data['lab_tests']);

		$this->load->view('lab/reporting_form',$data);
	}

	function save_lab_report($patient_id)
	{
		$lab_tests = $this->session->userdata('lab_tests');
		$employee = $this->Employee->get_logged_in_employee_info();
		$invoice_data = array();
		$invoice_item_data = array();

		if(count($lab_tests) > 0):
			foreach($lab_tests as $item):
				if($item['status'] == 1):
					if(!$invoice_data[$item['invoice_id']]):
						$invoice_data[$item['invoice_id']] = array(
							'invoice_status'=>2,
							'service_time'=>date('Y-m-d H:i:s'),
							'served_by'=>$employee->person_id);
					endif;
					$invoice_item_data[] = array(
						'results' => serialize($item['results']),
						'invoice_id'=> $item['invoice_id'],
						'line'=>$item['line'],
						'item_id'=>$item['item_id'],
						);
				endif;
			endforeach;

			if($this->Lab->save_lab_report($invoice_data,$invoice_item_data)):
				echo json_encode(array('success'=>true,'message'=>'Results saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('success'=>false,'message'=>'Unable to save results.','message_class'=>'error_message'));
			endif;
		endif;		
	}

	function complete_lab_report($patient_id)
	{
		$lab_tests = $this->session->userdata('lab_tests');
		$employee = $this->Employee->get_logged_in_employee_info();
		$invoice_data = array();
		$invoice_item_data = array();

		if(count($lab_tests) > 0):
			foreach($lab_tests as $item):
				if($item['status'] == 1):
					if(!$invoice_data[$item['invoice_id']]):
						$invoice_data[$item['invoice_id']] = array(
							'invoice_status'=>3,
							'service_time'=>date('Y-m-d H:i:s'),
							'served_by'=>$employee->person_id);
					endif;
					$invoice_item_data[] = array(
						'results' => serialize($item['results']),
						'invoice_id'=> $item['invoice_id'],
						'line'=>$item['line'],
						'item_id'=>$item['item_id'],
						);
				endif;
			endforeach;

			if($this->Lab->save_lab_report($invoice_data,$invoice_item_data)):
				echo json_encode(array('success'=>true,'message'=>'Results saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('success'=>false,'message'=>'Unable to save results.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function lab_test_results($lab_test_id)
	{
		$lab_tests = $this->session->userdata('lab_tests');
		$option = $this->input->post('option');
		$lab_tests[$lab_test_id]['results'][$option] = $this->input->post('results');

		$this->session->set_userdata('lab_tests',$lab_tests);
	}

	function view_all()
	{
		$data['main_queue'] = $this->Lab->get_main_queue();
		$data['pending_queue'] = $this->Lab->get_pending_queue();

		$this->load->view('lab/patients_table',$data);
	}

	function consultation_summary($patient_id,$consultation_id)
	{
		$patient = $this->Patient->get_info($patient_id);
		$patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
    	if($patient->dob) $patient->age = $this->Patient->patient_age($patient->dob);
    	$patient->patient_no = $this->Patient->patient_number($patient->patient_id);
		$data['patient'] = $patient;

		$consultation_info = $this->Consultation->get_info($consultation_id);
		$data['complaints'] = unserialize($consultation_info->consultation_complaints);
		$data['examination'] = unserialize($consultation_info->consultation_examination);
		$data['obst_gyn'] = unserialize($consultation_info->consultation_obst_gyn);
		$data['family_history'] = unserialize($consultation_info->consultation_family_history);
		$data['medical_history'] = unserialize($consultation_info->consultation_medical_history);
		$data['treatment_plan'] = unserialize($consultation_info->consultation_treatment_plan);

		$employee = $this->Employee->get_info($consultation_info->consultant_id);
		$employee->employee_name = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
		$data['consultant'] = $employee;

		foreach($consultation_info->diagnosis as $diagnosis):
			$data['diagnosis'][$diagnosis->diagnosis_code] = array(
				'diagnosis_code'=>$diagnosis->diagnosis_code,
				'remarks'=>$diagnosis->remarks,
				'description'=>$this->Consultation->get_diagnosis_info($diagnosis->diagnosis_code)->description,
				);
		endforeach;

		foreach($consultation_info->lab_tests as $lab_test):
			$lab_test_id = $this->Lab->get_lab_test_id($lab_test->item_id);
			$data['lab_tests'][$lab_test_id] = array(
				'lab_test_id'=>$lab_test_id,
				'value'=>$this->Lab->get_lab_test_info($lab_test_id)->value,
				'remarks'=>$lab_test->remarks,
				'results'=>unserialize($lab_test->results),
				'item_id'=>$lab_test->item_id,
				'status'=>$lab_test->status,
				'invoice_status'=>$lab_test->invoice_status,
				);
		endforeach;

		foreach($consultation_info->prescription as $drug):
			$drug_id = $this->Pharm->get_drug_id($drug->item_id);
			$drug_info = $this->Pharm->get_drug_info($drug_id);
			$drug->instructions = unserialize($drug->instructions);
			$data['prescription'][$drug_id] = array(
				'drug_id'=>$drug_id,
				'name'=>$drug_info->item_info->name,
				'strength'=>$drug_info->strength,
				'remarks'=>$drug->remarks,
				'dose'=>$drug->instructions['dose'],
				'frequency'=>$drug->instructions['frequency'],
				'duration'=>$drug->instructions['duration'],
				'quantity'=>$drug->quantity,
				'item_id'=>$lab_test->item_id,
				'status'=>$lab_test->status,
				'invoice_status'=>$lab_test->invoice_status,
				);
		endforeach;

		$data['consultation_date'] = date('j/n/Y',strtotime($consultation_info->consultation_start));

		$this->load->view("consultant/consultation_summary",$data);
	}

	function view_archive()
	{
		$date = ($this->session->userdata('lab_archive_date')) ? $this->session->userdata('lab_archive_date') : array("start"=>date("Y-m-d 00:00:00"),"end"=>date("Y-m-d 23:59:59"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));
		$data['lab_reports'] = $this->Lab->lab_reports($start_date,$end_date);
		$data["start_date"] = date("j/n/Y",strtotime($date["start"]));
		$data["end_date"] = date("j/n/Y",strtotime($date["end"]));
		
		$data["start_date_input"] = date("d-m-Y",strtotime($date["start"]));
		$data["end_date_input"] = date("d-m-Y",strtotime($date["end"]));

		$this->load->view("lab/archive",$data);
	}

	function archive_date_input()
	{
		$this->form_validation->set_rules('start_date', 'Start Date', 'callback_date_check|required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$start_date = isset($start_date) ? date("Y-m-d H:i:s",strtotime($start_date)) : date("Y-m-d H:i:s");
			$end_date = isset($end_date) ? date("Y-m-d H:i:s",strtotime($end_date)) : $start_date;
			$date = array("start"=>$start_date,"end"=>$end_date);
			
			$this->session->set_userdata('lab_archive_date', $date);
			echo json_encode(array('form_validation'=>true));
		endif;
	}

	public function date_check()
	{
		$start_date = strtotime($this->input->post('start_date'));
		$end_date = strtotime($this->input->post('end_date'));
		if ($start_date > $end_date):
			$this->form_validation->set_message('date_check', 'Start date cannot be later than end date');
			return FALSE;
		else:
			return TRUE;
		endif;
	}

	function view_report($invoice_id)
	{
		$data['lab_report'] = $this->Lab->get_lab_report($invoice_id);

		$this->load->view('lab/lab_report',$data);
	}

	function view_lab_tests()
	{
		$data['lab_tests'] = $this->Lab->lab_tests();

		$this->load->view('lab/lab_tests',$data);
	}

	function view_lab_test($lab_test_id = -1)
	{
		if($lab_test_id) $data['lab_test'] = $this->Lab->get_lab_test_info($lab_test_id);

		$this->load->view('lab/lab_test_form',$data);
	}

	function save_lab_test($lab_test_id = null)
	{
		$this->form_validation->set_rules('test_name', 'Test Name', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required|numeric');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$lab_test_data = array(
			'value'=>$this->input->post('test_name'),
			'category'=>$this->input->post('category'),
			'options'=>$this->input->post('options')
			);
			$item_data = array(
			'name'=>$this->input->post('test_name'),
			'category'=>'Lab',
			'sub_category'=>$this->input->post('category'),
			'quantity'=>1,
			'quantifiable'=>0,
			'unit_price'=>$this->input->post('price'),
			);

			$lab_test_id = $this->Lab->save_lab_test($lab_test_data,$item_data,$lab_test_id);
			if($lab_test_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Lab test information saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save lab test information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function suggest_category()
	{
		$suggestions = $this->Lab->get_category_suggestions($this->input->post('q'));
		echo implode("\n",$suggestions);
	}

	function delete_lab_test($lab_test_id)
	{
		$lab_test_id = $this->Lab->delete_lab_test($lab_test_id);
		if($lab_test_id):
			echo json_encode(array('success'=>true,'message'=>'Lab test deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('success'=>false,'message'=>'Unable to delete lab test.','message_class'=>'error_message'));
		endif;
	}

	function new_request()
	{
		$this->session->unset_userdata('selected_lab_tests');
		$data['lab_tests'] = $this->Lab->lab_tests();
		$this->load->view('lab/lab_request_form',$data);
	}

	function add_lab_tests()
	{
		$lab_test_ids = $this->input->post("lab_tests");		
		$lab_tests = array();

		foreach($lab_test_ids as $lab_test_id):
			$lab_test_info = $this->Lab->get_lab_test_info($lab_test_id);
			if($lab_test_info):
				$lab_tests[$lab_test_info->lab_test_id] = array(
					'lab_test_id'=>$lab_test_info->lab_test_id,
					'value'=>$lab_test_info->value,
					'item_id'=>$this->Item->get_info('LAB'.$lab_test_info->lab_test_id)->item_id,
					'status'=>0,
					'invoice_status'=>0,
					);
			endif;
		endforeach;

		$this->session->set_userdata('selected_lab_tests',$lab_tests);
	}

	function refresh_lab_request()
	{
		$data['lab_tests'] = $this->Lab->lab_tests();
		$data['selected_lab_tests'] = $this->session->userdata('selected_lab_tests');
		$this->load->view("lab/lab_request",$data);
	}

	function patient_search()
	{
		$suggestions = $this->Patient->get_patient_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function refresh_patient_info($patient_id = null)
	{
		$selected_lab_tests = $this->session->userdata('selected_lab_tests') ? $this->session->userdata('selected_lab_tests') : array();
		$lab_tests = $patient_id ? $this->Lab->get_invoice_lab_tests($patient_id) : array();
		foreach($lab_tests as $lab_test):
			$lab_test_id = $this->Lab->get_lab_test_id($lab_test->item_id);
			$selected_lab_tests[$lab_test_id] = array(
				'lab_test_id'=>$lab_test_id,
				'value'=>$this->Lab->get_lab_test_info($lab_test_id)->value,
				'remarks'=>$lab_test->remarks,
				'results'=>unserialize($lab_test->results),
				'item_id'=>$lab_test->item_id,
				'status'=>$lab_test->status,
				'invoice_id'=>$lab_test->invoice_id,
				'invoice_status'=>$lab_test->invoice_status,
				);
		endforeach;

		$this->session->set_userdata('selected_lab_tests',$selected_lab_tests);

		$data['patient_id'] = $patient_id;
		$this->load->view("lab/patient_info",$data);
	}

	function new_patient()
	{
		$this->load->view('lab/new_patient');
	}

	function save_patient()
	{
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('email', 'Email', 'valid_email');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$patient_data = array(
			'first_name'=>$this->input->post('first_name'),
			'last_name'=>$this->input->post('last_name'),
			'middle_name'=>$this->input->post('middle_name'),
			'gender'=>$this->input->post('gender'),
			'residence'=>$this->input->post('residence'),
			'national_id'=>$this->input->post('national_id'),
			'dob'=>$this->input->post('dob'),
			'phone_number'=>$this->input->post('phone_number'),
			'email'=>$this->input->post('email'),
			'address_1'=>$this->input->post('address_1'),
			'address_2'=>$this->input->post('address_2'),
			'city'=>$this->input->post('city'),
			'state'=>$this->input->post('state'),
			'zip'=>$this->input->post('zip'),
			'country'=>$this->input->post('country'),
			);

			$patient_id = $this->Patient->save($patient_data);
			if($patient_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Patient information saved successfully.','message_class'=>'success_message','patient_id'=>$patient_id));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save patient information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function save_lab_request($patient_id)
	{
		$lab_tests = $this->session->userdata('selected_lab_tests');
		$employee = $this->Employee->get_logged_in_employee_info();
		$invoice_data = array();
		$invoice_item_data = array();
		$invoice_id = $this->Invoice->already_invoiced($patient_id,$employee->person_id,'Lab');

		if(count($lab_tests) > 0):
			foreach($lab_tests as $item):
				$invoice_item_data[] = array(
					'remarks'=>$item['remarks'],
					'status'=>$item['status'],
					'item_id'=>$item['item_id'],
					);
			endforeach;

			$invoice_data = array(
					'patient_id'=>$patient_id,
					'employee_id'=>$employee->person_id,
					'invoice_type'=>'Lab',
					);
			if($this->Lab->save_lab_tests($invoice_data,$invoice_item_data,$invoice_id)):
				echo json_encode(array('success'=>true,'message'=>'Lab request saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('success'=>false,'message'=>'Unable to save lab request.','message_class'=>'success_message'));
			endif;
		else:
			echo json_encode(array('success'=>false,'message'=>'No lab tests have been selected.','message_class'=>'error_message'));
		endif;
	}

	function view_workload()
	{
		$date = ($this->session->userdata('labs_workload_date')) ? $this->session->userdata('labs_workload_date') : array("start"=>date("Y-m-d 00:00:00"),"end"=>date("Y-m-d 23:59:59"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));

		$employee = $this->Employee->get_logged_in_employee_info();
		$data['employee'] = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);

		$data['hospital_workload'] = $this->Lab->workload($start_date,$end_date);
		$data['employee_workload'] = $this->Lab->workload($start_date,$end_date,$employee->person_id);
		$lab_tests_workload = $this->Lab->lab_tests_workload($start_date,$end_date);
		arsort($lab_tests_workload);
		$data['lab_tests_workload'] = $lab_tests_workload;

		$data["start_date"] = date("j/n/Y",strtotime($date["start"]));
		$data["end_date"] = date("j/n/Y",strtotime($date["end"]));
		
		$data["start_date_input"] = date("d-m-Y",strtotime($date["start"]));
		$data["end_date_input"] = date("d-m-Y",strtotime($date["end"]));

		$this->load->view("lab/workload",$data);
	}

	function workload_date_input()
	{
		$this->form_validation->set_rules('start_date', 'Start Date', 'callback_date_check|required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$start_date = isset($start_date) ? date("Y-m-d H:i:s",strtotime($start_date)) : date("Y-m-d H:i:s");
			$end_date = isset($end_date) ? date("Y-m-d H:i:s",strtotime($end_date)) : $start_date;
			$date = array("start"=>$start_date,"end"=>$end_date);
			
			$this->session->set_userdata('labs_workload_date', $date);
			echo json_encode(array('form_validation'=>true));
		endif;
	}
}
?>