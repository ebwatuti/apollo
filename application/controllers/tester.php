<?php
//require(APPPATH.'/libraries/REST_Controller.php');
class Tester extends CI_Controller  
{
	function __construct()
	{
		//parent::__construct('reception');
		parent::__construct();	
		$this->load->library('curl');
		//error_reporting(E_ALL);
		//$this->output->enable_profiler(TRUE);
	}

	function index(){
		echo serialize(array("value one"=>1));
	}

	function curler()
	{
		$ch = curl_init();

		$data = array(  
		    'variable' => 'A00.-',
		    'conditions' => 'cough,blood,',
		    'label'=>'T047',
		    'limit'=>5
		);
		//$params = http_build_query($params, NULL, '&');

		curl_setopt($ch, CURLOPT_URL, 'http://daville91.pythonanywhere.com/bayes/');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		echo curl_exec($ch);
	}

	function json_data()
	{
		$data = json_decode('{"data": "{\"Mary\": 7, \"Rose\": 5, \"Macy\": 0, \"David\": 0, \"Fred\": 2, \"Triza\": 2}"}');
		print_r(json_decode($data->data));
	}

	function codes()
	{
		$codes = "[";
		$this->db->from('morbidity_categories');
		$categories = $this->db->get()->result();

		foreach($categories as $category):
			$category_codes = unserialize($category->category_codes);
			$morbidity_codes = unserialize($category->morbidity_codes);

			$this->db->distinct('diagnosis_code,description');
			$this->db->from('consultation_icd10');
			$this->db->where_in('diagnosis_code',$category_codes);
			$this->db->or_where_in('morbidity',$morbidity_codes);
			$icd_codes = $this->db->get()->result();

			foreach($icd_codes as $icd_code):
				$codes .= "'$icd_code->diagnosis_code',";
			endforeach;
		endforeach;
		$codes .= "]";
		echo $codes;
	}

	function email()
	{
		$this->load->library('email');

		$config['protocol'] = 'smtp';
		$config['smtp_host'] = 'smtp.gmail.com';
		$config['smtp_user'] = '_____@gmail.com';
		$config['smtp_pass'] = '_______';
		$config['smtp_port'] = '465';
		$config['smtp_timeout'] = '100';
		$config['smtp_crypto'] = 'ssl';
		$config['crlf']     = "\r\n";
		$config['newline']  = "\r\n";

		$this->email->initialize($config);

		$this->email->from('bakbandika@yahoo.com');
		$this->email->to('daudithuks@gmail.com'); 

		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');	

		if($this->email->send()) echo "Success";
		echo $this->email->print_debugger();
	}
}
?>