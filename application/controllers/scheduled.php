<?php
class Scheduled extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		//$this->output->enable_profiler(TRUE);
		//error_reporting(E_ALL);
	}
	
	function index()
	{
		
	}

	#Schedule every 24h
	function backup()
	{
		$this->output->enable_profiler(FALSE);
		$host = $this->db->hostname;
		$user = $this->db->username;
		$pass = $this->db->password;
		$db = $this->db->database;

		$dump = shell_exec("mysqldump --compact --opt --max-allowed-packet=1G -C --host=$host --user=$user --password=$pass --databases $db");
		$this->load->library('zip');
		$this->zip->add_data($db.'.sql',$dump);
		$this->zip->archive('./database/'.$db.'.zip');

		#$this->load->helper('file');
		#write_file('./database/'.$db.'.sql', $dump);
	}

	#Schedule every 24h
	function queue_clean()
	{
		$yesterday = date("Y-m-d H:i:s",mktime(date("H"), date("i"), date("s"), date("m")  , date("d")-1, date("Y")));	
		
		$this->db->where('encounter_start <',"$yesterday");
		$this->db->where('encounter_date <',"$yesterday");
		$this->db->where('encounter_status !=','5');
		$this->db->set('encounter_status','5');
		$this->db->update('encounter');
		
		$this->db->where('consultation_start <',"$yesterday");
		$this->db->where('consultation_status !=','3');
		$this->db->set('consultation_status','3');
		$this->db->update('consultation');
		
		$this->db->where('invoice_time <',"$yesterday");
		$this->db->where('invoice_status','0');
		$this->db->where('invoice_type','Registration');
		$this->db->delete('invoices');

		$week_ago = date("Y-m-d H:i:s",mktime(date("H"), date("i"), date("s"), date("m")  , date("d")-7, date("Y")));
		$this->db->where('invoice_time <',"$week_ago");
		$this->db->where('invoice_status','0');
		$this->db->set('invoice_status','5');
		$this->db->update('invoices');
	}

	#Schedule every 30m
	function active_users()
	{
		$activities = $this->Employee->active_users();
		
		if($activities->num_rows() > 0):
			foreach($activities->result() as $activity):
				if(date_diff(date_create($activity->activity_end),date_create(date('Y-m-d H:i:s')),true)->format('%i') > MAX_INACTIVE_PERIOD):
					$activity_data = array(
						'activity_end'=>date('Y-m-d H:i:s'),
						'online'=>0
					);
					$this->Employee->save_activity($activity_data,$activity->activity_id);
				endif;
			endforeach;
		endif;
	}

	function activity()
	{
		if($this->Employee->is_logged_in()):
			$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
			
			$activity = $this->Employee->get_activity($employee_id);
			
			if($activity->activity_id):
				if(date_diff(date_create($activity->activity_end),date_create(date('Y-m-d H:i:s')),true)->format('%i') > MAX_INACTIVE_PERIOD):
					$activity_data = array(
						'activity_end'=>date('Y-m-d H:i:s'),
						'online'=>0
					);
					$this->Employee->save_activity($activity_data,$activity->activity_id);
					
					$activity_data = array(
						'activity_end'=>date('Y-m-d H:i:s'),
						'employee_id'=>$employee_id,
						'ip_address'=>$this->input->ip_address()
					);
					$this->Employee->save_activity($activity_data);
				else:
					$activity_data = array(
						'activity_end'=>date('Y-m-d H:i:s'),
						'ip_address'=>$this->input->ip_address()
					);
					$this->Employee->save_activity($activity_data,$activity->activity_id);
				endif;
			else:
				$activity_data = array(
					'activity_end'=>date('Y-m-d H:i:s'),
					'employee_id'=>$employee_id,
					'ip_address'=>$this->input->ip_address()
				);
				$this->Employee->save_activity($activity_data);
			endif;
			echo json_encode(array('active'=>true));
		else:
			echo json_encode(array('active'=>false));
		endif;
	}

	function current_time()
	{
		echo date('M j, Y g:i a');
	}
}
?>