<?php
require_once ("secure_area.php");
class Reception extends Secure_area
{
	function __construct()
	{
		parent::__construct('reception');
		//error_reporting(E_ALL);
		//$this->output->enable_profiler(TRUE);
	}

	function index()
	{
		$this->load->view('reception/main');
	}

	function view_patient($patient_id = -1)
	{
		if($patient_id != -1) $data['patient_info'] = $this->Patient->get_info($patient_id);
		$institutions = array('' => '');
		foreach($this->Institution->get_all()->result_array() as $row)
		{
			$institutions[$row['institution_id']] = $row['institution_name'];
		}
		$data['institutions'] = $institutions;

		$this->load->view('reception/patient_form',$data);
	}

	function save_patient($patient_id = null)
	{
		$this->form_validation->set_rules('patient_id', 'Patient #', 'integer');
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('email', 'Email', 'valid_email');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$patient_data = array(
			'first_name'=>$this->input->post('first_name'),
			'last_name'=>$this->input->post('last_name'),
			'middle_name'=>$this->input->post('middle_name'),
			'gender'=>$this->input->post('gender'),
			#'residence'=>$this->input->post('residence'),
			'national_id'=>$this->input->post('national_id'),
			'dob'=>$this->input->post('dob'),
			'phone_number'=>$this->input->post('phone_number'),
			'email'=>$this->input->post('email'),
			#'address_1'=>$this->input->post('address_1'),
			#'address_2'=>$this->input->post('address_2'),
			#'city'=>$this->input->post('city'),
			#'state'=>$this->input->post('state'),
			#'zip'=>$this->input->post('zip'),
			#'country'=>$this->input->post('country'),
			'institution_id'=>$this->input->post('institution_id'),
			);

			if($this->input->post('patient_id')):
				$patient_id = $this->input->post('patient_id');
				$patient_data['patient_id'] = $patient_id;
			endif;
				
			

			$patient_id = $this->Patient->save($patient_data,$patient_id);
			if($patient_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Patient information saved successfully.','message_class'=>'success_message','patient_id'=>$patient_id));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save patient information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function allocate_patient($patient_id)
	{
		$this->form_validation->set_rules('clinic', 'Clinic', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$triage = ($this->input->post('triage')==1) ? 1 : 0;
			$review = ($this->input->post('review')==1) ? 1 : 0;
			$last_visit = $this->Encounter->last_visit($patient_id);
			#$encounter_status = ($review !== 1) ? 0 : 2 - $triage;
			$encounter_status = 2 - $triage;
			$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
			$encounter_type = $this->input->post('clinic');

			$encounter_data = array(
				'patient_id' => $patient_id,
				'encounter_type' => $encounter_type,
				'encounter_status' => $encounter_status,
				'triage' => $triage,
				'review' => $review, 
				'priority' => $this->input->post('priority'),
				'employee_id' => $employee_id,
				);
			if(date('Y-m-d', strtotime($this->input->post('encounter_date'))) > date('Y-m-d')):
				$encounter_data['encounter_date'] = date('Y-m-d', strtotime($this->input->post('encounter_date')));
			else:
				$encounter_data['encounter_date'] = date('Y-m-d');
			endif;

			$encounter_id = $this->Encounter->check_encounter($patient_id);
			$encounter = $this->Encounter->update_encounter($encounter_data,$encounter_id);

			if(!$encounter_id && !$review && !in_array($encounter_type, array('lab','nursing','pharmacy'))):
				$invoice_id = $this->Invoice->already_invoiced($patient_id,$employee_id,'Registration');
				$item = $this->Item->get_info($this->input->post('clinic'));
				$invoice_data = array(
					'patient_id'=>$patient_id,
					'employee_id'=>$employee_id,
					'encounter_id'=>$encounter_id,
					'invoice_type'=>'Registration',
					);
				$invoice_items_data = array(
					array(
						'item_id'=>$item->item_id,
					),
					);

				$invoice = $this->Invoice->save($invoice_data,$invoice_items_data,$invoice_id);
			else:
				$invoice = true;
			endif;
			if($encounter && $invoice):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Patient allocated successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to allocate patient.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function view_all()
	{
		$data['patients'] = $this->Patient->get_all();

		$this->load->view('reception/patients_table',$data);
	}

	function refresh_table()
	{
		if ( isset( $_POST['iDisplayStart'] ) && $_POST['iDisplayLength'] != '-1' ):
			$limit = intval( $_POST['iDisplayLength'] );
			$offset = intval( $_POST['iDisplayStart'] );
		endif;
		if ( isset($_POST['sSearch']) && $_POST['sSearch'] != "" ) $search = $_POST['sSearch'];
		
		$patients = $this->Patient->get_all($limit,$offset,$search);

		$output = array(
			"sEcho" => intval($this->input->post('sEcho')),
			"iTotalRecords" => $this->Patient->count_all(),
			"iTotalDisplayRecords" => $patients->num_rows(),
			"aaData" => array()
		);

		foreach($patients->result() as $patient):
			$patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
	        if($patient->dob) $patient->age = $this->Patient->patient_age($patient->dob);
	        $patient->patient_no = $this->Patient->patient_number($patient->patient_id);
	        $patient->last_encounter = $this->Encounter->last_visit($patient->patient_id);
	        $institution = $this->Institution->get_patient_institution($patient->patient_id);
	        $patient->institution = $institution ? $institution->institution_name : '';

	        if($patient->last_encounter) $patient->last_visit = date("M j, Y",strtotime($patient->last_encounter->encounter_start));
			$output['aaData'][] = array(
				$patient->patient_no,
				$patient->patient_name,
				$patient->age,
				$patient->phone_number,
				$patient->institution,
				$patient->last_visit,
				anchor("reception/view_patient/$patient->patient_id/width:750",
                    form_button(array('name'=>'submit','content'=>'View','class'=>'DTTT_button float_left')),
                    array("class"=>"thickbox vote_thickbox","id"=>"thickbox_$patient->patient_id","title"=>"Patient Details"))
				. '&emsp;' . 
				form_button(array(
                    'name'=>'submit',
                    'onclick'=>'delete_patient('.$patient->patient_id.')',
                    'content'=>'Delete',
                    'class'=>'DTTT_button float_left'))
				);
		endforeach;
		
		echo json_encode($output);
	}

	function delete_patient($patient_id)
	{
		$patient_id = $this->Patient->delete($patient_id);
		if($patient_id):
			echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Patient information deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to delete patient information.','message_class'=>'error_message'));
		endif;
	}

	function delete_encounter($encounter_id)
	{
		$encounter_id = $this->Encounter->delete_encounter($encounter_id);
		if($encounter_id):
			echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Visit information deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to delete visit information.','message_class'=>'error_message'));
		endif;
	}

	public function date_check()
	{
		$start_date = strtotime($this->input->post('start_date'));
		$end_date = strtotime($this->input->post('end_date'));
		if ($start_date > $end_date):
			$this->form_validation->set_message('date_check', 'Start date cannot be later than end date');
			return FALSE;
		else:
			return TRUE;
		endif;
	}

	function view_workload()
	{
		$date = ($this->session->userdata('reception_workload_date')) ? $this->session->userdata('reception_workload_date') : array("start"=>date("Y-m-d 00:00:00"),"end"=>date("Y-m-d 23:59:59"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));

		$employee = $this->Employee->get_logged_in_employee_info();
		$data['employee'] = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);

		$data['hospital_visits'] = $this->Encounter->workload($start_date,$end_date);
		$data['employee_visits'] = $this->Encounter->workload($start_date,$end_date,$employee->person_id);
		$data['reviews'] = $this->Encounter->visits($start_date,$end_date,1);
		$data['normal'] = $this->Encounter->visits($start_date,$end_date,0);
		$data['clinics'] = array();
		$clinics = $this->Consultation->get_clinics();
		foreach($clinics as $clinic):
			$data['clinics'][$clinic->clinic_code]['visits'] = $this->Encounter->clinic_workload($start_date,$end_date,$clinic->clinic_code);
			$data['clinics'][$clinic->clinic_code]['reviews'] = $this->Encounter->visits($start_date,$end_date,TRUE,$clinic->clinic_code);
			$data['clinics'][$clinic->clinic_code]['normal'] = $this->Encounter->visits($start_date,$end_date,FALSE,$clinic->clinic_code);
		endforeach;

		$data['depts'] = array();
	    $data['depts']['Lab'] = $this->Encounter->clinic_workload($start_date,$end_date,'lab');
	    $data['depts']['Pharmacy'] = $this->Encounter->clinic_workload($start_date,$end_date,'pharmacy');
	    $data['depts']['Nursing'] = $this->Encounter->clinic_workload($start_date,$end_date,'nursing');


		$data["start_date"] = date("j/n/Y",strtotime($date["start"]));
		$data["end_date"] = date("j/n/Y",strtotime($date["end"]));
		
		$data["start_date_input"] = date("d-m-Y",strtotime($date["start"]));
		$data["end_date_input"] = date("d-m-Y",strtotime($date["end"]));

		$this->load->view("reception/workload",$data);
	}

	function workload_date_input()
	{
		$this->form_validation->set_rules('start_date', 'Start Date', 'callback_date_check|required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$start_date = isset($start_date) ? date("Y-m-d H:i:s",strtotime($start_date)) : date("Y-m-d H:i:s");
			$end_date = isset($end_date) ? date("Y-m-d H:i:s",strtotime($end_date)) : $start_date;
			$date = array("start"=>$start_date,"end"=>$end_date);
			
			$this->session->set_userdata('reception_workload_date', $date);
			echo json_encode(array('form_validation'=>true));
		endif;
	}

	function view_archive()
	{
		$date = ($this->session->userdata('reception_archive_date')) ? $this->session->userdata('reception_archive_date') : array("start"=>date("Y-m-d 00:00:00"),"end"=>date("Y-m-d 23:59:59"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));
		$clinic = $this->session->userdata('reception_archive_clinic');
		$data['encounters'] = $this->Encounter->get_encounters($start_date,$end_date,$clinic);
		$data["start_date"] = date("j/n/Y",strtotime($date["start"]));
		$data["end_date"] = date("j/n/Y",strtotime($date["end"]));
		
		$data["start_date_input"] = date("d-m-Y",strtotime($date["start"]));
		$data["end_date_input"] = date("d-m-Y",strtotime($date["end"]));

		$data['clinics'] = $this->Consultation->get_clinics();
		$data['clinic'] = $clinic;

		$this->load->view("reception/archive",$data);
	}

	function archive_date_input()
	{
		$this->form_validation->set_rules('start_date', 'Start Date', 'callback_date_check|required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$start_date = isset($start_date) ? date("Y-m-d H:i:s",strtotime($start_date)) : date("Y-m-d H:i:s");
			$end_date = isset($end_date) ? date("Y-m-d H:i:s",strtotime($end_date)) : $start_date;
			$date = array("start"=>$start_date,"end"=>$end_date);
			
			$this->session->set_userdata('reception_archive_date', $date);
			$this->session->set_userdata('reception_archive_clinic', $this->input->post('clinic'));
			echo json_encode(array('form_validation'=>true));
		endif;
	}
}
?>
