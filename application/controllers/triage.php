<?php
require_once ("secure_area.php");
class Triage extends Secure_area
{
	function __construct()
	{
		parent::__construct('triage');
		//error_reporting(E_ALL);
		//$this->output->enable_profiler(TRUE);
	}

	function index()
	{
		$this->load->view('triage/main');
	}

	function view_triage($patient_id)
	{
		$patient = $this->Patient->get_info($patient_id);
		$patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
    	$patient->age = $this->Patient->patient_age($patient->dob);
    	$patient->patient_no = $this->Patient->patient_number($patient->patient_id);
    	$patient->last_encounter = $this->Encounter->last_visit($patient->patient_id);
    	if($patient->last_encounter) $patient->last_visit = date("M j, Y",strtotime($patient->last_encounter->encounter_start));
		$data['patient'] = $patient;
		$encounter_id = $this->Encounter->check_encounter($patient_id);
		$data['encounter_info'] = $this->Encounter->get_info($encounter_id);

		$this->load->view('triage/triage_form',$data);
	}

	function save_triage($patient_id)
	{
		$this->form_validation->set_rules('temperature', 'Temperature', 'required|numeric');
		$this->form_validation->set_rules('weight', 'Weight', 'required|numeric');
		$this->form_validation->set_rules('pulse_rate', 'Pulse rate', 'required|numeric');
		$this->form_validation->set_rules('blood_pressure_systolic', 'Systolic BP', 'required|numeric');
		$this->form_validation->set_rules('blood_pressure_diastolic', 'Diastolic BP', 'required|numeric');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$encounter_id = $this->Encounter->check_encounter($patient_id);
			$employee = $this->Employee->get_logged_in_employee_info();
			$triage_details = array(
			'temperature'=>$this->input->post('temperature'),
			'weight'=>$this->input->post('weight'),
			'pulse rate'=>$this->input->post('pulse_rate'),
			'blood pressure'=>array(
					'systolic'=>$this->input->post('blood_pressure_systolic'),
					'diastolic'=>$this->input->post('blood_pressure_diastolic'),
				),
			'other remarks'=>$this->input->post('other_remarks'),
			);
			$triage_data = array(
					'triage_details'=>serialize($triage_details),
					'patient_id'=>$patient_id,
					'encounter_id'=>$encounter_id,
					'priority'=>$this->input->post('priority'),
					'employee_id'=>$employee->person_id,
				);

			if($this->Triaging->save($triage_data,$patient_id)):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Triage information saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save triage information.','message_class'=>'error_message'));
			endif;
		endif;
	}	

	function view_all()
	{
		$data['main_queue'] = $this->Triaging->get_main_queue();
		$data['completed_queue'] = $this->Triaging->get_completed_queue();

		$this->load->view('triage/patients_table',$data);
	}


	function triage_chart($encounter_id){
		$data['triage_history'] = $this->Triaging->get_info($encounter_id);

		$this->load->view('triage/triage_chart',$data);
	}



    function view_archive()
	{
		$date = ($this->session->userdata('triage_archive_date')) ? $this->session->userdata('triage_archive_date') : array("start"=>date("Y-m-d 00:00:00"),"end"=>date("Y-m-d 23:59:59"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));
		$data['triages'] = $this->Triaging->archive_triages($start_date,$end_date);
		$data["start_date"] = date("j/n/Y",strtotime($date["start"]));
		$data["end_date"] = date("j/n/Y",strtotime($date["end"]));
		
		$data["start_date_input"] = date("d-m-Y",strtotime($date["start"]));
		$data["end_date_input"] = date("d-m-Y",strtotime($date["end"]));

		$this->load->view("triage/archive",$data);
	}

	function archive_date_input()
	{
		$this->form_validation->set_rules('start_date', 'Start Date', 'callback_date_check|required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$start_date = isset($start_date) ? date("Y-m-d H:i:s",strtotime($start_date)) : date("Y-m-d H:i:s");
			$end_date = isset($end_date) ? date("Y-m-d H:i:s",strtotime($end_date)) : $start_date;
			$date = array("start"=>$start_date,"end"=>$end_date);
			
			$this->session->set_userdata('triage_archive_date', $date);
			echo json_encode(array('form_validation'=>true));
		endif;
	}

	public function date_check()
	{
		$start_date = strtotime($this->input->post('start_date'));
		$end_date = strtotime($this->input->post('end_date'));
		if ($start_date > $end_date):
			$this->form_validation->set_message('date_check', 'Start date cannot be later than end date');
			return FALSE;
		else:
			return TRUE;
		endif;
	}

	function view_workload()
	{
		$date = ($this->session->userdata('triage_workload_date')) ? $this->session->userdata('triage_workload_date') : array("start"=>date("Y-m-d 00:00:00"),"end"=>date("Y-m-d 23:59:59"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));

		$employee = $this->Employee->get_logged_in_employee_info();
		$data['employee'] = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);

		$data['hospital_workload'] = $this->Triaging->workload($start_date,$end_date);
		$where = 'employee_id = "'.$employee->person_id.'"';
		$data['employee_workload'] = $this->Triaging->workload($start_date,$end_date,$where);

		$data["start_date"] = date("j/n/Y",strtotime($date["start"]));
		$data["end_date"] = date("j/n/Y",strtotime($date["end"]));
		
		$data["start_date_input"] = date("d-m-Y",strtotime($date["start"]));
		$data["end_date_input"] = date("d-m-Y",strtotime($date["end"]));

		$this->load->view("triage/workload",$data);
	}

	function workload_date_input()
	{
		$this->form_validation->set_rules('start_date', 'Start Date', 'callback_date_check|required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$start_date = isset($start_date) ? date("Y-m-d H:i:s",strtotime($start_date)) : date("Y-m-d H:i:s");
			$end_date = isset($end_date) ? date("Y-m-d H:i:s",strtotime($end_date)) : $start_date;
			$date = array("start"=>$start_date,"end"=>$end_date);
			
			$this->session->set_userdata('triage_workload_date', $date);
			echo json_encode(array('form_validation'=>true));
		endif;
	}

	function triage_details($patient_id,$triage_id)
	{
		$patient = $this->Patient->get_info($patient_id);
		$patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
    	$patient->age = $this->Patient->patient_age($patient->dob);
    	$patient->patient_no = $this->Patient->patient_number($patient->patient_id);
		$data['patient'] = $patient;
		$data['triage_info'] = $this->Triaging->get_triage_info($triage_id);

		$this->load->view('triage/triage_details',$data);
	}
}
?>