<?php
require_once ("secure_area.php");
class Items extends Secure_area
{
	function __construct()
	{
		parent::__construct('items');
	}

	function index()
	{
		$this->load->view('items/main');
	}

	function view_all()
	{
		$data['items'] = $this->Item->get_all();

		$this->load->view('items/items_table',$data);
	}

	function refresh_table()
	{
		if ( isset( $_POST['iDisplayStart'] ) && $_POST['iDisplayLength'] != '-1' ):
			$limit = intval( $_POST['iDisplayLength'] );
			$offset = intval( $_POST['iDisplayStart'] );
		endif;
		if ( isset($_POST['sSearch']) && $_POST['sSearch'] != "" ) $search = $_POST['sSearch'];
		
		$items = $this->Item->get_all($limit,$offset,$search);

		$output = array(
			"sEcho" => intval($this->input->post('sEcho')),
			"iTotalRecords" => $this->Item->count_all(),
			"iTotalDisplayRecords" => $items->num_rows(),
			"aaData" => array()
		);

		foreach($items->result() as $item):
			$output['aaData'][] = array(
				$item->name,
				$item->category,
				$item->sub_category,
				$item->cost_price,
				$item->unit_price,
				(int)$item->store_quantity,
				(int)$item->quantity,
				anchor("items/view_item/$item->item_id/width:750",
                    form_button(array('name'=>'submit','content'=>'View','class'=>'DTTT_button float_left')),
                    array("class"=>"thickbox vote_thickbox","title"=>"Item Details"))
				. '&emsp;' . 
				anchor("items/inventory_trail/$item->item_id/width:600",
                    form_button(array('name'=>'submit','content'=>'Trail','class'=>'DTTT_button float_left')),
                    array("class"=>"thickbox vote_thickbox","title"=>"Inventory Trail"))
				. '&emsp;' . 
				form_button(array(
                    'name'=>'submit',
                    'onclick'=>'delete_item('.$item->item_id.')',
                    'content'=>'Delete',
                    'class'=>'DTTT_button float_left'))
				);
		endforeach;
		
		echo json_encode($output);
	}

	function suggest_category()
	{
		$suggestions = $this->Item->get_category_suggestions($this->input->post('q'));
		echo implode("\n",$suggestions);
	}

	function suggest_subcategory()
	{
		$suggestions = $this->Item->get_subcategory_suggestions($this->input->post('q'));
		echo implode("\n",$suggestions);
	}

	function suggest_location()
	{
		$suggestions = $this->Item->get_location_suggestions($this->input->post('q'));
		echo implode("\n",$suggestions);
	}

	function view_item($item_id = -1)
	{
		$data['item_info']=$this->Item->get_info($item_id);
		$suppliers = array('' => '');
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$row['person_id']] = $row['company_name'] ;
		}

		$data['suppliers']=$suppliers;

		$this->load->view("items/item_form",$data);
	}
	
	function inventory_trail($item_id=-1)
	{
		$data['item_info']=$this->Item->get_info($item_id);
		$this->load->view("items/inventory_trail",$data);
	}

	function save_item($item_id = null)
	{
		$this->form_validation->set_rules('name', 'Item Name', 'required');
		$this->form_validation->set_rules('item_number', 'Item #', 'callback_item_number_check['.$item_id.']');
		$this->form_validation->set_rules('category', 'Category', 'required');
		$this->form_validation->set_rules('reorder_level', 'Reorder Level', 'is_natural');
		$this->form_validation->set_rules('unit_price', 'Unit Price', 'required|numeric');
		$this->form_validation->set_rules('cost_price', 'Cost Price', 'numeric');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$item_data = array(
			'name'=>$this->input->post('name'),
			'description'=>$this->input->post('description'),
			'category'=>$this->input->post('category'),
			'sub_category'=>$this->input->post('sub_category'),
			'supplier_id'=>$this->input->post('supplier_id') ? $this->input->post('supplier_id') : null,
			'item_number'=>$this->input->post('item_number') ? $this->input->post('item_number') : null,
			'cost_price'=>$this->input->post('cost_price'),
			'unit_price'=>$this->input->post('unit_price'),
			'reorder_level'=>$this->input->post('reorder_level'),
			'location'=>$this->input->post('location')
			);

			$cur_item_info = $this->Item->get_info($item_id);
			$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
			$item_id = $this->Item->save($item_data,$item_id);
			if($item_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Item information saved successfully.','message_class'=>'success_message','item_id'=>$item_id));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save item information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	public function item_number_check($item_number,$item_id)
	{
		if ($this->Item->item_number_exists($item_number,$item_id)):
			$this->form_validation->set_message('item_number_check', 'Item # already exists');
			return FALSE;
		else:
			return TRUE;
		endif;
	}
	
	function save_inventory($item_id)
	{	
		$this->form_validation->set_rules('quantity', 'Issued qty to add/subtract', 'integer');
		$this->form_validation->set_rules('store_quantity', 'Qty in store to add/subtract', 'integer');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
			$cur_item_info = $this->Item->get_info($item_id);
			$store_quantity = $this->input->post('store_quantity') ? $this->input->post('store_quantity') : 0;
			$quantity = $this->input->post('quantity') ? $this->input->post('quantity') : 0;
			
			$item_data = array(
			'store_quantity'=>$cur_item_info->store_quantity + $store_quantity,
			'quantity'=>$cur_item_info->quantity + $quantity,
			);
			$item_id = $this->Item->save($item_data,$item_id);
			if($item_id):
				$inv_data = array
				(
					'trans_date'=>date('Y-m-d H:i:s'),
					'trans_items'=>$item_id,
					'trans_user'=>$employee_id,
					'trans_comment'=>trim($this->input->post('trans_comment')) ? $this->input->post('trans_comment') : 'Manual edit of quantity.',
					'trans_inventory'=>$this->input->post('quantity')
				);
				//$this->Inventory->insert($inv_data);
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Item information saved successfully.','message_class'=>'success_message','item_id'=>$item_id));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save item information.','message_class'=>'error_message'));
			endif;
		endif;

	}

	function delete_item($item_id)
	{
		$item_id = $this->Item->delete($item_id);
		if($item_id):
			echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Item information deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to delete item information.','message_class'=>'error_message'));
		endif;
	}

	function view_procedures()
	{
		$data['procedures'] = $this->Consultation->get_procedures();
		$this->load->view('items/procedures_table',$data);
	}

	function view_procedure($procedure_id = -1)
	{
		if($procedure_id != -1) $data['procedure_info'] = $this->Consultation->get_procedure_info($procedure_id);

		$data['clinics'] = $this->Consultation->get_clinics();
		$this->load->view('items/procedure_form',$data);
	}

	function save_procedure($procedure_id = null)
	{
		$this->form_validation->set_rules('procedure_name', 'Procedure Name', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required|numeric');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$procedure_data = array(
			'name'=>$this->input->post('procedure_name'),
			'clinic'=>$this->input->post('clinic')
			);

			switch ($this->input->post('clinic')) {
				case 'clinic4':
					$category = 'Dental';
					$sub_category = '';
					break;
				case 'nursing':
					$category = 'Nursing';
					$sub_category = $this->input->post('procedure_name');
					break;
				default:
					$category = 'Medical Services';
					$sub_category = $this->Consultation->clinic_name($this->input->post('clinic'));
					break;
			}
			$item_data = array(
			'name'=>$this->input->post('procedure_name'),
			'category'=>$category,
			'sub_category'=>$sub_category,
			'quantity'=>1,
			'quantifiable'=>0,
			'unit_price'=>$this->input->post('price'),
			);

			$procedure_id = $this->Consultation->save_procedure($procedure_data,$item_data,$procedure_id);
			if($procedure_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Procedure information saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save procedure information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function delete_procedure($procedure_id)
	{
		$procedure_id = $this->Consultation->delete_procedure($procedure_id);
		if($procedure_id):
			echo json_encode(array('success'=>true,'message'=>'Procedure deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('success'=>false,'message'=>'Unable to delete procedure.','message_class'=>'error_message'));
		endif;
	}

	function view_lab_tests()
	{
		$data['lab_tests'] = $this->Lab->lab_tests();

		$this->load->view('items/lab_tests',$data);
	}

	function view_lab_test($lab_test_id = -1)
	{
		if($lab_test_id) $data['lab_test'] = $this->Lab->get_lab_test_info($lab_test_id);

		$this->load->view('items/lab_test_form',$data);
	}

	function save_lab_test($lab_test_id = null)
	{
		$this->form_validation->set_rules('test_name', 'Test Name', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required|numeric');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$lab_test_data = array(
			'value'=>$this->input->post('test_name'),
			'category'=>$this->input->post('category'),
			'options'=>$this->input->post('options')
			);
			$item_data = array(
			'name'=>$this->input->post('test_name'),
			'category'=>'Lab',
			'sub_category'=>$this->input->post('category'),
			'quantity'=>1,
			'quantifiable'=>0,
			'unit_price'=>$this->input->post('price'),
			);

			$lab_test_id = $this->Lab->save_lab_test($lab_test_data,$item_data,$lab_test_id);
			if($lab_test_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Lab test information saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save lab test information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function suggest_lab_category()
	{
		$suggestions = $this->Lab->get_category_suggestions($this->input->post('q'));
		echo implode("\n",$suggestions);
	}

	function delete_lab_test($lab_test_id)
	{
		$lab_test_id = $this->Lab->delete_lab_test($lab_test_id);
		if($lab_test_id):
			echo json_encode(array('success'=>true,'message'=>'Lab test deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('success'=>false,'message'=>'Unable to delete lab test.','message_class'=>'error_message'));
		endif;
	}	

	function view_drugs_list()
	{
		$data['drugs_list'] = $this->Pharm->drugs_list(10);

		$this->load->view('items/drugs_list',$data);
	}

	function refresh_drugs_list()
	{
		if ( isset( $_POST['iDisplayStart'] ) && $_POST['iDisplayLength'] != '-1' ):
			$limit = intval( $_POST['iDisplayLength'] );
			$offset = intval( $_POST['iDisplayStart'] );
		endif;
		if ( isset($_POST['sSearch']) && $_POST['sSearch'] != "" ) $search = $_POST['sSearch'];
		
		$drugs = $this->Pharm->drugs_list($limit,$offset,$search);

		$output = array(
			"sEcho" => intval($this->input->post('sEcho')),
			"iTotalRecords" => $this->Pharm->count_all_drugs(),
			"iTotalDisplayRecords" => $drugs->num_rows(),
			"aaData" => array()
		);

		foreach($drugs->result() as $drug):
			$drug = $this->Pharm->get_drug_info($drug->drug_id);
			$output['aaData'][] = array(
				$drug->generic_name,
				$drug->brand_name,
				$drug->class,
				$drug->item_info->cost_price,
				$drug->item_info->unit_price,
				(int)$drug->item_info->store_quantity,
				(int)$drug->item_info->quantity,
				form_button(array(
	                'name'=>'submit',
	                'onclick'=>'edit_drug('.$drug->drug_id.')',
	                'content'=>'Edit',
	                'class'=>'DTTT_button float_left')
	            )
				. '&emsp;' . 
				form_button(array(
	                'name'=>'submit',
	                'onclick'=>'inventory('.$drug->item_info->item_id.')',
	                'content'=>'Trail',
	                'class'=>'DTTT_button float_left')
	            )
				. '&emsp;' . 
				form_button(array(
	                'name'=>'submit',
	                'onclick'=>'delete_drug('.$drug->drug_id.')',
	                'content'=>'Delete',
	                'class'=>'DTTT_button float_left')
	            )
	        );
		endforeach;
		
		echo json_encode($output);
	}

	function view_drug($drug_id = -1)
	{
		if($drug_id) $data['drug'] = $this->Pharm->get_drug_info($drug_id);
		$suppliers = array('' => '');
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$row['person_id']] = $row['company_name'] ;
		}

		$data['suppliers']=$suppliers;

		$this->load->view('items/drug_form',$data);
	}

	function save_drug($drug_id = null)
	{
		$this->form_validation->set_rules('brand_name', 'Brand Name', 'required');
		$this->form_validation->set_rules('unit_price', 'Unit Price', 'required|numeric');
		$this->form_validation->set_rules('cost_price', 'Cost Price', 'numeric');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$drug_data = array(
			'generic_name'=>$this->input->post('generic_name'),
			'brand_name'=>$this->input->post('brand_name'),
			'class'=>$this->input->post('class'),
			'strength'=>$this->input->post('strength')
			);
			$item_data = array(
			'name'=>$this->input->post('generic_name') ? $this->input->post('brand_name').'('.$this->input->post('generic_name').')' : $this->input->post('brand_name'),
			'category'=>'Pharmacy',
			'sub_category'=>$this->input->post('class'),
			'supplier_id'=>$this->input->post('supplier_id') ? $this->input->post('supplier_id') : null,
			'quantifiable'=>1,
			'unit_price'=>$this->input->post('unit_price'),
			'cost_price'=>$this->input->post('cost_price'),
			);

			$drug_id = $this->Pharm->save_drug($drug_data,$item_data,$drug_id);
			if($drug_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Drug information saved successfully.','message_class'=>'success_message','drug_id'=>$drug_id));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save drug information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function suggest_class()
	{
		$suggestions = $this->Pharm->get_class_suggestions($this->input->post('q'));
		echo implode("\n",$suggestions);
	}

	function delete_drug($drug_id)
	{
		$drug_id = $this->Pharm->delete_drug($drug_id);
		if($drug_id):
			echo json_encode(array('success'=>true,'message'=>'Drug deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('success'=>false,'message'=>'Unable to delete drug.','message_class'=>'error_message'));
		endif;
	}

	function view_clinics()
	{
		$data['clinics'] = $this->Administration->get_clinics();
		$this->load->view('items/clinics_table',$data);
	}

	function view_clinic($clinic_id = -1)
	{
		if($clinic_id != -1) $data['clinic_info'] = $this->Administration->get_clinic_info($clinic_id);

		$this->load->view('items/clinic_form',$data);
	}

	function save_clinic($clinic_id = null)
	{
		$this->form_validation->set_rules('price', 'Consultation Fee', 'required|numeric');
		$this->form_validation->set_rules('clinic_name', 'Clinic Name', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$clinic_data = array(
				'clinic_name'=>ucwords(strtolower($this->input->post('clinic_name'))),
			);
			$item_data = array(
				'name'=>ucwords(strtolower($this->input->post('clinic_name'))),
				'category'=>'Medical Services',
				'sub_category'=>$this->input->post('clinic_name'),
				'quantity'=>1,
				'quantifiable'=>0,
				'unit_price'=>$this->input->post('price'),
			);

			$clinic_id = $this->Administration->save_clinic($clinic_data,$item_data,$clinic_id);
			if($clinic_id):
				$clinic_code = 'clinic'.$clinic_id;
				$this->clinic_controller($clinic_code,$this->input->post('clinic_name'));
				$this->clinic_module($this->input->post('clinic_name'));
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Clinic information saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save clinic information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function delete_clinic($clinic_id)
	{
		$clinic_id = $this->Administration->delete_clinic($clinic_id);
		if($clinic_id):
			echo json_encode(array('success'=>true,'message'=>'Clinic deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('success'=>false,'message'=>'Unable to delete clinic.','message_class'=>'error_message'));
		endif;
	}

	function clinic_controller($clinic_code,$clinic_name)
	{
		$module_id = strtolower(preg_replace('/[^a-zA-Z0-9_]/s', '_',$clinic_name));
		$controller_name = ucfirst($module_id);
		$file_data = 
"<?php
require_once('consultant.php');
class $controller_name extends Consultant {			
	function __construct()
	{
		parent::__construct('$module_id','$clinic_code');
	}
}
?>";

		$this->load->helper('file');
		if(!read_file("./application/controllers/$module_id.php")):
			return write_file("./application/controllers/$module_id.php", $file_data);
		endif;
	}

	function clinic_module($clinic_name)
	{
		$module_id = strtolower(preg_replace('/[^a-zA-Z0-9_]/s', '_',$clinic_name));
		$module_data = array(
			'module_id'=>$module_id,
			'sort'=>50,
			'name'=>$clinic_name,
		);

		if($this->Administration->module_exists($module_id)):
			$module_id = $this->Administration->save_module($module_data,$module_id);
		else:
			$module_id = $this->Administration->save_module($module_data);
		endif;

		$this->load->helper('file');
		$module_icon = read_file("./images/menubar/$module_id.png");
		if(!$module_icon):
			$module_icon = read_file("./images/menubar/consultant.png");
			$module_icon = write_file("./images/menubar/$module_id.png",$module_icon);
		endif;
		return $module_id && $module_icon;
	}

	function view_orders()
	{
		$data['pending_orders'] = $this->Order->get_pending_orders();
		$data['processed_orders'] = $this->Order->get_processed_orders();

		$this->load->view('items/orders_table',$data);
	}

	function view_order($order_id)
	{
		$this->session->unset_userdata('order_items');
		if($order_id) $order = $this->Order->get_info($order_id);
		foreach($order->order_items as $item):
			$item_info = $this->Item->get_info($item->item_id);
			$order_items[$item->item_id] = array(
				'name'=>$item_info->name,
				'quantity'=>$item->quantity,
				'issued'=>$item->issued,
				'item_id'=>$item->item_id,
				'order_id'=>$order->order_id,
				'order_status'=>$order->order_status,
				);
		endforeach;

		$this->session->set_userdata('order_items',$order_items);
		$data['order_id'] = $order_id;
		$data['order'] = $order;
		$data['order_items'] = $order_items;
		
		$this->load->view('items/order_form',$data);
	}

	function order_item_info($item_id,$var)
	{
		$order_items = $this->session->userdata('order_items');
		if($var == 'issued'):
			$order_items[$item_id][$var] = is_numeric($this->input->post('value')) && $this->input->post('value') > 0 ? (int)$this->input->post('value') : 1;
		else:
			$order_items[$item_id][$var] = $this->input->post('value');
		endif;

		$this->session->set_userdata('order_items',$order_items);
	}

	function save_order($order_id)
	{
		$order_items = $this->session->userdata('order_items');
		$employee = $this->Employee->get_logged_in_employee_info();
		$order_data = array();
		$order_item_data = array();

		if(count($order_items) > 0):
			foreach($order_items as $item):
				$order_item_data[] = array(
					'quantity'=>$item['quantity'],
					'issued'=>$item['issued'],
					'item_id'=>$item['item_id'],
					);
			endforeach;

			$order_data = array(
					'served_by'=>$employee->person_id,					
					'service_remarks'=>$this->input->post('remarks'),
					'service_time'=>date('Y-m-d H:i:s'),
					'order_status'=>2,
					);

			$order_id = $this->Order->save($order_data,$order_item_data,$order_id);
			if($order_id):
				foreach($order_item_data as $item):
					$cur_item_info = $this->Item->get_info($item['item_id']);
			
					$item_data = array(
						'store_quantity'=>$cur_item_info->store_quantity - $item['issued'],
						'quantity'=>$cur_item_info->quantity + $item['issued'],
					);
					$item_id = $this->Item->save($item_data,$item['item_id']);
					$inv_data = array
					(
						'trans_date'=>date('Y-m-d H:i:s'),
						'trans_items'=>$item['item_id'],
						'trans_user'=>$employee->person_id,
						'trans_comment'=>'Order '.$order_id,
						'trans_inventory'=> -($item['issued'])
					);
					$this->Inventory->insert($inv_data);
				endforeach;
				echo json_encode(array('success'=>true,'message'=>'Order saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('success'=>false,'message'=>'Unable to save order.','message_class'=>'success_message'));
			endif;
		else:
			echo json_encode(array('success'=>false,'message'=>'No items have been selected.','message_class'=>'error_message'));
		endif;
	}
}
?>