<?php
require_once ("secure_area.php");
class Admin extends Secure_area
{
	function __construct()
	{
		parent::__construct('admin');
		//error_reporting(E_ALL);
		//$this->output->enable_profiler(TRUE);
	}

	function index()
	{
		$this->load->view('admin/main');
	}

	function view_config()
	{
		$data['config'] = $this->Administration->get_config();
		$this->load->view("admin/config",$data);
	}

	function save_config()
	{
		$this->form_validation->set_rules('email', 'Email', 'valid_email');
		$this->form_validation->set_rules('company', 'Hospital Name', 'required');
		$this->form_validation->set_rules('website', 'Website', 'prep_url');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$config_data=array(
				'company'=>$this->input->post('company'),
				'address'=>$this->input->post('address'),
				'phone'=>$this->input->post('phone'),
				'email'=>$this->input->post('email'),
				'website'=>$this->input->post('website'),		
				'currency_symbol'=>$this->input->post('currency_symbol'),
				'timezone'=>$this->input->post('timezone'),
				'print_after_sale'=>$this->input->post('print_after_sale'),
			);
			
			if( $this->Administration->save_config($config_data) ):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Configuration information saved successfully.','message_class'=>'success_message',));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save configuration information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function view_modules()
	{
		$data['modules'] = $this->Administration->get_modules();
		$this->load->view('admin/modules_table',$data);
	}

	function view_module($module_id = -1)
	{
		if($module_id != -1) $data['module_info'] = $this->Administration->get_module_info($module_id);

		$this->load->view('admin/module_form',$data);
	}

	function save_module($module_id = null)
	{
		$this->form_validation->set_rules('sort', 'Sort Order', 'is_natural|required');
		$this->form_validation->set_rules('module_id', 'Controller', 'trim|required|alpha_dash|callback_module_id_check['.$module_id.']');
		$this->form_validation->set_rules('name', 'Module Name', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$config['upload_path'] = './images/menubar/';
			$config['allowed_types'] = 'png';
			$config['max_size']	= '100';
			$config['max_width']  = '48';
			$config['max_height']  = '48';
			$config['file_name']  = strtolower($this->input->post('module_id'));
			$config['overwrite']  = TRUE;
			$this->load->library('upload', $config);
			$this->upload->do_upload('icon');
			
			$module_data = array(
				'module_id'=>strtolower($this->input->post('module_id')),
				'sort'=>$this->input->post('sort'),
				'name'=>ucwords(strtolower($this->input->post('name'))),
				'description'=>$this->input->post('description'),
			);

			$module_id = $this->Administration->save_module($module_data,$module_id);
			if($module_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Module information saved successfully.<u>'.$this->upload->display_errors('<li>', '</li>').'</u>','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save module information.<u>'.$this->upload->display_errors('<li>', '</li>').'</u>','message_class'=>'error_message'));
			endif;
		endif;
	}

	function delete_module($module_id)
	{
		$module_id = $this->Administration->delete_module($module_id);
		if($module_id):
			echo json_encode(array('success'=>true,'message'=>'Module deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('success'=>false,'message'=>'Unable to delete module.','message_class'=>'error_message'));
		endif;
	}

	public function module_id_check($module_id,$form_module_id)
	{
		if ($this->Administration->module_exists($module_id) && ($module_id != $form_module_id)):
			$this->form_validation->set_message('module_id_check', 'Controller has already been assigned to another module');
			return FALSE;
		else:
			return TRUE;
		endif;
	}

	function view_complaints()
	{
		$data['complaints'] = $this->Administration->get_complaints();
		$this->load->view('admin/complaints_table',$data);
	}

	function view_complaint($complaint_id = -1)
	{
		if($complaint_id != -1) $data['complaint_info'] = $this->Administration->get_complaint_info($complaint_id);

		$data['clinics'] = $this->Consultation->get_clinics();
		$this->load->view('admin/complaint_form',$data);
	}

	function save_complaint($complaint_id = null)
	{
		$this->form_validation->set_rules('value', 'Complaint', 'required');
		$this->form_validation->set_rules('category', 'Category', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$complaint_data = array(
				'value'=>$this->input->post('value'),
				'category'=>$this->input->post('category'),
				'sub_category'=>$this->input->post('sub_category'),
				'options'=>$this->input->post('options'),
				'clinic'=>implode(',', $this->input->post('clinic')),
			);

			$complaint_id = $this->Administration->save_complaint($complaint_data,$complaint_id);
			if($complaint_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Complaint information saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save complaint information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function delete_complaint($complaint_id)
	{
		$complaint_id = $this->Administration->delete_complaint($complaint_id);
		if($complaint_id):
			echo json_encode(array('success'=>true,'message'=>'Complaint deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('success'=>false,'message'=>'Unable to delete complaint.','message_class'=>'error_message'));
		endif;
	}

	function view_examinations()
	{
		$data['examinations'] = $this->Administration->get_examinations();
		$this->load->view('admin/examinations_table',$data);
	}

	function view_examination($examination_id = -1)
	{
		if($examination_id != -1) $data['examination_info'] = $this->Administration->get_examination_info($examination_id);

		$data['clinics'] = $this->Consultation->get_clinics();
		$this->load->view('admin/examination_form',$data);
	}

	function save_examination($examination_id = null)
	{
		$this->form_validation->set_rules('value', 'Examination', 'required');
		$this->form_validation->set_rules('category', 'Category', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$examination_data = array(
				'value'=>$this->input->post('value'),
				'category'=>$this->input->post('category'),
				'sub_category'=>$this->input->post('sub_category'),
				'options'=>$this->input->post('options'),
				'description'=>$this->input->post('description'),
				'section'=>$this->input->post('section'),
				'clinic'=>implode(',', $this->input->post('clinic')),
			);

			$examination_id = $this->Administration->save_examination($examination_data,$examination_id);
			if($examination_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Examination information saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save examination information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function delete_examination($examination_id)
	{
		$examination_id = $this->Administration->delete_examination($examination_id);
		if($examination_id):
			echo json_encode(array('success'=>true,'message'=>'Examination deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('success'=>false,'message'=>'Unable to delete examination.','message_class'=>'error_message'));
		endif;
	}

	function suggest_complaint_category()
	{
		$suggestions = $this->Administration->get_category_suggestions('chief_complaints',$this->input->post('q'));
		echo implode("\n",$suggestions);
	}

	function suggest_examination_category()
	{
		$suggestions = $this->Administration->get_category_suggestions('examinations',$this->input->post('q'));
		echo implode("\n",$suggestions);
	}

	function suggest_complaint_subcategory()
	{
		$suggestions = $this->Administration->get_subcategory_suggestions('chief_complaints',$this->input->post('q'));
		echo implode("\n",$suggestions);
	}

	function suggest_examination_subcategory()
	{
		$suggestions = $this->Administration->get_subcategory_suggestions('examinations',$this->input->post('q'));
		echo implode("\n",$suggestions);
	}

	function import_db()
	{
		$this->load->view('admin/import_form');
	}

	function import_sql()
	{
		$config['upload_path'] = './database/temp/';
		$config['allowed_types'] = 'sql';
		$config['file_name']  = 'import_db';
		$config['overwrite']  = TRUE;
		$this->load->library('upload', $config);
		if($this->upload->do_upload('sql_file')):
			$host = $this->db->hostname;
			$user = $this->db->username;
			$pass = $this->db->password;
			$db = $this->db->database;
			$file = $this->upload->data();
			$path = $file['full_path'];

			$import = system("mysql --compress --max-allowed-packet=1G -C --host=$host --user=$user --password=$pass < $path",$output);
			$this->load->helper('file');
			delete_files($config['upload_path']);
			if($output):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Database imported successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to import database.','message_class'=>'success_message'));
			endif;
		else:
			echo json_encode(array('form_validation'=>false,'error_messages'=>$this->upload->display_errors('<li>', '</li>'),'message_class'=>'success_message'));
		endif;
	}

	function export_db()
	{
		$host = $this->db->hostname;
		$user = $this->db->username;
		$pass = $this->db->password;
		$db = $this->db->database;

		$dump = shell_exec("mysqldump --compact --opt --max-allowed-packet=1G -C --host=$host --user=$user --password=$pass --databases $db");
		$this->load->helper('download');
		force_download($db.'.sql', $dump);
	}
}
?>