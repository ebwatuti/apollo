<?php
require_once ("secure_area.php");
class Expenses extends Secure_area
{
	function __construct()
	{
		parent::__construct('expenses');
		//$this->output->enable_profiler(TRUE);
		//error_reporting(E_ALL);
	}

	function index()
	{
		$period = $this->session->userdata('expenses_period') ? $this->session->userdata('expenses_period') : date('Y-m-01');
		$data['period'] = date('M Y',strtotime($period));
		$data['month_input'] = date('m/Y',strtotime($period));
		$this->load->view('expenses/main',$data);
	}

	function view_all()
	{
		$period = $this->session->userdata('expenses_period') ? $this->session->userdata('expenses_period') : date('Y-m-01');
		$chapter = $this->input->post('chapter');
		$category = $this->input->post('category');
		$subcategory = $this->input->post('subcategory');		
		
		$data['expenses'] = $this->Expense->get_all($period,$chapter,$category,$subcategory);
		$data['period'] = date('M Y',strtotime($period));
		$data['chapter'] = $chapter;
		$data['category'] = $category;
		$data['subcategory'] = $subcategory;

		$this->load->view('expenses/expenses_table',$data);
	}
	
	function new_expense()
	{
		$this->load->view('expenses/expense_form');	
	}

	function view_expense($expense_id)
	{
		$data['expense_info'] = $this->Expense->get_info($expense_id);

		$this->load->view('expenses/expense_form',$data);
	}
	
	function save_expense($expense_id=null)
	{
		$this->form_validation->set_rules('expense_no', 'Expense #', 'callback_expense_no_check['.$expense_id.']');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_rules('chapter', 'Chapter', 'required');		
		if($this->input->post('subcategory')) $this->form_validation->set_rules('category', 'Category', 'required');
		
		if($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$expense_data = array(
				'expense_no'=>$this->input->post('expense_no'),
				'description'=>$this->input->post('description'),
				'chapter'=>$this->input->post('chapter'),
				'category'=>$this->input->post('category'),
				'subcategory'=>$this->input->post('subcategory'),
			);

			$budget_data = array(
				'amount'=>$this->input->post('amount'),
				'period'=>date('Y-m-01'),
			);
			
			$expense_id = $this->Expense->save($expense_data,$budget_data,$expense_id);
			if($expense_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Expense information saved successfully.','message_class'=>'success_message','expense_id'=>$expense_id));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save expense information.','message_class'=>'error_message'));
			endif;
		endif;		
	}

	public function expense_no_check($expense_no,$expense_id)
	{		
		if ($this->Expense->expense_no_exists($expense_no,$expense_id)):
			$this->form_validation->set_message('expense_no_check', 'Expense # already exists');
			return FALSE;
		else:
			return TRUE;
		endif;
	}

	function delete_expense($expense_id)
	{
		$expense_id = $this->Expense->delete($expense_id);
		if($expense_id):
			echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Expense information deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to delete expense information.','message_class'=>'error_message'));
		endif;
	}

	function expenses_date_input()
	{
		$this->form_validation->set_rules('month', 'Month', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$month = '01/'.$this->input->post('month');
			$month = preg_replace('/[\/]/s', '-',$month);
			$period = isset($month) ? date("Y-m-01",strtotime($month)) : date("Y-m-01");
			
			$this->session->set_userdata('expenses_period', $period);
			echo json_encode(array('form_validation'=>true,'period'=>date('M Y',strtotime($month))));
		endif;
	}

	function suggest_category()
	{
		$suggestions = $this->Expense->get_category_suggestions($this->input->post('q'));
		echo implode("\n",$suggestions);
	}

	function suggest_subcategory()
	{
		$suggestions = $this->Expense->get_subcategory_suggestions($this->input->post('q'));
		echo implode("\n",$suggestions);
	}

	function view_chapters()
	{
		$data['chapters'] = $this->Expense->get_chapters();

		$this->load->view('expenses/chapters_table',$data);
	}

	function chapter_info($chapter_id = null)
	{
		if($chapter_id) $data['chapter_info'] = $this->Expense->get_chapter_info($chapter_id);

		$this->load->view('expenses/chapter_form',$data);
	}

	function save_chapter($chapter_id=null)
	{
		$this->form_validation->set_rules('chapter_no', 'Chapter #', "required|callback_chapter_no_check[$chapter_id]");
		$this->form_validation->set_rules('description', 'Description', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$chapter_data = array(
				'chapter_no'=>$this->input->post('chapter_no'),
				'description'=>$this->input->post('description'),
				'source'=>'expense',
				'type'=>'expense',
			);
			
			$chapter_id = $this->Expense->save_chapter($chapter_data,$chapter_id);
			if($chapter_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Chapter information saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save chapter information.','message_class'=>'error_message'));
			endif;
		endif;		
	}

	function delete_chapter($chapter_id)
	{
		$chapter_id = $this->Expense->delete_chapter($chapter_id);
		if($chapter_id):
			echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Chapter information deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to delete chapter information.','message_class'=>'error_message'));
		endif;
	}

	public function chapter_no_check($chapter_no,$chapter_id)
	{
		if ($this->Expense->chapter_no_exists($chapter_no,$chapter_id)):
			$this->form_validation->set_message('chapter_no_check', 'Chapter # already exists');
			return FALSE;
		else:
			return TRUE;
		endif;
	}

	function suggest_payee()
	{
		$suggestions = $this->Expense->get_payee_suggestions($this->input->post('q'));
		echo implode("\n",$suggestions);
	}

	function view_expenditures($expense_id)
	{
		$period = $this->session->userdata('expenses_period') ? $this->session->userdata('expenses_period') : date('Y-m-01');
		$data['expenditures'] = $this->Expense->get_expenditures($expense_id,$period);
		$data['period'] = $period;
		$data['expense_id'] = $expense_id;

		$this->load->view('expenses/expenditures_table',$data);
	}

	function expenditure_info($expense_id, $expenditure_id = null)
	{
		if($expenditure_id) $data['expenditure_info'] = $this->Expense->get_expenditure_info($expenditure_id);
		$data['expense_id'] = $expense_id;

		$this->load->view('expenses/expenditure_form',$data);
	}

	function save_expenditure($expense_id,$expenditure_id=null)
	{
		$this->form_validation->set_rules('payee', 'Payee', 'required');
		$this->form_validation->set_rules('ref_no', 'Ref #', "callback_ref_no_check[$expenditure_id]");
		$this->form_validation->set_rules('amount', 'Amount', 'required|is_numeric|callback_amount_check['.$expense_id.'.'.$expenditure_id.']');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$expenditure_data = array(
				'expense_id'=>$expense_id,
				'period'=>date('Y-m-01'),
				'ref_no'=>$this->input->post('ref_no'),
				'payee'=>$this->input->post('payee'),
				'amount'=>$this->input->post('amount'),
				'remarks'=>$this->input->post('remarks'),
				'employee_id'=>$this->Employee->get_logged_in_employee_info()->person_id,
			);
			
			$expenditure_id = $this->Expense->save_expenditure($expenditure_data,$expenditure_id);
			if($expenditure_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Payments information saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save payment information.','message_class'=>'error_message'));
			endif;
		endif;		
	}

	function delete_expenditure($expenditure_id)
	{
		$expenditure_id = $this->Expense->delete_expenditure($expenditure_id);
		if($expenditure_id):
			echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Payment information deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to delete payment information.','message_class'=>'error_message'));
		endif;
	}

	public function ref_no_check($ref_no,$expenditure_id)
	{
		if ($this->Expense->ref_no_exists($ref_no,$expenditure_id)):
			$this->form_validation->set_message('ref_no_check', 'Ref # already exists');
			return FALSE;
		else:
			return TRUE;
		endif;
	}

	public function amount_check($amount,$ids)
	{
		$ids = explode('.', $ids);
		$expenditure = $this->Expense->get_expenditure_info($ids[1]);
		$expense = $this->Expense->get_info($ids[0]);
		$period = date('Y-m-01');
		$balance_cf = $this->Expense->get_total_balance($period);
		$budget = $this->Expense->get_total_budget($period);
		$expenditure = $this->Expense->get_total_expenditure($period) - $expenditure->amount;
		$balance = $balance_cf + $budget - $expenditure;
		if ($amount > $balance):
			$this->form_validation->set_message('amount_check', 'Amount has exceeded the budget by '.number_format($amount - $balance,2));
			return FALSE;
		else:
			return TRUE;
		endif;
	}

	function view_budgets()
	{
		$data['budgets'] = $this->Expense->get_budgets();

		$this->load->view('expenses/budgets_table',$data);
	}

	function save_budget()
	{
		$this->form_validation->set_rules('amount', date('M Y').' Budget', 'required|is_numeric');
		
		if($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$budget_id = $this->Expense->get_current_budget()->budget_id;
			$budget_data = array(
				'amount'=>$this->input->post('amount'),
				'period'=>date('Y-m-01'),
			);
			
			$budget_id = $this->Expense->save_budget($budget_data,$budget_id);
			if($budget_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Budget information saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save budget information.','message_class'=>'error_message'));
			endif;
		endif;		
	}
}
?>
