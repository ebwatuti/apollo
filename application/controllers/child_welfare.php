<?php
require_once('consultant.php');
class Child_welfare extends Consultant {			
	function __construct()
	{
		parent::__construct('child_welfare','clinic12');
	}

	function save($consultation_status, $encounter_status)
	{
		$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
		$patient_id = $this->input->post('patient_id');
		$encounter_id = $this->Encounter->check_encounter($patient_id);

		$consultation_data = array(
			'patient_id'=>$patient_id,
			'consultant_id'=>$employee_id,
			'consultation_complaints'=>serialize($this->session->userdata('complaints')),
			'consultation_obst_gyn'=>serialize($this->session->userdata('obst_gyn')),
			'consultation_medical_history'=>serialize($this->session->userdata('medical_history')),
			'consultation_family_history'=>serialize($this->session->userdata('family_history')),
			'consultation_examination'=>serialize($this->session->userdata('examination')),
			'consultation_treatment_plan'=>serialize($this->session->userdata('treatment_plan')),
			'consultation_other_sections'=>serialize($this->session->userdata('other_sections')),
			'consultation_status'=>$consultation_status,
			'consultation_type'=>$this->session->userdata('clinic'),
			'consultation_end'=>date('Y-m-d H:i:s'),
		);

		$diagnosis = $this->session->userdata('diagnosis');
		$diagnosis_data = array();

		foreach($diagnosis as $item):
			$diagnosis_data[] = array(
				'diagnosis_code' => $item['diagnosis_code'],
				'remarks'=> $item['remarks']
				);
		endforeach;

		$lab_tests = $this->session->userdata('lab_tests');
		$lab_test_data = array();
		$lab_request_data = array();

		if(!empty($lab_tests)):
			$consultation_data['lab_request'] = 1;
			foreach($lab_tests as $item):
				if($item['status'] == 0 && $item['invoice_status'] == 0):
					$lab_test_data[] = array(
						'item_id' => $item['item_id'],
						'remarks'=> $item['remarks'],						
						'status'=>$item['status'],
						'discount'=>100,
						);
				endif;
			endforeach;

			if(!empty($lab_test_data)):
				$lab_request_data = array(
					'invoice_id'=>$this->Invoice->already_invoiced($patient_id,$employee_id,'Lab'),
					'invoice_data'=>array(
						'patient_id'=>$patient_id,
						'employee_id'=>$employee_id,
						'encounter_id'=>$encounter_id,
						'invoice_type'=>'Lab',
						),
					'invoice_item_data'=>$lab_test_data,
					);
			endif;
		endif;

		$prescription = $this->session->userdata('prescription');
		$drug_data = array();
		$prescription_data = array();

		if(!empty($prescription)):
			foreach($prescription as $item):
				if($item['status'] == 0 && $item['invoice_status'] == 0):
					$drug_data[] = array(
						'remarks'=>$item['remarks'],
						'status'=>$item['status'],
						'item_id'=>$item['item_id'],
						'quantity'=>$item['quantity'],
						'instructions'=>serialize(array('dose'=>$item['dose'],'frequency'=>$item['frequency'],'duration'=>$item['duration'])),
						);
				endif;
			endforeach;

			if(!empty($drug_data)):
				$prescription_data = array(
					'invoice_id'=>$this->Invoice->already_invoiced($patient_id,$employee_id,'Pharmacy'),
					'invoice_data'=>array(
						'patient_id'=>$patient_id,
						'employee_id'=>$employee_id,
						'encounter_id'=>$encounter_id,
						'invoice_type'=>'Pharmacy',
						),
					'invoice_item_data'=>$drug_data,
					);
			endif;
		endif;

		$procedures = $this->session->userdata('procedures');
		$procedure_data = array();
		$procedures_data = array();
		$nursing_procedure_data = array();
		$nursing_data = array();

		if(!empty($procedures)):
			foreach($procedures as $item):
				if($item['status'] == 0 && $item['invoice_status'] == 0):
					if($item['clinic'] == 'nursing'):
						$nursing_procedure_data[] = array(
							'remarks'=>$item['remarks'],
							'results'=>$item['results'],
							'status'=>$item['status'],
							'item_id'=>$item['item_id'],
							'quantity'=>$item['quantity'],
							'unit_price'=>$item['unit_price'],
							);
					else:
						$procedure_data[] = array(
							'remarks'=>$item['remarks'],
							'results'=>$item['results'],
							'status'=>$item['status'],
							'item_id'=>$item['item_id'],
							'quantity'=>$item['quantity'],
							'unit_price'=>$item['unit_price'],
							);
					endif;
				endif;
			endforeach;

			if(!empty($procedure_data)):
				$procedures_data = array(
					'invoice_id'=>$this->Invoice->already_invoiced($patient_id,$employee_id,$this->Consultation->clinic_name($this->session->userdata('clinic')).' Procedures'),
					'invoice_data'=>array(
						'patient_id'=>$patient_id,
						'employee_id'=>$employee_id,
						'encounter_id'=>$encounter_id,
						'invoice_type'=>$this->Consultation->clinic_name($this->session->userdata('clinic')).' Procedures',
						'service_time'=>date('Y-m-d H:i:s'),
						),
					'invoice_item_data'=>$procedure_data,
					);
			endif;
			if(!empty($nursing_procedure_data)):
				$nursing_data = array(
					'invoice_id'=>$this->Invoice->already_invoiced($patient_id,$employee_id,'Nursing'),
					'invoice_data'=>array(
						'patient_id'=>$patient_id,
						'employee_id'=>$employee_id,
						'encounter_id'=>$encounter_id,
						'invoice_type'=>'Nursing',
						),
					'invoice_item_data'=>$nursing_procedure_data,
					);
			endif;
		endif;

		$consultation_id = $this->Consultation->returning($patient_id,$employee_id,$this->session->userdata('clinic'))->consultation_id;

		return $this->Consultation->save($consultation_data,$diagnosis_data,$lab_request_data,$prescription_data,$procedures_data,$nursing_data,$encounter_status,$patient_id,$consultation_id);
	}
}
?>