<?php
require_once ("secure_area.php");
class Nursing extends Secure_area
{
	function __construct()
	{
		parent::__construct('nursing');
		//error_reporting(E_ALL);
		//$this->output->enable_profiler(TRUE);
	}

	function index()
	{
		$this->load->view('nursing/main');
	}

	function view_all()
	{
		$data['main_queue'] = $this->Nurse->get_main_queue();
		$data['pending_queue'] = $this->Nurse->get_pending_queue();

		$this->load->view('nursing/patients_table',$data);
	}

	function view_invoice($invoice_id)
	{
		$this->session->unset_userdata('invoice_items');
		$invoice = $this->Invoice->get_info($invoice_id);
		foreach($invoice->invoice_items as $item):
			$item_info = $this->Item->get_info($item->item_id);
			$invoice_items[$item->item_id] = array(
				'name'=>$item_info->name,
				'remarks'=>$item->remarks,
				'quantity'=>$item->quantity,
				'unit_price'=>$item->unit_price,
				'discount'=>$item->discount,
				'item_id'=>$item->item_id,
				'status'=>$item->status,
				'invoice_id'=>$invoice->invoice_id,
				'invoice_status'=>$invoice->invoice_status,
				);
		endforeach;

		$this->session->set_userdata('invoice_items',$invoice_items);
		$data['patient_id'] = $invoice->patient_id;
		$data['invoice_id'] = $invoice_id;
		$data['invoice_items'] = $invoice_items;
		
		$this->load->view('nursing/invoice_form',$data);
	}

	function new_invoice()
	{
		$this->session->unset_userdata('invoice_items');
		$this->load->view('nursing/new_invoice_form',$data);
	}

	function item_search()
	{
		$suggestions = $this->Nurse->procedure_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function add_item()
	{
		$procedure_id = $this->input->post("item");
		$procedure_info = $this->Nurse->get_procedure_info($procedure_id);
		$item_info = $this->Item->get_info($procedure_info->item_info->item_id);
		$invoice_items = $this->session->userdata('invoice_items');

		if($item_info):
			if(array_key_exists($item_info->item_id, $invoice_items)):
				echo json_encode(array('success'=>false,'message_class'=>'warning_message','message'=>"Item already added"));
			else:
				$invoice_items[$item_info->item_id] = array(
					'item_id'=>$item_info->item_id,
					'name'=>$item_info->name,
					'remarks'=>'',
					'quantity'=>1,
					'unit_price'=>$item_info->unit_price,
					'discount'=>0,
					'status'=>0,
					'invoice_status'=>0,
					);
				$this->session->set_userdata('invoice_items',$invoice_items);
				echo json_encode(array('success'=>true,'message_class'=>'success_message','message'=>"Item added successfully"));
			endif;
		else:
			echo json_encode(array('success'=>false,'message_class'=>'error_message','message'=>"Please select a valid item from the list of suggestions"));
		endif;
	}

	function remove_item($item_id)
	{
		$invoice_items = $this->session->userdata('invoice_items');
		unset($invoice_items[$item_id]);
		$this->session->set_userdata('invoice_items',$invoice_items);

		$data['invoice_items'] = $invoice_items;
		$this->load->view("nursing/invoice_items",$data);
	}

	function item_info($item_id,$var)
	{
		$invoice_items = $this->session->userdata('invoice_items');
		if($var == 'quantity'):
			$invoice_items[$item_id][$var] = is_numeric($this->input->post('value')) && $this->input->post('value') > 0 ? (int)$this->input->post('value') : 1;
		elseif($var == 'discount'):
			$invoice_items[$item_id][$var] = is_numeric($this->input->post('value')) && $this->input->post('value') >= 0 && $this->input->post('value') <= 100 ? $this->input->post('value') : 0;
		else:
			$invoice_items[$item_id][$var] = $this->input->post('value');
		endif;

		$this->session->set_userdata('invoice_items',$invoice_items);
		$quantity = $invoice_items[$item_id]['quantity'];
		$unit_price = $invoice_items[$item_id]['unit_price'];
		$discount = $invoice_items[$item_id]['discount'];
		echo to_currency($quantity * $unit_price * (100 - $discount) / 100);
	}

	function refresh_invoice()
	{
		$data['invoice_items'] = $this->session->userdata('invoice_items');
		
		$this->load->view("nursing/invoice_items",$data);
	}

	function patient_search()
	{
		$suggestions = $this->Patient->get_patient_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function refresh_patient_info($patient_id = null)
	{
		$invoice_items = $this->session->userdata('invoice_items') ? $this->session->userdata('invoice_items') : array();
		$invoice_id = $this->Invoice->already_invoiced($patient_id,$this->Employee->get_logged_in_employee_info()->person_id,'Nursing');
		$invoice = $this->Invoice->get_info($invoice_id);
		foreach($invoice->invoice_items as $item):
			$item_info = $this->Item->get_info($item->item_id);
			$invoice_items[$item->item_id] = array(
				'name'=>$item_info->name,
				'remarks'=>$item->remarks,
				'quantity'=>$item->quantity,
				'unit_price'=>$item->unit_price,
				'discount'=>$item->discount,
				'item_id'=>$item->item_id,
				'status'=>$item->status,
				'invoice_id'=>$invoice->invoice_id,
				'invoice_status'=>$invoice->invoice_status,
				);
		endforeach;

		$this->session->set_userdata('invoice_items',$invoice_items);

		$data['patient_id'] = $patient_id;
		$this->load->view("nursing/patient_info",$data);
	}

	function new_patient()
	{
		$this->load->view('nursing/new_patient');
	}

	function save_patient()
	{
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('email', 'Email', 'valid_email');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$patient_data = array(
			'first_name'=>$this->input->post('first_name'),
			'last_name'=>$this->input->post('last_name'),
			'middle_name'=>$this->input->post('middle_name'),
			'gender'=>$this->input->post('gender'),
			'residence'=>$this->input->post('residence'),
			'national_id'=>$this->input->post('national_id'),
			'dob'=>$this->input->post('dob'),
			'phone_number'=>$this->input->post('phone_number'),
			'email'=>$this->input->post('email'),
			'address_1'=>$this->input->post('address_1'),
			'address_2'=>$this->input->post('address_2'),
			'city'=>$this->input->post('city'),
			'state'=>$this->input->post('state'),
			'zip'=>$this->input->post('zip'),
			'country'=>$this->input->post('country'),
			);

			$patient_id = $this->Patient->save($patient_data);
			if($patient_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Patient information saved successfully.','message_class'=>'success_message','patient_id'=>$patient_id));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save patient information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function save_new_invoice($patient_id)
	{
		$invoice_items = $this->session->userdata('invoice_items');
		$employee = $this->Employee->get_logged_in_employee_info();
		$invoice_data = array();
		$invoice_item_data = array();
		$invoice_id = $this->Invoice->already_invoiced($patient_id,$employee->person_id,'Nursing');

		if(count($invoice_items) > 0):
			foreach($invoice_items as $item):
				$invoice_item_data[] = array(
					'remarks'=>$item['remarks'],
					'quantity'=>$item['quantity'],
					'discount'=>$item['discount'],
					'status'=>$item['status'],
					'item_id'=>$item['item_id'],
					);
			endforeach;

			$invoice_data = array(
					'patient_id'=>$patient_id,
					'employee_id'=>$employee->person_id,					
					'invoice_type'=>'Nursing',
					);
			if($this->Invoice->save($invoice_data,$invoice_item_data,$invoice_id)):
				echo json_encode(array('success'=>true,'message'=>'Invoice saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('success'=>false,'message'=>'Unable to save Invoice.','message_class'=>'success_message'));
			endif;
		else:
			echo json_encode(array('success'=>false,'message'=>'No items have been selected.','message_class'=>'error_message'));
		endif;
	}

	function save_invoice($invoice_id)
	{
		$invoice_items = $this->session->userdata('invoice_items');
		$employee = $this->Employee->get_logged_in_employee_info();
		$invoice_data = array();
		$invoice_item_data = array();
		$invoice_id = $this->Invoice->already_invoiced($patient_id,$employee->person_id,'Nursing');

		if(count($invoice_items) > 0):
			foreach($invoice_items as $item):
				$invoice_item_data[] = array(
					'remarks'=>$item['remarks'],
					'quantity'=>$item['quantity'],
					'discount'=>$item['discount'],
					'status'=>$item['status'],
					'item_id'=>$item['item_id'],
					);
			endforeach;

			$invoice_data = array(
					'served_by'=>$employee->person_id,
					'invoice_status'=>3,
					'service_time'=>date('Y-m-d H:i:s')
					);
			if($this->Invoice->save($invoice_data,$invoice_item_data,$invoice_id)):
				echo json_encode(array('success'=>true,'message'=>'Services completed successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('success'=>false,'message'=>'Unable to complete services.','message_class'=>'success_message'));
			endif;
		else:
			echo json_encode(array('success'=>false,'message'=>'No items have been selected.','message_class'=>'error_message'));
		endif;
	}

	function view_workload()
	{
		$date = ($this->session->userdata('nursing_workload_date')) ? $this->session->userdata('nursing_workload_date') : array("start"=>date("Y-m-d 00:00:00"),"end"=>date("Y-m-d 23:59:59"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));

		$employee = $this->Employee->get_logged_in_employee_info();
		$data['employee'] = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);

		$data['hospital_workload'] = $this->Nurse->workload($start_date,$end_date);
		$data['employee_workload'] = $this->Nurse->workload($start_date,$end_date,$employee->person_id);

		$procedures_workload = $this->Nurse->procedures_workload($start_date,$end_date);
		arsort($procedures_workload);
		$data['procedures_workload'] = $procedures_workload;

		$data["start_date"] = date("j/n/Y",strtotime($date["start"]));
		$data["end_date"] = date("j/n/Y",strtotime($date["end"]));
		
		$data["start_date_input"] = date("d-m-Y",strtotime($date["start"]));
		$data["end_date_input"] = date("d-m-Y",strtotime($date["end"]));

		$this->load->view("nursing/workload",$data);
	}

	function workload_date_input()
	{
		$this->form_validation->set_rules('start_date', 'Start Date', 'callback_date_check|required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$start_date = isset($start_date) ? date("Y-m-d H:i:s",strtotime($start_date)) : date("Y-m-d H:i:s");
			$end_date = isset($end_date) ? date("Y-m-d H:i:s",strtotime($end_date)) : $start_date;
			$date = array("start"=>$start_date,"end"=>$end_date);
			
			$this->session->set_userdata('nursing_workload_date', $date);
			echo json_encode(array('form_validation'=>true));
		endif;
	}

	function view_archive()
	{
		$date = ($this->session->userdata('nursing_archive_date')) ? $this->session->userdata('nursing_archive_date') : array("start"=>date("Y-m-d 00:00:00"),"end"=>date("Y-m-d 23:59:59"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));
		$data['invoices'] = $this->Nurse->archive_invoices($start_date,$end_date);
		$data["start_date"] = date("j/n/Y",strtotime($date["start"]));
		$data["end_date"] = date("j/n/Y",strtotime($date["end"]));
		
		$data["start_date_input"] = date("d-m-Y",strtotime($date["start"]));
		$data["end_date_input"] = date("d-m-Y",strtotime($date["end"]));

		$this->load->view("nursing/archive",$data);
	}

	function archive_date_input()
	{
		$this->form_validation->set_rules('start_date', 'Start Date', 'callback_date_check|required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$start_date = isset($start_date) ? date("Y-m-d H:i:s",strtotime($start_date)) : date("Y-m-d H:i:s");
			$end_date = isset($end_date) ? date("Y-m-d H:i:s",strtotime($end_date)) : $start_date;
			$date = array("start"=>$start_date,"end"=>$end_date);
			
			$this->session->set_userdata('nursing_archive_date', $date);
			echo json_encode(array('form_validation'=>true));
		endif;
	}

	public function date_check()
	{
		$start_date = strtotime($this->input->post('start_date'));
		$end_date = strtotime($this->input->post('end_date'));
		if ($start_date > $end_date):
			$this->form_validation->set_message('date_check', 'Start date cannot be later than end date');
			return FALSE;
		else:
			return TRUE;
		endif;
	}

	function view_services($invoice_id)
	{
		$data['invoice'] = $this->Invoice->get_info($invoice_id);

		$this->load->view('nursing/services_info',$data);
	}

	function view_procedures()
	{
		$data['procedures'] = $this->Nurse->get_procedures();
		$this->load->view('consultant/procedures_table',$data);
	}

	function view_procedure($procedure_id = -1)
	{
		if($procedure_id != -1) $data['procedure_info'] = $this->Nurse->get_procedure_info($procedure_id);

		$this->load->view('consultant/procedure_form',$data);
	}

	function save_procedure($procedure_id = null)
	{
		$this->form_validation->set_rules('procedure_name', 'Procedure Name', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required|numeric');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$procedure_data = array(
			'name'=>$this->input->post('procedure_name'),
			'clinic'=>'nursing'
			);
			$item_data = array(
			'name'=>$this->input->post('procedure_name'),
			'category'=>'Nursing',
			'sub_category'=>$this->input->post('procedure_name'),
			'quantity'=>1,
			'quantifiable'=>0,
			'unit_price'=>$this->input->post('price'),
			);

			$procedure_id = $this->Nurse->save_procedure($procedure_data,$item_data,$procedure_id);
			if($procedure_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Procedure information saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save procedure information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	function delete_procedure($procedure_id)
	{
		$procedure_id = $this->Nurse->delete_procedure($procedure_id);
		if($procedure_id):
			echo json_encode(array('success'=>true,'message'=>'Procedure deleted successfully.','message_class'=>'success_message'));
		else:
			echo json_encode(array('success'=>false,'message'=>'Unable to delete procedure.','message_class'=>'error_message'));
		endif;
	}
}
?>