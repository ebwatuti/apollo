<?php
require_once ("secure_area.php");
class Reports extends Secure_area 
{	
	function __construct()
	{
		parent::__construct('reports');
		//error_reporting(E_ALL);
		//$this->output->enable_profiler(TRUE);
	}
	
	function index()
	{
		$this->load->view("reports/listing");	
	}
	
	public function date_check()
	{
		$start_date = strtotime($this->input->post('start_date'));
		$end_date = strtotime($this->input->post('end_date'));
		if ($start_date > $end_date):
			$this->form_validation->set_message('date_check', 'Start date cannot be later than end date');
			return FALSE;
		else:
			return TRUE;
		endif;
	}

	public function age_check()
	{
		$start_age = $this->input->post('start_age');
		$end_age = $this->input->post('end_age');
		if ($start_age > $end_age):
			$this->form_validation->set_message('age_check', 'Start age cannot be greater than end age');
			return FALSE;
		else:
			return TRUE;
		endif;
	}

	function view_workload()
	{
		$date = ($this->session->userdata('reports_workload_date')) ? $this->session->userdata('reports_workload_date') : array("start"=>date("Y-m-d 00:00:00"),"end"=>date("Y-m-d 23:59:59"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));
		$gender = $this->session->userdata('reports_workload_gender');
		$ages = $this->session->userdata('reports_workload_ages');

		$clinics = $this->Consultation->get_clinics();
		$data['clinics'] = array();
		foreach($clinics as $clinic):
			$data['clinics'][$clinic->clinic_code]['visits'] = $this->Encounter->clinic_workload($start_date,$end_date,$clinic->clinic_code,$gender,$ages);
			$data['clinics'][$clinic->clinic_code]['reviews'] = $this->Encounter->visits($start_date,$end_date,1,$clinic->clinic_code,$gender,$ages);
			$data['clinics'][$clinic->clinic_code]['normal'] = $this->Encounter->visits($start_date,$end_date,0,$clinic->clinic_code,$gender,$ages);			
			$data['clinics'][$clinic->clinic_code]['name'] = $clinic->clinic_name;
	    endforeach;

	    $data['depts'] = array();
	    $data['depts']['Lab'] = $this->Lab->workload($start_date,$end_date,'',$gender,$ages);
	    $data['depts']['Pharmacy'] = $this->Pharm->workload($start_date,$end_date,'',$gender,$ages);
	    $data['depts']['Nursing'] = $this->Nurse->workload($start_date,$end_date,'',$gender,$ages);

		$data["start_date"] = date("j/n/Y",strtotime($date["start"]));
		$data["end_date"] = date("j/n/Y",strtotime($date["end"]));
		
		$data["start_date_input"] = date("d-m-Y",strtotime($date["start"]));
		$data["end_date_input"] = date("d-m-Y",strtotime($date["end"]));
		$data["start_age"] = $ages['start'];
		$data["end_age"] = $ages['end'];
		$data["gender"] = $gender;

		$this->load->view("reports/workload",$data);
	}

	function workload_date_input()
	{
		$this->form_validation->set_rules('start_date', 'Start Date', 'callback_date_check|required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		$this->form_validation->set_rules('start_age', 'Age range', 'is_natural');
		if($this->input->post('start_age')):
			$this->form_validation->set_rules('end_age', 'Age range', 'required|is_natural|callback_age_check');
		endif;
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$start_date = isset($start_date) ? date("Y-m-d H:i:s",strtotime($start_date)) : date("Y-m-d H:i:s");
			$end_date = isset($end_date) ? date("Y-m-d H:i:s",strtotime($end_date)) : $start_date;
			$date = array("start"=>$start_date,"end"=>$end_date);

			$start_age = $this->input->post('start_age');
			$end_age = $this->input->post('end_age');
			$ages = ($start_age !== '' && $end_age !== '') ? array('start'=>$start_age,'end'=>$end_age) : '';
			$gender = $this->input->post('gender');
			
			$this->session->set_userdata('reports_workload_date', $date);
			$this->session->set_userdata('reports_workload_ages', $ages);
			$this->session->set_userdata('reports_workload_gender', $gender);
			echo json_encode(array('form_validation'=>true));
		endif;
	}

	function view_morbidity()
	{
		$date = ($this->session->userdata('reports_morbidity_date')) ? $this->session->userdata('reports_morbidity_date') : array("start"=>date("Y-m-d 00:00:00"),"end"=>date("Y-m-d 23:59:59"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));
		$gender = $this->session->userdata('reports_morbidity_gender');
		$ages = $this->session->userdata('reports_morbidity_ages');

		$clinic = $this->session->userdata('reports_morbidity_clinic');
		$morbidity = $this->Consultation->morbidity($start_date,$end_date,$clinic,$gender,$ages);
		if($clinic):
			$data['title'] = $this->Consultation->clinic_name($clinic).' Morbidity';
		else:
			$data['title'] = 'Hospital Morbidity';
		endif;

		arsort($morbidity);
		$data['morbidity'] = $morbidity;

		$data["start_date"] = date("j/n/Y",strtotime($date["start"]));
		$data["end_date"] = date("j/n/Y",strtotime($date["end"]));
		
		$data["start_date_input"] = date("d-m-Y",strtotime($date["start"]));
		$data["end_date_input"] = date("d-m-Y",strtotime($date["end"]));
		$data["start_age"] = $ages['start'];
		$data["end_age"] = $ages['end'];
		$data["gender"] = $gender;

		$this->load->view("reports/morbidity",$data);
	}

	function morbidity_date_input()
	{
		$this->form_validation->set_rules('start_date', 'Start Date', 'callback_date_check|required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		$this->form_validation->set_rules('start_age', 'Age range', 'is_natural');
		if($this->input->post('start_age')):
			$this->form_validation->set_rules('end_age', 'Age range', 'required|is_natural|callback_age_check');
		endif;
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$start_date = isset($start_date) ? date("Y-m-d H:i:s",strtotime($start_date)) : date("Y-m-d H:i:s");
			$end_date = isset($end_date) ? date("Y-m-d H:i:s",strtotime($end_date)) : $start_date;
			$date = array("start"=>$start_date,"end"=>$end_date);

			$start_age = $this->input->post('start_age');
			$end_age = $this->input->post('end_age');
			$ages = ($start_age !== '' && $end_age !== '') ? array('start'=>$start_age,'end'=>$end_age) : '';
			$gender = $this->input->post('gender');
			
			$this->session->set_userdata('reports_morbidity_date', $date);
			$this->session->set_userdata('reports_morbidity_clinic', $this->input->post('morbidity_clinic'));
			$this->session->set_userdata('reports_morbidity_ages', $ages);
			$this->session->set_userdata('reports_morbidity_gender', $gender);
			echo json_encode(array('form_validation'=>true));
		endif;
	}
	
	function view_sales()
	{
		$date = ($this->session->userdata('reports_sales_date')) ? $this->session->userdata('reports_sales_date') : array("start"=>date("Y-m-d 00:00:00"),"end"=>date("Y-m-d 23:59:59"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));
		$report_type = ($this->session->userdata('reports_sales_type')) ? $this->session->userdata('reports_sales_type') : 'categories';
		$category = $this->session->userdata('reports_sales_category');

		switch ($report_type) {
			case 'categories':
				$data['sales'] = $category ? $this->Sale->subcategories_workload($start_date,$end_date,$category) : $this->Sale->categories_workload($start_date,$end_date);
				$data['title'] = $category ? ucwords($category).' Category' : 'Item Categories';
				$data['th'] = $category ? 'Sub-category' : 'Category';
				$data['discounts'] = TRUE;
				$data['categories'] = $this->Item->get_categories()->Result();
				$data['category'] = $category;
				break;
			case 'employees':
				$data['sales'] = $this->Sale->employees_workload($start_date,$end_date);
				$data['title'] = 'Employees';
				$data['th'] = 'Employee';
				$data['discounts'] = TRUE;
				break;
			case 'items':
				$data['sales'] = $this->Sale->items_workload($start_date,$end_date);
				$data['title'] = 'Items';
				$data['th'] = 'Item';
				$data['discounts'] = TRUE;
				break;
			case 'payments':
				$data['sales'] = $this->Sale->payments_workload($start_date,$end_date);
				$data['title'] = 'Payment Types';
				$data['th'] = 'Payment Type';
				$data['discounts'] = FALSE;
				break;
			default:
				$data['sales'] = $category ? $this->Sale->subcategories_workload($start_date,$end_date,$category) : $this->Sale->categories_workload($start_date,$end_date);
				$data['title'] = $category ? ucwords($category).' Category' : 'Item Categories';
				$data['th'] = $category ? 'Sub-category' : 'Category';
				$data['discounts'] = TRUE;
				$data['categories'] = $this->Item->get_categories()->Result();
				$data['category'] = $category;
		}
		$data['report_type'] = $report_type;
		$data["start_date"] = date("j/n/Y",strtotime($date["start"]));
		$data["end_date"] = date("j/n/Y",strtotime($date["end"]));
		
		$data["start_date_input"] = date("d-m-Y",strtotime($date["start"]));
		$data["end_date_input"] = date("d-m-Y",strtotime($date["end"]));

		$this->load->view("reports/sales",$data);
	}

	function sales_date_input()
	{
		$this->form_validation->set_rules('start_date', 'Start Date', 'callback_date_check|required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$start_date = isset($start_date) ? date("Y-m-d H:i:s",strtotime($start_date)) : date("Y-m-d H:i:s");
			$end_date = isset($end_date) ? date("Y-m-d H:i:s",strtotime($end_date)) : $start_date;
			$date = array("start"=>$start_date,"end"=>$end_date);
			
			$this->session->set_userdata('reports_sales_date', $date);
			$this->session->set_userdata('reports_sales_type', $this->input->post('report_type'));
			$this->session->set_userdata('reports_sales_category', $this->input->post('category'));
			echo json_encode(array('form_validation'=>true));
		endif;
	}

	function view_archives()
	{
		$date = ($this->session->userdata('reports_archives_date')) ? $this->session->userdata('reports_archives_date') : array("start"=>date("Y-m-d 00:00:00"),"end"=>date("Y-m-d 23:59:59"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));
		$archive_type = ($this->session->userdata('reports_archives_type')) ? $this->session->userdata('reports_archives_type') : 'receipts';
		$clinic = $this->session->userdata('reports_archives_clinic');

		switch ($archive_type) {
			case 'receipts':
				$data['records'] = array();
				$sales = $this->Sale->sales_archive($start_date,$end_date);
				foreach($sales->Result() as $sale):
					$sale = $this->Sale->get_sale_info($sale->sale_id);
			        $patient = $this->Patient->get_info($sale->customer_id);
			        $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
			        $patient->patient_no = $this->Patient->patient_number($patient->patient_id);

			        $sale->date = date('j/n/Y g:i a',strtotime($sale->sale_time));
			        $sale->receipt_no = $this->Sale->receipt_no($sale->sale_id);
			        $sale->value = 0;
			        $sale->discount = 0;

			        foreach($sale->sale_items as $sale_item):
			            $sale->value = $sale->value + $sale_item->quantity_purchased * $sale_item->item_unit_price * (100 - $sale_item->discount_percent) / 100;
			            $sale->discount = $sale->discount + $sale_item->quantity_purchased * $sale_item->item_unit_price * $sale_item->discount_percent / 100;
			        endforeach;

			        $employee = $this->Employee->get_info($sale->employee_id);
			        $employee->employee_name = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
			        $data['records'][] = array($sale->date,$sale->receipt_no,
			        	$patient->patient_no,$patient->patient_name,$employee->employee_name,
			        	to_currency($sale->discount),to_currency($sale->value),
			        	form_button(array('name'=>'submit','onclick'=>'view_sale('.$sale->sale_id.')','content'=>'View','class'=>'DTTT_button float_left')
        ));
			    endforeach;

				$data['title'] = 'Receipts';
				$data['thead'] = array('Date','Receipt #','Patient #','Patient Name','Served by','Discount','Value','&nbsp;');
				break;
			case 'consultation_notes':
				$data['records'] = array();
				$consultations = $clinic ? $this->Consultation->archive_consultations($start_date,$end_date,$clinic) : $this->Consultation->archive_consultations($start_date,$end_date);
				foreach($consultations->result() as $consultation):
			        $patient = $this->Patient->get_info($consultation->patient_id);
			        $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
			        $patient->patient_no = $this->Patient->patient_number($patient->patient_id);
			        $consultation->clinic_name = $this->Consultation->clinic_name($consultation->consultation_type);
			        $consultation->date = date('j/n/Y g:i a',strtotime($consultation->consultation_start));
			        $employee = $this->Employee->get_info($consultation->consultant_id);
			        $employee->employee_name = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
			        $data['records'][]=array($consultation->date,$patient->patient_no,$patient->patient_name,
			        	$employee->employee_name,$consultation->clinic_name,
			        	form_button(array('name'=>'submit','onclick'=>'view_consultation('.$consultation->patient_id.','.$consultation->consultation_id.')','content'=>'View','class'=>'DTTT_button float_left'))
			        	);
			    endforeach;
			    $clinic_name = $this->Consultation->clinic_name($clinic);
				$data['title'] = $clinic ? $clinic_name.' Consultation Notes' : 'Consultation Notes';
				$data['thead'] = array('Date','Patient #','Patient Name','Examined by','Clinic','&nbsp;');
				$data['clinics'] = $this->Consultation->get_clinics();
				$data['clinic'] = $clinic;
				break;
			case 'prescriptions':
				$data['records'] = array();
				$prescriptions = $this->Pharm->archive_prescriptions($start_date,$end_date);
				foreach($prescriptions->result() as $prescription):
			        $patient = $this->Patient->get_info($prescription->patient_id);
			        $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
			        $patient->patient_no = $this->Patient->patient_number($patient->patient_id);

			        $prescription->date = date('j/n/Y g:i a',strtotime($prescription->service_time));
			        $employee = $this->Employee->get_info($prescription->served_by);
			        $employee->employee_name = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
			        $data['records'][]=array($prescription->date,$patient->patient_no,$patient->patient_name,$employee->employee_name,
			        	form_button(array('name'=>'submit','onclick'=>'view_prescription('.$prescription->invoice_id.')','content'=>'View','class'=>'DTTT_button float_left'))
			        	);
			    endforeach;
				$data['title'] = 'Prescriptions';
				$data['thead'] = array('Date','Patient #','Patient Name','Dispensed by','&nbsp;');
				break;
			case 'lab_reports':
				$data['records'] = array();
				$lab_reports = $this->Lab->lab_reports($start_date,$end_date);
				foreach($lab_reports->result() as $report):
			        $patient = $this->Patient->get_info($report->patient_id);
			        $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
			        $patient->patient_no = $this->Patient->patient_number($patient->patient_id);

			        $report->date = date('j/n/Y g:i a',strtotime($report->service_time));
			        $employee = $this->Employee->get_info($report->served_by);
			        $employee->employee_name = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
			        $data['records'][]=array($report->date,$patient->patient_no,$patient->patient_name,$employee->employee_name,
			        	form_button(array('name'=>'submit','onclick'=>'view_lab_report('.$report->invoice_id.')','content'=>'View','class'=>'DTTT_button float_left'))
			        	);
			    endforeach;
				$data['title'] = 'Lab Reports';
				$data['thead'] = array('Date','Patient #','Patient Name','Served by','&nbsp;');
				break;
			case 'nursing_services':
				$data['records'] = array();
				$services = $this->Nurse->archive_invoices($start_date,$end_date);
				foreach($services->result() as $service):
			        $patient = $this->Patient->get_info($service->patient_id);
			        $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
			        $patient->patient_no = $this->Patient->patient_number($patient->patient_id);

			        $service->date = date('j/n/Y g:i a',strtotime($service->service_time));
			        $employee = $this->Employee->get_info($service->served_by);
			        $employee->employee_name = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
			        $data['records'][]=array($service->date,$patient->patient_no,$patient->patient_name,$employee->employee_name,
			        	form_button(array('name'=>'submit','onclick'=>'view_services('.$service->invoice_id.')','content'=>'View','class'=>'DTTT_button float_left'))
			        	);
			    endforeach;
				$data['title'] = 'Nursing Services';
				$data['thead'] = array('Date','Patient #','Patient Name','Served by','&nbsp;');
				break;
			default:
				$data['records'] = array();
				$sales = $this->Sale->sales_archive($start_date,$end_date);
				foreach($sales->Result() as $sale):
					$sale = $this->Sale->get_sale_info($sale->sale_id);
			        $patient = $this->Patient->get_info($sale->customer_id);
			        $patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
			        $patient->patient_no = $this->Patient->patient_number($patient->patient_id);

			        $sale->date = date('j/n/Y g:i a',strtotime($sale->sale_time));
			        $sale->receipt_no = $this->Sale->receipt_no($sale->sale_id);
			        $sale->value = 0;
			        $sale->discount = 0;

			        foreach($sale->sale_items as $sale_item):
			            $sale->value = $sale->value + $sale_item->quantity_purchased * $sale_item->item_unit_price * (100 - $sale_item->discount_percent) / 100;
			            $sale->discount = $sale->discount + $sale_item->quantity_purchased * $sale_item->item_unit_price * $sale_item->discount_percent / 100;
			        endforeach;

			        $employee = $this->Employee->get_info($sale->employee_id);
			        $employee->employee_name = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
			        $data['records'][] = array($sale->date,$sale->receipt_no,
			        	$patient->patient_no,$patient->patient_name,$employee->employee_name,
			        	to_currency($sale->discount),to_currency($sale->value),
			        	form_button(array('name'=>'submit','onclick'=>'view_sale('.$sale->sale_id.')','content'=>'View','class'=>'DTTT_button float_left')
        ));
			    endforeach;

				$data['title'] = 'Receipts';
				$data['thead'] = array('Date','Receipt #','Patient #','Patient Name','Served by','Discount','Value','&nbsp;');
				break;
		}
		$data['archive_type'] = $archive_type;
		$data["start_date"] = date("j/n/Y",strtotime($date["start"]));
		$data["end_date"] = date("j/n/Y",strtotime($date["end"]));
		
		$data["start_date_input"] = date("d-m-Y",strtotime($date["start"]));
		$data["end_date_input"] = date("d-m-Y",strtotime($date["end"]));

		$this->load->view("reports/archives",$data);
	}

	function archives_date_input()
	{
		$this->form_validation->set_rules('start_date', 'Start Date', 'callback_date_check|required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$start_date = isset($start_date) ? date("Y-m-d H:i:s",strtotime($start_date)) : date("Y-m-d H:i:s");
			$end_date = isset($end_date) ? date("Y-m-d H:i:s",strtotime($end_date)) : $start_date;
			$date = array("start"=>$start_date,"end"=>$end_date);
			
			$this->session->set_userdata('reports_archives_date', $date);
			$this->session->set_userdata('reports_archives_type', $this->input->post('archive_type'));
			$this->session->set_userdata('reports_archives_clinic', $this->input->post('clinic'));
			echo json_encode(array('form_validation'=>true));
		endif;
	}

	function sale_details($sale_id)
	{
		$data['sale'] = $this->Sale->get_sale_info($sale_id);

		$this->load->view('sales/sale_details',$data);
	}

	function consultation_summary($patient_id,$consultation_id)
	{
		$patient = $this->Patient->get_info($patient_id);
		$patient->patient_name = $this->Patient->patient_name($patient->first_name, $patient->middle_name, $patient->last_name);
    	if($patient->dob) $patient->age = $this->Patient->patient_age($patient->dob);
    	$patient->patient_no = $this->Patient->patient_number($patient->patient_id);
		$data['patient'] = $patient;

		$consultation_info = $this->Consultation->get_info($consultation_id);
		$data['complaints'] = unserialize($consultation_info->consultation_complaints);
		$data['examination'] = unserialize($consultation_info->consultation_examination);
		$data['obst_gyn'] = unserialize($consultation_info->consultation_obst_gyn);
		$data['family_history'] = unserialize($consultation_info->consultation_family_history);
		$data['medical_history'] = unserialize($consultation_info->consultation_medical_history);
		$data['treatment_plan'] = unserialize($consultation_info->consultation_treatment_plan);

		$employee = $this->Employee->get_info($consultation_info->consultant_id);
		$employee->employee_name = $this->Patient->patient_name($employee->first_name, $employee->middle_name, $employee->last_name);
		$data['consultant'] = $employee;

		foreach($consultation_info->diagnosis as $diagnosis):
			$data['diagnosis'][$diagnosis->diagnosis_code] = array(
				'diagnosis_code'=>$diagnosis->diagnosis_code,
				'remarks'=>$diagnosis->remarks,
				'description'=>$this->Consultation->get_diagnosis_info($diagnosis->diagnosis_code)->description,
				);
		endforeach;

		foreach($consultation_info->lab_tests as $lab_test):
			$lab_test_id = $this->Lab->get_lab_test_id($lab_test->item_id);
			$data['lab_tests'][$lab_test_id] = array(
				'lab_test_id'=>$lab_test_id,
				'value'=>$this->Lab->get_lab_test_info($lab_test_id)->value,
				'remarks'=>$lab_test->remarks,
				'results'=>unserialize($lab_test->results),
				'item_id'=>$lab_test->item_id,
				'status'=>$lab_test->status,
				'invoice_status'=>$lab_test->invoice_status,
				);
		endforeach;

		foreach($consultation_info->prescription as $drug):
			$drug_id = $this->Pharm->get_drug_id($drug->item_id);
			$drug_info = $this->Pharm->get_drug_info($drug_id);
			$drug->instructions = unserialize($drug->instructions);
			$data['prescription'][$drug_id] = array(
				'drug_id'=>$drug_id,
				'name'=>$drug_info->item_info->name,
				'strength'=>$drug_info->strength,
				'remarks'=>$drug->remarks,
				'dose'=>$drug->instructions['dose'],
				'frequency'=>$drug->instructions['frequency'],
				'duration'=>$drug->instructions['duration'],
				'quantity'=>$drug->quantity,
				'item_id'=>$lab_test->item_id,
				'status'=>$lab_test->status,
				'invoice_status'=>$lab_test->invoice_status,
				);
		endforeach;

		$data['consultation_date'] = date('j/n/Y',strtotime($consultation_info->consultation_start));

		$this->load->view("consultant/consultation_summary",$data);
	}

	function view_report($invoice_id)
	{
		$data['lab_report'] = $this->Lab->get_lab_report($invoice_id);

		$this->load->view('lab/lab_report',$data);
	}

	function view_prescription($invoice_id)
	{
		$data['prescription'] = $this->Pharm->get_prescription($invoice_id);

		$this->load->view('pharmacy/prescription',$data);
	}

	function view_services($invoice_id)
	{
		$data['invoice'] = $this->Invoice->get_info($invoice_id);

		$this->load->view('nursing/services_info',$data);
	}

	function sale_receipt($sale_id)
	{
		$this->load->library('sale_lib');
		$sale_info = $this->Sale->get_info($sale_id)->row_array();
		$this->sale_lib->copy_entire_sale($sale_id);
		$data['cart']=$this->sale_lib->get_cart();
		$data['payments']=$this->sale_lib->get_payments();
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes();
		$data['total']=$this->sale_lib->get_total();
		$data['receipt_title']=$this->lang->line('sales_receipt');
		$data['transaction_time']= date('d/m/Y h:i a', strtotime($sale_info['sale_time']));
		$patient_id=$this->sale_lib->get_customer();
		$emp_info=$this->Employee->get_info($sale_info['employee_id']);
		$data['payment_type']=$sale_info['payment_type'];
		$data['amount_change']=to_currency($this->sale_lib->get_amount_due() * -1);
		$data['employee']=$this->Patient->patient_name($emp_info->first_name,$emp_info->middle_name,$emp_info->last_name);

		if($patient_id!=-1)
		{
			$patient_info=$this->Patient->get_info($patient_id);
			$data['patient_no']=$this->Patient->patient_number($patient_info->patient_id);
			$data['patient_name'] = $this->Patient->patient_name($patient_info->first_name,$patient_info->middle_name,$patient_info->last_name);
		}

		$data['sale_id']=$sale_id;
		$data['receipt_no']= $this->Sale->receipt_no($sale_id);
		$this->load->view("sales/receipt_page",$data);
		$this->sale_lib->clear_all();
	}

	function view_institutions_workload()
	{
		$date = ($this->session->userdata('institutions_workload_date')) ? $this->session->userdata('institutions_workload_date') : array("start"=>date("Y-m-01"),"end"=>date("Y-m-t"));
		$start_date = date("Y-m-d 00:00:00", strtotime($date['start']));
		$end_date = date("Y-m-d 23:59:59", strtotime($date['end']));

		$institution_id = $this->session->userdata('institutions_workload_institution');
		if($institution_id):
			$institution = $this->Institution->get_info($institution_id);
			$workloads = $this->Institution->get_invoice($start_date,$end_date,$institution_id);
			$data['headers'] = $workloads['headers'];
			$data['workloads'] = $workloads['workload'];
			$data['title'] = $this->Patient->patient_name($institution->institution_name).' Invoice';
		else:
			$workloads = $this->Institution->workload($start_date,$end_date);
			$data['workloads'] = $workloads;
			$data['title'] = 'Institutions Workload';
		endif;

		$data['institutions'] = $this->Institution->get_all()->Result();
		$data['institution'] = $institution_id;
		$data["month"] = date("F Y",strtotime($date["start"]));
		$data["month_input"] = date("m/Y",strtotime($date["start"]));
		
		$data["start_date_input"] = date("d-m-Y",strtotime($date["start"]));
		$data["end_date_input"] = date("d-m-Y",strtotime($date["end"]));

		$this->load->view("reports/institutions_workload",$data);
	}

	function institutions_workload_date_input()
	{
		$this->form_validation->set_rules('month', 'Month', 'required');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$month = '01/'.$this->input->post('month');
			$month = preg_replace('/[\/]/s', '-',$month);
			$start_date = isset($month) ? date("Y-m-01",strtotime($month)) : date("Y-m-01");
			$end_date = isset($month) ? date("Y-m-t",strtotime($month)) : date("Y-m-t");
			$date = array("start"=>$start_date,"end"=>$end_date);
			
			$this->session->set_userdata('institutions_workload_date', $date);
			$this->session->set_userdata('institutions_workload_institution', $this->input->post('institution'));
			echo json_encode(array('form_validation'=>true));
		endif;
	}

	function export_contacts()
	{
		$data['patients'] = $this->Patient->get_contacts();

		$this->load->view("reports/contacts",$data);
	}
}
?>