<?php
require_once ("secure_area.php");
class Suppliers extends Secure_area
{
	function __construct()
	{
		parent::__construct('suppliers');
	}
	
	function index()
	{
		$this->load->view('suppliers/main');
	}
	
	function view_supplier($supplier_id=-1)
	{
		if($supplier_id != -1):
			$data['supplier_info'] = $this->Supplier->get_info($supplier_id);
		endif;

		$this->load->view('suppliers/supplier_form',$data);
	}

	function view_all()
	{
		$data['suppliers'] = $this->Supplier->get_all();

		$this->load->view('suppliers/suppliers_table',$data);
	}
	
	function save_supplier($supplier_id = null)
	{
		$this->form_validation->set_rules('company_name', 'Company Name', 'required');
		$this->form_validation->set_rules('account_number', 'Account #', 'callback_account_no_check['.$supplier_id.']');
		
		if ($this->form_validation->run() == FALSE):
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			echo json_encode(array('form_validation'=>false,'error_messages'=>validation_errors()));
		else:
			$supplier_data = array(
				'company_name'=>$this->input->post('company_name'),
				'account_number'=>$this->input->post('account_number'),
				'phone_number'=>$this->input->post('phone_number'),
				'address_1'=>$this->input->post('address_1'),
			);
			
			$supplier_id = $this->Supplier->save($supplier_data,$supplier_id);
			if($supplier_id):
				echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Supplier information saved successfully.','message_class'=>'success_message'));
			else:
				echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to save supplier information.','message_class'=>'error_message'));
			endif;
		endif;
	}

	public function account_no_check($account_no,$supplier_id)
	{
		if ($this->Supplier->account_no_exists($account_no,$supplier_id)):
			$this->form_validation->set_message('account_no_check', 'Account # already exists');
			return FALSE;
		else:
			return TRUE;
		endif;
	}
	
	function delete_supplier($supplier_id)
	{
		$supplier_id = $this->Supplier->delete($supplier_id);
		if($supplier_id):
			echo json_encode(array('form_validation'=>true,'success'=>true,'message'=>'Supplier information deleted successfully.','message_class'=>'success_message','patient_id'=>$patient_id));
		else:
			echo json_encode(array('form_validation'=>true,'success'=>false,'message'=>'Unable to delete supplier information.','message_class'=>'error_message'));
		endif;
	}
}
?>